<?php
/**
 * Core Exception class definitions.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "config.php";

/**
 * Root exception class within the Abstraction framework.
 * 
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class Exception extends \Exception {
	/**
	 * Constructs a new exception.
	 * 
	 * If the EXCEPTIONS_ENVIRONMENT_HTML flag is set, then everything is HTML encoded.
	 * 
	 * @param string $message message to report
	 * @param string $value any context relevant value causing the exception, rendered using print_r
	 * @param string $extra additional information of relevance
	 */
	public function __construct($message = "Unspecified exception", $value = "\t_NO_VALUE", $extra = null) {
		if (EXCEPTIONS_SHOW_VALUES) {
			if ($value !== "\t_NO_VALUE") {
				ob_start();
				print_r($value);
				$message .= ":" . substr(ob_get_clean(), 0, 4096);
			}
		}
			
		if ($extra !== null) $message .= " ({$extra})";
		
		if (EXCEPTIONS_ENVIRONMENT_HTML) $message = htmlentities($message);

		parent::__construct($message);
	}
}

/**
 * Specific exception super-type for data type mismatches, sub-classed extensively.
 *  
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class TypeMismatchException extends Exception {
	/**
	 * Constructs a new exception.
	 * 
	 * @param string $type expected data type
	 * @param mixed $value mismatching value supplied
	 * @param string $extra additional information of relevance
	 */
	public function __construct($type, $value, $extra = null) {
		parent::__construct("Type mismatch exception for {$type}", $value, $extra);
	}
}
