<?php
/**
 * Rendering abstraction for HTTP header output.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTTP;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";

use \Abstraction\Renderer as Renderer;
use \Abstraction\Data as Data;

/**
 * @internal
 */
class Exception extends Renderer\Exception {}

/**
 * Rendering engine for HTTP header control output.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class HTTP implements Renderer\Engine {
	/**
	 * Redirects to the specified URL, then terminates the script.
	 * 
	 * A 307 temporary redirect is used.
	 * 
	 * <b>Note any subclasses should make sure that this method does not accidentally return.</b>
	 * 
	 * @param string $url the location to redirect to
	 * @var string $file
	 * @var int $line
	 * @throws Exception
	 * @return void
	 */
	public static function redirect($url) {
		if (headers_sent($file, $line)) throw new Exception("Headers have already been sent", "{$file}, {$line}");
		header("Location: {$url}", true, 307);
		die();
	}

	/**
	 * Sets header flags that the following content is intended as a download file rather than to be displayed inline.
	 * 
	 * @param string $content_type the MIME type of the download
	 * @param string $filename the filename for the download
	 * @throws Exception
	 * @return void
	 */
	public static function download_headers($content_type, $filename) {
		if (headers_sent($file, $line)) throw new Exception("Headers have already been sent", "{$file}, {$line}");

		header("Content-Type: {$content_type}");
		
		$filename = str_replace('"', "", $filename);
		header("Content-Disposition: attachment; filename=\"{$filename}\"");
	}

	/**
	 * Sets the header flags for a download, then writes the given data to stdout and terminates the script.
	 * 
	 * <b>Note any subclasses should make sure that this method does not accidentally return.</b>
	 * 
	 * @param string $content_type the MIME type of the download
	 * @param string $filename the filename for the download
	 * @param string $data the download content
	 */
	public static function download($content_type, $filename, $data) {
		self::download_headers($content_type, $filename);
		
		header("Content-Length: " . strlen($data));
		
		echo $data;
		flush();
		die();
	}

	/**
	 * @internal
	 */
	private static function http_code_response($code, $description, $message) {
		if (!Data\Data::validate_id($code)) throw new Exception("Invalid HTTP code");
		if (!Data\Data::validate_string($description)) throw new Exception("Invalid HTTP description");
		
		if (headers_sent($file, $line)) throw new Exception("Headers have already been sent", "{$file}, {$line}");
		
		header("HTTP/1.0 {$code} {$description}");
		
		echo $message;
		
		flush();
		die();
	}

	/**
	 * @internal
	 */
	private static function build_html_message($code, $description, $message) {
		return <<<HTML
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>{$code} {$description}</title>
</head><body>
<h1>{$description}</h1>
<p>{$message}</p>
<hr>
<address>Abstraction HTTP object at {$_SERVER["SERVER_ADDR"]} Port {$_SERVER["SERVER_PORT"]}</address>
</body></html>
HTML;
	}
	
	// NB these methods have to be not static in order to match renderer interface definition

	/**
	 * Returns a 500 (Internal Server Error) error message with a barebones HTML body, terminating script execution.
	 * 
	 * <b>Subclasses should ensure this method does not accidently return.</b>
	 * 
	 * @param string $message an error message to display
	 * @return void
	 */
	public function error($message = "") {
		self::http_code_response(500, "Internal Server Error", self::build_html_message(500, "Internal Server Error", $message));
	}
	
	/**
	 * Returns a 403 (Forbidden) error message with a barebones HTML body, terminating script execution.
	 * 
	 * <b>Subclasses should ensure this method does not accidently return.</b>
	 * 
	 * @param string $message an error message to display
	 * @return void
	 */
	public function forbidden($message = "") {
		self::http_code_response(403, "Forbidden", self::build_html_message(403, "Forbidden", $message));
	}
	
	/**
	 * Returns a 204 (No Content) page with a blank body, terminating script execution.
	 * 
	 * <b>Subclasses should ensure this method does not accidently return.</b>
	 * 
	 * @return void
	 */
	public function complete() {
		self::http_code_response(204, "No Content", "");
	}
}
