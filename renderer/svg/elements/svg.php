<?php
/**
 * SVG element: SVG.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\SVG;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/multielements.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/svg/svgelement.php";

use \Abstraction\Renderer\HTML as HTML;

/**
 * Represents an svg tag within the SVG abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class SVG extends HTML\WrappingElement implements SVGElement {
	private $elements;

	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string $viewBox the viewBox for the svg, e.g. "0 0 100 100"
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param HTML\Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($viewBox, $id = null, $class = null) {
		parent::__construct("svg", $this->elements = new HTML\MultiElements(), $id, $class);
		
		$this->add_param("xmlns", "http://www.w3.org/2000/svg");
		$this->add_param("viewBox", $viewBox);
		
		$this->elements->set_compact(true);
	}
	
	/**
	 * Adds a piece of content to the end of the stack.
	 * 
	 * @param SVGElement $element the content to add
	 * @throws Exception
	 * @return void
	 */
	public function add(SVGElement $element) {
		$this->elements->add($element);
	}
	
	/**
	 * Adds a piece of content to the beginning of the stack.
	 * 
	 * @param SVGElement $element the content to add
	 * @throws Exception
	 * @return void
	 */
	public function preadd(SVGElement $element) {
		$this->elements->preadd($element);
	}

	/**
	 * Wipes the current content stack storage.
	 * 
	 * @return void
	 */
	public function clear() {
		$this->elements->clear();
	}

	/**
	 * Returns the number of items in this content stack.
	 * 
	 * @return int
	 */
	public function sizeof() {
		return $this->elements->sizeof();
	}
}
