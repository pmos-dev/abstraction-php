<?php
/**
 * SVG element: Line.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\SVG;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/element.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/svg/svg.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/svg/svgelement.php";

use \Abstraction\Renderer\HTML as HTML;

/**
 * Represents a line tag within the SVG abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Line extends HTML\Element implements SVGElement {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string|int|float $x1 the x1 value
	 * @param string|int|float $y1 the y1 value
	 * @param string|int|float $x2 the x2 value
	 * @param string|int|float $y2 the y2 value
	 * @param string|int|float $width the stroke width value
	 * @param string $color the stroke color value
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param HTML\Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($x1, $y1, $x2, $y2, $width, $color, $id = null, $class = null) {
		parent::__construct("line", $id, $class);

		SVGEngine::assert_geometric($x1);
		SVGEngine::assert_geometric($y1);
		SVGEngine::assert_geometric($x2);
		SVGEngine::assert_geometric($y2);
		SVGEngine::assert_geometric($width);
		SVGEngine::assert_color($color);
		
		$this->add_param("x1", $x1);
		$this->add_param("y1", $y1);
		$this->add_param("x2", $x2);
		$this->add_param("y2", $y2);
		$this->add_param("stroke-width", $width);
		$this->add_param("stroke", $color);
	}

	/**
	 * @internal
	 */
	public function render() {
		$this->atom_element();
	}
}
