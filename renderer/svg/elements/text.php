<?php
/**
 * SVG element: Line.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\SVG;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/svg/svg.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/svg/svgelement.php";

use \Abstraction\Renderer\HTML as HTML;

/**
 * Represents a text tag within the SVG abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Text extends HTML\WrappingElement implements SVGElement {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string|int|float $content the textual content, which must be able to be displayed as a string
	 * @param string|int|float $x the x value
	 * @param string|int|float $y the y value
	 * @param string $font the font family
	 * @param string|int|float $size the font size
	 * @param string $color the text color value
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param HTML\Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($content, $x, $y, $font, $size, $color, $id = null, $class = null) {
		if (!is_string($content) && !is_numeric($content)) throw new InvalidParameterException("Text content must be string, int, float or similar");

		SVGEngine::assert_geometric($x);
		SVGEngine::assert_geometric($y);
		SVGEngine::assert_geometric($size);
		SVGEngine::assert_color($color);
		
		parent::__construct("text", $content, $id, $class);

		$this->add_param("x", $x);
		$this->add_param("y", $y);
		$this->add_param("font-family", $font);
		$this->add_param("font-size", $size);
		$this->add_param("fill", $color);
	}
}
