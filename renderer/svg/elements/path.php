<?php
/**
 * SVG element: Path.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\SVG;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/element.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/svg/svg.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/svg/svgelement.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;

/**
 * Represents a path tag within the SVG abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Path extends HTML\Element implements SVGElement {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string $d the raw path data
	 * @param string|NULL $fill the fill color value, or null for none
	 * @param string|int|float|NULL $strokewidth the stroke width value, or null for none
	 * @param string|NULL $strokecolor the stroke color value, or null for none
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param HTML\Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($d, $fill = null, $strokewidth = null, $strokecolor = null, $id = null, $class = null) {
		parent::__construct("path", $id, $class);

		if (null === ($d = Data\Data::trim_text_or_null($d))) throw new InvalidParameterException("Invalid path", $d);
		
		if ($fill !== null) SVGEngine::assert_color($fill);
		if ($strokewidth !== null) SVGEngine::assert_geometric($strokewidth);
		if ($strokecolor !== null) SVGEngine::assert_color($strokecolor);
		
		$this->add_param("d", $d);
		if ($fill !== null) $this->add_param("fill", $fill);
		if ($strokewidth !== null) $this->add_param("stroke-width", $strokewidth);
		if ($strokecolor !== null) $this->add_param("stroke", $strokecolor);
	}

	/**
	 * @internal
	 */
	public function render() {
		$this->atom_element();
	}
}
