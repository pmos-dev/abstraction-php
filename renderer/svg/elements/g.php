<?php
/**
 * SVG element: G.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\SVG;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/element.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/svg/svg.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/svg/svgelement.php";

use \Abstraction\Renderer\HTML as HTML;

/**
 * Represents a g tag within the SVG abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class G extends HTML\WrappingElement implements SVGElement {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $content the child content for this element
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param HTML\Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($content, $id = null, $class = null) {
		parent::__construct("g", $content, $id, $class);
	}
}
