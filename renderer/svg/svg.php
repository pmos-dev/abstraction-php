<?php
/**
 * Rendering abstraction for SVG output.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\SVG;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/svg/svgelement.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/svg/elements/svg.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";

use \Abstraction\Renderer as Renderer;
use \Abstraction\Renderer\SVG as SVG;
use \Abstraction\Data as Data;

/**
 * @internal
 */
class Exception extends Renderer\Exception {}

/**
 * @internal
 */
class InvalidParameterException extends Exception {}

// import all the various elements
foreach (explode(",", "svg,line,text,rect,circle,path,g") as $type) require_once ABSTRACTION_ROOT_PATH . "renderer/svg/elements/{$type}.php";

/**
 * Rendering engine for SVG structured output.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class SVGEngine implements Renderer\Engine {
	const REGEX_PATTERN_GEOMETRIC = "`^(-?[0-9]+(\.[0-9]+)?(px|em|%)?)$`iDS";
	
	protected $svg_element;
	
	/**
	 * Constructs a new instance of this engine.
	 * 
	 * @param string $viewBox the viewBox for the svg, e.g. "0 0 100 100"
	 */
	public function __construct($viewBox) {
		$this->svg_element = new \Abstraction\Renderer\SVG\SVG($viewBox);
	}
	
	/**
	 * Checks whether the specified geometric value is valid, and throws an exception if not.
	 * 
	 * Valid geometric values include numbers such as 1, 2, 0.5, -3.453, 45px, 2.3%, 1.1em etc.
	 * 
	 * @param string|int|float $geometric the value to check
	 * @throws InvalidParameterException
	 * @return boolean always returns true
	 */
	public static function assert_geometric(&$geometric) {
		if (!Data\Data::validate_regex(self::REGEX_PATTERN_GEOMETRIC, $geometric)) throw new InvalidParameterException("Invalid geometric value", $geometric);
		return true;
	}
	
	/**
	 * Checks whether the specified color value is valid, and throws an exception if not.
	 * 
	 * @param string $color the value to check
	 * @throws InvalidParameterException
	 * @return boolean always returns true
	 */
	public static function assert_color(&$color) {
		if (!Data\Data::validate_string($color)) throw new InvalidParameterException("Invalid color value", $color);
		if ($color === "none") return true;
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_CSS_COLOR, $color)) throw new InvalidParameterException("Invalid color value", $color);
		return true;
	}
	
	/**
	 * @internal
	 * @throws Exception
	 */
	public function error($message = "(unspecified error)") {
		throw Exception("The error() method is not supported for the SVG engine");
	}
	
	/**
	 * Returns the SVG element for inclusion within an HTML renderer.
	 * 
	 * @return SVG
	 */
	function get_svg_element() {
		return $this->svg_element;
	}

	/**
	 * Opens the SVG document, rendering to stdout.
	 * 
	 * The doctype is written, alongside the svg tag itself.
	 * This method is invoked by methods such as render() and not intended for direct usage.
	 * 
	 * @internal
	 * @return void
	 */
	function open() {
		echo <<<SVG
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

SVG;
	}

	/**
	 * Closes the SVG document, rendering to stdout.
	 * 
	 * This method is invoked by methods such as render() and not intended for direct usage.
	 * This is a placeholder method that doesn't really do anything, but may be useful for subclasses potentially.
	 * 
	 * @internal
	 * @return void
	 */
	function close() {}
	
	/**
	 * Renders the entire constructed SVG content to stdout, without terminating script execution.
	 * 
	 * Usually the complete() method is preferred to render and then terminate execution, however there are some instances where an SVG dump needs to be captured using ob_start/ob_get_clean etc., so this method is public.
	 * 
	 * @return void
	 */
	function render() {
		$this->open();
		
		$this->svg_element->render();
		
		$this->close();
	}

	/**
	 * Renders the stored data as SVG to stdout, and then terminates script execution.
	 * 
	 * This method shouldn't be called directly, as the return data will not have been set.
	 * This method is only really implemented to satisfy the method requirements of Engine. SVG graphics tend to be embedded within HTML renderer objects using the export() method.
	 * 
	 * <b>Subclasses should ensure this method does not accidently return.</b>
	 *
	 * @internal
	 * @return void
	 */
	public function complete() {
		header("Content-type: image/svg");
		$this->render();
		die();
	}
}
