<?php
/**
 * Rendering abstraction for multipart structured email output.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\ICalendar;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";

use \Abstraction\Renderer as Renderer;
use \Abstraction\Data as Data;

/**
 * @internal
 */
class Exception extends Renderer\Exception {}

/**
 * @internal
 */
class InvalidParameterException extends Exception {}

/**
 * Rendering engine for multipart structured email output.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class ICalendar implements Renderer\Engine, Renderer\Renderable {
	private $events = array();
	private $prod_id = null;
	private $utc;
	
	/**
	 * Constructs a new instance of this engine.
	 */
	public function __construct($utc = true) {
		$this->utc = $utc;
	}
	
	/**
	 * @internal
	 * @throws Exception
	 */
	public function error($message = "(unspecified error)") {
		throw Exception("The error() method is not supported for the ICalendar renderer");
	}
	
	/**
	 * Sets the PRODID field for the iCalendar
	 * 
	 * @param string $prod_id
	 * @return void
	 */
	public function set_prod_id($prod_id) {
		if (!Data\Data::validate_regex_or_null(Data\Data::REGEX_PATTERN_ID_NAME, $prod_id)) throw new InvalidParameterException("Invalid iCalendar prodid");
		$this->prod_id = $prod_id;
	}
	
	/**
	 * Adds a new event to the calendar
	 * 
	 * @param array $event the event to add
	 * @return void
	 */
	public function add_event_direct(array $event) {
		$this->events[] = $event;
	}

	private static function timestamp_to_ics($timestamp, $utc) {
		if ($utc) {
			return gmdate("Ymd", $timestamp) . "T" . gmdate("His", $timestamp) . "Z";
		} else {
			return date("Ymd", $timestamp) . "T" . date("His", $timestamp) . "Z";
		}
	}
	
	private static function clean($value) {
		$value = Data\Data::trim_text_or_empty_string($value);
		$value = Data\Data::sanitise_word_characters($value);
		$value = str_replace("\n", "", $value);
		$value = str_replace("\r", "", $value);
		$value = str_replace("\t", "", $value);
		$value = str_replace("\0", "", $value);
		
		return preg_replace("`[^- a-z0-9!%&*()=+@#,.?]`i", "", $value);
	}
	
	public function add_event(
			$title,
			$start,
			$end,
			$location = null,
			$description = null,
			$url = null
	) {
		if (!Data\Data::validate_string($title)) throw new InvalidParameterException("Invalid iCalendar title");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATETIME_YMDHIS, $start)) throw new InvalidParameterException("Invalid iCalendar start date time");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATETIME_YMDHIS, $end)) throw new InvalidParameterException("Invalid iCalendar end date time");
		if (!Data\Data::validate_string_or_null($location)) throw new InvalidParameterException("Invalid iCalendar location");
		if (!Data\Data::validate_string_or_null($description)) throw new InvalidParameterException("Invalid iCalendar description");
		if (!Data\Data::validate_regex_or_null(Data\Data::REGEX_PATTERN_URL, $url)) throw new InvalidParameterException("Invalid iCalendar url");
		
		$this->add_event_direct(array(
				"title" => $title,
				"start" => $start,
				"end" => $end,
				"location" => $location,
				"description" => $description,
				"url" => $url
		));
	}
	
	public function render() {
		echo <<<ICALENDAR
BEGIN:VCALENDAR
VERSION:2.0

ICALENDAR;
		
		if ($this->prod_id !== null) echo <<<ICALENDAR
PRODID:{$this->prod_id}

ICALENDAR;
		
		echo <<<ICALENDAR
CALSCALE:GREGORIAN
METHOD:PUBLISH

ICALENDAR;
		
		if ($this->utc) echo <<<ICALENDAR
TZ:+00

ICALENDAR;
		
		foreach ($this->events as $event) {
			$now = self::timestamp_to_ics(time(), $this->utc);
			$start = self::timestamp_to_ics(strtotime($event["start"]), $this->utc);
			$end = self::timestamp_to_ics(strtotime($event["end"]), $this->utc);
			
			$title = self::clean($event["title"]);
			
			echo <<<ICALENDAR
BEGIN:VEVENT
DTSTAMP:${now}
DTSTART;TZID=Europe/London:${start}
DTEND;TZID=Europe/London:${end}
SUMMARY:${title}

ICALENDAR;
			
			if ($event["description"] !== null) {
				$description = self::clean($event["description"]);
				echo <<<ICALENDAR
DESCRIPTION:${description}

ICALENDAR;
			}
			
			if ($event["location"] !== null) {
				$location = self::clean($event["location"]);
				echo <<<ICALENDAR
LOCATION:${location}

ICALENDAR;
			}
			
			if ($event["url"] !== null) {
				$url = rawurlencode($event["url"]);
				echo <<<ICALENDAR
URL:${url}

ICALENDAR;
			}
			
			echo <<<ICALENDAR
SEQUENCE:0
STATUS:CONFIRMED
TRANSP:OPAQUE
END:VEVENT

ICALENDAR;
		}
		
		echo <<<ICALENDAR
END:VCALENDAR
ICALENDAR;
	}
	
	/**
	 * @internal
	 * @throws Exception
	 */
	public function complete() {
		header("Content-type: text/calendar");
		$this->render();
		flush();
		
		die();
	}
}
