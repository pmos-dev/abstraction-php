<?php
/**
 * Rendering abstraction for REST (JSON) output.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\REST;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";

use \Abstraction\Renderer as Renderer;

/**
 * @internal
 */
class Exception extends Renderer\Exception {}

/**
 * Rendering engine for REST (JSON) structured output.
 * 
 * This is mainly used by REST AJAX implementations.
 * This is largely a clone of JSON, without the success/error structure.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class REST implements Renderer\Engine {
	/**
	 * Constructs a new instance of this engine.
	 */
	public function __construct() {
	}
	
	/**
	 * Permits HTTP cross domain usage.
	 */
	public function enable_origin() {
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
		header("Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST, PUT, PATCH, DELETE");
		header("Access-Control-Max-Age: 599");
	}
	
	public function disable_caching() {
		header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	}

	// can't use http_response_code, as PHP 5.3 doesn't support it.
	private function get_http_header($response_code) {
		$text = "";
		switch ($response_code) {
			// @phpcs:disable Generic.Formatting.DisallowMultipleStatements
			case 100: $text = "Continue"; break;
			case 101: $text = "Switching Protocols"; break;
			case 200: $text = "OK"; break;
			case 201: $text = "Created"; break;
			case 202: $text = "Accepted"; break;
			case 203: $text = "Non-Authoritative Information"; break;
			case 204: $text = "No Content"; break;
			case 205: $text = "Reset Content"; break;
			case 206: $text = "Partial Content"; break;
			case 300: $text = "Multiple Choices"; break;
			case 301: $text = "Moved Permanently"; break;
			case 302: $text = "Moved Temporarily"; break;
			case 303: $text = "See Other"; break;
			case 304: $text = "Not Modified"; break;
			case 305: $text = "Use Proxy"; break;
			case 400: $text = "Bad Request"; break;
			case 401: $text = "Unauthorized"; break;
			case 402: $text = "Payment Required"; break;
			case 403: $text = "Forbidden"; break;
			case 404: $text = "Not Found"; break;
			case 405: $text = "Method Not Allowed"; break;
			case 406: $text = "Not Acceptable"; break;
			case 407: $text = "Proxy Authentication Required"; break;
			case 408: $text = "Request Time-out"; break;
			case 409: $text = "Conflict"; break;
			case 410: $text = "Gone"; break;
			case 411: $text = "Length Required"; break;
			case 412: $text = "Precondition Failed"; break;
			case 413: $text = "Request Entity Too Large"; break;
			case 414: $text = "Request-URI Too Large"; break;
			case 415: $text = "Unsupported Media Type"; break;
			case 500: $text = "Internal Server Error"; break;
			case 501: $text = "Not Implemented"; break;
			case 502: $text = "Bad Gateway"; break;
			case 503: $text = "Service Unavailable"; break;
			case 504: $text = "Gateway Time-out"; break;
			case 505: $text = "HTTP Version not supported"; break;
			// @phpcs:enable
			default:
				throw new Exception("Unknown HTTP status code");
		}
		
		return "HTTP/1.1 {$response_code} {$text}";
	}
	
	public function responseCodeError($response_code, $message = "Internal Server Error") {
		header(self::get_http_header($response_code));
		header("Content-type: application/json");
		echo json_encode(array("code" => $response_code, "message" => $message));
		flush();
		
		die();
	}
	
	// can't use response code for this, as the default Adamantine error method doesn't have knowledge of it.
	public function error($message = "Internal server error") {
		$this->responseCodeError(500, $message);
	}
	
	public function badRequest($message = "Bad Request") {
		$this->responseCodeError(400, $message);
	}
	
	public function notFound($message = "Not Found") {
		$this->responseCodeError(404, $message);
	}
	
	public function forbidden($message = "Forbidden") {
		$this->responseCodeError(403, $message);
	}
	
	public function conflict($message = "Conflict") {
		$this->responseCodeError(409, $message);
	}
	
	public function success($response_code = 200, $result = "") {
		header(self::get_http_header($response_code));
		if ($response_code !== 204) {
			header("Content-type: application/json");
			echo json_encode(self::recurse_utf8($result));
		}
		flush();
		
		die();
	}
	
	public function ok($result = "") {
		$this->success(200, $result);
	}
	
	public function created($result = "") {
		$this->success(201, $result);
	}
	
	public function accepted($result = "") {
		$this->success(202, $result);
	}
	
	public function noContent() {
		$this->success(204, "");
	}
	
	public function partialContent($result = "") {
		$this->success(206, $result);
	}

	private static function recurse_utf8($node) {
		if (is_string($node) && !preg_match("`^.*$`u", $node)) return utf8_encode($node);

		if (is_array($node)) {
			$rebuild = array();
			
			if (array_diff_key($node, array_keys(array_keys($node)))) {
				foreach ($node as $key => $value) $rebuild[self::recurse_utf8($key)] = self::recurse_utf8($value);
			} else {
				foreach ($node as $value) $rebuild[] = self::recurse_utf8($value);
			}

			return $rebuild;
		}
		
		return $node;
	}
	
	public function complete() {
		throw new Exception("Not implemented");
	}
}
