<?php
/**
 * Rendering abstraction for multipart structured email output.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\MultiPartEmail;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";

use \Abstraction\Renderer as Renderer;
use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Data as Data;

/**
 * @internal
 */
class Exception extends Renderer\Exception {}

/**
 * @internal
 */
class InvalidParameterException extends Exception {}

/**
 * Rendering engine for multipart structured email output.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class MultiPartEmail implements Renderer\Engine {
	const REGEX_PATTERN_EXTENDED_EMAIL = "`^(((\"[^\"<]+\")|([^\"<]+)) )<[-a-z0-9._%+']{1,64}@([-a-z0-9]{1,64}\.)*[-a-z]{2,12}>$`iSD";
	
	private $random_hash;
	private $plain = null;
	private $html = null;
	private $attachments = array();
	private $cc = null;
	private $bcc = null;
	private $replyto = null;
	
	/**
	 * Constructs a new instance of this engine.
	 */
	public function __construct() {
		$this->random_hash = md5(date('r', time()));
	}

	/**
	 * Set the plain-text part of this multipart email.
	 * 
	 * @param string $plain the message.
	 * @return void
	 */
	public function set_plain($plain) {
		$this->plain = $plain;
	}

	/**
	 * Set the HTML part of this multipart email.
	 * 
	 * @param HTML\Element|HTML\MultiElements $html the message.
	 * @return void
	 */
	public function set_html(Renderer\Renderable $html) {
		$this->html = $html;
	}

	/**
	 * Adds a new attachment to this multipart email.
	 * 
	 * @param string $filename the filename
	 * @param string $type the MIME type
	 * @param mixed $data the raw binary data
	 * @return void
	 */
	public function add_attachment($filename, $type, $data) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $filename)) throw new Exception("Filename contains invalid characters");
		if (!Data\Data::validate_regex("`^([a-z0-9]{1,64})/([a-z0-9]{1,64})(.*)?$`i", $type)) throw new Exception("Invalid MIME type for attachment");
		if ($data === null) throw new Exception("Cannot add a null attachment");
		
		$this->attachments[] = array($filename, $type, $data);
	}
	
	/**
	 * Adds a CC email address to the send operation.
	 * 
	 * Note that only one such address is permitted at this time.
	 * 
	 * @param string $email
	 * @return void
	 */
	public function add_cc($email) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_EMAIL, $email) && !Data\Data::validate_regex(self::REGEX_PATTERN_EXTENDED_EMAIL, $email)) throw Exception("Invalid CC address for the email");
		$this->cc = $email;
	}
	
	/**
	 * Adds a BCC email address to the send operation.
	 * 
	 * Note that only one such address is permitted at this time.
	 * 
	 * @param string $email
	 * @return void
	 */
	public function add_bcc($email) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_EMAIL, $email) && !Data\Data::validate_regex(self::REGEX_PATTERN_EXTENDED_EMAIL, $email)) throw Exception("Invalid BCC address for the email");
		$this->bcc = $email;
	}
	
	/**
	 * Adds a Reply-to email address to the send operation.
	 *
	 * Note that only one such address is permitted at this time.
	 *
	 * @param string $email
	 * @return void
	 */
	public function add_replyto($email) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_EMAIL, $email) && !Data\Data::validate_regex(self::REGEX_PATTERN_EXTENDED_EMAIL, $email)) throw Exception("Invalid BCC address for the email");
		$this->replyto = $email;
	}
	
	/**
	 * @internal
	 * @throws Exception
	 */
	public function error($message = "(unspecified error)") {
		throw Exception("The error() method is not supported for the MultiPartEmail renderer");
	}
	
	public function generate_multipart_code($from) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_EMAIL, $from) && !Data\Data::validate_regex(self::REGEX_PATTERN_EXTENDED_EMAIL, $from)) throw Exception("Invalid From address for the email");
		
		$date = date("r (T)");
		
		$email_headers = <<<EMAILHEADERS
From: {$from}

EMAILHEADERS;
		
		if ($this->cc !== null) $email_headers .= <<<EMAILHEADERS
CC: {$this->cc}

EMAILHEADERS;
		
		if ($this->bcc !== null) $email_headers .= <<<EMAILHEADERS
BCC: {$this->bcc}

EMAILHEADERS;
		
		if ($this->replyto !== null) $email_headers .= <<<EMAILHEADERS
Reply-To: {$this->replyto}

EMAILHEADERS;
		
		$email_headers .= <<<EMAILHEADERS
Date: {$date}

EMAILHEADERS;
		
		$email_headers .= <<<EMAILHEADERS
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="PHP-mixed-{$this->random_hash}"

EMAILHEADERS;
		
		ob_start();
		echo <<<EMAILBODY
--PHP-mixed-{$this->random_hash}
Content-Type: multipart/alternative; boundary="PHP-alt-{$this->random_hash}"


EMAILBODY;
		
		if ($this->plain !== null) echo <<<EMAILBODY
--PHP-alt-{$this->random_hash}
Content-Type: text/plain

{$this->plain}


EMAILBODY;

		if ($this->html !== null) {
			echo <<<EMAILBODY
--PHP-alt-{$this->random_hash}
Content-Type: text/html


EMAILBODY;
			$this->html->render();
	
			echo <<<EMAILBODY



EMAILBODY;
		}

		echo <<<EMAILBODY
--PHP-alt-{$this->random_hash}--

EMAILBODY;

		foreach ($this->attachments as $attachment) {
			list($filename, $type, $data) = $attachment;
			$chunk = chunk_split(base64_encode($data));
			echo <<<EMAILBODY
--PHP-mixed-{$this->random_hash}
Content-Type: {$type};name="{$filename}"
Content-Transfer-Encoding: base64
Content-Disposition: attachment;filename="{$filename}"

{$chunk}

EMAILBODY;
		}

		echo <<<EMAILBODY
--PHP-mixed-{$this->random_hash}--


EMAILBODY;
		$email_body = ob_get_clean();
		
		return array(
				"headers" => $email_headers,
				"body" => $email_body
		);
	}
	
	/**
	 * Attempts to send the multipart email to the specified address, without terminating script execution.
	 * 
	 * @param string $to the email address to send to
	 * @param string $from the email address to specify the email has come from
	 * @param string $subject the subject to use
	 * @return bool true if the message was successfully sent; false on failure
	 * @throws Exception
	 */
	function mail($to, $from, $subject) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_EMAIL, $to) && !Data\Data::validate_regex(self::REGEX_PATTERN_EXTENDED_EMAIL, $to)) throw Exception("Invalid To address for the email");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_EMAIL, $from) && !Data\Data::validate_regex(self::REGEX_PATTERN_EXTENDED_EMAIL, $from)) throw Exception("Invalid From address for the email");
		
		if ($this->plain === null && $this->html === null) throw new Exception("No plain or HTML body for this multipart email");
		
		$code = $this->generate_multipart_code($from);
		$email_headers = $code["headers"];
		$email_body = $code["body"];
		
		if (!@mail($to, $subject, $email_body, $email_headers)) return false;
		return true;		
	}
	
	/**
	 * @internal
	 * @throws Exception
	 */
	public function complete() {
		throw new Exception("The complete() method is not supported for the MultiPartEmail renderer");
	}
}
