<?php
/**
 * Element for storage of inline string output.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/element.php";

use \Abstraction\Renderer as Renderer;

/**
 * Provides storage for inline textual output as a Renderable object.
 *
 * Content is automatically processed to convert various characters to their HTML equivalents.
 * This is done for security and convenience.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Inline_Text implements Renderer\Renderable {	
	private $text;

	/**
	 * Constructs a new instance
	 * 
	 * @param string|NULL $text any initial content output to store
	 */	
	public function __construct($text = null) {
		$this->text = "" . $text;
	}

	/**
	 * Replaces the current textual content.
	 * 
	 * @param string|NULL $text the new content to output
	 */
	public function replace($text = null) {
		$this->text = "" . $text;
	}
	
	/**
	 * Renders the stored textual content, converting various characters to their HTML encoded equivalents, and then writing to stdout.
	 * 
	 * @return void
	 */
	public function render() {
		$converted = $this->text;

		// There is a problem with PHP 5.3 handling pound signs between different char sets and Windows/Linux for some reason, so we manually handle them here:
		// This should be possible to fix with the UTF-8 encoding directive, but it doesn't seem to be reliable. It is possible that the database tables themselves don't support UTF8 and therefore don't return characters that can be handled by the UTF-8 encoding directive.
		$converted = str_replace("_TEMPGBPCHRSYMBOLPLACEHOLDER_", "", $converted);	// strip out any existing manual matches; unlikely, but could be a vague potential security hole otherwise
		$converted = str_replace(chr(163), "_TEMPGBPCHRSYMBOLPLACEHOLDER_", $converted);
		$converted = str_replace("£", "_TEMPGBPCHRSYMBOLPLACEHOLDER_", $converted);
		$converted = htmlentities($converted, ENT_IGNORE | ENT_COMPAT, "UTF-8");
		$converted = str_replace("_TEMPGBPCHRSYMBOLPLACEHOLDER_", "&pound;", $converted);
		
		echo $converted;
	}
}
