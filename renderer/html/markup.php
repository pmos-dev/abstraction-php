<?php
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/raw.php";

use \Abstraction\Renderer as Renderer;
use \Abstraction\Data as Data;

class Markup extends Renderer\HTML\Raw {	
	public function render() {
		$data = $this->get_raw();
		$data = Data\Data::trim_text_or_empty_string($data);

		// There is a problem with PHP 5.3 handling pound signs between different char sets and Windows/Linux for some reason, so we manually handle them here:
		// This should be possible to fix with the UTF-8 encoding directive, but it doesn't seem to be reliable. It is possible that the database tables themselves don't support UTF8 and therefore don't return characters that can be handled by the UTF-8 encoding directive.
		$data = str_replace("_TEMPGBPCHRSYMBOLPLACEHOLDER_", "", $data);	// strip out any existing manual matches; unlikely, but could be a vague potential security hole otherwise
		$data = str_replace(chr(163), "_TEMPGBPCHRSYMBOLPLACEHOLDER_", $data);
		$data = str_replace("£", "_TEMPGBPCHRSYMBOLPLACEHOLDER_", $data);
		$data = htmlentities($data, ENT_IGNORE | ENT_COMPAT, "UTF-8");
		$data = str_replace("_TEMPGBPCHRSYMBOLPLACEHOLDER_", "&pound;", $data);
		
		$data = str_replace("&nbsp;", " ", $data);
		while (strpos($data, "  ") !== false) $data = str_replace("  ", " ", $data);

		// surplus whitespace

		$data = str_replace("\r\n", "\n", $data);
		$data = str_replace("\r", "\n", $data);
		$data = str_replace("\n ", "\n", $data);
		while (strpos($data, "\n ") !== false) $data = str_replace("\n ", "\n", $data);
		while (strpos($data, " \n") !== false) $data = str_replace(" \n", "\n", $data);
		while (strpos($data, "\n\n\n") !== false) $data = str_replace("\n\n\n", "\n\n", $data);
		
		// tables
		$data = preg_replace_callback("`\[table=([^|\]]+(\|[^|\]]+)*)\](.+?)\[/table\]`s", function($matches) {
			list(, $header, , $body) = $matches;
			$columns = array();
			foreach (explode("|", $header) as $column) $columns[] = "<th>" . trim($column) . "</th>";

			$rows = array();
			foreach (preg_split("`[\r\n]`", $body) as $row) if ("" !== ($row = trim($row))) {
				$cells = array();
				foreach (explode("|", $row) as $cell) $cells[] = "<td>" . trim($cell) . "</td>";
				$rows[] = "<tr>" . implode("", $cells) . "</tr>";
			}
			return "<table><thead><tr>" . implode("", $columns) . "</tr></thead><tbody>" . implode("\n", $rows) . "</tbody></table>";
		}, $data);
		
		// lists

		$data = preg_replace("`^\*\h(\H.*)$`m", "<unorderedli>\\1</unorderedli>", $data);
		$data = preg_replace("`^#\h(\H.*)$`m", "<orderedli>\\1</orderedli>", $data);

		$data = preg_replace("`(<unorderedli>.+?</unorderedli>)((\n<unorderedli>.+?</unorderedli>)*)`", "\n\n<ul>\\1\\2</ul>\n\n", $data);
		$data = preg_replace("`(<orderedli>.+?</orderedli>)((\n<orderedli>.+?</orderedli>)*)`", "\n\n<ol>\\1\\2</ol>\n\n", $data);
		while (strpos($data, "\n\n\n") !== false) $data = str_replace("\n\n\n", "\n\n", $data);

		$data = preg_replace("`\n?<(/)?(un)?orderedli>\n?`D", "<\\1li>", $data);

		// basic styling
		$data = preg_replace("`\[h4\](.*?)\[/h4\]`D", "<h4>\\1</h4>", $data);	// deprecated old [h4] tag
		$data = preg_replace("`\[title\](.*?)\[/title\]`D", "<h4>\\1</h4>", $data);	// new [title] tag
		$data = preg_replace("`\[b\](.*?)\[/b\]`D", "<strong>\\1</strong>", $data);
		$data = preg_replace("`\[i\](.*?)\[/i\]`D", "<em>\\1</em>", $data);

		// quotes
		$data = preg_replace("`\[quote\](.*?)\[/quote\]`sD", "<blockquote>\\1</blockquote>", $data);
		$data = preg_replace("`\[quote=(.+?)\](.+?)\[/quote\]`sD", "<blockquote>\\2<cite>\\1</cite></blockquote>", $data);
		
		// urls
		$data = preg_replace("`\[url\](.+?)\[/url\]`D", "<a href=\"\\1\">\\1</a>", $data);
		$data = preg_replace("`\[url=(.+?)\](.+?)\[/url\]`D", "<a href=\"\\1\">\\2</a>", $data);

		// emails
		$data = preg_replace("`\[email\](.+?)\[/email\]`D", "<a href=\"mailto:\\1\">\\1</a>", $data);
		$data = preg_replace("`\[email=(.+?)\](.+?)\[/email\]`D", "<a href=\"mailto:\\1\">\\2</a>", $data);

		// images
		$data = preg_replace("`\[img\](.+?)\[/img\]`D", "<img src=\"\\1\">", $data);
		
		// hr
		$data = preg_replace("`^-{2,}$`m", "<hr>", $data);
		
		// paragraphs
		$data = preg_replace("`^(.+)$`m", "<p>\\1</p>", $data);
		$data = preg_replace("`^<p>(<ul>.*?</ul>)</p>$`m", "\\1", $data);
		$data = preg_replace("`^<p>(<ol>.*?</ol>)</p>$`m", "\\1", $data);

		// remove superfluous paragraphs from headers
		$data = preg_replace("`<p>(<h4>.*?</h4>)</p>`", "\\1", $data);
		
		echo trim($data);
	}

	public function render_plain() {
		$data = $this->get_raw();
		$data = Data\Data::trim_text_or_empty_string($data);

		// paragraphs
		$data = preg_replace("`^(.+)$`m", "\\1\n", $data);

		// surplus whitespace

		$data = str_replace("\r\n", "\n", $data);
		$data = str_replace("\r", "\n", $data);
		$data = str_replace("\n ", "\n", $data);
		while (strpos($data, "\n ") !== false) $data = str_replace("\n ", "\n", $data);
		while (strpos($data, " \n") !== false) $data = str_replace(" \n", "\n", $data);
		while (strpos($data, "\n\n\n") !== false) $data = str_replace("\n\n\n", "\n\n", $data);
		
		// hr
		$data = preg_replace("`^-{2,}$`m", "------------------------------", $data);

		// tables
		$data = preg_replace_callback("`\[table=([^|\]]+(\|[^|\]]+)*)\](.+?)\[/table\]`s", function($matches) {
			list(, $header, , $body) = $matches;
			$columns = array();
			foreach (explode("|", $header) as $column) $columns[] = str_replace(",", " ", trim($column));

			$rows = array();
			foreach (preg_split("`[\r\n]`", $body) as $row) if ("" !== ($row = trim($row))) {
				$cells = array();
				foreach (explode("|", $row) as $cell) $cells[] = str_replace(",", " ", trim($cell));
				$rows[] = $cells;
			}

			for ($c = 0; $c < sizeof($columns); $c++) {
				$width = strlen($columns[$c]);
	
				foreach ($rows as $row) {
					$width = max($width, strlen($row[$c]));
				}

				$columns[$c] = str_pad($columns[$c], $width, " ");
				foreach ($rows as &$row) {
					$row[$c] = str_pad($row[$c], $width, " ");
				}; unset($row);
			}
			foreach ($rows as &$row) {
				$row = implode(" | ", $row);
			}; unset($row);

			$columns = implode(" | ", $columns);
			$bar = str_repeat("-", strlen($columns));
			return "{$bar}\n{$columns}\n{$bar}\n" . implode("\n", $rows) . "\n{$bar}\n";
		}, $data);
		
		// lists
		// leave as is; can't easily do the # list numbering

		// basic styling
		$data = preg_replace("`\[h4\](.*?)\[/h4\]`D", "\\1", $data);	// deprecated old [h4] tag
		$data = preg_replace("`\[title\](.*?)\[/title\]`D", "\\1", $data);	// new [title] tag
		$data = preg_replace("`\[b\](.*?)\[/b\]`D", "*\\1*", $data);
		$data = preg_replace("`\[i\](.*?)\[/i\]`D", "\\1", $data);

		// quotes
		$data = preg_replace("`\[quote\](.*?)\[/quote\]`sD", "\"\\1\"", $data);
		$data = preg_replace("`\[quote=(.+?)\](.+?)\[/quote\]`sD", "\"\\2\" -- \\1", $data);
		
		// urls
		$data = preg_replace("`\[url\](.+?)\[/url\]`D", "\\1", $data);
		$data = preg_replace("`\[url=(.+?)\](.+?)\[/url\]`D", "\\2 [\\1]", $data);

		// emails
		$data = preg_replace("`\[email\](.+?)\[/email\]`D", "\\1", $data);
		$data = preg_replace("`\[email=(.+?)\](.+?)\[/email\]`D", "\\2 [\\1]", $data);

		// images
		$data = preg_replace("`\[img\](.+?)\[/img\]`D", "", $data);
		
		// remove superfluous paragraphs from headers
		
		echo trim($data);
	}
}
