<?php
/**
 * HTML element: Article.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML\Semantics;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

use \Abstraction\Renderer\HTML as HTML;

/**
 * Represents a article tag within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 2.0.0
 */
class Main extends HTML\WrappingElement {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $content the child content for this element
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param HTML\Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($content, $id = null, $class = null) {
		parent::__construct("main", $content, $id, $class);
	}
}
