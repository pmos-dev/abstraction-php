<?php
/**
 * Storage of elements that can themselves content other Renderable content (i.e. non-atomic).
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/classes.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/element.php";

use \Abstraction\Renderer as Renderer;

/**
 * Root representation class for all tag elements that potentially allow child content.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class WrappingElement extends Element {
	private $content;

	protected $ALLOW_ATOMIC_ELEMENT = true;
	protected $COMPACT = true;
	
	/**
	 * Constructs a new instance of this tag element.
	 * 
	 * @param string $tag the HTML tag name
	 * @param string|int|float|\Abstraction\Renderer\Renderable $content any optional child content to add from the outset
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($tag, $content = null, $id = null, $class = null) {
		parent::__construct($tag, $id, $class);
		$this->set_content($content);
	}
	
	/**
	 * Renders the element (and any child content recursively) to stdout.
	 * 
	 * @return void
	 */
	public function render() {
		if ($this->content === null) {
			if ($this->ALLOW_ATOMIC_ELEMENT) $this->atom_element();
			else {
				$this->open_element();
				if (!$this->COMPACT) echo "\r\n";
				$this->close_element();
				if (!$this->COMPACT) echo "\r\n";
			}
		} else {
			$this->open_element();
			if (!$this->COMPACT) echo "\r\n";
			$this->content->render();
			if (!$this->COMPACT) echo "\r\n";
			$this->close_element();
			if (!$this->COMPACT) echo "\r\n";
		}
	}

	/**
	 * Replaces the existing child content with new content.
	 * 
	 * Strings and numeric values are automatically wrapped in the Inline_Text class, so HTML encoding of various characters is performed automatically.
	 * Anything else must implement the Renderable interface.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $content the content to add
	 * @throws Exception
	 * @return void
	 */
	public function set_content($content) {
		if (is_string($content) || is_numeric($content)) $content = new Inline_Text($content);
		if (!($content instanceof Renderer\Renderable)) throw new Exception("Element does not implement the Renderable interface");
		$this->content = $content;
	}

	/**
	 * Sets the COMPACT flag for rendering.
	 * 
	 * By default, child content is separated by new lines. Setting the COMPACT flag to true suppresses this.
	 * 
	 * @param boolean $state true to set the COMPACT flag; false to remove it
	 * @return void
	 */
	public function set_compact($state) {
		$this->COMPACT = $state;
	}
	
	/**
	 * Returns the content wrapped within this WrappingElement.
	 * 
	 * @return string|int|float|\Abstraction\Renderer\Renderable the inner content
	 */
	public function get_content() {
		return $this->content;
	}
}
