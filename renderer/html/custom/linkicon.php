<?php
/**
 * HTML custom structure: Link icon.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/elements/link.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/elements/image.php";

/**
 * Builds a structure for a hyperlink wrapped image.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class LinkIcon extends Link {
	/**
	 * Constructs a new instance of this component.
	 * 
	 * @param string $url the URL of the image
	 * @param string $title an optional value for the title="..." parameter
	 * @param string $href a value for the href="..." parameter
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($url, $title, $href, $id = null, $class = null) {
		parent::__construct(new Image($url, $title, $title), $href, $id, $class);
	}
}
