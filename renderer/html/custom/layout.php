<?php
/**
 * HTML custom structure: Layout.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

use \Abstraction as Abstraction;

/**
 * Builds a structure for creating 'layouts' HTML using a combination of Divs and Labels.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Layout extends MultiElements {
	/**
	 * Constructs a new instance of this component.
	 */
	public function __construct() {
		parent::__construct();
		$this->set_compact(false);
	}

	/**
	 * Adds a piece of content to the end of the stack.
	 * 
	 * This method is essentially the same as calling add on a MultiElement class.
	 * 
	 * @see MultiElements::add($element)
	 * @param \Abstraction\Renderer\Renderable|string|int|float $content the content to add
	 * @return void
	 */
	public function add_item($content) {
		$this->add($content);
	}
	
	/**
	 * Creates a new row with a Label element preceding the given content.
	 * 
	 * Returns the div tag wrapping this row so that classes can be added if desired.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $label the value to use for the row label
	 * @param string|int|float|\Abstraction\Renderer\Renderable $content the content for the row, usually a subclass of Form_Input
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @return Div
	 */
	public function add_labelled_row($label, $content, $id = null, $class = null, $for = null) {
		$row = new MultiElements();
		$row->add($label_element = new Label($label));
		if ($for !== null) $label_element->add_param("for", $for);

		if (is_array($content)) {
			$multi = new MultiElements();
			foreach ($content as $c) $multi->add($c);
			$content = $multi;
		}

		$row->add($content);
		
		$this->add_item($div = new Div($row, $id, $class));
		return $div;
	}
}
