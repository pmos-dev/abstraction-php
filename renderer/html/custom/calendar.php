<?php
namespace Abstraction\Renderer\HTML;

use \Abstraction\Data as Data;

class CalendarException extends Exception {}

class BadParameterException extends CalendarException {
	function __construct($value) {
		parent::__construct("Bad parameter value", $value);
	}
}

class Calendar extends Table {
	private $entries;
	private $stack;
	private $period, $date;
	private $start, $end;
	
	public function __construct($id = null, $class = null) {
		parent::__construct($id, $class);
			
		$this->entries = array();
		$this->stack = null;
		
		$this->date = null;
		$this->start = null;
		$this->end = null;
	}
	
	public function add_range($start, $end, $description, $link = null, $class = null, $additional = null) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATETIME_YMDHIS, $start)) throw new BadParameterException($start);
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATETIME_YMDHIS, $end)) throw new BadParameterException($end);
		
		$this->entries[] = array(
			"index" => sizeof($this->entries) + 1,
			"start" => $start,
			"end" => $end,
			"description" => $description,
			"link" => $link,
			"class" => $class,
			"additional" => $additional
		);
	}

	public function add_occurance($time, $description, $link = null, $class = null, $additional = null) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATETIME_YMDHIS, $time)) throw new BadParameterException($time);
		
		$this->entries[] = array(
			"index" => sizeof($this->entries) + 1,
			"start" => $time,
			"end" => $time,
			"description" => $description,
			"link" => $link,
			"class" => $class,
			"additional" => $additional
		);
	}
	
	public function compute_for_date($date, $period) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATE_YMD, $date)) throw new BadParameterException($date);
		
		$this->date = $date;
		$this->start = $this->end = null;
		
		$this->period = $period;
		
		$this->stack = array();
		
		$date_start = strtotime("{$this->date} 00:00:00");
		$date_end = strtotime("{$this->date} 23:59:59") + 1;
		
		// sort into time order
		usort($this->entries, function($a, $b) {
			if (strtotime($a["start"]) < strtotime($b["start"])) return -1;
			if (strtotime($a["start"]) > strtotime($b["start"])) return 1;
			if (strtotime($a["end"]) < strtotime($b["end"])) return -1;
			if (strtotime($a["end"]) > strtotime($b["end"])) return 1;
			return 0;
		});
		
		foreach ($this->entries as $entry) {
			$entry["timestamp_start"] = strtotime($entry["start"]);
			$entry["timestamp_end"] = strtotime($entry["end"]);
			
			// unlikely, but in case to belt-n-braces against problems
			if ($entry["timestamp_start"] > $entry["timestamp_end"]) list($entry["timestamp_start"], $entry["timestamp_end"]) = array($entry["timestamp_end"], $entry["timestamp_start"]);

			if ($entry["timestamp_start"] > $date_end || $entry["timestamp_end"] < $date_start) continue;
			
			$entry["period_start"] = floor(($entry["timestamp_start"] - $date_start) / $period);
			$entry["period_end"] = ceil(($entry["timestamp_end"] - $date_start) / $period);
			
			$match = null;
			for ($i = 0; $i < sizeof($this->stack); $i++) {
				$free = true;
				foreach ($this->stack[$i] as $existing) {
					if (
						($entry["period_start"] >= $existing["period_start"] && $entry["period_start"] < $existing["period_end"])
						|| ($entry["period_end"] > $existing["period_start"] && $entry["period_end"] <= $existing["period_end"])
						|| ($entry["period_start"] < $existing["period_start"] && $entry["period_end"] > $existing["period_end"])
					) {
						$free = false; 
						break;
					}
				}
				if ($free) { 
					$match = $i;
					break;
				}
			}

			if ($match === null) {
				$match = sizeof($this->stack);
				$this->stack[$match] = array();
			}
			
			$this->stack[$match][] = $entry;
		}

		for ($i = 0; $i < sizeof($this->stack); $i++) {
			$sorted = array();
			$j = 0;
			foreach ($this->stack[$i] as $entry) $sorted[date("His", $entry["timestamp_start"]) . date("His", $entry["timestamp_end"]) . "_" . ($j++)] = $entry;
			ksort($sorted);
			$this->stack[$i] = array_values($sorted);
		}
	}
	
	public function compute_for_range($start, $end, $period) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATE_YMD, $start)) throw new BadParameterException($start);
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATE_YMD, $end)) throw new BadParameterException($end);
		
		$this->start = $start;
		$this->end = $end;
		$this->date = null;
		
		$this->period = $period;
		
		$this->stack = array();

		$date_start = strtotime("{$this->start} 00:00:00");
		$date_end = strtotime("{$this->end} 23:59:59") + 1;
		
		// sort into time order
		usort($this->entries, function($a, $b) {
			if (strtotime($a["start"]) < strtotime($b["start"])) return -1;
			if (strtotime($a["start"]) > strtotime($b["start"])) return 1;
			if (strtotime($a["end"]) < strtotime($b["end"])) return -1;
			if (strtotime($a["end"]) > strtotime($b["end"])) return 1;
			return 0;
		});
		
		foreach ($this->entries as $entry) {
			$entry["timestamp_start"] = strtotime($entry["start"]);
			$entry["timestamp_end"] = strtotime($entry["end"]);
			
			// unlikely, but in case to belt-n-braces against problems
			if ($entry["timestamp_start"] > $entry["timestamp_end"]) list($entry["timestamp_start"], $entry["timestamp_end"]) = array($entry["timestamp_end"], $entry["timestamp_start"]);

			if ($entry["timestamp_start"] > $date_end || $entry["timestamp_end"] < $date_start) continue;
			
			$entry["period_start"] = floor(($entry["timestamp_start"] - $date_start) / $period);
			$entry["period_end"] = ceil(($entry["timestamp_end"] - $date_start) / $period);
			
			$match = null;
			for ($i = 0; $i < sizeof($this->stack); $i++) {
				$free = true;
				foreach ($this->stack[$i] as $existing) {
					if (
							($entry["period_start"] >= $existing["period_start"] && $entry["period_start"] < $existing["period_end"])
							|| ($entry["period_end"] > $existing["period_start"] && $entry["period_end"] <= $existing["period_end"])
							|| ($entry["period_start"] < $existing["period_start"] && $entry["period_end"] > $existing["period_end"])
					) {
						$free = false; 
						break; 
					}
				}
				if ($free) { 
					$match = $i; 
					break; 
				}
			}
			
			if ($match === null) {
				$match = sizeof($this->stack);
				$this->stack[$match] = array();
			}
			
			$this->stack[$match][] = $entry;
		}
		
		for ($i = 0; $i < sizeof($this->stack); $i++) {
			$sorted = array(); 
			$j = 0;
			foreach ($this->stack[$i] as $entry) $sorted[date("His", $entry["timestamp_start"]) . date("His", $entry["timestamp_end"]) . "_" . ($j++)] = $entry;
			ksort($sorted);
			$this->stack[$i] = array_values($sorted);
		}
	}
	
	public function compute_matrix() {
		$matrix = array();
		for ($i = 0; $i < sizeof($this->stack); $i++) {
			foreach ($this->stack[$i] as $entry) {
				for ($p = $entry["period_start"]; $p < $entry["period_end"]; $p++) {
					$matrix[$p][$i] = $entry;
				}
			}
		}

		return $matrix;
	}
		
	public function render() {
		$matrix = $this->compute_matrix();
		
		$rowspan = array();
		foreach ($matrix as $p => $col) foreach ($col as $i => $entry) {
			if ($p < 0) continue;
			if (!array_key_exists($entry["index"], $rowspan)) $rowspan[$entry["index"]] = 0;
			$rowspan[$entry["index"]]++;
		}
		
		// wipe any existing
		$this->setColumns(array("Time"));
		
		for ($i = 0; $i < max(sizeof($this->stack), 1); $i++) {
			$this->add_column("col_{$i}", "");
		}

		if ($this->date !== null) {
			// single date
			$date_start = strtotime("{$this->date} 00:00:00");
			$end_index = 24 * 60 * 60;
			$show_dates = false;
		} else {
			// range
			$date_start = strtotime("{$this->start} 00:00:00");
			$end_index = (strtotime("{$this->end} 23:59:59") + 1) - $date_start;
			$show_dates = true;
		}
		
		$first = true; 
		$started = false;
		for ($p = 0; $p < $end_index / $this->period; $p += 1) {
			$any = false;
			for ($i = 0; $i < max(sizeof($this->stack), 1); $i++) {
				if (isset($matrix[$p][$i])) $any = true;
			}
			
			if (!$any && !$started && $p < 8) continue;
			
			$started = true;
				
			$row = $this->new_row();
			$row->set_cell("Time", ($show_dates ? date("d M ", $date_start + ($p * $this->period)) : "") . date("H:i", $date_start + ($p * $this->period)));

			if (((time() - $date_start) / $this->period) > ($p + 1)) $row->get_classes()->add("past");
			
			for ($i = 0; $i < max(sizeof($this->stack), 1); $i++) {
				$row->set_cell("col_{$i}", "");
			}
			if (sizeof($this->stack) === 0 && $first) {
				$row->set_cell("col_0", "(none)");
				$first = false;
			}
			
			for ($i = 0; $i < sizeof($this->stack); $i++) if (isset($matrix[$p][$i])) {
				if ($rowspan[$matrix[$p][$i]["index"]] === null) {
					$row->set_cell_span_filler("col_{$i}");
				} else {
					$entry = $matrix[$p][$i];

					$start = date(date("Y-m-d", $entry["timestamp_start"]) === $this->date ? "H:i" : "D d H:i", $entry["timestamp_start"]);
					$end = date(date("Y-m-d", $entry["timestamp_end"]) === $this->date ? "H:i" : "D d H:i", $entry["timestamp_end"]);
						
					$cell = $row->set_cell("col_{$i}", $multi = new MultiElements(array(
						new Div("{$start} - {$end}", null, "time"),
						$entry["link"] === null ? $entry["description"] : new Link($entry["description"], $entry["link"], null, "command icon_go")
					)), "entry");
					if (isset($entry["additional"])) {
						$multi->add($list = new ItemList(ItemList::UNORDERED));
						foreach ($entry["additional"] as $item) $list->new_item($item);
					}
					$cell->add_param("rowspan", $rowspan[$matrix[$p][$i]["index"]]);
					if ($entry["class"] !== null) $cell->get_classes()->add($entry["class"]);
					$rowspan[$matrix[$p][$i]["index"]] = null;
				}
			}
		}

		parent::render();
	}
}
