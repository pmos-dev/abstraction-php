<?php
/**
 * HTML custom structure: Link bar.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/classes.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/elements/itemlist.php";

use \Abstraction as Abstraction;

/**
 * Builds a structure for a list of hyperlinks, generally styled in css as a float bar.
 * 
 * The class 'linkbar' is added automatically to the list.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class LinkBar implements Abstraction\Renderer\Renderable {
	private $list;

	/**
	 * Constructs a new instance of this component.
	 * 
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($id = null, $class = null) {
		if ($class instanceof Abstraction\Renderer\HTML\Classes) $class->add("linkbar");
		elseif (is_array($class)) $class[] = "linkbar";
		elseif ($class === null) $class = "linkbar";
		else $class = "{$class} linkbar";

		$this->list = new ItemList(ItemList::UNORDERED, $id, $class);
	}

	/**
	 * Adds a new Link to this bar.
	 * 
	 * @param Link $link
	 * @return void
	 */
	public function add(Link $link) {
		$this->list->new_item($link);
	}

	/**
	 * @internal
	 */
	public function render() {
		$this->list->render();
	}

	/**
	 * Constructs a new LinkBar statically using the given single Link object.
	 * 
	 * @param Link $link
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @return LinkBar
	 */
	public static function singular(Link $link, $id = null, $class = null) {
		$linkbar = new LinkBar($id, $class);
		$linkbar->add($link);

		return $linkbar;
	}
}
