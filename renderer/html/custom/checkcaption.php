<?php
/**
 * HTML custom structure: Check caption.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/classes.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/elements/itemlist.php";

use \Abstraction as Abstraction;

/**
 * Builds a structure for creating a checkbox alongside a caption.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class CheckCaption extends MultiElements {
	private $checkbox;
	private $span;
		
	/**
	 * Constructs a new instance of this component.
	 * 
	 * @param \Abstraction\Renderer\Renderable|string|int|float $caption the caption for this item
	 * @param boolean $checked whether the box is checked or not initially
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($caption, $checked = false, $id = null, $class = null) {
		parent::__construct();
		$this->set_compact(true);

		$this->add($this->checkbox = new Form_Checkbox($checked, $id, $class));
		$this->add($this->span = new Span($caption, null, "checkbox_caption"));
	}

	/**
	 * Returns the Checkbox element of this component.
	 * 
	 * @return Form_Checkbox
	 */
	public function get_checkbox() {
		return $this->checkbox;
	}

	/**
	 * Returns the caption element of this component.
	 * 
	 * @return Span
	 */
	public function get_span() {
		return $this->span;
	}
}
