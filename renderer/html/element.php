<?php
/**
 * HTML root element.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/classes.php";

use \Abstraction\Renderer as Renderer;
use \Abstraction\Renderer\HTML as HTML;

/**
 * Root class of all HTML element classes used by the Abstraction framework
 *
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Element implements Renderer\Renderable {
	protected $tag;
	protected $id;
	protected $class;
	private $params;
	private $atomparams;

	/**
	 * Constructs a new instance of this element.
	 * 
	 * @param string $tag the HTML tag name
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @throws HTML\Exception
	 */
	protected function __construct($tag, $id = null, $class = null) {
		$this->tag = $tag;
		$this->id = $id;

		if ($class === null) $class = new Classes();
		else if (is_string($class)) $class = new Classes($class);
		if (!($class instanceof Classes)) throw new HTML\Exception("Class is not an instanceof Classes");

		$this->class = $class;

		$this->params = array();
		$this->atomparams = array();
	}

	/**
	 * Renders the element opening tag to stdout, potentially atomically (e.g. <img />)
	 * 
	 * Element parameters are automatically rendered into the tag.
	 * 
	 * @internal
	 * @param boolean $atom true for atomic opening
	 * @return void
	 */
	private function open_or_atom_element($atom = false) {
		$params = array($this->tag);

		if ($this->id !== null) $params[] = "id=\"{$this->id}\"";

		if ("" !== ($class = $this->class->to_string())) $params[] = $class;

		foreach ($this->params as $key => $value) {
			if (is_int($value) || is_numeric($value));
			elseif ($value === null) $value = "\"\"";
			else $value = "\"" . htmlentities($value, ENT_IGNORE | ENT_COMPAT, "UTF-8") . "\"";
			$params[] = "{$key}={$value}";
		}
		foreach ($this->atomparams as $key) $params[] = $key;

		if ($atom) $params[] = "/";

		echo "<" . implode(" ", $params) . ">";
	}

	/**
	 * Renders the element opening tag to stdout.
	 * 
	 * Element parameters are automatically rendered into the tag.
	 * 
	 * @return void
	 */
	protected function open_element() {
		$this->open_or_atom_element(false);
	}


	/**
	 * Renders the element to stdout atomically (e.g. <img />).
	 * 
	 * Element parameters are automatically rendered into the tag.
	 * 
	 * @return void
	 */
	protected function atom_element() {
		$this->open_or_atom_element(true);
	}

	/**
	 * Renders the element closing tag to stdout.
	 * 
	 * @return void
	 */
	protected function close_element() {
		echo "</{$this->tag}>";
	}

	/**
	 * Adds a parameter to the tag.
	 * 
	 * @param string $key the parameter name
	 * @param mixed $value the parameter value
	 * @return void
	 */
	public function add_param($key, $value) {
		$this->params[$key] = $value;
	}

	/**
	 * Adds an atomic flag parameter to the tag (e.g. CHECKED)
	 * 
	 * @param string $key the flag name
	 * @return void
	 */
	public function add_atomic_param($key) {
		$this->atomparams[] = $key;
	}

	/**
	 * Returns the current Classes object for this tag.
	 * 
	 * Note that manipulation of the returned object is reflected back in the tag itself.
	 * 
	 * @return Classes
	 */
	public function get_classes() {
		return $this->class;
	}
	
	/**
	 * Returns the current id attribute of this element, if any.
	 * 
	 * This is largely to assist with form row auto-label for.
	 * 
	 * @return string|null the id field
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * Merges the specified Classes object with the existing tag Classes.
	 * 
	 * This is useful for mimicking inherited classes programmatically.
	 * 
	 * @param Classes $b
	 * @return void
	 */
	public function merge_in_classes(Classes $b) {
		$this->class->merge_in($b);
	}
}
