<?php
/**
 * Rendering abstraction for HTML output.
 *
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/raw.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/multielements.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/classes.php";

use \Abstraction\Renderer as Renderer;

/**
 * @internal
 */
class Exception extends Renderer\Exception {}

// import all the various elements
foreach (explode(",", "anchor,blockquote,body,cite,div,fieldset,form_button,form_checkbox,form_radio,form_file,form_hidden,form_input,form_password,form_reset,form_select,form_submit,form_text,form_textarea,form_email,form,header,hr,iframe,image,itemlist,definitionlist,label,link,meta,paragraph,pre,span,table") as $type) require_once ABSTRACTION_ROOT_PATH . "renderer/html/elements/{$type}.php";
foreach (explode(",", "header,nav,main,footer,article,section,aside") as $type) require_once ABSTRACTION_ROOT_PATH . "renderer/html/semantics/{$type}.php";
foreach (explode(",", "checkcaption,layout,linkbar,linkicon") as $type) require_once ABSTRACTION_ROOT_PATH . "renderer/html/custom/{$type}.php";

// import markup element
require_once ABSTRACTION_ROOT_PATH . "renderer/html/markup.php";

/**
 * Rendering engine for HTML output.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class HTML implements Renderer\Engine, Renderer\Renderable {
	const TYPE_CSS = "css";
	const TYPE_JAVASCRIPT = "js";
	
	protected $includes;
	protected $codes;
	protected $meta;
	protected $variables;
	
	protected $print_css = null;
	
	private $body;
	private $page;
	private $raw_head_content = null;
	
	/**
	 * Constructs a new instance of this engine.
	 */
	public function __construct() {
		$this->includes = array(
				self::TYPE_CSS => array(),
				self::TYPE_JAVASCRIPT => array()
		);
		$this->codes = array(
				self::TYPE_CSS => array(),
				self::TYPE_JAVASCRIPT => array()
		);
		$this->meta = array();
		
		$this->variables = array(
				"page_title" => null
		);
		
		$this->body = new Body($this->page = new MultiElements());
		$this->page->set_compact(false);
	}
	
	/**
	 * Returns the current 'page' for this engine, to which elements can be added.
	 *
	 * @return MultiElements
	 */
	public function get_page() {
		return $this->page;
	}
	
	/**
	 * Returns the body of the HTML for this engine.
	 *
	 * Generally speaking this should only be used to add classes to the body tag, and content should be attached via get_page instead.
	 *
	 * @return Body
	 */
	public function get_body() {
		return $this->body;
	}
	
	/**
	 * Adds a file to be included by the HTML code.
	 *
	 * @param string $type either TYPE_CSS or TYPE_JAVASCRIPT
	 * @param string $filename the URL to the file
	 * @throws Exception
	 * @return void
	 */
	public function add_include($type, $filename) {
		switch($type) {
			case self::TYPE_CSS:
				$this->includes[$type][$filename] = $filename;
				break;
			case self::TYPE_JAVASCRIPT:
				$this->includes[$type][$filename] = $filename;
				break;
			default: throw new Exception("Unknown include type", $type);
		}
	}
	
	/**
	 * Sets the overarching print media CSS file.
	 *
	 * @param string $filename the URL to the file
	 * @throws Exception
	 * @return void
	 */
	public function set_print_css($filename) {
		$this->print_css = $filename;
	}
	
	/**
	 * Adds a block of inline code to be rendered in the HTML head tag region.
	 *
	 * @param string $type either TYPE_CSS or TYPE_JAVASCRIPT
	 * @param string $code the inline code
	 * @throws Exception
	 * @return void
	 */
	public function add_code($type, $code) {
		switch($type) {
			case self::TYPE_CSS:
				$this->codes[$type][] = $code;
				break;
			case self::TYPE_JAVASCRIPT:
				$this->codes[$type][] = $code;
				break;
			default: throw new Exception("Unknown code type", $type);
		}
	}
	
	/**
	 * Added a meta tag to the header area of the HTML page.
	 *
	 * This is useful for directives that set things like the viewport.
	 *
	 * @param Meta $tag the meta tag to add
	 * @return void
	 */
	public function add_meta(Meta $tag) {
		$this->meta[] = $tag;
	}
	
	/**
	 * Sets an internal variable for use by the engine.
	 *
	 * This is used for the page_title predominately, and other aspects that cannot be controlled within the get_page() context.
	 *
	 * @param string $var the variable name
	 * @param mixed $value the value to store
	 * @return void
	 */
	public function set_variable($var, $value) {
		$this->variables[$var] = $value;
	}
	
	public function add_raw_head_content($content) {
		$this->raw_head_content = $content;
	}
	
	const SCRIPT_BREAK_DIVIDER = "\r\n\r\n/* -------------------- */\r\n\r\n";
	
	/**
	 * Opens the HTML document, rendering to stdout.
	 *
	 * The doctype is written, alongside the html tag itself. Then the head tag is written, with any includes for styles and scripts, any inline code, and finally the title tag.
	 * This method is invoked by methods such as render() and not intended for direct usage.
	 *
	 * @internal
	 * @return void
	 */
	function open() {
		echo <<<HTML
<!doctype html>
<html lang="en-GB">

<head>

HTML;
		
		foreach ($this->meta as $meta) {
			$meta->render();
			echo "\r\n";
		}
		
		foreach ($this->includes["css"] as $filename) echo <<<HTML
<style type="text/css" media="screen">@import url("{$filename}");</style>

HTML;
		
		if ($this->print_css !== null) echo <<<HTML
<style type="text/css" media="print">@import url("{$this->print_css}");</style>

HTML;
		
		if (sizeof($this->codes["css"]) > 0) {
			$code = implode(self::SCRIPT_BREAK_DIVIDER, $this->codes["css"]);
			
			echo <<<HTML
<style type="text/css" media="screen"><!--

{$code}

--></style>

HTML;
		}
		
		foreach ($this->includes["js"] as $filename) echo <<<HTML
<script type="text/javascript" src="{$filename}"></script>

HTML;
		
		if (sizeof($this->codes["js"]) > 0) {
			$code = implode(self::SCRIPT_BREAK_DIVIDER, $this->codes["js"]);
			
			echo <<<HTML
<script type="text/javascript"><!--

{$code}

//--></script>

HTML;
		}
		
		if ($this->variables["page_title"] !== null) echo <<<HTML
		
<title>{$this->variables["page_title"]}</title>

HTML;
		
		if ($this->raw_head_content !== null) echo <<<HTML
{$this->raw_head_content}

HTML;

echo <<<HTML
</head>

HTML;
	}
	
	/**
	 * Closes the HTML document, rendering to stdout.
	 *
	 * This method is invoked by methods such as render() and not intended for direct usage.
	 *
	 * @internal
	 * @return void
	 */
	function close() {
		echo <<<HTML
		
</html>
HTML;
	}
	
	/**
	 * Renders the entire constructed HTML content to stdout, without terminating script execution.
	 *
	 * Note that this also closes the HTML tag, so no further output show be written to stdout after this method has been called.
	 * Usually the complete() method is preferred to render and then terminate execution, however there are some instances where an HTML dump needs to be captured using ob_start/ob_get_clean etc., so this method is public.
	 *
	 * @return void
	 */
	function render() {
		$this->open();
		
		$this->body->render();
		
		$this->close();
	}
	
	/**
	 * Renders the entire constructed HTML content to stdout, and then terminates script execution.
	 *
	 * This should be the last line of the script workflow, and <b>subclasses should ensure this method does not accidently return</b>.
	 *
	 * @return void
	 */
	public function complete() {
		$this->render();
		die();
	}
	
	/**
	 * Wipes any page content, replacing it with a preformatted error message, then writes the resulting HTML content to stdout, and terminates script execution.
	 *
	 * Note that only page content is wiped, not includes and inline code.
	 * <b>Subclasses should ensure this method does not accidently return.</b>
	 *
	 * @param string $message an optional error message to display
	 * @return void
	 */
	public function error($message = "(unspecified error)") {
		$this->body->get_classes()->add("error");
		
		$this->page->clear();
		$this->page->add(new Header("Opps, an error has occurred!", Header::LEVEL_1, null, "error"));
		$this->page->add(new Paragraph($message));
		
		$this->complete();
	}
	
	/**
	 * Wipes any page content, replacing it with a preformatted confirmation request, then writes the resulting HTML content to stdout, and terminates script execution.
	 *
	 * The generated confirmation request is a link to the current URL, but with ?confirm=1 (or &confirm=1 for existing parameters) appended.
	 * This allows scripts to preface any potentially dangerous operation with a simple check, for example: if (!isset($_GET["confirm"])) $_HTML->confirm("Do this?");
	 * Note that only page content is wiped, not includes and inline code.
	 * <b>Subclasses should ensure this method does not accidently return.</b>
	 *
	 * @param string $message the confirmation message to be displayed
	 * @return void
	 */
	public function confirm($message) {
		$this->page->clear();
		$this->page->add(new Header("Confirmation required", Header::LEVEL_1, null, "confirm"));
		$this->page->add(new Paragraph($message));
		
		$url = $_SERVER["REQUEST_URI"] . (preg_match("`\?`", $_SERVER["REQUEST_URI"]) ? "&" : "?") . "confirm=1";
		$this->page->add(LinkBar::singular(new Link($url, "Yes, do this", null, "command icon_confirm")));
		
		$this->complete();
	}
	
	/**
	 * Wipes any page content, replacing it with a preformatted success message, then writes the resulting HTML content to stdout, and terminates script execution.
	 *
	 * This is a shorthand method that allows successfully executed operations to return a stock "success" message to the user.
	 * Note that only page content is wiped, not includes and inline code.
	 * <b>Subclasses should ensure this method does not accidently return.</b>
	 *
	 * @param string $title the header title to be displayed
	 * @param string $message an optional sub-message to be displayed
	 * @return void
	 */
	public function success($title, $message = "") {
		$this->page->clear();
		$this->page->add(new Header($title, Header::LEVEL_1));
		if ($message !== "") $this->page->add(new Paragraph($message));
		
		$this->complete();
	}
}
