<?php
/**
 * Storage of multiple Renderable content.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/element.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/inline_text.php";

use \Abstraction\Renderer as Renderer;

/**
 * Allows singular content variables to store multiple content classes.
 * 
 * This class is used extensively to provide a singular node (Renderable) stack for many pieces of serial Renderable content.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class MultiElements implements Renderer\Renderable {
	private $elements;
	
	protected $COMPACT = true;
	protected $INDENT = false;

	/**
	 * Constructs a new instance.
	 * 
	 * @param (Renderer\Renderable|string|int|float)[] $elements any content to add from the outset
	 */
	public function __construct($elements = array()) {
		$this->elements = array();
		foreach ($elements as $e) $this->add($e);
	}

	/**
	 * Sets the COMPACT flag for rendering.
	 * 
	 * By default, multiple pieces of content are separated by new lines. Setting the COMPACT flag to true suppresses this.
	 * 
	 * @param boolean $state true to set the COMPACT flag; false to remove it
	 * @return void
	 */
	public function set_compact($state) {
		$this->COMPACT = $state;
	}
	
	/**
	 * Sets the INDENT flag for rendering.
	 * 
	 * If the COMPACT flag is false and the INDENT flag it true, then multiple pieces of content on new lines will be indented using the tab character.
	 * By default, INDENTing is disabled.
	 * 
	 * @param boolean $state true to set the INDENT flag; false to remove it
	 * @return void
	 */
	public function set_indent($state) {
		$this->INDENT = $state;
	}

	/**
	 * Adds a piece of content to the end of the stack.
	 * 
	 * Strings and numeric values are automatically wrapped in the Inline_Text class, so HTML encoding of various characters is performed automatically.
	 * Anything else must implement the Renderable interface.
	 * 
	 * @param Renderer\Renderable|string|int|float $element the content to add
	 * @throws Exception
	 * @return void
	 */
	public function add($element) {
		if (is_string($element) || is_numeric($element)) $element = new Inline_Text($element);
		if (!($element instanceof Renderer\Renderable)) throw new Exception("Element does not implement the Renderable interface");
		
		$this->elements[] = $element;
	}
	
	/**
	 * Adds a piece of content to the beginning of the stack.
	 * 
	 * Strings and numeric values are automatically wrapped in the Inline_Text class, so HTML encoding of various characters is performed automatically.
	 * Anything else must implement the Renderable interface.
	 * 
	 * @param Renderer\Renderable|string|int|float $element the content to add
	 * @throws Exception
	 * @return void
	 */
	public function preadd($element) {
		if (is_string($element) || is_numeric($element)) $element = new Inline_Text($element);
		if (!($element instanceof Renderer\Renderable)) throw new Exception("Element does not implement the Renderable interface");
		
		array_unshift($this->elements, $element);
	}

	/**
	 * Renders the stack contents to stdout.
	 * 
	 * @return void
	 */
	public function render() {
		foreach ($this->elements as $element) {
			if (!$this->COMPACT) {
				echo "\r\n";
				if ($this->INDENT) echo "\t";
			}
			$element->render();
		}
		if (!$this->COMPACT) echo "\r\n";
	}

	/**
	 * Wipes the current content stack storage.
	 * 
	 * @return void
	 */
	public function clear() {
		$this->elements = array();
	}

	/**
	 * Returns the number of items in this content stack.
	 * 
	 * @return int
	 */
	public function sizeof() {
		return sizeof($this->elements);
	}
}
