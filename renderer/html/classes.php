<?php
/**
 * Class handling for elements.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");

/**
 * Provides storage and maintenance of class information for elements.
 * 
 * Rather than just storing a string property in the element, this class allows features such as adding, removing and merging classes.
 * 
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class Classes {
	private $classes;
	private $classes_lc;

	/**
	 * Constructs a new instance of this class manager.
	 * 
	 * @param string[]|string|NULL $classes an optional set of classes to include from the outset, either as an array or space-delimited string
	 * @throws Exception
	 */
	public function __construct($classes = null) {
		$this->classes = array();
		$this->classes_lc = array();

		if ($classes === null) $classes = array();
		elseif (is_string($classes)) {
			$incoming = explode(" ", $classes);
			$classes = array();
			foreach ($incoming as $class) if ("" != ($class = trim($class))) $classes[] = $class;
		}

		if (!is_array($classes)) throw new \Exception("Given classes is not an array");
		foreach ($classes as $class) $this->add($class);
	}

	/**
	 * Checks if a class exists within this class manager.
	 * 
	 * Note, comparison with existing classes is performed case insensitively.
	 * 
	 * @param string $class the name of the class to check for
	 * @throws Exception
	 * @return boolean true if the class exists, or false if it does not
	 */
	public function has($class) {
		if (!is_string($class)) throw new \Exception("Class name is not a string");

		return in_array(strtolower($class), $this->classes_lc);
	}

	/**
	 * Adds a new class to this class manager.
	 * 
	 * Note, comparison with existing classes is performed case insensitively.
	 * 
	 * @param string $class the name of the class to add
	 * @throws Exception
	 * @return boolean true if the class was added, or false if was already existing
	 */
	public function add($class) {
		if ($this->has($class)) return false;

		$this->classes[] = $class;
		$this->classes_lc[] = strtolower($class);

		return true;
	}

	/**
	 * Removes a new class to this class manager.
	 * 
	 * Note, comparison with existing classes is performed case insensitively.
	 * 
	 * @param string $class the name of the class to remove
	 * @return boolean true if the class was removed, or false if wasn't present
	 */
	public function remove($class) {
		if (!$this->has($class)) return false;

		$lc = strtolower($class);

		$index = false;
		for ($i = sizeof($this->classes); $i-- > 0;) {
			if (strtolower($this->classes_lc[$i]) === $lc) {
				$index = $i;
				break;
			}
		}

		$rebuild = array();
		for ($i = sizeof($this->classes); $i-- > 0;) {
			if ($i !== $index) $rebuild[] = $this->classes[$i];
		}

		$this->classes = array(); 
		$this->classes_lc = array();
		foreach ($rebuild as $class) $this->add($class);

		return true;
	}

	/**
	 * Renders any stored classes into the format: class="..."
	 * 
	 * If no classes are stored, returns an empty string.
	 * 
	 * @return string
	 */
	public function to_string() {
		return sizeof($this->classes) == 0 ? "" : ("class=\"" . implode(" ", $this->classes) . "\"");
	}

	/**
	 * Merges this class manager's classes with another manager's and returns the combined (unique) results.
	 * 
	 * Note, this method does not alter either class managers' existing classes at all, and creates a manager.
	 * 
	 * @param Classes $b the second class manager to merge with
	 * @return Classes
	 */
	public function merge(Classes $b) {
		$merged = new Classes($this->classes);
		$merged->merge_in($b);

		return $merged;
	}

	/**
	 * Imports another class manager's classes to supplement any existing ones.
	 * 
	 * @param Classes $b the second class manager to merge with
	 * @return void
	 */
	public function merge_in(Classes $b) {
		foreach ($b->classes as $class) $this->add($class);
	}
}
