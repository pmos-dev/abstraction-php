<?php
/**
 * HTML element: Image.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

/**
 * Represents an img tag within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Image extends Element {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string $url the URL of the image
	 * @param string|NULL $alt an optional value for the alt="..." parameter
	 * @param string|NULL $title an optional value for the title="..." parameter
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($url, $alt = null, $title = null, $id = null, $class = null) {
		parent::__construct("img", $id, $class);
		$this->add_param("src", $url);
		if (null !== $alt) $this->add_param("alt", $alt);
		if (null !== $title) $this->add_param("title", $title);
	}

	/**
	 * @internal
	 */
	public function render() {
		$this->atom_element();
	}
}
