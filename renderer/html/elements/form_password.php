<?php
/**
 * HTML element: Password input box.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/elements/form_input.php";

/**
 * Represents an input[type=password] tag within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Form_Password extends Form_Input {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string $value an optional value for the value="..." parameter
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($value = "", $id = null, $class = null) {
		parent::__construct("password", $id, $class);
		
		$this->add_param("value", $value);
	}
}
