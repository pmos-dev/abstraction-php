<?php
/**
 * HTML element: Check box.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/elements/form_input.php";

/**
 * Represents an input[type=checkbox] tag within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Form_Checkbox extends Form_Input {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param boolean $checked whether the box is checked or not initially
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($checked = false, $id = null, $class = null) {
		parent::__construct("checkbox", $id, $class);
		
		if ($checked) $this->add_atomic_param("checked");
	}
}
