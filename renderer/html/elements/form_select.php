<?php
/**
 * HTML element: Drop-down select list.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/elements/form_input.php";

/**
 * Represents an option tag within the HTML abstraction.
 *
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class Select_Option extends WrappingElement {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param mixed $value the value for the value="..." parameter
	 * @param string $description an optional value to use as displayed child content; if null, copies $value
	 * @param boolean $selected whether this option is initially selected or not
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($value, $description = null, $selected = false, $id = null, $class = null) {
		parent::__construct("option", $description === null ? $value : $description, $id, $class);
		$this->add_param("value", $value);
		if ($selected) $this->add_atomic_param("selected");
	}
}

/**
 * Represents a select tag within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Form_Select extends WrappingElement {
	private $options;

	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @param boolean $_PLEASE_SELECT if set to true, an initial option with an empty value and the description "(pleases select)" is added
	 */
	public function __construct($id, $class = null, $_PLEASE_SELECT = false) {
		parent::__construct("select", $this->options = new MultiElements(), $id, $class);
		
		$this->add_param("name", $id);
		$this->COMPACT = false;

		if ($_PLEASE_SELECT) $this->add_option("", "(please select)");
	}

	/**
	 * Adds a new option to this select by constructing an option tag automatically.
	 * 
	 * @param mixed $value the value for the value="..." parameter
	 * @param string $description an optional value to use as displayed child content; if null, copies $value
	 * @param boolean $selected whether this option is initially selected or not
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @return void
	 */
	public function add_option($value, $description = null, $selected = false, $id = null, $class = null) {
		$this->options->add(new Select_Option($value, $description, $selected, $id, $class));
	}

	/**
	 * Builds a new select tag and option array from a list of possible values
	 * 
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param string[] $list an associative array of option values in the format value => description
	 * @param mixed|NULL $selected which, if any, option is initially selected
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @return Form_Select
	 */
	public static function buildFromKeyValArray($id, array $list, $selected = null, $class = null) {
		$select = new Form_Select($id, $class);
		foreach ($list as $key => $val) $select->add_option($key, $val, ($key === $selected));
		return $select;
	}
}
