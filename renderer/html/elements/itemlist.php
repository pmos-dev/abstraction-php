<?php
/**
 * HTML element: Ordered and unordered lists.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

use \Abstraction\Renderer as Renderer;

/**
 * Represents a li tag within the HTML abstraction.
 * 
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class ItemList_Item extends WrappingElement {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $value the child content for this list item
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($value, $id = null, $class = null) {
		if (is_string($value) || is_numeric($value)) $value = new Inline_Text($value);
		if (!($value instanceof Renderer\Renderable)) throw new Exception("Value does not implement the AbstractionRenderer_Renderable interface");

		parent::__construct("li", $value, $id, $class);
		$this->COMPACT = true;
	}
}

/**
 * Represents a ul or ol tag within the HTML abstraction.
 * 
 * Note that 'List' is a reserved word in PHP, so cannot be used for this class, hence ItemList.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class ItemList extends WrappingElement {
	const UNORDERED = "unordered";
	const ORDERED = "ordered";
	
	private $items, $multi;
	
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string $type the list type, either UNORDERED or ORDERED
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @throws Exception
	 */
	public function __construct($type, $id = null, $class = null) {
		switch ($type) {
			case self::UNORDERED:
				$tag = "ul"; 
				break;
			case self::ORDERED: 
				$tag = "ol"; 
				break;
			default:
				throw new Exception("Unknown list type");			
		}
		
		parent::__construct($tag, $this->multi = new MultiElements(), $id, $class);
		$this->multi->set_compact(false);
		$this->multi->set_indent(true);
		
		$this->items = array();
	}

	/**
	 * Directly adds a new item to the list.
	 * 
	 * This method is not used often; new_item is generally preferred, because it accepts a greater range of content.
	 * @param ItemList_Item $item
	 * @return void
	 */
	public function add_item(ItemList_Item $item) {
		$this->multi->add($item);
		$this->items[] = $item;
	}

	/**
	 * Builds a new list item using the specified content and adds it to the list.
	 * 
	 * Returns the newly constructed ItemList_Item so that classes can be added if desired.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $value the child content for this element
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @return ItemList_Item
	 */
	public function new_item($value, $id = null, $class = null) {
		$item = new ItemList_Item($value, $id, $class);
		$this->add_item($item);
		return $item;
	}

	/**
	 * Returns the number of items in the list.
	 * 
	 * @return int
	 */
	public function sizeof() {
		return sizeof($this->items);
	}

	/**
	 * Returns the items in the list.
	 * 
	 * @return ItemList_Item[]
	 */
	public function get_items() {
		return $this->items;
	}

	/**
	 * Wipes the existing list items
	 *
	 * @return void
	 */
	public function clear() {
		$this->multi->clear();
		$this->items = array();
	}
}
