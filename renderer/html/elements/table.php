<?php
/**
 * HTML element: Table and its components.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

use \Abstraction\Renderer as Renderer;

/**
 * Root interface for all table row types.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
interface Table_Row extends Renderer\Renderable {
	const TYPE_NORMAL = 0;
	const TYPE_TITLE = 1;
}

/**
 * Represents a td or th tag within the HTML abstraction.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class Table_Cell extends WrappingElement {
	/**
	 * Constructs a new instance of this element.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $value the child content value for this cell
	 * @param int $type the row type, either TYPE_NORMAL or TYPE_TITLE
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @throws Exception
	 */
	public function __construct($value, $type = Table_Row::TYPE_NORMAL, $class = null) {
		switch($type) {
			case Table_Row::TYPE_NORMAL:
				$tag = "td";
				break;
			case Table_Row::TYPE_TITLE:
				$tag = "th";
				break;
			default:
				throw new Exception("Row type is not valid"); 
		}
		
		if (is_string($value) || is_numeric($value)) $value = new Inline_Text($value);
		if (!($value instanceof Renderer\Renderable)) throw new Exception("Value does not implement the Renderable interface");

		parent::__construct($tag, $value, null, $class);
	}
}

/**
 * Represents a virtual element as a 'filler' when using colspan and rowspan.
 * 
 * The table matrix layout requires a Table_Cell for each cell, however HTML tables using colspan and rowspan cannot have these corresponding non-existent tags explicitly defined.
 * This class compromises this situation by providing a Renderable Table_Cell which does not actually produce any output content.
 *
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class Table_SpanFiller extends Table_Cell {
	/**
	 * Constructs a new instance of this filler.
	 */
	public function __construct() {
		// blank placeholder class
	}

	/**
	 * Does nothing (intentionally; see class documentation)
	 * 
	 * @return void
	 */
	public function render() {}
}

/**
 * Title rows are tr tags which are defined to automatically span all columns of the table at render time, so only have one content cell.
 * 
 * Title rows have the class "titlerow" added to them automatically to help with CSS styling.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class Table_TitleRow extends Element implements Table_Row {
	private $caption;
	private $table;
	
	/**
	 * Constructs a new instance of this title row.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $caption the child caption for the title
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @param Table $table the Table element this row is being added to, which is needed to fetch the number of columns at render time
	 * @throws Exception
	 */
	public function __construct($caption, $class, Table &$table) {
		parent::__construct("tr", null, $class);
		
		if (is_string($caption) || is_numeric($caption)) $caption = new Inline_Text($caption);
		if (!($caption instanceof Renderer\Renderable)) throw new Exception("Caption does not implement the Renderable interface");
		
		$this->caption = $caption;
		$this->table = &$table;

		$this->class->add("titlerow");
	}

	/**
	 * @internal
	 */
	public function render() {
		$this->open_element();
		
		$columns = $this->table->get_columns();
		
		$cell = new Table_Cell($this->caption, Table_Row::TYPE_TITLE, null, $this->class);
		$cell->add_param("colspan", sizeof($columns));
		
		$cell->render();

		$this->close_element();
	}
}

/**
 * Represents a non-TitleRow tr tag within the HTML abstraction.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class Table_CellRow extends Element implements Table_Row {
	private $table;
	private $cells;
	private $type;

	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param Table $table the Table element this row is being added to, which is needed to fetch the number of columns at render time
	 * @param int $type either TYPE_NORMAL or TYPE_TITLE to indicate the row type
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @throws Exception
	 */
	public function __construct(Table &$table, $type = Table_Row::TYPE_NORMAL, $id = null, $class = null) {
		parent::__construct("tr", $id, $class);

		switch($type) { 
			case Table_Row::TYPE_NORMAL: break; 
			case Table_Row::TYPE_TITLE: break;
			default: 
				throw new Exception("Row type is not valid"); 
		}

		$this->type = $type;
		
		$this->table = &$table;
		
		$this->cells = array();
		for ($i = $this->table->get_num_columns(), $j = 0; $i-- > 0; $j++) {
			$this->cells[$j] = null;
		}
	}

	/**
	 * @internal
	 */
	private function set_cell_direct($name, Table_Cell $cell) {
		if (false === ($index = $this->table->get_column_index_by_name($name))) throw new Exception("Unable to identify index for column {$name}");
		$this->cells[$index] = $cell;
	}

	/**
	 * @internal
	 */
	private function set_cell_indirect($name, $value, $class = null) {
		if ($class === null) $class = new Classes();
		else if (is_string($class)) $class = new Classes($class);
		if (!($class instanceof Classes)) throw new Exception("Class is not an instanceof Classes");
		
		if ($value === null) $value = "";
		if (is_string($value) || is_numeric($value)) $value = new Inline_Text($value);
		if (!($value instanceof Renderer\Renderable)) throw new Exception("Value does not implement the Renderable interface");
		
		$cell = new Table_Cell($value, $this->type, $class);
		$this->set_cell_direct($name, $cell);
		
		return $cell;
	}
	
	/**
	 * Sets the value of a cell within the table matrix for this row.
	 * 
	 * @param string $name the cell column identifier
	 * @param string|int|float|\Abstraction\Renderer\Renderable $value the child content value for this cell
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @return Table_Cell
	 */
	public function set_cell($name, $value = null, $class = null) {
		if ($value instanceof Table_Cell) return $this->set_cell_direct($name, $value);
		return $this->set_cell_indirect($name, $value, $class);
	}

	/**
	 * Indicates that a specific cell within the table matrix for this row is non-existent due to a colspan or rowspan.
	 * 
	 * @param string $name the cell column identifier
	 */
	public function set_cell_span_filler($name) {
		return $this->set_cell_direct($name, new Table_SpanFiller());
	}

	/**
	 * Sets all the cells within the table matrix for this row using the supplied array of content values.
	 * 
	 * @param (string|int|float|\Abstraction\Renderer\Renderable)[] $cells an associative array of cell content, taking the formal cell column identifier => content value
	 */
	public function set_cells(array $cells) {
		foreach ($cells as $name => $value) $this->set_cell($name, $value);
	}

	/**
	 * Returns the current content value for a given cell in this table row.
	 * 
	 * @param string $name the cell column identifier
	 * @throws Exception
	 * @return string|int|float|\Abstraction\Renderer\Renderable
	 */
	public function get_cell($name) {
		if (false === ($index = $this->table->get_column_index_by_name($name))) throw new Exception("No such cell column", $name);
		return $this->cells[$index];
	}

	/**
	 * @internal
	 */
	public function render() {
		$this->open_element();
		$columns = $this->table->get_columns();
		$i = 0;
		foreach ($this->cells as $cell) {
			if ($cell instanceof Table_SpanFiller) continue;	// allows for colspan and rowspan
			if ($cell === null) throw new Exception("Table cell has not been set", $columns[$i]["name"]);
			
			$cell->merge_in_classes($columns[$i++]["class"]);
			$cell->render();
		}
	
		$this->close_element();
	}
}

/**
 * Root class for representating the various components of a table structure (header, body and footer).
 * 
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Table_SubElement extends Element {
	protected $table;

	const HEADER = "thead";
	const BODY = "tbody";
	const FOOTER = "tfoot";
	
	/**
	 * Constructs a new instance of this component.
	 * 
	 * @param string $tag one of the component types: HEADER, BODY or FOOTER
	 * @param Table $table the Table element this component belongs to
	 */
	public function __construct($tag, Table &$table) {
		parent::__construct($tag, null, null);
		$this->table = &$table;
	}
}

/**
 * Represents a thead tag within the HTML abstraction.
 * 
 * When rendered, a row of column headings is automatically generated unless overridden.
 * 
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class Table_Head extends Table_SubElement {
	private $override = false;
	
	/**
	 * Constructs a new instance of this component.
	 * 
	 * @param Table $table the Table element this component belongs to
	 */
	public function __construct(Table &$table) {
		parent::__construct(self::HEADER, $table);
	}

	/**
	 * Sets an override for to be used instead of automatically generating column headings.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $override the content to override with
	 * @return void
	 */
	public function _set_override($override) {
		$this->override = $override;
	}

	/**
	 * @internal
	 */
	public function render() {
		$this->open_element();
		
		if ($this->override !== false) {
			if ($this->override !== null) $this->override->render();
		} else {		
			echo "\r\n";
			
			$row = new Table_CellRow($this->table, Table_Row::TYPE_TITLE);
			$columns = $this->table->get_columns();
			
			foreach ($columns as $column) $row->set_cell($column["name"], $column["description"], $column["class"]);
			
			$row->render();
					
			echo "\r\n";
		}
		$this->close_element();
	}
}

/**
 * Represents a tfoot tag within the HTML abstraction.
 * 
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class Table_Foot extends Table_SubElement {
	/**
	 * Constructs a new instance of this component.
	 * 
	 * @param Table $table the Table element this component belongs to
	 */
	public function __construct(Table &$table) {
		parent::__construct("tfoot", $table);
	}
	
	/**
	 * @internal
	 */
	public function render() {
		echo "\r\n";
		$this->open_element();
		$this->close_element();
		echo "\r\n";
	}
}

/**
 * Represents a tbody tag within the HTML abstraction.
 * 
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class Table_Body extends Table_SubElement {
	private $rows;
	
	/**
	 * Constructs a new instance of this component.
	 * 
	 * @param Table $table the Table element this component belongs to
	 */
	public function __construct(Table &$table) {
		parent::__construct("tbody", $table);
		$this->rows = array();
	}
	
	/**
	 * @internal
	 */
	public function render() {
		$this->open_element();
		echo "\r\n";
		
		foreach ($this->rows as $row) $row->render();

		echo "\r\n";
		$this->close_element();
	}

	/**
	 * Builds a new TitleRow using the given content caption and adds it to the table body.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $caption the content to build with TitleRow with
	 * @param Classes|string|NULL $class an optional set of classes for the row
	 * @return void
	 */
	public function add_title_row($caption, $class = null) {
		$this->rows[] = new Table_TitleRow($caption, $class, $this->table);
	}

	/**
	 * Adds the specified row to this table body.
	 * 
	 * @param Table_Row $row the row to add
	 * @return void
	 */
	public function add_row(Table_Row $row) {
		$this->rows[] = $row;
	}

	/**
	 * Adds a Raw element to the table body, for advanced table coding.
	 *  
	 * @param Raw $raw a Raw element.
	 * @return void
	 */
	public function add_raw_row(Raw $raw) {
		$this->rows[] = $raw;
	}
	
	/**
	 * Returns the number of rows in this table body.
	 * 
	 * @return int
	 */
	public function get_num_rows() {
		return sizeof($this->rows);
	}
	
	/**
	 * Returns a specific row from the table body.
	 * 
	 * @param int $index the index of the row to return
	 * @return Table_Body
	 */
	public function get_row($index) {
		return $this->rows[$index];
	}
}

/**
 * Represents a table tag and its sub components within the HTML abstraction.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class Table extends WrappingElement {
	private $thead, $tbody, $tfoot;
	private $columns;

	/**
	 * Constructs a new instance of this component.
	 * 
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($id = null, $class = null) {
		parent::__construct("table", $content = new MultiElements(), $id, $class);
		
		$this->thead = new Table_Head($this);
		$this->tfoot = new Table_Foot($this);
		$this->tbody = new Table_Body($this);

		$this->columns = array();

		$this->add_param("cellspacing", 0);
		$this->add_param("cellpadding", 0);
		
		$content->add($this->thead);
		$content->add($this->tbody);
		$content->add($this->tfoot);
		
		$content->set_compact(false);
		$content->set_indent(true);
	}

	/**
	 * Calls the _set_override function for the embedded thead component, allowing the table header to be altered manually.
	 * 
	 * @see Table_Head::_set_override()
	 * @param string|int|float|\Abstraction\Renderer\Renderable $override the content to override with
	 * @return void
	 */
	public function manually_override_header($override) {
		$this->thead->_set_override($override);
	}

	/**
	 * Adds a new column to the table matrix.
	 * 
	 * @param string $name the cell column identifier
	 * @param string|NULL $description a description to use in the displayed column title header, or null to use the value of $name
	 * @param Classes|string|NULL $class an optional set of classes for this column, which will be inherited by matrix row cells in this column
	 * @throws Exception
	 * @return void
	 */
	public function add_column($name, $description = null, $class = null) {
		if ($class === null) $class = new Classes();
		else if (is_string($class)) $class = new Classes($class);
		if (!($class instanceof Classes)) throw new Exception("Class is not an instanceof Classes");
				
		if ($description === null) $description = $name;
		if (is_string($description) || is_numeric($description)) $description = new Inline_Text($description);
		if (!($description instanceof Renderer\Renderable)) throw new Exception("Description does not implement the Renderable interface");
		
		$this->columns[] = array("name" => $name, "description" => $description, "class" => $class);
	}
	
	/**
	 * Sets the columns for the table in one go, wiping any existing ones.
	 * 
	 * @param array $columns either an array of strings or an array of associative arrays containing the keys name, description and class.
	 * @throws Exception
	 * @return void
	 */
	public function setColumns(array $columns) {
		if ((sizeof($columns) > 0) && is_string($columns[0])) {
			$rebuild = array();
			foreach ($columns as $column) $rebuild[] = array("name" => $column);
			$columns = $rebuild;
		}
		
		$this->columns = array();
		foreach ($columns as $column) {
			if (!isset($column["name"])) throw new Exception("Column does not have a name");
			
			if (!isset($column["description"])) $column["description"] = null;
			if (!isset($column["class"])) $column["class"] = null;
			
			$this->add_column($column["name"], $column["description"], $column["class"]);
		}
	}

	/**
	 * Returns the columns making up this table.
	 * 
	 * The returned result is an array of associative arrays, containing the keys name, description and class.
	 * 
	 * @return array
	 */
	public function get_columns() {
		return $this->columns;
	}

	/**
	 * Returns the index of the specified cell column name identifier, or false if it does not exist.
	 * 
	 * @param string $name the cell column identifier
	 * @return int|boolean
	 */
	public function get_column_index_by_name($name) {
		for ($i = sizeof($this->columns); $i-- > 0;) {
			if ($this->columns[$i]["name"] == $name) return $i;
		}
		return false;
	}

	/**
	 * Returns the number of columns in this table.
	 * 
	 * @return int
	 */
	public function get_num_columns() {
		return sizeof($this->columns);
	}

	/**
	 * Builds a new TitleRow using the given content caption and adds it to the table's body.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $caption the content to build with TitleRow with
	 * @param Classes|string|NULL $class an optional set of classes for the row
	 * @return void
	 */
	public function add_title_row($caption, $class = null) {
		$this->tbody->add_title_row($caption, $class);
	}

	/**
	 * Builds a new regular tr row, adds it to the table's body, and then returns it for further use.
	 * 
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @return Table_CellRow
	 */
	public function new_row($id = null, $class = null) {
		$row = new Table_CellRow($this, Table_Row::TYPE_NORMAL, $id, $class);
		$this->add_row($row);
		return $row;
	}
	
	/**
	 * Adds the specified row to this table's body.
	 * 
	 * @param Table_Row $row the row to add
	 * @return void
	 */
	public function add_row(Table_Row $row) {
		$this->tbody->add_row($row);
	}
	
	/**
	 * Adds a Raw element to the table's body, for advanced table coding.
	 *  
	 * @param Raw $raw a Raw element.
	 * @return void
	 */
	public function add_raw_row(Raw $raw) {
		$this->tbody->add_raw_row($raw);
	}
	
	/**
	 * Returns the number of rows in the table body.
	 * 
	 * @return int
	 */
	public function get_num_rows() {
		return $this->tbody->get_num_rows();
	}
	
	/**
	 * Returns a specific row from the table body.
	 * 
	 * @param int $index the index of the row to return
	 * @return Table_Body
	 */
	public function get_row($index) {
		return $this->tbody->get_row($index);
	}
}
