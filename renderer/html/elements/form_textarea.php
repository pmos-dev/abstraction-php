<?php
/**
 * HTML element: Multi-line text area input box.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/elements/form_input.php";

/**
 * Represents a textarea tag within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Form_TextArea extends WrappingElement {
	private $text;
	
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * Note child content is automatically wrapped in an Inline_Text for security and convenience, so explicit encoding of HTML characters is not necessary.
	 * 
	 * @param string $value optional child content to use as the 'value' of the textarea
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($value = "", $id = null, $class = null) {
		parent::__construct("textarea", $this->text = new Inline_Text(), $id, $class);
		
		$this->add_param("name", $id);
		$this->COMPACT = true;
		
		$this->set_text($value);
	}

	/**
	 * Replaces the existing child content value with the specified new text.
	 * 
	 * @param string $text the new child content value
	 */
	public function set_text($text) {
		$this->text->replace($text);
	}
}
