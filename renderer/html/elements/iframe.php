<?php
/**
 * HTML element: Inline frame.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

/**
 * Represents an iframe tag within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class IFrame extends Element {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string $url the URL of the image
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($url, $id = null, $class = null) {
		parent::__construct("iframe", $id, $class);
		$this->add_param("src", $url);
	}

	/**
	 * Renders the element to stdout.
	 * 
	 * @return void
	 */
	public function render() {
		$this->open_element();
		$this->close_element();
	}
}
