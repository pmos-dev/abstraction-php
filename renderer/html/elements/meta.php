<?php
/**
 * HTML element: Meta.
 * 
 * @copyright 2016 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/element.php";

/**
 * Represents a meta tag within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.3.0
 */
class Meta extends Element {
	/**
	 * Constructs a new instance of this tag.
	 */
	public function __construct() {
		parent::__construct("meta");
	}

	/**
	 * @internal
	 */
		public function render() {
		$this->atom_element();
	}
}
