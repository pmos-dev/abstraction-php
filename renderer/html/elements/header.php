<?php
/**
 * HTML element: Header headings.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

/**
 * Represents a h# tag within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Header extends WrappingElement {
	const LEVEL_1 = 1;
	const LEVEL_2 = 2;
	const LEVEL_3 = 3;
	const LEVEL_4 = 4;
	const LEVEL_5 = 5;

	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $content the child content for this element
	 * @param int $level one of the possible header levels: LEVEL_1 through to LEVEL_5
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($content, $level, $id = null, $class = null) {
		switch ($level) {
			case self::LEVEL_1: break;
			case self::LEVEL_2: break;
			case self::LEVEL_3: break;
			case self::LEVEL_4: break;
			case self::LEVEL_5: break;
			default:
				throw new Exception("Bad header level: {$level}");	
		}

		parent::__construct("h{$level}", $content, $id, $class);
	}
}
