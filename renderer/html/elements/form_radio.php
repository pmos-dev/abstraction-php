<?php
/**
 * HTML element: Radio button.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/elements/form_input.php";

/**
 * Represents an input[type=radio] tag within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Form_Radio extends Form_Input {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param mixed $value the value for the value=<...>|"..." parameter
	 * @param boolean $checked whether this is selected (checked) within the group or not initially
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($value, $checked = false, $id = null, $class = null) {
		parent::__construct("radio", $id, $class);
		
		$this->add_param("value", $value);
		
		if ($checked) $this->add_atomic_param("checked");
	}
}
