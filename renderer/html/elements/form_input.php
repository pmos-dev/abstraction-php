<?php
/**
 * Storage of form-based input elements that don't wrap child content.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/element.php";

/**
 * Root representation class for all form-based tag elements that don't wrap child content.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Form_Input extends Element {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string $type the value for the type="..." parameter to define the input tag type
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($type, $id = null, $class = null) {
		parent::__construct("input", $id, $class);

		$this->add_param("type", $type);
		$this->add_param("name", $id);
	}

	/**
	 * @internal
	 */
		public function render() {
		$this->atom_element();
	}
}
