<?php
/**
 * HTML element: Hyperlink.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

/**
 * Represents an a tag within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Link extends WrappingElement {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $content the child content for this element
	 * @param string $href a value for the href="..." parameter
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($content, $href, $id = null, $class = null) {
		parent::__construct("a", $content, $id, $class);
		$this->add_param("href", $href);
	}
}
