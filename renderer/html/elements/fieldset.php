<?php
/**
 * HTML element: Fieldset grouping.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

/**
 * Represents a legend tag within the HTML abstraction.
 *
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class Legend extends WrappingElement {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $content the child content for this element
	 */
	public function __construct($content) {
		parent::__construct("legend", $content);
	}
}

/**
 * Represents a fieldset tag within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Fieldset extends WrappingElement {
	private $content;
	
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * Note, the legend tag is built automatically, so only the child content for it needs to be supplied.
	 * A MultiElements is created automatically within the fieldset, and can be added to directly.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $legend the child content for the legend tag within this fieldset
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($legend = null, $id = null, $class = null) {
		parent::__construct("fieldset", $this->content = new MultiElements(), $id, $class);
		
		if ($legend !== null) $this->content->add(new Legend($legend));
	}

	/**
	 * Adds a new piece of content to the fieldset.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $element the child content for the legend tag within this fieldset
	 */
	public function add($element) {
		$this->content->add($element);
	}
}
