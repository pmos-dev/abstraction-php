<?php
/**
 * HTML element: Ordered and unordered lists.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

use \Abstraction\Renderer as Renderer;

/**
 * Represents a dt tag within the HTML abstraction.
 *
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class DefinitionList_Title extends WrappingElement {
	/**
	 * Constructs a new instance of this tag.
	 *
	 * @param string|int|float|\Abstraction\Renderer\Renderable $value the child content for this list item
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($value, $id = null, $class = null) {
		if (is_string($value) || is_numeric($value)) $value = new Inline_Text($value);
		if (!($value instanceof Renderer\Renderable)) throw new Exception("Value does not implement the AbstractionRenderer_Renderable interface");
		
		parent::__construct("dt", $value, $id, $class);
		$this->COMPACT = true;
	}
}

/**
 * Represents a dt tag within the HTML abstraction.
 *
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class DefinitionList_Definition extends WrappingElement {
	/**
	 * Constructs a new instance of this tag.
	 *
	 * @param string|int|float|\Abstraction\Renderer\Renderable $value the child content for this list item
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($value, $id = null, $class = null) {
		if (is_string($value) || is_numeric($value)) $value = new Inline_Text($value);
		if (!($value instanceof Renderer\Renderable)) throw new Exception("Value does not implement the AbstractionRenderer_Renderable interface");
		
		parent::__construct("dd", $value, $id, $class);
		$this->COMPACT = true;
	}
}

/**
 * Represents a ul or ol tag within the HTML abstraction.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class DefinitionList extends WrappingElement {
	private $titles, $definitions, $multi;
	
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @throws Exception
	 */
	public function __construct($id = null, $class = null) {
		parent::__construct("dl", $this->multi = new MultiElements(), $id, $class);
		$this->multi->set_compact(false);
		$this->multi->set_indent(true);
		
		$this->items = array();
	}

	/**
	 * Directly adds a new item to the list.
	 * 
	 * This method is not used often; new_item is generally preferred, because it accepts a greater range of content.
	 * @param DefinitionList_Title $title the title of the definition item
	 * @param DefinitionList_Definition $definition the definition of the definition item
	 * @return void
	 */
	public function add_item(DefinitionList_Title $title, DefinitionList_Definition $definition) {
		$this->multi->add(new MultiElements(array($title, $definition)));
		$this->titles[] = $title;
		$this->definitions[] = $definition;
	}

	/**
	 * Builds a new list item using the specified content and adds it to the list.
	 * 
	 * Returns the newly constructed DefinitionList_Item so that classes can be added if desired.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $value the child content for this element
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @return void
	 */
	public function new_item($title, $definition, $id = null, $class = null) {
		$title = new DefinitionList_Title($title, $id, $class);
		$definition = new DefinitionList_Definition($definition, $id, $class);
		$this->add_item($title, $definition);
	}

	/**
	 * Returns the number of items in the list.
	 * 
	 * @return int
	 */
	public function sizeof() {
		return sizeof($this->titles);
	}

	/**
	 * Wipes the existing list items
	 *
	 * @return void
	 */
	public function clear() {
		$this->multi->clear();
		$this->titles = array();
		$this->definitions = array();
	}
}
