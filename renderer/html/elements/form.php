<?php
/**
 * HTML element: Form.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

/**
 * Represents an form tag within the HTML abstraction.
 * 
 * This class also inserts a Layout class for input items and a MultiElements for form buttons.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Form extends WrappingElement {
	const BUTTON_SUBMIT = "submit";
	const BUTTON_RESET = "reset";
	const BUTTON_OTHER = "";
	
	const SUBMIT_GET = "GET";
	const SUBMIT_POST = "POST";
	const SUBMIT_FILEPOST = "FILEPOST";

	private $items, $buttons;
	
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string $url the URL of the destination submission, stored as the value of action="..."
	 * @param string $method the form submission method: SUBMIT_GET, SUBMIT_POST or SUBMIT_FILEPOST
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @param bool $button_structure whether to include a button structure
	 * @throws Exception
	 */
	public function __construct($url, $method = self::SUBMIT_GET, $id = null, $class = null, $button_structure = true) {
		switch (strtoupper($method)) {
			case self::SUBMIT_GET:
			case self::SUBMIT_POST:
			case self::SUBMIT_FILEPOST:
				break;
			default:
				throw new Exception("Bad form method type: {$method}");
		}
		
		parent::__construct("form", $multi = new MultiElements(), $id, $class);

		$this->items = new Layout();
		$multi->add($this->items);
		
		$this->buttons = new MultiElements();
		if ($button_structure) {
			$multi->add(new Div($this->buttons, null, "form_buttons"));
			$this->buttons->add(new Label("", null, "button_indent"));
		}
		
		if (strtoupper($method) == self::SUBMIT_FILEPOST) {
			$this->add_param("enctype", "multipart/form-data");
			$method = self::SUBMIT_POST;
		}
		
		$this->add_param("method", strtoupper($method));
		$this->add_param("action", $url);
	}

	/**
	 * Adds a hidden value to the form.
	 * 
	 * @param string $name the id/name of the hidden tag
	 * @param mixed $value the value to store
	 * @return void
	 */
	public function add_hidden($name, $value) {
		$this->items->add(new Form_Hidden($value, $name));
	}

	/**
	 * Shorthand method to add a new input item row to the layout of this form.
	 * 
	 * Returns the div tag wrapping this row so that classes can be added if desired.
	 * 
	 * @see Layout::add_labelled_row($label, $content)
	 * @param string|int|float|\Abstraction\Renderer\Renderable $label the value to use for the row label
	 * @param string|int|float|\Abstraction\Renderer\Renderable $content the content for the row, usually a subclass of Form_Input
	 * @return Div
	 */
	public function add_row($label, $content, $for = null) {
		if ($for === null && gettype($content) === "object" && $content instanceof Element) {
			// attempt to derive $for
			$for = $content->get_id();
		}
		return $this->items->add_labelled_row($label, $content, null, "form_row", $for);
	}
	
	/**
	 * Adds child content directly to the layout of this form.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $content the content to add
	 * @return void
	 */
	public function add($content) {
		$this->items->add($content);
	}
	
	/**
	 * Shorthand method to add a new button to the bottom of this form.
	 * 
	 * @param string $caption the value to use for the button caption
	 * @param string $type the type of button to add: BUTTON_SUBMIT, BUTTON_RESET or BUTTON_OTHER
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @return void
	 */
	public function add_button($caption, $type = self::BUTTON_OTHER, $id = null, $class = null) {
		switch ($type) {
			case self::BUTTON_SUBMIT:
				$this->buttons->add(new Form_Submit($caption, $id, $class));
				break;

			case self::BUTTON_RESET:
				$this->buttons->add(new Form_Reset($caption, $id, $class));
				break;

			case self::BUTTON_OTHER:
				$this->buttons->add(new Form_Button($caption, $id, $class));
				break;
				
			default:
				throw new Exception("Unknown button type: {$type}");
		}
	}

	/**
	 * Shorthand method to add a submit button to the bottom of this form.
	 * 
	 * @param string $caption the value to use for the button caption
	 * @param string|NULL $id the value for the id="..." parameter, which is also used to generate the name="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 * @return void
	 */
	public function add_submit($caption, $id = null, $class = null) {
		$this->add_button($caption, self::BUTTON_SUBMIT, $id, $class);
	}
}
