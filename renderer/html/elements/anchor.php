<?php
/**
 * HTML element: Anchor.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

/**
 * Represents a non-hyperlink a tag anchor within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Anchor extends WrappingElement {
	/**
	 * Constructs a new instance of this tag.
	 * 
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct($id, $class = null) {
		parent::__construct("a", new Inline_Text(""), $id, $class);
		$this->add_param("name", $id);
	}
}
