<?php
/**
 * Element for storage of raw (non-processed) output.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTML;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";

use \Abstraction\Renderer as Renderer;

/**
 * Provides storage for raw output that should not be processed.
 * 
 * Most string content added to elements is automatically wrapped in an Inline_Text class, which in turn automatically converts various characters to their HTML equivalents.
 * This can cause problems if direct HTML needs to be rendered, as the < and > characters amongst others will be encoded to display.
 * This class allows storage of raw output against the Renderable interface, and should be used with caution for security reasons.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Raw implements Renderer\Renderable {	
	private $raw;
	
	/**
	 * Constructs a new instance
	 * 
	 * @param mixed|NULL $raw any initial content output to store
	 */
	public function __construct($raw = null) {
		$this->raw = $raw;
	}

	/**
	 * Replaces the current raw content.
	 * 
	 * @param mixed|NULL $raw the new content to output
	 * @return void
	 */
	public function replace($raw = null) {
		$this->raw = $raw;
	}

	/**
	 * Returns the current raw content stored.
	 * 
	 * @return mixed|NULL
	 */
	public function get_raw() {
		return $this->raw;
	}
	
	/**
	 * Renders the stored raw content directly to stdout without processing.
	 * 
	 * @return void
	 */
	public function render() {
		echo $this->raw;
	}
}
