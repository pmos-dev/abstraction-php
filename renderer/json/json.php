<?php
/**
 * Rendering abstraction for JSON output.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\JSON;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";

use \Abstraction\Renderer as Renderer;

/**
 * @internal
 */
class Exception extends Renderer\Exception {
	public function __construct($message, $value = "\t_NO_VALUE", $extra = null) {
		parent::__construct($message, $value, $extra);
	}
}

/**
 * Rendering engine for JSON structured output.
 * 
 * This is mainly used by AJAX implementations.
 * 
 * The result array structure takes the format:
 * array("outcome" => "success", "result" => ...) or array("outcome" => "error", "message" => "...")
 * AJAX clients can therefore check for the value of result->outcome in jQuery to determine whether the request/operation succeeded or not.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class JSON implements Renderer\Engine {
	protected $data;
	
	/**
	 * Constructs a new instance of this engine.
	 */
	public function __construct() {
		$this->data = null;
	}
	
	/**
	 * Permits HTTP cross domain usage.
	 */
	public function enable_origin() {
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
		header("Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST, PUT, PATCH, DELETE");
	}
	
	public function disable_caching() {
		header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	}
	
	/**
	 * Encodes an error message into a result array structure, writes the JSON encoded result to stdout to indicate the request or operation failed, and then terminates script execution.
	 * 
	 * <b>Subclasses should ensure this method does not accidently return.</b>
	 *
	 * @param string $message an optional error message to encode
	 * @return void
	 */
	public function error($message = "(unspecified error)") {
		$this->data = array(
			"outcome" => "error",
			"message" => $message
		);
		$this->complete();
	}
	
	/**
	 * Encodes the given data into a result array structure, writes the JSON encoded result to stdout, and then terminates script execution.
	 * 
	 * <b>Subclasses should ensure this method does not accidently return.</b>
	 *
	 * @param mixed $result the data to encode
	 * @return void
	 */
	public function success($result = "") {
		$this->data = array(
			"outcome" => "success",
			"result" => $result
		);
		$this->complete();
	}

	/**
	 * Writes the given data to JSON output on stdout without first encoding it into a result array structure, and then terminates script execution.
	 * 
	 * <b>Subclasses should ensure this method does not accidently return.</b>
	 *
	 * @param mixed $result
	 * @return void
	 */
	public function raw($result) {
		$this->data = $result;
		$this->complete();
	}

	private static function recurse_utf8($node) {
		if (is_string($node) && !preg_match("`^.*$`u", $node)) return utf8_encode($node);

		if (is_array($node)) {
			$rebuild = array();
			
			if (array_diff_key($node, array_keys(array_keys($node)))) {
				foreach ($node as $key => $value) $rebuild[self::recurse_utf8($key)] = self::recurse_utf8($value);
			} else {
				foreach ($node as $value) $rebuild[] = self::recurse_utf8($value);
			}

			return $rebuild;
		}
		
		return $node;
	}
	
	/**
	 * Renders the stored data as JSON to stdout, and then terminates script execution.
	 * 
	 * This method shouldn't be called directly, as the return data will not have been set. Instead use success(), raw() or error()
	 * 
	 * <b>Subclasses should ensure this method does not accidently return.</b>
	 *
	 * @internal
	 * @return void
	 */
	public function complete() {
		header("Content-type: application/json");
		echo json_encode(self::recurse_utf8($this->data));
		flush();

		die();
	}
}
