<?php
/**
 * Rendering abstraction for JSONP output.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\JSON;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/json/json.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";

use \Abstraction\Data as Data;

/**
 * Rendering engine for JSONP structured output.
 * 
 * This is used by AJAX implementations requiring cross-domain support.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class JSONP extends JSON {
	private $callback;
	
	/**
	 * Constructs a new instance of this engine.
	 * 
	 * @param string $callback the callback ID for the JSONP call
	 */
	public function __construct($callback) {
		parent::__construct();

		if (!Data\Data::validate_regex("`^[a-z0-9_]{1,128}$`iSD", $callback)) throw new Exception("JSONP callback contains potentially dangerous characters.");
		$this->callback = $callback;
	}
	
	/**
	 * Renders the stored data as JSONP to stdout using the constructor specified callback, and then terminates script execution.
	 * 
	 * This method shouldn't be called directly, as the return data will not have been set. Instead use success(), raw() or error()
	 * 
	 * <b>Subclasses should ensure this method does not accidently return.</b>
	 *
	 * @internal
	 * @return void
	 */
	public function complete() {
		echo $this->callback . "(" . json_encode($this->data) . ")";
		flush();

		die();
	}
}
