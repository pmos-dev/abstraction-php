<?php
/**
 * Rendering abstraction for HTML output, with in-built jQuery support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTMLJQuery;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/html.php";

use \Abstraction\Renderer as Renderer;

/**
 * @internal
 */
class Exception extends Renderer\Exception {}

// import tabs element
require_once ABSTRACTION_ROOT_PATH . "renderer/htmljquery/uielements/tabs.php";

/**
 * Rendering engine for HTML output, with in-built jQuery support.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class HTML extends Renderer\HTML\HTML {
	const TYPE_JQUERY = "jquery";
	
	/**
	 * Constructs a new instance of this engine.
	 */
	public function __construct() {
		parent::__construct();
		$this->codes["jquery"] = array();
	}

	/**
	 * Adds a file to be included by the HTML code.
	 * 
	 * @param string $type either TYPE_CSS, TYPE_JAVASCRIPT or TYPE_JQUERY (which is handled the same as TYPE_JAVASCRIPT)
	 * @param string $filename the URL to the file
	 * @throws Exception
	 * @return void
	 */
	public function add_include($type, $filename) {
		switch($type) {
			case self::TYPE_JQUERY:
				parent::add_include(self::TYPE_JAVASCRIPT, $filename);
				break;
			case self::TYPE_CSS:
			case self::TYPE_JAVASCRIPT:
				parent::add_include($type, $filename);
				break;
			default: throw new Exception("Unknown include type", $type);
		}
	}

	/**
	 * Adds a block of inline code to be rendered in the HTML head tag region.
	 * 
	 * jQuery code is automatically wrapped in $(document).ready(function() { ... });
	 * 
	 * @param string $type either TYPE_CSS, TYPE_JAVASCRIPT or TYPE_JQUERY
	 * @param string $code the inline code
	 * @throws Exception
	 * @return void
	 */
	public function add_code($type, $code) {
		switch($type) {
			case self::TYPE_JQUERY:
				$this->codes[self::TYPE_JQUERY][] = $code;
				break;
			case self::TYPE_CSS:
			case self::TYPE_JAVASCRIPT:
				parent::add_code($type, $code);
				break;
			default: throw new Exception("Unknown code type", $type);
		}
	}

	/**
	 * Opens the HTML document, rendering to stdout.
	 * 
	 * The doctype is written, alongside the html tag itself. Then the head tag is written, with any includes for styles and scripts, any inline code, and finally the title tag.
	 * This method is invoked by methods such as render() and not intended for direct usage.
	 * 
	 * @internal
	 * @return void
	 */
	function open() {
		if (sizeof($this->codes[self::TYPE_JQUERY]) > 0) {
			$code = implode(self::SCRIPT_BREAK_DIVIDER, $this->codes[self::TYPE_JQUERY]);

$jquery = <<<JQUERY
$(document).ready(function() {

{$code}

});

JQUERY;

			$this->add_code(self::TYPE_JAVASCRIPT, $jquery);
		}
		parent::open();
	}
}
