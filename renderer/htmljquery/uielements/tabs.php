<?php
/**
 * HTML custom structure: jQueryUI tab support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\HTMLJQuery\UI;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/elements/div.php";

use \Abstraction\Renderer as Renderer;
use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Renderer\HTMLJQuery as HTMLJQuery;

/**
 * Builds a structure for representing jQueryUI tabs.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Tabs extends HTML\Div {
	private $list;
	private $body;
	
	private $pre_id;
	
	/**
	 * Constructs a new instance of this component.
	 * 
	 * This component is two part, consisting of a List for the tabs and a body for the content. 
	 * 
	 * The class 'jqueryuitabs' is added to the compoments automatically.
	 * 
	 * @param HTMLJQuery\HTML $html a reference to the parent HTML component, which will need to have jQuery code added to invoke the tabs
	 * @param string|NULL $id the value used to identify the tab set in Javascript and the DOM
	 * @param HTML\Classes|string|NULL $class an optional set of classes for this element
	 * @throws HTMLJQuery\Exception
	 * @throws HTML\Exception
	 * @return void
	 */
	public function __construct(HTMLJQuery\HTML $html, $id, $class = null) {
		if ($id === null) throw new HTMLJQuery\Exception("Tabs must have an ID");

		if ($class === null) $class = new HTML\Classes();
		else if (is_string($class)) $class = new HTML\Classes($class);
		if (!($class instanceof HTML\Classes)) throw new HTML\Exception("Class is not an instanceof Classes");
		
		$class->add("jqueryuitabs");
		
		$this->list = new HTML\ItemList(HTML\ItemList::UNORDERED);
		$this->body = new HTML\MultiElements();
		
		$multi = new HTML\MultiElements();
		$multi->add($this->list);
		$multi->add($this->body);
		
		parent::__construct($multi, $id, $class);
		
		$this->pre_id = $id;
		
		$html->add_code("jquery", <<<JQUERY
		
$("div#{$id}").tabs();
		
JQUERY
		);
	}
	
	/**
	 * Adds a new tab to this set, with the given name and content.
	 * 
	 * @param string|int|float|\Abstraction\Renderer\Renderable $name the name of the tab
	 * @param Renderer\Renderable $content the content for the tab page, usually an instance of MultiElements
	 * @param string|NULL $id an optional identifier for this tab, or null to make one using the tab index
	 * @return void
	 */
	public function add_tab($name, Renderer\Renderable $content, $id = null) {
		$index = $this->list->sizeof() + 1;
		
		if ($id === null) $id = $index;

		$this->list->new_item(new HTML\Link(new HTML\Span($name, "{$this->pre_id}_tab_{$id}_innertext"), "#{$this->pre_id}_tab_{$id}"));
		$this->body->add(new HTML\Div($content, "{$this->pre_id}_tab_{$id}"));
	}
}
