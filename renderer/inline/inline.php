<?php
/**
 * Rendering abstraction for inline output.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer\Inline;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";

use \Abstraction\Renderer as Renderer;

/**
 * @internal
 */
class Exception extends Renderer\Exception {}

/**
 * Rendering engine for inline output.
 * 
 * This is mainly used by 'live' implementations of error messages.
 * 
 * Only error reporting is supported.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Inline implements Renderer\Engine {
	/**
	 * Constructs a new instance of this engine.
	 */
	public function __construct() {
	}
	
	public function error($message = "(unspecified error)") {
		die($message);
	}
	
	public function complete() {
		die();
	}
}
