<?php
/**
 * Rendering (output) abstraction.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Renderer;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "config.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction as Abstraction;

/**
 * @internal
 */
class Exception extends Abstraction\Exception {}

/**
 * Defines an implementing class as being 'renderable'.
 * 
 * @author Pete Morris
 * @version 1.2.0
*/
interface Renderable {
	/**
	 * Renders to stdout.
	 * 
	 * @return void
	 */
	function render();
}

/**
 * Root methods required for a rendering engine.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
interface Engine {
	/**
	 * Renders the specific error message to stdout and then terminates script execution.
	 * 
	 * Implementing classes should use die() or similar to terminate execution; <b>this method must never return.</b>
	 * 
	 * @param string $message an optional error message
	 * @return void
	 */
	public function error($message = "(unspecified error)");
	
	/**
	 * Renders everything to stdout and then terminates script execution.
	 * 
	 * Implementing classes should use die() or similar to terminate execution; <b>this method must never return.</b>
	 * 
	 * @return void
	 */
	public function complete();
}
