<?php
/**
 * Atomic and pseudo-atomic IO filesystem operations.
 *
 * @copyright 2017 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\IO;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction as Abstraction;
use \Abstraction\Data as Data;

/**
 * @internal
 */
class Atomic_Exception extends Abstraction\Exception {}

/**
 * IO operations for atomic style filesystem writing. All methods are static.
 *
 * @api
 * @author Pete Morris
 * @version 1.0
 */
class Atomic {
	/**
	 * Performs a file_put_contents atomically, by first writing to a temporary file, and then renaming that file to the original requested filename.
	 * 
	 * @param string $filename the intended original filename.
	 * @param mixed $data the data to be written.
	 * @param string $tmp_ext an optional extension for the temporary file.
	 * @return int|false the number of bytes written, or false if an error occurred.
	 */
	public static function file_put_contents($filename, $data, $tmp_ext =  "~atomic-write") {
		if (false === ($written = file_put_contents("{$filename}{$tmp_ext}", $data, LOCK_EX))) return false;
		return rename("{$filename}{$tmp_ext}", $filename) ? $written : false;
	}
	
	/**
	 * Performs a scandir ignoring files being atomically created.
	 * 
	 * @param string $path the path to scan
	 * @param string $tmp_ext an optional extension for the temporary file.
	 * @param boolean $include_dots true to include the . and .. files, false to ignore them.
	 * @return string[]|false an array of files that aren't being atomically created at the time of the scan, or false if an error occurred.
	 */
	public static function scandir($path, $tmp_ext =  "~atomic-write", $include_dots = false) {
		if (false === ($scan = scandir($path))) return false;
		
		$files = array();
		foreach ($scan as $file) {
			if (!$include_dots && ($file === "." || $file === "..")) continue;
			if (Data\Data::ends_with($file, $tmp_ext, false)) continue;
			
			$files[] = $file;
		}
		
		return $files;
	}

	/**
	 * Generates a unique temporary filename.
	 * 
	 * NB, unlike PHP tempnam, the file is not touch created.
	 * 
	 * @param string $path the path to the directory to that will contain the temporary file.
	 * @param string $prefix any prefix to use for the file name.
	 * @param string $ext the extension suffix to use for the file name.
	 * @param string $tmp_ext the optional extension for files currently being atomically created.
	 * @return string the unique filename (with path)
	 * @throws Atomic_Exception if a unique name cannot be generated in 999 attempts, which is highly unlikely.
	 */
	public static function tempnam($path, $prefix = "", $ext = ".tmp", $tmp_ext =  "~atomic-write") {
		$filename = null;
		$path = Data\Data::enslash_path($path);
		
		$tmp_hash_prefix = $path . "_" . microtime();
		for ($i = 999; $i-- > 0;) {
			$hash = md5("{$tmp_hash_prefix}_{$i}_" . rand() . "_" . rand());
			$attempt = "{$prefix}{$hash}{$ext}";
			
			// check the potential tmp_ext first, as the order for atomic naming would be 1) tmp_ext, 2) normal
			if (file_exists("{$path}{$attempt}{$tmp_ext}")) continue;
			if (file_exists("{{$path}{$attempt}")) continue;

			$filename = $attempt;
			break;
		}
		
		if ($filename === null) throw new Atomic_Exception("Unable to generate unique temp file name in 999 iterations. This is highly unlikely, so may be an error elsewhere?");
		return "{$path}{$filename}";
	}

	/**
	 * Performs a file_put_contents atomically, using a generated temporary file name.
	 * 
	 * @param mixed $data the data to be written.
	 * @param string $path the path to the directory to that will contain the temporary file.
	 * @param string $prefix any prefix to use for the file name.
	 * @param string $ext the extension suffix to use for the file name.
	 * @param string $tmp_ext the optional extension for files currently being atomically created.
	 * @return string|false the filename (with path) of the new file, or false on failure.
	 */
	public static function tempfile_put_contents($data, $path, $prefix = "", $ext = ".tmp", $tmp_ext =  "~atomic-write") {
		$filename = self::tempnam($path, $prefix, $ext, $tmp_ext);
		
		if (false === self::file_put_contents($filename, $data, $tmp_ext)) return false;
		
		return $filename;
	}

	/**
	 * Gets the content from a file atomically, by first renaming it, then getting it, then renaming it back.
	 * 
	 * @param string $filename the filename.
	 * @param string $tmp_ext an optional extension for the temporary file.
	 * @return string|false the contents of the file, or false on error.
	 */
	public static function file_get_contents($filename, $tmp_ext =  "~atomic-write") {
		if (!rename($filename, "{$filename}{$tmp_ext}")) return false;
		$data = file_get_contents("{$filename}{$tmp_ext}"); // ignore false as we need to rename back
		@rename("{$filename}{$tmp_ext}", $filename);
		
		return $data;
	}
	
	/**
	 * Gets the content from a file atomically, and deletes it immediately afterwards.
	 *
	 * @param string $filename the filename.
	 * @param string $tmp_ext an optional extension for the temporary file.
	 * @return string|false the contents of the file, or false on error.
	 */
	public static function file_get_contents_and_delete($filename, $tmp_ext =  "~atomic-write") {
		if (!rename($filename, "{$filename}{$tmp_ext}")) return false;
		$data = file_get_contents("{$filename}{$tmp_ext}"); // ignore false as we need to rename back
		@unlink("{$filename}{$tmp_ext}");
		
		return $data;
	}
}
