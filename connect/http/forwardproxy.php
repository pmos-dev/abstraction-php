<?php
/**
 * HTTP fetch proxy.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Connect\HTTP;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "config.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "connect/http/direct.php";

use \Abstraction\Data as Data;

/**
 * @internal
 */
class ForwardProxy_Exception extends Direct_Exception {}

/**
 * Forward proxy class using cURL to fetch resources.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class ForwardProxy extends Direct {
	const REGEX_PATTERN_PROXYADDR = "`^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}:[0-9]{1,6}$`iSD";
	
	/**
	 * Constructs a new instance of this forward proxy.
	 * 
	 * @param string $proxyaddr the address of the proxy server to use; formatted [ip]:[port], e.g. 192.168.0.1:1234
	 */
	public function __construct($proxyaddr) {
		if (!Data\Data::validate_regex(self::REGEX_PATTERN_PROXYADDR, $proxyaddr)) throw new ForwardProxy_Exception("Bad proxy address supplied.");

		parent::__construct();

		curl_setopt($this->curl, CURLOPT_HTTPPROXYTUNNEL, true);
		curl_setopt($this->curl, CURLOPT_PROXY, $proxyaddr);
	}

	/**
	 * Static implementation of the forward proxy, constructing, fetching and closing automatically.
	 * 
	 * @param string $url the URL to fetch
	 * @param string $proxyaddr the address of the proxy server to use; formatted [ip]:[port], e.g. 192.168.0.1:1234
	 * @return string|NULL the fetched data, or null on failure
	 */
	public static function get_via_proxy($url, $proxyaddr) {
		$proxy = new ForwardProxy($proxyaddr);
		$data = $proxy->get($url);
		$proxy->close();
		
		return $data;
	}
}
