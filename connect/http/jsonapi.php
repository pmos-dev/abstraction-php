<?php
/**
 * HTTP fetch proxy.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Connect\HTTP;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "config.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "connect/http/direct.php";

use \Abstraction\Data as Data;

/**
 * @internal
 */
class JSONAPI_Exception extends Direct_Exception {
	public function __construct($message, $value = "\t_NO_VALUE", $extra = null) {
		parent::__construct($message, $value, $extra);
	}
}

/**
 * @internal
 */
class JSONAPI_ErrorOutcome_Exception extends JSONAPI_Exception {
	public function __construct($message, $extra = null) {
		parent::__construct("JSON outcome was error", $message, $extra);
	}
}

/**
 * Forward proxy class using cURL to fetch resources.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class JSONAPI extends Direct {
	private $rootAPIURL;
	
	/**
	 * Constructs a new instance of this forward proxy.
	 * 
	 * @param string $proxyaddr the address of the proxy server to use; formatted [ip]:[port], e.g. 192.168.0.1:1234
	 */
	public function __construct($rootAPIURL) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_URL, $rootAPIURL)) throw new JSONAPI_Exception("Bad root API URL supplied.");
		if (!preg_match("`/$`", $rootAPIURL)) throw new JSONAPI_Exception("The root URL must end with a trailing slash");

		$this->rootAPIURL = $rootAPIURL;
		
		parent::__construct();
	}

	/**
	 * Call an API method, and return any result obtained.
	 * 
	 * The call results is returned in JSON format, with a success or failure outcome flag.
	 * This is used to determine whether the call succeeded or not. Only successful outcomes
	 * are returned with their decoded data. Error outcomes throw an exception.
	 * 
	 * @param string $method the method name, which often corresponds to a script filename.
	 * @param array $params any optional parameters to send to the call.
	 * @return mixed the result resulted from the call.
	 * @throws JSONAPI_ErrorOutcome_Exception if the call returned an error outcome.
	 * @throws JSONAPI_Exception if the call return value wasn't valid JSON.
	 */
	public function call($method, array $params = array()) {
		$encoded = http_build_query($params);
		if ($encoded !== "") $encoded = "?{$encoded}";
		
		$result = $this->get_json("{$this->rootAPIURL}{$method}.php{$encoded}");
		
		if ($result === null) throw new JSONAPI_Exception("The returned API response was not valid JSON");
		if (!array_key_exists("outcome", $result)) throw new JSONAPI_Exception("The returned API response did not contain an outcome");
		if ($result["outcome"] === "error") throw new JSONAPI_Exception("An error occurred whilst processing the API call: {$result["message"]}");
		
		return $result["result"];
	}
}
