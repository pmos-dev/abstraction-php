<?php
/**
 * HTTP direct URL fetching.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Connect\HTTP;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "config.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction as Abstraction;

/**
 * @internal
 */
class Direct_Exception extends Abstraction\Exception {}

/**
 * URL fetch class using direct cURL to fetch resources.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Direct {
	protected $curl;
	
	private $headers;
	
	/**
	 * Constructs a new instance of this proxy.
	 */
	public function __construct() {
		$this->curl = curl_init();

		curl_setopt($this->curl, CURLOPT_HEADER, false);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, 10);
		
		$this->headers = array();
	}
	
	/**
	 * Allows direct setting of CURL options.
	 * 
	 * @param int $key the CURL_* key
	 * @param mixed $value the value to set
	 * @return void
	 */
	public function set_option($key, $value) {
		curl_setopt($this->curl, $key, $value);
	}
	
	public function add_header($header) {
		$this->headers[] = $header;
	}
	
	/**
	 * Fetches the specified URL.
	 * 
	 * @param string $url the URL to fetch
	 * @return string|NULL the fetched data, or null on failure
	 */
	public function get($url) {
		curl_setopt($this->curl, CURLOPT_URL, $url);
		if (sizeof($this->headers) > 0) curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->headers);
		
		if (false === ($result = @curl_exec($this->curl))) return null;
		
		return $result;
	}

	/**
	 * Returns the HTTP response code from the last cURL call.
	 * @return int the response code.
	 */
	public function get_response_code() {
		return curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
	}
	
	/**
	 * Fetches the specified URL and attempts to decode it as JSON.
	 * 
	 * @param string $url the URL to fetch
	 * @return mixed|NULL the fetched data as JSON, or null on failure
	 */
	public function get_json($url) {
		if (null === ($result = $this->get($url))) return null;
		
		return json_decode($result, true);
	}
	
	/**
	 * Posts to the specified URL.
	 * 
	 * @param string $url the URL to fetch
	 * @param array $params an associated array of parameters to send
	 * @return string|NULL the fetched data, or null on failure
	 */
	public function post($url, array $params) {
		curl_setopt($this->curl, CURLOPT_URL, $url);
		if (sizeof($this->headers) > 0) curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($this->curl, CURLOPT_POST, sizeof(array_keys($params)));
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $params);
		
		if (false === ($result = @curl_exec($this->curl))) return null;
		
		return $result;
	}
	
	/**
	 * Posts to the specified URL.
	 *
	 * @param string $url the URL to fetch
	 * @param mixed $data the data to send, in any format
	 * @return string|NULL the fetched data, or null on failure
	 */
	public function postJson($url, $data) {
		$json = json_encode($data);
		
		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $json);
		
		$custom_headers = array(
				"Content-Type: application/json",
				"Content-Length: " . strlen($json)
		);
		foreach ($this->headers as $header) $custom_headers[] = $header;
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $custom_headers);
		
		if (false === ($result = @curl_exec($this->curl))) return null;
		
		return $result;
	}
	
	/**
	 * PUTs to the specified URL.
	 *
	 * @param string $url the URL to fetch
	 * @param array $data an associated array of values to send
	 * @return string|NULL the fetched data, or null on failure
	 */
	public function put($url, array $data) {
		// POST is handled by PHP automatically, but PUT/PATCH are not and are manually parsed by REST\Data.php
		// This can't cope with PHP's Content-Disposition approach, so have to encode them manually as URL encoded here.
		
		$built = http_build_query($data);
		
		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $built);
		
		$custom_headers = array(
				"Content-Type: application/x-www-form-urlencoded",
				"Content-Length: " . strlen($built)
		);
		foreach ($this->headers as $header) $custom_headers[] = $header;
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $custom_headers);
		
		if (false === ($result = @curl_exec($this->curl))) return null;
		
		return $result;
	}
	
	/**
	 * PUTs to the specified URL.
	 *
	 * @param string $url the URL to fetch
	 * @param mixed $data the data to send, in any format
	 * @return string|NULL the fetched data, or null on failure
	 */
	public function putJson($url, $data) {
		$json = json_encode($data);
		
		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $json);
		
		$custom_headers = array(
				"Content-Type: application/json",
				"Content-Length: " . strlen($json)
		);
		foreach ($this->headers as $header) $custom_headers[] = $header;
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $custom_headers);
		
		if (false === ($result = @curl_exec($this->curl))) return null;
		
		return $result;
	}
	
	/**
	 * PATCHs to the specified URL.
	 *
	 * @param string $url the URL to fetch
	 * @param array $params an associated array of parameters to send
	 * @return string|NULL the fetched data, or null on failure
	 */
	public function patch($url, array $data) {
		// POST is handled by PHP automatically, but PUT/PATCH are not and are manually parsed by REST\Data.php
		// This can't cope with PHP's Content-Disposition approach, so have to encode them manually as URL encoded here.
		
		$built = http_build_query($data);
		
		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "PATCH");
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $built);
		
		$custom_headers = array(
				"Content-Type: application/x-www-form-urlencoded",
				"Content-Length: " . strlen($built)
		);
		foreach ($this->headers as $header) $custom_headers[] = $header;
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $custom_headers);
		
		if (false === ($result = @curl_exec($this->curl))) return null;
		
		return $result;
	}
	
	/**
	 * PATCHs to the specified URL.
	 *
	 * @param string $url the URL to fetch
	 * @param mixed $data the data to send, in any format
	 * @return string|NULL the fetched data, or null on failure
	 */
	public function patchJson($url, $data) {
		$json = json_encode($data);
		
		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "PATCH");
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $json);
		
		$custom_headers = array(
				"Content-Type: application/json",
				"Content-Length: " . strlen($json)
		);
		foreach ($this->headers as $header) $custom_headers[] = $header;
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $custom_headers);
		
		if (false === ($result = @curl_exec($this->curl))) return null;
		
		return $result;
	}
	
	/**
	 * Fetches the specified URL.
	 *
	 * @param string $url the URL to fetch
	 * @return string|NULL the fetched data, or null on failure
	 */
	public function head($url) {
		curl_setopt($this->curl, CURLOPT_URL, $url);
		if (sizeof($this->headers) > 0) curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->headers);
		
		curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "HEAD");
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 0);
		
		if (false === @curl_exec($this->curl)) return false;
		return true;
	}
	
	/**
	 * Fetches the specified URL.
	 *
	 * @param string $url the URL to fetch
	 * @return string|NULL the fetched data, or null on failure
	 */
	public function delete($url) {
		curl_setopt($this->curl, CURLOPT_URL, $url);
		if (sizeof($this->headers) > 0) curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->headers);
		
		curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		
		if (false === ($result = @curl_exec($this->curl))) return null;
		
		return $result;
	}
	
	/**
	 * Fetches the specified URL and attempts to decode it as JSON.
	 *
	 * @param string $url the URL to fetch
	 * @return mixed|NULL the fetched data as JSON, or null on failure
	 */
	public function delete_json($url) {
		if (null === ($result = $this->delete($url))) return null;
		
		return json_decode($result, true);
	}
	
	/**
	 * Closes the active connection.
	 * 
	 * @return void
	 */
	public function close() {
		curl_close($this->curl);
	}
}
