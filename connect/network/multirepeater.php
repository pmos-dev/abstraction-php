<?php
/**
 * NetworkRepeater broadcasting.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Connect\Network;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "config.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

require_once ABSTRACTION_ROOT_PATH . "connect/network/repeater.php";

use \Abstraction\Data as Data;

/**
 * Multiple repeater servers.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class MultiRepeater {
	private $repeaters;
	private $channel;
	
	/**
	 * Constructs a new instance of this multi-injector.
	 * 
	 * @param string $urls the URLs of the repeaters
	 * @param string $channel the channel to broadcast on
	 * @param int $timeout_seconds the number of seconds to wait before giving up
	 */
	public function __construct(array $urls, $key, $channel, $timeout_seconds = 10) {
		$this->repeaters = array();
		foreach ($urls as $url) $this->repeaters[] = new Repeater($url, $key, $channel, $timeout_seconds);
		
		$this->channel = $channel;
	}
	
	public function get_channel() {
		return $this->channel;
	}
	
	/**
	 * Broadcasts the specified data to the repeater array.
	 * 
	 * A single packet ID is automatically generated and reused for each injector to stop peer share duplication.
	 * 
	 * @param string $ns the namespace of the command
	 * @param string $command the command to broadcast
	 * @param string $data the data payload for the packet
	 * @return number the number of successful broadcasts made, if any
	 */
	public function broadcast($ns, $command, $data) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $ns)) throw new Repeater_Exception("Invalid ns");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $command)) throw new Repeater_Exception("Invalid command");
		
		$packet = array(
				"id" => Repeater::generate_packet_id(),
				"method" => "broadcast",
				"channel" => $this->get_channel(),
				"source" => "00000000",
				"timestamp" => date("Y-m-d H:i:s"),
				"ns" => $ns,
				"command" => $command,
				"data" => $data
		);

		$any_succeeded = 0;
		foreach ($this->repeaters as $repeater) {
			try {
				$repeater->raw($packet);
				$any_succeeded++;
			// @phpcs:ignore Generic.CodeAnalysis.EmptyStatement
			} catch(\Exception $ex) {}
		}
		
		return $any_succeeded;
	}
	
	/**
	 * Directs the specified data to the repeater.
	 *
	 * @param string $destination the destination device ID for the packet
	 * @param string $ns the namespace of the command
	 * @param string $command the command to direct
	 * @param string $data the data payload for the packet
	 * @return number the number of successful broadcasts made, if any
	 */
	public function direct($destination, $ns, $command, $data) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $destination)) throw new Repeater_Exception("Invalid destination device ID");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $ns)) throw new Repeater_Exception("Invalid ns");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $command)) throw new Repeater_Exception("Invalid command");
		
		$packet = array(
				"id" => Repeater::generate_packet_id(),
				"method" => "direct",
				"channel" => $this->get_channel(),
				"source" => "00000000",
				"destination" => $destination,
				"timestamp" => date("Y-m-d H:i:s"),
				"ns" => $ns,
				"command" => $command,
				"data" => $data
		);
		
		$any_succeeded = 0;
		foreach ($this->repeaters as $repeater) {
			try {
				$repeater->raw($packet);
				$any_succeeded++;
			// @phpcs:ignore Generic.CodeAnalysis.EmptyStatement
			} catch(\Exception $ex) {}
		}
		
		return $any_succeeded;
	}
}
