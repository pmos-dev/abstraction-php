<?php
/**
 * NetworkRepeater broadcasting.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Connect\Network;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "config.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

require_once ABSTRACTION_ROOT_PATH . "connect/http/direct.php";

use \Abstraction as Abstraction;
use \Abstraction\Data as Data;
use \Abstraction\Connect\HTTP as HTTP;

/**
 * @internal
 */
class Repeater_Exception extends Abstraction\Exception {}

/**
 * Base class for Node.js Network Repeater injection.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Repeater {
	protected $http;
	protected $url;
	protected $channel;
	
	/**
	 * Constructs a new instance of this injector.
	 * 
	 * @param string $url the URL of the repeater
	 * @param string $channel the channel to broadcast on
	 * @param int $timeout_seconds the number of seconds to wait before giving up
	 */
	public function __construct($url, $key, $channel, $timeout_seconds = 10) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_URL, $url)) throw new Repeater_Exception("Invalid URL");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $key)) throw new Repeater_Exception("Invalid key ID");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $channel)) throw new Repeater_Exception("Invalid channel ID");
		
		$this->http = new HTTP\Direct();
		$this->http->set_option(CURLOPT_CONNECTTIMEOUT, $timeout_seconds);
		$this->http->set_option(CURLOPT_TIMEOUT, $timeout_seconds);
		$this->http->add_header("Authorization: key ${key}");
		
		if (!preg_match("`/$`", $url)) $url = "${url}/";
		$this->url = $url;
		
		$this->channel = $channel;
	}
	
	public function get_channel() {
		return $this->channel;
	}
	
	/**
	 * Broadcasts the specified data to the repeater.
	 * 
	 * @param string $ns the namespace of the command
	 * @param string $command the command to broadcast
	 * @param string $data the data payload for the packet
	 * @return boolean true on success
	 */
	public function broadcast($ns, $command, $data) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $ns)) throw new Repeater_Exception("Invalid ns");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $command)) throw new Repeater_Exception("Invalid command");
		
		$this->http->postJson("{$this->url}broadcast/{$this->channel}/{$ns}/{$command}", $data);
		
		return true;
	}
	
	/**
	 * Directs the specified data to the repeater.
	 *
	 * @param string $destination the destination device ID for the packet
	 * @param string $ns the namespace of the command
	 * @param string $command the command to direct
	 * @param string $data the data payload for the packet
	 * @return boolean true on success
	 */
	public function direct($destination, $ns, $command, $data) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $destination)) throw new Repeater_Exception("Invalid destination device ID");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $ns)) throw new Repeater_Exception("Invalid ns");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $command)) throw new Repeater_Exception("Invalid command");
		
		$this->http->postJson("{$this->url}direct/{$this->channel}/${destination}/{$ns}/{$command}", $data);
		
		return true;
	}

	/**
	 * Generates a random packet ID (base 62) so that multiple raw packets can be constructed with the same ID to prevent duplicates across multiple repeaters sharing.
	 * 
	 * @return string
	 */
	public static function generate_packet_id() {
		return Data\Base62::generate_random_base62_id();
	}
	
	/**
	 * Pushes a raw packet onto the repeater without any checking.
	 *
	 * @return boolean true on success
	 */
	public function raw(array $packet) {
		$this->http->postJson("{$this->url}raw", $packet);
		
		return true;
	}

	/**
	 * Closes the HTTP connection underpinning the repeater REST
	 */
	public function close() {
		$this->http->close();
	}
}
