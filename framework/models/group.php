<?php
/**
 * Group modelling within the Abstraction base framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Framework\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/secondclass.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

/**
 * @internal
 */
class Group_Exception extends Models\Exception {}

/**
 * Defines a root model for groups.
 * 
 * Note this is a skeleton model only, so only the bare aspects needed to support the concept of groups is defined. In particular, content aspects such as group names are not present.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Group extends Models\SecondClass {
	const NORMAL = "normal";
	const EVERYONE = "everyone";
	const SYSTEM = "system";
	
	private $instance;

	/**
	 * Constructs a new instance of this model.
	 * 
	 * The instance and type fields are implicit to the model and should not be defined explicitly by subclasses.
	 * Similarly, foreign key mappings to instance are defined implicitly and should not be defined explicitly by subclasses.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Instance $instance the Instance model to associate with
	 * @param Database\Type[] $structure an associated array to define any further content within the model structure, taking the format field name => field type object
	 * @param Models\ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param Models\UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @throws Group_Exception
	 */
	public function __construct(Database\Wrapper $database, $table, Instance $instance, array $structure, array $foreign_keys = array(), array $unique_keys = array()) {
		if (array_key_exists("type", $structure)) throw new Group_Exception("The type field is implicit for group models and should not be explicitly stated in the structure");
		$structure["type"] = new Database\Type_Enum(array(self::NORMAL, self::EVERYONE, self::SYSTEM), Database\Type::NOT_NULL, self::NORMAL, "type__{$table}__type");
	
		$this->instance = $instance;
		parent::__construct($database, $table, $structure, $instance, "instance", "id", $foreign_keys, $unique_keys);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();
		
		$fields = array();
		foreach (array_keys($this->structure) as $field) $fields[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field);
		$fields = implode(",", $fields);
		
		$this->database->preprepare("FRAMEWORK_MODELS_GROUP__{$this->table}__GET_BY_TYPE", "
			SELECT {$fields}
			FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=:instance
			AND " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "type") . "=:type
		", array(
			"instance" => new Database\Type_Id(Database\Type::NOT_NULL),
			"type" => $this->structure["type"]
		), $this->structure);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns a group for the given instance and identifier.
	 * 
	 * @param mixed[] $instance an existing row within the Instance model
	 * @param int $id the model row identifier
	 * @return mixed[]|NULL
	 */
	public function get(array $instance, $id) {
		return $this->get_by_firstclass_and_id($instance, $id);
	}
	
	/**
	 * Returns the group flagged with the type SYSTEM for the specified instance.
	 * 
	 * @param mixed[] $instance an existing row within the Instance model
	 * @return mixed[]|NULL
	 */
	public function get_system(array $instance) {
		self::assert_id_object($instance, $this->instance->get_id_field());
		
		return $this->database->execute_params_single("FRAMEWORK_MODELS_GROUP__{$this->table}__GET_BY_TYPE", array(
			"instance" => $instance[$this->instance->get_id_field()],
			"type" => "system"
		));
	}
	
	/**
	 * Returns the group flagged with the type EVERYONE for the specified instance.
	 * 
	 * @param mixed[] $instance an existing row within the Instance model
	 * @return mixed[]|NULL
	 */
	public function get_everyone(array $instance) {
		self::assert_id_object($instance, $this->instance->get_id_field());
		
		return $this->database->execute_params_single("FRAMEWORK_MODELS_GROUP__{$this->table}__GET_BY_TYPE", array(
			"instance" => $instance[$this->instance->get_id_field()],
			"type" => "everyone"
		));
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Creates a new group using the supplied details. The newly created row in the group model table is returned on success.
	 * 
	 * @param mixed[] $instance an existing row within the Instance model
	 * @param string $type one of the available group types: NORMAL, EVERYONE or SYSTEM
	 * @param mixed[] $values an associative array of any additional values required by the model, in the form field name => value
	 * @throws Group_Exception
	 * @return mixed[]
	 */
	public function create(array $instance, $type = "normal", $values = array()) {
		self::assert_id_object($instance, $this->instance->get_id_field());
		
		if (!in_array($type, array(self::NORMAL, self::EVERYONE, self::SYSTEM), true)) throw new Group_Exception("Bad group type", $type);
		if (array_key_exists("type", $values)) throw new Group_Exception("The type field is implicit for group models and should not be explicitly provided in the values");
		$values["type"] = $type;
		
		return $this->insert_for_firstclass($instance, $values);
	}
}
