<?php
/**
 * Instance modelling within the Abstraction base framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Framework\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/firstclass.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

/**
 * @internal
 */
class Instance_Exception extends Models\Exception {}

/**
 * Defines a root model for instances.
 * 
 * Note this is a skeleton model only, so only the bare aspects needed to support the concept of instances are defined. In particular, content aspects such as the instance name not present.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Instance extends Models\FirstClass {
	const VISIBLE = "visible";
	const HIDDEN = "hidden";
	
	const DISABLED = "disabled";
	const ENABLED = "enabled";
	const LOCKED = "locked";
	
	/**
	 * Constructs a new instance of this model.
	 * 
	 * The visibility and status fields are implicit to the model and should not be defined explicitly by subclasses.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Database\Type[] $structure an associated array to define any further content within the model structure, taking the format field name => field type object
	 * @param Models\ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param Models\UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @throws Instance_Exception
	 */
	public function __construct(Database\Wrapper $database, $table, array $structure, array $foreign_keys = array(), array $unique_keys = array()) {
		if (array_key_exists("visibility", $structure)) throw new Instance_Exception("The visiblity field is implicit for instance models and should not be explicitly stated in the structure");
		$structure["visibility"] = new Database\Type_Enum(array(self::VISIBLE, self::HIDDEN), Database\Type::NOT_NULL, self::HIDDEN, "type__{$table}__visibility");
	
		if (array_key_exists("status", $structure)) throw new Instance_Exception("The status field is implicit for instance models and should not be explicitly stated in the structure");
		$structure["status"] = new Database\Type_Enum(array(self::ENABLED, self::DISABLED, self::LOCKED), Database\Type::NOT_NULL, self::DISABLED, "type__{$table}__status");
						
		parent::__construct($database, $table, $structure, "id", $foreign_keys, $unique_keys);
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Returns an instance for the given identifier.
	 * 
	 * @param int $id the model row identifier
	 * @return mixed[]|NULL
	 */
	public function get($id) {
		return $this->get_by_id($id);
	}
	
	/**
	 * Creates a new instance using the supplied details. The newly created row in the instance model table is returned on success.
	 * 
	 * @param string $visibility one of the available user visibilities: VISIBLE or HIDDEN
	 * @param string $status one of the available user statuses: ENABLED, DISABLED or LOCKED
	 * @param mixed[] $values an associative array of any additional values required by the model, in the form field name => value
	 * @throws Instance_Exception
	 */
	public function create($visibility, $status, $values = array()) {
		if (!in_array($visibility, array(self::VISIBLE, self::HIDDEN), true)) throw new Instance_Exception("Bad instance visibility", $visibility);
		if (array_key_exists("visibility", $values)) throw new Instance_Exception("The visibility field is implicit for instance models and should not be explicitly provided in the values");
		$values["visibility"] = $visibility;
		
		if (!in_array($status, array(self::ENABLED, self::DISABLED, self::LOCKED), true)) throw new Instance_Exception("Bad instance status", $status);
		if (!Data\Data::validate_regex("`^(enabled|disabled|locked)$`D", $status)) throw new Instance_Exception("Bad instance status", $status);
		if (array_key_exists("status", $values)) throw new Instance_Exception("The status field is implicit for instance models and should not be explicitly provided in the values");
		$values["status"] = $status;
		
		return $this->insert($values);
	}
}
