<?php
/**
 * User group membership modelling within the Abstraction base framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Framework\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/m2mlinktable.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/user.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/group.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

/**
 * @internal
 */
class M2M_User_Group_Exception extends Models\M2MLinkTable_Exception {}

/**
 * Defines a root model for conceptualising user group membership.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class M2M_User_Group extends Models\M2MLinkTable {
	/**
	 * Constructs a new instance of this model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param User $user the User model to associate with
	 * @param Group $group the Group model to associate with
	 * @param Database\Type[] $structure an associated array to define any further content within the model structure, taking the format field name => field type object
	 */
	public function __construct(Database\Wrapper $database, $table, User $user, Group $group, array $structure = array()) {
		parent::__construct($database, $table, $user, $group, "user", "group", $structure);
	}
	
	//-------------------------------------------------------------------------
}
