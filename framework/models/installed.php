<?php
/**
 * Installed instance area modelling within the Abstraction base framework.
 * 
 * 'Installeds' are effectively a link between the Instance and Area models, allowing for varying areas in different instances, and also customised ordering.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Framework\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/orientatedordered.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/area.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/instance.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction as Abstraction;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

/**
 * @internal
 */
class Installed_Exception extends Models\Exception {}

/**
 * Defines a root model for installed instance areas.
 * 
 * Note this is a skeleton model only, so only the bare aspects needed to support the concept of user access is defined. In particular, content aspects such as usernames and passwords are not present.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Installed extends Models\OrientatedOrdered {
	const DISABLED = "disabled";
	const ENABLED = "enabled";
	const LOCKED = "locked";
	
	public static $STATUSES = array(self::DISABLED, self::LOCKED, self::ENABLED);
	
	protected $area;
	
	/**
	 * Constructs a new instance of this model.
	 * 
	 * The instance, type, visibility and status fields are implicit to the model and should not be defined explicitly by subclasses.
	 * Similarly, foreign key mappings to instance are defined implicitly and should not be defined explicitly by subclasses.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Area $area the Area model to associate with
	 * @param Instance $instance the Instance model to associate with
	 * @param Database\Type[] $structure an associated array to define any further content within the model structure, taking the format field name => field type object
	 * @param Models\ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param Models\UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @throws Installed_Exception
	 */
	public function __construct(Database\Wrapper $database, $table, Area $area, Instance $instance, array $structure, array $foreign_keys = array(), array $unique_keys = array()) {
		$this->area = $area;
		
		if (array_key_exists("area", $structure)) throw new Installed_Exception("The area field is implicit for installed models and should not be explicitly stated in the structure");
		$structure["area"] = new Database\Type_Id(Database\Type::NOT_NULL);

		if (array_key_exists("area", $foreign_keys)) throw new Installed_Exception("The area field is implicit for installed models and should not be explicitly stated in the foreign keys");
		$foreign_keys["area"] = new Abstraction\Models\ForeignKey($area, "CASCADE", "CASCADE", $table . "__fk_area");
		
		$unique_keys[] = new Abstraction\Models\UniqueKey(array("instance", "area"), $table . "__uk");

		if (array_key_exists("status", $structure)) throw new Installed_Exception("The status field is implicit for installed models and should not be explicitly stated in the structure");
		$structure["status"] = new Database\Type_Enum(array(self::DISABLED, self::ENABLED, self::LOCKED), Database\Type::NOT_NULL, self::DISABLED, "type__{$table}__status");
		
		parent::__construct($database, $table, $structure, $instance, "instance", "ordered", "id", $foreign_keys, $unique_keys);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function define_views() {
		parent::define_views();
		
		$this->define_view("JOIN_AREA", "
			SELECT {$this->fields},
				" . $this->modelfield($this->area, $this->area->get_id_field()) . " AS " . $this->tempfield("area_id") . "
			FROM " . $this->model($this) . "
			INNER JOIN " . $this->model($this->area) . "
				ON " . $this->modelfield($this, "area") . "=" . $this->modelfield($this->area, $this->area->get_id_field()) . "
		");
	}

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		$fields = array();
		foreach (array_keys($this->structure) as $field) $fields[] = $this->modelviewfield($this, "JOIN_AREA", $field);
		$fields = implode(",", $fields);
		
		$this->database->preprepare("FRAMEWORK_MODELS_INSTALLED__{$this->table}__LIST_BY_AREA", "
			SELECT {$fields}
			FROM " . $this->modelview($this, "JOIN_AREA") . "
			WHERE " . $this->modelviewfield($this, "JOIN_AREA", "area_id") . "=:area
		", array(
			"area" => new Database\Type_Id(Database\Type::NOT_NULL)
		), $this->structure);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Installs an area into the instance by creating its associated row in this model.
	 * 
	 * @param mixed[] $instance an existing row within the Instance model
	 * @param mixed[] $area an existing row within the Area model
	 * @return mixed[]
	 */
	public function install(array $instance, array $area) {
		self::assert_id_object($instance, $this->firstclass->get_id_field());
		self::assert_id_object($area, $this->area->get_id_field());
		
		return $this->insert_for_firstclass($instance, array(
			"area" => $area[$this->area->get_id_field()],
			"status" => self::DISABLED
		));
	}

	/**
	 * Sets the status for the specified installed instance area.
	 * 
	 * @param mixed[] $installed an existing row within the Installed model
	 * @param string $status the status to set: ENABLED, DISABLED or LOCKED
	 * @throws Installed_Exception
	 * @return mixed[]
	 */
	public function set_status(array $installed, $status) {
		self::assert_id_object($installed, $this->id_field);
				
		switch ($status) {
			case self::DISABLED:
			case self::LOCKED:
			case self::ENABLED:
				break;
			default: throw new Installed_Exception("Bad status", $status);
		}

		$installed["status"] = $status;
		return $this->update($installed);
	}

	/**
	 * Uninstalls a previously defined instance area by deleting its row in this model.
	 * 
	 * @param mixed[] $installed an existing row within the Installed model
	 * @return boolean always return true; failure will throw an exception
	 */
	public function uninstall(array $installed) {
		self::assert_id_object($installed, $this->id_field);

		$instance = $this->firstclass->get_by_id($installed["instance"]);
		
		return $this->delete_for_firstclass($instance, $installed);
	}

	//-------------------------------------------------------------------------

	/**
	 * Lists all Instance model rows which have the specified area currently installed.
	 * 
	 * @param mixed[] $area an existing row within the Area model
	 * @return array
	 */
	function list_by_area(array $area) {
		self::assert_id_object($area, $this->area->get_id_field());
		
		return $this->database->execute_params("FRAMEWORK_MODELS_INSTALLED__{$this->table}__LIST_BY_AREA", array(
			"area" => $area[$this->area->get_id_field()]
		));
	}
}
