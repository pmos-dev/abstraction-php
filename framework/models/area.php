<?php
/**
 * Area modelling within the Abstraction base framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Framework\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/firstclass.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

/**
 * @internal
 */
class Area_Exception extends Models\Exception {}

/**
 * Defines a root model for areas.
 * 
 * Note this is a skeleton model only, so only the bare aspects needed to support the concept of areas are defined. In particular, content aspects such as the area name not present.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Area extends Models\FirstClass {
	/**
	 * Constructs a new instance of this model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Database\Type[] $structure an associated array to define any further content within the model structure, taking the format field name => field type object
	 * @param Models\ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param Models\UniqueKey[] $unique_keys any unique keys to apply to the model
	 */
	public function __construct(Database\Wrapper $database, $table, array $structure, array $foreign_keys = array(), array $unique_keys = array()) {
		parent::__construct($database, $table, $structure, "id", $foreign_keys, $unique_keys);
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Returns an area for the given identifier.
	 * 
	 * @param int $id the model row identifier
	 * @return mixed[]|NULL
	 */
	public function get($id) {
		return $this->get_by_id($id);
	}

	/**
	 * Creates a new area using the supplied details. The newly created row in the area model table is returned on success.
	 * 
	 * @param mixed[] $values an optional associative array of additional values, in the format field name => value
	 */
	public function create($values = array()) {
		return $this->insert($values);
	}
}
