<?php
/**
 * Group area access modelling within the Abstraction base framework.
 * 
 * Group access is inherited by users which are members of that group unless explicitly overridden with user access settings.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Framework\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/m2mlinktable.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/group.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/installed.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Abstraction\Framework as Framework;

/**
 * @internal
 */
class M2M_Group_Installed_Exception extends Models\M2MLinkTable_Exception {}

/**
 * Defines a root model for conceptualising group access to areas (via the Installed model).
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class M2M_Group_Installed extends Models\M2MLinkTable {
	private $group, $installed;

	/**
	 * Constructs a new instance of this model.
	 * 
	 * Access levels are defined within the Framework\Access model.
	 * 
	 * @see Framework\Access
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Group $group the Group model to associate with
	 * @param Installed $installed the Installed model to associate with
	 * @param Database\Type[] $structure an associated array to define any further content within the model structure, taking the format field name => field type object
	 * @throws M2m_Group_Installed_Exception
	 */
	public function __construct(Database\Wrapper $database, $table, Group $group, Installed $installed, array $structure = array()) {
		if (array_key_exists("access", $structure)) throw new M2m_Group_Installed_Exception("The access field is implicit for group-installed models and should not be explicitly stated in the structure");
		$structure["access"] = new Database\Type_TinyIntEnum(Framework\Access::$ACCESS_LEVELS, Database\Type::NOT_NULL, null, "type__{$table}__access");
		
		$this->group = $group;
		$this->installed = $installed;
		
		parent::__construct($database, $table, $group, $installed, "group", "installed", $structure);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		$this->database->preprepare("FRAMEWORK_MODELS_M2M_GROUP_INSTALLED__{$this->table}__GET_DIRECT", "
			SELECT " . $this->modelviewfield($this, "M2M_JOIN_DIRECT", "access") . "
			FROM " . $this->modelview($this, "M2M_JOIN_DIRECT") . "
			WHERE " . $this->modelviewfield($this, "M2M_JOIN_DIRECT", "a_id") . "=:group
			AND " . $this->modelviewfield($this, "M2M_JOIN_DIRECT", "b_id") . "=:installed
		", array(
			"group" => new Database\Type_Id(Database\Type::NOT_NULL),
			"installed" => new Database\Type_Id(Database\Type::NOT_NULL)
		), array(
			"access" => $this->structure["access"]
		));
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Returns the access level for the specified group and installed area, or null if none is defined.
	 * 
	 * Access levels are defined within the Framework\Access model.
	 * 
	 * @see Framework\Access
	 * @param mixed[] $group an existing row within the Group model
	 * @param mixed[] $installed an existing row within the Installed model
	 * @return int|NULL
	 */
	public function get_access($group, $installed) {
		self::assert_id_object($group, $this->group->get_id_field());
		self::assert_id_object($installed, $this->installed->get_id_field());
		
		$access = $this->database->execute_params_value("FRAMEWORK_MODELS_M2M_GROUP_INSTALLED__{$this->table}__GET_DIRECT", array(
			"group" => $group[$this->table_a->get_id_field()],
			"installed" => $installed[$this->table_b->get_id_field()]
		), true);
		
		return $access;
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Sets the access level for the specified group and installed area.
	 * 
	 * Access levels are defined within the Framework\Access model.
	 * 
	 * @see Framework\Access
	 * @param mixed[] $group an existing row within the Group model
	 * @param mixed[] $installed an existing row within the Installed model
	 * @param int $access the access level to set
	 * @throws M2M_Group_Installed_Exception
	 * @return true always returns true; failure throws an exception
	 */
	public function set_access(array $group, array $installed, $access) {
		if ($access === null) throw new M2M_Group_Installed_Exception("Do not set NULL access; instead use revoke");
		self::assert_int($access);
		if (!in_array($access, Framework\Access::$ACCESS_LEVELS)) throw new M2M_Group_Installed_Exception("Unknown access level given", $access);
		
		return $this->set_access_direct($group, $installed, $access);
	}
	
	/**
	 * Revokes access for the specified group and installed area by deleting the corresponding row in this model.
	 * 
	 * @param mixed[] $group an existing row within the Group model
	 * @param mixed[] $installed an existing row within the Installed model
	 * @return true always returns true; failure throws an exception
	 */
	public function revoke_access(array $group, array $installed) {
		return $this->set_access_direct($group, $installed, null);
	}
	
	/**
	 * @internal
	 */
	private function set_access_direct(array $group, array $installed, $access) {
		self::assert_id_object($group, $this->group->get_id_field());
		self::assert_id_object($installed, $this->installed->get_id_field());
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$this->unlink($group, $installed, true);

		if ($access !== null) $this->link($group, $installed, array(
			"access" => $access
		));
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}
}
