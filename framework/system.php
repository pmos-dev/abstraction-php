<?php
/**
 * Core system for the Abstraction base framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Framework;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "framework/framework.php";
require_once ABSTRACTION_ROOT_PATH . "framework/access.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as AbstractionModels;
use \Abstraction\Framework\Models as Models;

/**
 * @internal
 */
class SystemException extends FrameworkException {}

/**
 * The core system for using the Abstraction framework. Conceptualises within a database the notion of areas, instances and the installed link between the two.
 * 
 * All base functionality is contained within this class, so subclassing is only needed to provide additional aspects such as name support.
 * Note, this class extends the ModelHelperMethods to gain functionality methods, but is not really a model itself.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class System extends AbstractionModels\ModelHelperMethods {
	protected $area;
	protected $instance;
	protected $installed;
	
	protected $installed_matrix = array();
	
	//-------------------------------------------------------------------------
	
	/**
	 * Constructs a new instance of the system.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param Models\Area $area the Area model to associate with
	 * @param Models\Instance $instance the Instance model to associate with
	 * @param Models\Installed $installed the Installed model to associate with
	 */
	public function __construct(
		Database\Wrapper $database,
		Models\Area $area, 
		Models\Instance $instance, 
		Models\Installed $installed 
	) {
		parent::__construct($database);
		
		$this->area = $area;
		$this->instance = $instance;
		$this->installed = $installed;

		// include all fields except the ID field which we'll put in manually with a new name
		$fields = $this->modelfield_array($this->area);
		unset($fields[$this->area->get_id_field()]);
		$fields["area_id"] = $this->modelfield($this->area, $this->area->get_id_field()) . " AS " . $this->tempfield("area_id");
		
		$returns = $this->area->get_structure();
		$returns["area_id"] = $returns[$this->area->get_id_field()];
		unset($returns[$this->area->get_id_field()]);
		
		foreach ($this->installed->get_structure() as $name => $type) {
			$fields[] = $this->modelfield($this->installed, $name);
			$returns[$name] = $type;
		}
		
		$fields = implode(",", $fields);
		
		$this->database->preprepare("SYSTEM_LOAD_INSTALLED_MATRIX", "
			SELECT {$fields}
			FROM " . $this->model($this->installed) . "
			INNER JOIN " . $this->model($this->area) . "
				ON " . $this->modelfield($this->installed, "area") . "=" . $this->modelfield($this->area, $this->area->get_id_field()) . "
			INNER JOIN " . $this->model($this->instance) . "
				ON " . $this->modelfield($this->installed, "instance") . "=" . $this->modelfield($this->instance, $this->instance->get_id_field()) . "
			WHERE " . $this->modelfield($this->instance, $this->instance->get_id_field()) . "=:instance
			ORDER BY " . $this->modelfield($this->installed, "ordered") . " ASC
		", array(
			"instance" => new Database\Type_Id(Database\Type::NOT_NULL)
		), $returns);
	}

	//-------------------------------------------------------------------------
	
	/**
	 * Builds the system into the database by creating the necessary tables.
	 * 
	 * @return void
	 */
	public function create_tables() {
		$this->area->create_table();
		$this->instance->create_table();
		$this->installed->create_table();
	}
	
	/**
	 * Destroys the system from the database by dropping the associated tables.
	 * 
	 * @return void
	 */
	public function drop_tables() {
		$this->installed->drop_table();
		$this->instance->drop_table();
		$this->area->drop_table();
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Creates a new area in the system using the supplied optional values.
	 * 
	 * @param mixed[] $values an optional associative array of additional values, in the format field name => value
	 * @return void
	 */
	public function create_area(array $values = array()) {
		return $this->area->create($values);
	}

	/**
	 * Removes the specified area from the system.
	 * 
	 * @param mixed[] $area an existing row within the Area model
	 * @return void
	 */
	public function delete_area(array $area) {
		return $this->area->delete($area);
	}
	
	/**
	 * Creates a new instance in the system using the supplied optional values.
	 * 
	 * @param mixed[] $values an optional associative array of additional values, in the format field name => value
	 * @return void
	 */
	public function create_instance(array $values = array()) {
		return $this->instance->create("hidden", "disabled", $values);
	}
	
	/**
	 * Removes the specified instance from the system.
	 * 
	 * @param mixed[] $instance an existing row within the Instance model
	 * @return void
	 */
	public function delete_instance(array $instance) {
		return $this->instance->delete($instance);
	}
	
	/**
	 * Installs an area into the specified instance within the system..
	 * 
	 * @param mixed[] $instance an existing row within the Instance model
	 * @param mixed[] $area an existing row within the Area model
	 * @return mixed[]
	 */
	public function install(array $instance, array $area) {
		return $this->installed->install($instance, $area);
	}
	
	/**
	 * Uninstalls a previously defined installed instance area from the system.
	 * 
	 * @param mixed[] $installed an existing row within the Installed model
	 * @return boolean always return true; failure will throw an exception
	 */
	public function uninstall(array $installed) {
		return $this->installed->uninstall($installed);
	}

	//-------------------------------------------------------------------------
	
	/**
	 * Loads the matrix of corresponding Installed model rows for the specified Instance model row.
	 *  
	 * @param mixed[] $instance an existing row within the Instance model
	 * @throws NotIDObjectException
	 * @return void
	 */
	public function load_installed_matrix(array $instance) {
		if (!Data\Data::validate_id_object($instance, $this->instance->get_id_field())) throw new NotIDObjectException("Supplied instance is not an ID object");

		$this->installed_matrix = $this->database->execute_params("SYSTEM_LOAD_INSTALLED_MATRIX", array(
			"instance" => $instance[$this->instance->get_id_field()]
		));
	}

	/**
	 * Returns the corresponding Installed model row for the specified Area model row for currently loaded installed matrix.
	 * 
	 * Note this method cannot be used until the load_installed_matrix method has been called.
	 * 
	 * @param mixed[] $area an existing row within the Area model
	 * @throws NotIDObjectException
	 * @return mixed[]|NULL
	 */
	public function get_installed_by_area(array $area) {
		if (!Data\Data::validate_id_object($area, $this->area->get_id_field())) throw new NotIDObjectException($area);

		foreach ($this->installed_matrix as $lookup) if ($lookup["area_id"] === $area[$this->area->get_id_field()]) return $lookup;

		return null;
	}
	
	/**
	 * Returns the Area model row for this Installed model row programmatically from the loaded cache rather than making a subsequent call to the database.
	 * 
	 * Note this method cannot be used until the load_installed_matrix method has been called.
	 * 
	 * @param mixed[] $installed an existing row within the Installed model
	 * @throws NotIDObjectException
	 * @return mixed[]|NULL
	 */
	public function get_area_by_installed(array $installed) {
		if (!Data\Data::validate_id_object($installed, $this->id_field)) throw new NotIDObjectException($installed);

		foreach ($this->installed_matrix as $lookup) if ($lookup[$this->id_field] === $installed[$this->id_field]) {
			unset($lookup[$this->id_field]);
			unset($lookup["area"]);
			unset($lookup["instance"]);
			$lookup[$this->area->get_id_field()] = $lookup["area_id"];
			return $lookup;
		}

		return null;
	}
	
	/**
	 * Lists all Area model rows which have a corresponding mapping to the currently loaded Installed matrix.
	 * 
	 * This is done programmatically rather than using database queries.
	 * Note this method cannot be used until the load_installed_matrix method has been called.
	 * 
	 * @return array
	 */
	public function list_installed_areas() {
		$areas = array();
		foreach ($this->installed_matrix as $lookup) {
			unset($lookup[$this->installed->get_id_field()]);
			unset($lookup["area"]);
			unset($lookup["instance"]);
			$lookup[$this->area->get_id_field()] = $lookup["area_id"];
			$areas[] = $lookup;
		}
		return $areas;
	}
	
	//-------------------------------------------------------------------------
	
}
