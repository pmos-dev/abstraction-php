<?php
/**
 * Core access management for the Abstraction base framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Framework;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "framework/framework.php";
require_once ABSTRACTION_ROOT_PATH . "framework/system.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/user.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/group.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/m2m_user_group.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/m2m_user_installed.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/m2m_group_installed.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as AbstractionModels;
use \Abstraction\Framework\Models as Models;

/**
 * @internal
 */
class AccessException extends FrameworkException {}

/**
 * The core of access management within the Abstraction framework. Brings together the all the models and System class to implement full granularity for ACLs.
 * 
 * All base functionality is contained within this class, so subclassing is rarely needed.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Access extends AbstractionModels\ModelHelperMethods {
	const NONE = 0;
	const READ = 1;
	const USER = 2;
	const EDITOR = 3;
	const FULL = 4;
	
	public static $ACCESS_LEVELS = array(self::NONE, self::READ, self::USER, self::EDITOR, self::FULL);
	
	private $system;
	private $instance, $installed, $user, $group;
	private $m2m_user_group, $m2m_user_installed, $m2m_group_installed;
	
	private $user_access_matrix = array();
	private $group_access_matrix = array();
	
	//-------------------------------------------------------------------------

	/**
	 * Constructs a new instance of the access management component.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param System $system the core framework System resource
	 * @param Models\Instance $instance the Instance model to associate with
	 * @param Models\Installed $installed the Installed model to associate with
	 * @param Models\User $user the User model to associate with
	 * @param Models\Group $group the Group model to associate with
	 * @param Models\M2M_User_Group $m2m_user_group the User_Group membership model to associate with
	 * @param Models\M2M_User_Installed $m2m_user_installed the User_Installed model to associate with
	 * @param Models\M2M_Group_Installed $m2m_group_installed the Group_Installed model to associate with
	 */
	public function __construct(
		Database\Wrapper $database,
		System $system,
		Models\Instance $instance, 
		Models\Installed $installed, 
		Models\User $user, 
		Models\Group $group, 
		Models\M2M_User_Group $m2m_user_group, 
		Models\M2M_User_Installed $m2m_user_installed, 
		Models\M2M_Group_Installed $m2m_group_installed 
	) {
		parent::__construct($database);
		
		$this->system = $system;
		
		$this->instance = $instance;
		$this->installed = $installed;
		$this->user = $user;
		$this->group = $group;
		
		$this->m2m_user_group = $m2m_user_group;
		$this->m2m_user_installed = $m2m_user_installed;
		$this->m2m_group_installed = $m2m_group_installed;
		
		$this->database->preprepare("ACCESS_LOAD_USER_MATRIX", "
			SELECT " . $this->modelfield($this->installed, $this->installed->get_id_field()) . " AS " . $this->tempfield("installed") . ",
				" . $this->modelfield($this->m2m_user_installed, "access") . "
			FROM " . $this->model($this->m2m_user_installed) . "
			INNER JOIN " . $this->model($this->user) . "
				ON " . $this->modelfield($this->m2m_user_installed, "user") . "=" . $this->modelfield($this->user, $this->user->get_id_field()) . "
			INNER JOIN " . $this->model($this->installed) . "
				ON " . $this->modelfield($this->m2m_user_installed, "installed") . "=" . $this->modelfield($this->installed, $this->installed->get_id_field()) . "
			WHERE " . $this->modelfield($this->user, $this->user->get_id_field()) . "=:user
		", array(
			"user" => new Database\Type_Id(Database\Type::NOT_NULL)
		), array(
			"installed" => new Database\Type_Id(Database\Type::NOT_NULL),
			"access" => new Database\Type_TinyIntEnum(self::$ACCESS_LEVELS, Database\Type::NOT_NULL)
		));

		$this->database->preprepare("ACCESS_LOAD_GROUP_MATRIX", "
			SELECT " . $this->modelfield($this->installed, $this->installed->get_id_field()) . " AS " . $this->tempfield("installed") . ",
				MAX(" . $this->modelfield($this->m2m_group_installed, "access") . ") AS " . $this->tempfield("access") . "
			FROM " . $this->model($this->m2m_group_installed) . "
			INNER JOIN " . $this->model($this->group) . "
				ON " . $this->modelfield($this->m2m_group_installed, "group") . "=" . $this->modelfield($this->group, $this->group->get_id_field()) . "
			INNER JOIN " . $this->model($this->m2m_user_group) . "
				ON " . $this->modelfield($this->m2m_user_group, "group") . "=" . $this->modelfield($this->group, $this->group->get_id_field()) . "
			INNER JOIN " . $this->model($this->user) . "
				ON " . $this->modelfield($this->m2m_user_group, "user") . "=" . $this->modelfield($this->user, $this->user->get_id_field()) . "
			INNER JOIN " . $this->model($this->installed) . "
				ON " . $this->modelfield($this->m2m_group_installed, "installed") . "=" . $this->modelfield($this->installed, $this->installed->get_id_field()) . "
			WHERE " . $this->modelfield($this->user, $this->user->get_id_field()) . "=:user
			GROUP BY " . $this->modelfield($this->user, $this->user->get_id_field()) . ",
				" . $this->modelfield($this->installed, $this->installed->get_id_field()) . "
		", array(
			"user" => new Database\Type_Id(Database\Type::NOT_NULL)
		), array(
			"installed" => new Database\Type_Id(Database\Type::NOT_NULL),
			"access" => new Database\Type_TinyIntEnum(self::$ACCESS_LEVELS, Database\Type::NOT_NULL)
		));
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Builds the access manager into the database by creating the necessary tables.
	 * 
	 * @return void
	 */
	public function create_tables() {
		$this->group->create_table();
		$this->user->create_table();
		
		$this->m2m_user_group->create_table();
		$this->m2m_user_installed->create_table();
		$this->m2m_group_installed->create_table();
	}
	
	/**
	 * Destroys the access manager from the database by dropping the associated tables.
	 * 
	 * @return void
	 */
	public function drop_tables() {
		$this->m2m_group_installed->drop_table();
		$this->m2m_user_installed->drop_table();
		$this->m2m_user_group->drop_table();
		
		$this->user->drop_table();
		$this->group->drop_table();
	}
	
	/**
	 * Sets up a basic instance within the system.
	 * 
	 * This method automatically creates the SYSTEM and EVERYONE groups, alongside the GUEST and ROOT user accounts.
	 * It also adds both of these created user accounts to the SYSTEM group automatically.
	 * 
	 * @param mixed[] $instance an existing row within the Instance model
	 * @param mixed[] $system_structure an optional associative array of additional values required by the Group model for the SYSTEM group, in the format field name => value
	 * @param mixed[] $everyone_structure an optional associative array of additional values required by the Group model for the EVERYONE group, in the format field name => value
	 * @param mixed[] $guest_structure an optional associative array of additional values required by the User model for the GUEST user, in the format field name => value
	 * @param mixed[] $root_structure an optional associative array of additional values required by the User model for the ROOT user, in the format field name => value
	 * @throws NotIDObjectException
	 */
	public function setup_instance(array $instance, array $system_structure = array(), array $everyone_structure = array(), array $guest_structure = array(), array $root_structure = array()) {
		if (!Data\Data::validate_id_object($instance, $this->instance->get_id_field())) throw new NotIDObjectException("Supplied instance is not an ID object");
				
		$system = $this->group->create($instance, Models\Group::SYSTEM, $system_structure);
		$this->group->create($instance, Models\Group::EVERYONE, $everyone_structure);
		$guest = $this->user->create($instance, Models\User::GUEST, Models\User::HIDDEN, Models\User::ENABLED, $guest_structure);
		$root = $this->user->create($instance, Models\User::ROOT, Models\User::HIDDEN, Models\User::ENABLED, $root_structure);

		$this->add_user_to_group($guest, $system);
		$this->add_user_to_group($root, $system);
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Loads the matrix of access permissions for the specified User model row, both explicit and inherited.
	 * 
	 * Note that the instance to use is implicit to the User model.
	 *  
	 * @param mixed[] $user an existing row within the User model
	 * @throws NotIDObjectException
	 * @return void
	 */
	public function load_user_access_matrix(array $user) {
		if (!Data\Data::validate_id_object($user, $this->user->get_id_field())) throw new NotIDObjectException("Supplied user is not an ID object");
		
		$this->user_access_matrix = $this->database->execute_params("ACCESS_LOAD_USER_MATRIX", array(
			"user" => $user[$this->user->get_id_field()]
		));
		$this->group_access_matrix = $this->database->execute_params("ACCESS_LOAD_GROUP_MATRIX", array(
			"user" => $user[$this->user->get_id_field()]
		));
	}
	
	/**
	 * Returns the calculated access permission for the specified user and installed combination, or null if none is defined.
	 * 
	 * Explicitly defined user permissions override inherited member group permissions.
	 * Additionally, setting the $direct_only flag returns only explicitly defined permissions.
	 * Note this method works programmatically, and cannot be used until the load_user_access_matrix method has been called.
	 * 
	 * @param mixed[] $user an existing row within the User model
	 * @param mixed[] $installed an existing row within the Installed model
	 * @param boolean $direct_only true to disable group access inheritance
	 * @throws NotIDObjectException
	 * @return int|NULL
	 */
	public function get_access(array $user, array $installed, $direct_only = false) {
		if (!Data\Data::validate_id_object($user, $this->user->get_id_field())) throw new NotIDObjectException($user);
		if (!Data\Data::validate_id_object($installed, $this->installed->get_id_field())) throw new NotIDObjectException($installed);

		foreach ($this->user_access_matrix as $acl) if ($acl["installed"] === $installed[$this->installed->get_id_field()]) return $acl["access"];

		if ($direct_only) return null;
		
		foreach ($this->group_access_matrix as $acl) if ($acl["installed"] === $installed[$this->installed->get_id_field()]) return $acl["access"];
		
		return null;
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Makes a user a member of the specified group.
	 * 
	 * @param mixed[] $user an existing row within the User model
	 * @param mixed[] $group an existing row within the Group model
	 * @return void
	 */
	public function add_user_to_group(array $user, array $group) {
		$this->m2m_user_group->link($user, $group);
	}
	
	/**
	 * Removes a user from the membership of the specified group.
	 * 
	 * @param mixed[] $user an existing row within the User model
	 * @param mixed[] $group an existing row within the Group model
	 * @return void
	 */
	public function remove_user_from_group(array $user, array $group) {
		$this->m2m_user_group->unlink($user, $group, true);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Creates a new NORMAL user within the current system.
	 * 
	 * Membership of the EVERYONE group is automatically added.
	 * 
	 * @param mixed[] $instance an existing row within the Instance model
	 * @param mixed[] $values an optional associative array of additional values required by the User model, in the format field name => value
	 * @return mixed[]
	 */
	public function create_user(array $instance, array $values = array()) {
		$user = $this->user->create($instance, Models\User::NORMAL, Models\User::VISIBLE, Models\User::ENABLED, $values);
		
		$everyone = $this->group->get_everyone($instance);
		$this->add_user_to_group($user, $everyone);
		
		return $user;
	}
	
	/**
	 * Creates a new NORMAL group within the current system.
	 * 
	 * @param mixed[] $instance an existing row within the Instance model
	 * @param mixed[] $values an optional associative array of additional values required by the Group model, in the format field name => value
	 * @return mixed[]
	 */
	public function create_group(array $instance, array $values = array()) {
		return $this->group->create($instance, Models\Group::NORMAL, $values);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Sets the explicit access permission for the specified user and installed combination.
	 * 
	 * @param mixed[] $user an existing row within the User model
	 * @param mixed[] $installed an existing row within the Installed model
	 * @param int $access one of the defined access levels: NONE, READ, USER, EDITOR or FULL
	 * @throws AccessException
	 * @return void
	 */
	public function set_user_installed_access(array $user, array $installed, $access) {
		if (!in_array($access, self::$ACCESS_LEVELS, true)) throw new AccessException("Unknown access level", $access);
		
		$this->m2m_user_installed->set_access($user, $installed, $access);
	}

	/**
	 * Sets the access permission for the specified group and installed combination.
	 * 
	 * This may be inherited by users who are members of the group if no explicit permission is defined for the user.
	 * 
	 * @param mixed[] $group an existing row within the Group model
	 * @param mixed[] $installed an existing row within the Installed model
	 * @param int $access one of the defined access levels: NONE, READ, USER, EDITOR or FULL
	 * @throws AccessException
	 * @return void
	 */
	public function set_group_installed_access(array $group, array $installed, $access) {
		if (!in_array($access, self::$ACCESS_LEVELS, true)) throw new AccessException("Unknown access level", $access);
				
		$this->m2m_group_installed->set_access($group, $installed, $access);
	}

	/**
	 * Revokes explicit access for the specified user and installed combination.
	 * 
	 * Note, group membership permissions will then become inherited, so revoking user access is not the same as <b>denying</b> user access.
	 * 
	 * @param mixed[] $user an existing row within the User model
	 * @param mixed[] $installed an existing row within the Installed model
	 * @return void
	 */
	public function revoke_user_installed_access(array $user, array $installed) {
		$this->m2m_user_installed->revoke_access($user, $installed);
	}
	
	/**
	 * Revokes explicit access for the specified group and installed combination.
	 * 
	 * Any users who are members of this group and don't have other group member access or their own explicit user access level will lose access to the area.
	 * 
	 * @param mixed[] $group an existing row within the Group model
	 * @param mixed[] $installed an existing row within the Installed model
	 * @return void
	 */
	public function revoke_group_installed_access(array $group, array $installed) {
		$this->m2m_group_installed->revoke_access($group, $installed);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Lists the users who are members of the specified group.
	 * 
	 * @param mixed[] $group an existing row within the Group model
	 * @throws NotIDObjectException
	 * @return array
	 */
	public function get_users_by_group(array $group) {
		if (!Data\Data::validate_id_object($group, $this->group->get_id_field())) throw new NotIDObjectException($group);
		
		return $this->m2m_user_group->list_as_by_b($group);
	}
	
	/**
	 * Lists the groups that the specified user is a member of.
	 * 
	 * @param mixed[] $user an existing row within the User model
	 * @throws NotIDObjectException
	 * @return array
	 */
	public function get_groups_by_user(array $user) {
		if (!Data\Data::validate_id_object($user, $this->user->get_id_field())) throw new NotIDObjectException($user);
		
		return $this->m2m_user_group->list_bs_by_a($user);
	}
}
