<?php
/**
 * Core Abstraction framework loader script.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Framework;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction as Abstraction;

/**
 * @internal
 */
class FrameworkException extends Abstraction\Exception {}

/**
 * @internal
 */
class NotIDObjectException extends FrameworkException {
	public function __construct($value = "\t_NO_VALUE", $extra = null) {
		parent::__construct("Supplied value is not an ID object", $value, $extra);
	}
}

require_once ABSTRACTION_ROOT_PATH . "framework/system.php";
require_once ABSTRACTION_ROOT_PATH . "framework/access.php";
require_once ABSTRACTION_ROOT_PATH . "framework/session.php";
