<?php
/**
 * Session management and intelligent access control for the Abstraction base framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Framework;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "framework/framework.php";
require_once ABSTRACTION_ROOT_PATH . "framework/system.php";
require_once ABSTRACTION_ROOT_PATH . "framework/access.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction\Data as Data;
use \Abstraction\Framework\Models as Models;

/**
 * @internal
 */
class SessionException extends FrameworkException {}

/**
 * Session management support for the Abstraction framework. Combines both the System and Access components to create an intelligent framework. For example, requests for user access on a disabled instance will always return NONE.
 * 
 * All base functionality is contained within this class, so subclassing is only needed to add practical login methodology support such as usernames and passwords.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Session {
	protected $name;

	protected $system, $access, $area, $instance, $user;
	
	private $session_instance = null;
	private $active_user = null;
	
	/**
	 * Constructs a new instance of the session management component.
	 * 
	 * @param string $name a name to use for the session in cookies etc.
	 * @param System $system the core framework System resource
	 * @param Access $access the access management resource
	 * @param Models\Area $area the Area model to associate with
	 * @param Models\Instance $instance the Instance model to associate with
	 * @param Models\User $user the User model to associate with
	 */
	public function __construct($name, System $system, Access $access, Models\Area $area, Models\Instance $instance, Models\User $user) {
		$this->name = $name;

		$this->system = $system;
		$this->access = $access;
		$this->area = $area;
		$this->instance = $instance;
		$this->user = $user;
	}

	/**
	 * Loads the specified user into the session and fetches the access matrix.
	 * 
	 * @param mixed[] $user an existing row within the User model
	 * @return void
	 */
	private function load_user(array $user) {
		$this->active_user = $user;
		$this->access->load_user_access_matrix($user);
	}

	/**
	 * Initialises or restores the PHP session for the specified instance.
	 * 
	 * The GUEST user is automatically loaded if the session is new or there is a problem loading the existing one.
	 * 
	 * @param mixed[] $session_instance an existing row within the Instance model
	 * @throws NotIDObjectException
	 * @return boolean true on initialisation or restore success, false if the load could not be loaded into a restored session
	 */
	public function initialize(array $session_instance) {
		if (!Data\Data::validate_id_object($session_instance, $this->instance->get_id_field())) throw new NotIDObjectException("Instance ID is not valid");
		
		$this->session_instance = $session_instance;

		session_name($this->name . "xsi" . $this->session_instance[$this->instance->get_id_field()]);
		@session_start();

		$this->system->load_installed_matrix($session_instance);
		
		if (!isset($_SESSION["user"])) {
			$this->load_user($this->user->get_guest($this->session_instance));
		
			return true;
		} else {
			if (!Data\Data::validate_id($_SESSION["user"])) throw new NotIDObjectException("Session user ID is not valid");
			
			$user = $this->user->get($this->session_instance, $_SESSION["user"]);
			if ($user === null || ($user["instance"] !== $this->session_instance[$this->instance->get_id_field()]) || $user["type"] === Models\User::GUEST) {
				$this->logoff();
				return false;
			}
			$this->load_user($user);
		}

		$_SESSION["user"] = $this->active_user[$this->user->get_id_field()];
		return true;
	}

	/**
	 * Logs the existing user off, explicitly unloads them from the session, and then destroys all session data for this instance.
	 * 
	 * @return true
	 */
	public function logoff() {
		unset($_SESSION["user"]);
		session_destroy();
		
		$this->load_user($this->user->get_guest($this->session_instance));

		return true;
	}

	/**
	 * Logs the specified user into the current session.
	 * 
	 * @param mixed[] $user an existing row within the User model
	 * @throws NotIDObjectException
	 * @throws SessionException thrown if the specified user is not part of the current session instance
	 * @return true
	 */
	public function login(array $user) {
		if (!Data\Data::validate_id_object($user, $this->user->get_id_field())) throw new NotIDObjectException($user);
		if ($user["instance"] !== $this->session_instance[$this->instance->get_id_field()]) throw new SessionException("User and instance ID mismatch");
		
		$this->load_user($user);
		
		$_SESSION["user"] = $user[$this->user->get_id_field()];
		
		return true;
	}

	//-------------------------------------------------------------------------

	/**
	 * Sets the session instance manually, without initialising a PHP session.
	 * 
	 * This method is exclusively for use during full export, and shouldn't be used otherwise.
	 *
	 * @return mixed[]|NULL
	 */
	public function set_session_instance_for_full_export(array $instance) {
		if (!Data\Data::validate_id_object($instance, $this->instance->get_id_field())) throw new NotIDObjectException("Instance ID is not valid");
		$this->session_instance = $instance;
		$this->system->load_installed_matrix($this->session_instance);
	}

	/**
	 * Returns the Instance model row for the currently loaded session, if any.
	 *
	 * @return mixed[]|NULL
	 */
	public function get_session_instance() {
		return $this->session_instance;
	}
	
	/**
	 * Returns the User model row for the currently loaded session, if any.
	 * 
	 * @return mixed[]|NULL
	 */
	public function get_active_user() {
		return $this->active_user;
	}
	
	/**
	 * Returns whether or not the currently loaded session user is the GUEST user.
	 * 
	 * @return boolean
	 */
	public function is_guest() {
		return $this->active_user["type"] === Models\User::GUEST;
	}

	/**
	 * Returns whether or not the currently loaded session user is the ROOT user.
	 * 
	 * @return boolean
	 */
	public function is_root() {
		return $this->active_user["type"] === Models\User::ROOT;
	}

	/**
	 * Returns whether or not the currently loaded session user is a NORMAL user (i.e. not ROOT or GUEST).

	 * @return boolean
	 */
	public function is_normal() {
		return $this->active_user["type"] === Models\User::NORMAL;
	}

	//-------------------------------------------------------------------------

	/**
	 * Returns the overall meaningful access that the currently loaded session user has to the specified area.
	 * 
	 * As well as explicit user access levels and inherited group access, aspects such as the instance and installed statuses (ENABLED, DISABLED, LOCKED etc.) are factored in to give an intelligent and meaningful result.
	 * 
	 * @param mixed[] $area an existing row within the Area model
	 * @throws NotIDObjectException
	 * @return int|NULL
	 */
	public function get_access(array $area) {
		if (!Data\Data::validate_id_object($area, $this->user->get_id_field())) throw new NotIDObjectException($area);

		// disabled instances have zero access
		if ($this->session_instance["status"] === Models\Instance::DISABLED) return Access::NONE;

		$installed = $this->system->get_installed_by_area($area);
		
		// disabled installeds have zero access
		if ($installed === null || $installed["status"] === Models\Installed::DISABLED) return Access::NONE;

		// root has access to everything
		if ($this->active_user["type"] === Models\User::ROOT) return Access::FULL;

		$existing = $this->access->get_access($this->active_user, $installed);
		if ($existing === Access::NONE) return Access::NONE;
		
		// if we get to this point, we have at least READ access to the area.
		
		// locked instances have read access except for root
		if ($this->session_instance["status"] === Models\Instance::LOCKED) return Access::READ;

		// locked installeds have read access except for root
		if ($installed["status"] === Models\Installed::LOCKED) return Access::READ;

		return $existing;
	}
	
	/**
	 * Returns whether the currently loaded session user has access to the specified area at least to the specified level.
	 * 
	 * Calculation of access is performed intelligently using get_access, so factors such as instance status are considered automatically.
	 * Access levels are defined within the Access model.
	 * 
	 * @see Access
	 * @param mixed[] $area an existing row within the Area model
	 * @param int $level the minimum required access level
	 * @throws NotIDObjectException
	 * @throws SessionException
	 * @return boolean
	 */
	public function has_access(array $area, $level) {
		if (!Data\Data::validate_id_object($area, $this->user->get_id_field())) throw new NotIDObjectException($area);
		if (!Data\Data::validate_int($level)) throw new SessionException("Bad access level", $level);
		if (!in_array($level, array(Access::NONE, Access::READ, Access::USER, Access::EDITOR, Access::FULL), true)) throw new SessionException("Bad access level", $level);
		
		return $this->get_access($area) >= $level;
	}
}
