<?php
/**
 * Database types: String.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

/**
 * Database Type for string data (VARCHAR etc.)
 * 
 * Note there is a hard limit of 255 characters of strings. For longer lengths, consider using Text types.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_String extends Type {
	protected $max_length;
	
	/**
	 * Constructs a new instance of the type
	 *
	 * @param int $max_length the maximum length of string value to allow
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($max_length, $not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct($not_null, $default);
		
		if (!is_int($max_length) || $max_length < 1) throw new TypeMismatchException("Bad max length", $max_length);
		if ($max_length > 255) throw new TypeMismatchException("Max length for STRING cannot be more than 255; consider using TEXT instead", $max_length);
		
		$this->max_length = $max_length;
	}

	/**
	 * @internal
	 */
	public function get_db_type($database) {
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
			case Wrapper::DATABASE_POSTGRESQL:
			case Wrapper::DATABASE_MSSQL:
				return "VARCHAR({$this->max_length})";
		}
		throw new Exception("This type has not been implemented for this database yet", $database);
	}
	
	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		if (strlen($value) > $this->max_length) throw new TypeMismatchException("STRING", $value, "Greater than 255 characters. Consider using TEXT instead.");
		
		return true;
	}
	
	/**
	 * @internal
	 */
	public function process_in(Param $param, $database) {
		switch ($database) {
			default:
				$param->pdotype = \PDO::PARAM_STR;
				$param->xsqlitype = "s";
		}
	}
}
