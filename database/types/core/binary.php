<?php
/**
 * Database types: Binary.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

/**
 * Database Type for binary data (BLOB, BYTEA etc.)
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_Binary extends Type {
	/**
	 * @internal
	 */
	public function get_db_type($database) {
		switch ($database) {
			case Wrapper::DATABASE_POSTGRESQL:
				return "BYTEA";
			case Wrapper::DATABASE_MYSQL:
				return "BLOB";
		}
		throw new Exception("This type has not been implemented for this database yet", $database);
	}

	/**
	 * @internal
	 */
	public function process_in(Param $param, $database) {
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
				$param->pdotype = \PDO::PARAM_STR;
				break;
			case Wrapper::DATABASE_POSTGRESQL:
				$param->pdotype = \PDO::PARAM_LOB;
				break;
		}
		$param->xsqlitype = "s";
	}
}
