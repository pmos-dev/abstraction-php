<?php
/**
 * Database types: numeric superclass.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";

use \Abstraction\Data as Data;

/**
 * Database superclass Type for all numeric data
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Type_Numeric extends Type {
	const MAX_SIGNED_32BIT_INTEGER = 2147483647;
	
	const UNSIGNED = true;
	const SIGNED = false;

	protected $unsigned;
	protected $upper, $lower;

	/**
	 * Constructs a new instance of the type
	 * 
	 * @param int|float $lower the lower range limit for values
	 * @param int|float $upper the upper range limit for values
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param int|float|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($lower, $upper, $not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct($not_null, $default);

		if (!Data\Data::validate_numeric($lower)) throw new Exception("The lower range is not a number");
		if (!Data\Data::validate_numeric($upper)) throw new Exception("The upper range is not a number");
		
		if ($upper > self::MAX_SIGNED_32BIT_INTEGER) throw new Exception("Type upper range is greater than signed 32 bit integer support");
		if ($lower < -self::MAX_SIGNED_32BIT_INTEGER) throw new Exception("Type lower range is less than signed 32 bit integer support");
		
		$this->lower = $lower;
		$this->upper = $upper;
	}

	/**
	 * Returns any CREATE TABLE definitions for signage (used by MySQL)
	 * 
	 * @internal
	 * @param int $database one of the DATABASE_* constants defined in Wrapper to specify the database engine
	 * @return string
	 */
	protected function get_signing($database) {
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
				if ($this->lower >= 0) return " UNSIGNED";
			case Wrapper::DATABASE_POSTGRESQL:
			case Wrapper::DATABASE_MSSQL:
		}
		return "";
	}
	
	/**
	 * Returns any CREATE TABLE ... CHECK definitions for ranges (used by Postgres)
	 * 
	 * @internal
	 * @param fieldname the name of the field using the type
	 * @param database one of the DATABASE_* constants defined in Wrapper to specify the database engine
	 * @return string
	 */
	public function get_check($fieldname, $database) {
		switch ($database) {
			case Wrapper::DATABASE_POSTGRESQL:
				if ($this->unsigned && ($this->upper < 0 || $this->lower < 0)) throw new Exception("Unsigned numerics cannot be assigned a negative range");
				return " CHECK (" . Wrapper::_delimit($fieldname, $database) . ">={$this->lower} AND " . Wrapper::_delimit($fieldname, $database) . "<={$this->upper})";
			case Wrapper::DATABASE_MYSQL:
			case Wrapper::DATABASE_MSSQL:
		}
		return "";
	}

	/**
	 * Validates that the given value is within the range limit of this type.
	 * 
	 * @param int|float $value the value to check
	 * @throws TypeMismatchException
	 * @return void
	 */
	protected function assert_range(&$value) {
		if ($value < $this->lower) throw new TypeMismatchException("<numeric>", $value, "Underflow from {$this->lower}");
		if ($value > $this->upper) throw new TypeMismatchException("<numeric>", $value, "Overflow from {$this->upper}");
	}
}
