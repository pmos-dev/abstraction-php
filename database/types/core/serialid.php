<?php
/**
 * Database types: SerialId.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/types/custom/id.php";

/**
 * Database Type for serial auto-generated IDs (SERIAL, INT AUTO_INCREMENT etc.)
 * 
 * NB, this type is implicitly a PRIMARY KEY for the table.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_SerialId extends Type_Id {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @throws Exception
	 */
	public function __construct() {
		parent::__construct(self::NOT_NULL);
	}

	/**
	 * @internal
	 */
	public function get_db_type($database) {
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
				return "INT UNSIGNED AUTO_INCREMENT PRIMARY KEY";
			case Wrapper::DATABASE_POSTGRESQL:
				return "SERIAL UNIQUE PRIMARY KEY";
			case Wrapper::DATABASE_MSSQL:
		}
		throw new Exception("This type has not been implemented for this database yet", $database);
	}
	
	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		if (!is_int($value)) throw new TypeMismatchException("ID", $value);
		$this->assert_range($value);
	 
		return true;
	}
}
