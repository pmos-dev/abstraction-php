<?php
/**
 * Database types: Time.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

/**
 * Database Type for times (TIME etc.)
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_Time extends Type {
	/**
	 * @internal
	 */
	public function get_db_type($database) {
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
			case Wrapper::DATABASE_POSTGRESQL:
			case Wrapper::DATABASE_MSSQL:
				return "TIME";
		}
		throw new Exception("This type has not been implemented for this database yet", $database);
	}

	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		if (!is_string($value)) throw new TypeMismatchException("TIME", $value);
		if (!preg_match("`^[0-9]{2}:[0-9]{2}:[0-9]{2}$`SD", $value)) throw new TypeMismatchException("TIME", $value);
	 
		return true;
	}
	
	/**
	 * @internal
	 */
	public function process_in(Param $param, $database) {
		switch ($database) {
			default:
				$param->pdotype = \PDO::PARAM_STR;
				$param->xsqlitype = "s";
		}
	}
}
