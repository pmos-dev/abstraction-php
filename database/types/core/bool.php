<?php
/**
 * Database types: Boolean.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

/**
 * Database Type for boolean data (BOOL etc.)
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_Bool extends Type {
	/**
	 * @internal
	 */
	public function get_db_type($database) {
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
			case Wrapper::DATABASE_POSTGRESQL:
			case Wrapper::DATABASE_MSSQL:
				return "BOOL";
		}
		throw new Exception("This type has not been implemented for this database yet", $database);
	}

	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		if (!is_bool($value)) throw new TypeMismatchException("BOOL", $value);
		
		return true;
	}

	/**
	 * @internal
	 */
	public function render($database) {
		$modifiers = "";
		if ($this->not_null) $modifiers = " NOT NULL";
	
		$default = "";
		if ($this->default !== null) $default = " DEFAULT " . ($this->default ? "TRUE" : "FALSE");
		else {
			if (!$this->not_null) $default = " DEFAULT NULL";
		}
		
		if ($this->primary_key) $default .= " PRIMARY KEY";
	
		return $this->get_db_type($database) . "{$modifiers}{$default}";
	}
	
	/**
	 * @internal
	 */
	public function process_in(Param $param, $database) {
		switch ($database) {
			default:
				$param->pdotype = \PDO::PARAM_BOOL;
				$param->xsqlitype = "i";
		}
		
		if ($param->value === null) return;
		$param->value = $param->value ? 1 : 0;
	}
	
	/**
	 * @internal
	 */
	public function process_out(&$value) {
		if (!is_bool($value) && $value !== "true" && $value !== "false" && $value !== 1 && $value !== 0 && $value !== "1" && $value !== "0") throw new TypeMismatchException("BOOL", $value);
		$value = $value ? true : false;
	}
}
