<?php
/**
 * Database types: InetAddr.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

use \Abstraction\Data as Data;

/**
 * Database Type for network IP addresses (InetAddr etc.)
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_InetAddr extends Type {
	/**
	 * @internal
	 */
	public function get_db_type($database) {
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
			case Wrapper::DATABASE_MSSQL:
				return "VARCHAR(15)";
			case Wrapper::DATABASE_POSTGRESQL:
				return "inet";
		}
		throw new Exception("This type has not been implemented for this database yet", $database);
	}

	/**
	 * @internal
	 * @var array $array
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		if (!is_string($value)) throw new TypeMismatchException("INETADDR", $value);
		if (!preg_match("`^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})(/([0-9]{1,2}))?$`SD", $value, $array)) throw new TypeMismatchException("INETADDR", $value);
		
		$array[2] *= 1;
		$array[3] *= 1;
		$array[4] *= 1;
		for ($i = 1; $i <= 4; $i++) {
			if (!Data\Data::validate_int($array[$i], 0, 255)) throw new TypeMismatchException("INETADDR", $value, "IP address digits are invalid");
		}
		
		if (isset($array[5]) && $array[5] !== null && $array[5] !== "") {
			if (!Data\Data::validate_int($array[6], 32, 32)) throw new TypeMismatchException("INETADDR", $value, "Subnets for InetAddr must be host-only, i.e. 32");
		}
		
		$value = "{$array[1]}.{$array[2]}.{$array[3]}.{$array[4]}";
		
		return true;
	}
	
	/**
	 * @internal
	 */
	public function process_in(Param $param, $database) {
		switch ($database) {
			default:
				$param->pdotype = \PDO::PARAM_STR;
				$param->xsqlitype = "s";
		}
	}
}
