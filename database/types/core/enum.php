<?php
/**
 * Database types: Enum.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

/**
 * Database Type for enumerated String data (ENUM, CREATE TYPE etc.)
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_Enum extends Type {
	private $options;
	private $enum_id;

	/**
	 * Constructs a new instance of the type
	 * 
	 * @param string[] $options available values for the enumeration
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @param string|NULL $enum_id unique identifier for Postgres in order to automatically CREATE TYPE
	 * @throws Exception
	 */
	public function __construct(array $options, $not_null = self::ALLOW_NULL, $default = null, $enum_id = null) {
		parent::__construct($not_null, $default);

		if (sizeof($options) === 0) throw new Exception("No options supplied for ENUM type");
		$this->options = $options;
		
		if ($default !== null) if (!in_array($default, $options)) throw new Exception("Default value is not in the listed options");
		
		$this->enum_id = $enum_id;
	}

	/**
	 * Returns the CREATE TYPE definition for needed Postgres and related to implement the enumeration, or null if unnecessary.
	 * 
	 * This method is used automatically by Wrapper create_table method and should not be called explicitly.
	 *
	 * @internal
	 * @param int $database one of the DATABASE_* constants defined in Wrapper to specify the database engine
	 * @throws Exception
	 * @return string|NULL
	 */
	public function get_create_enum($database) {
		switch ($database) {
			case Wrapper::DATABASE_POSTGRESQL:
				if ($this->enum_id === null) throw new Exception("Postgres ENUM types need to have enum_id names defined");

				$os = array();
				foreach ($this->options as $option) $os[] = Wrapper::_quote($option, $database);
				return "CREATE TYPE " . Wrapper::_delimit($this->enum_id, $database) . " AS ENUM(" . implode(",", $os) . ")";
			case Wrapper::DATABASE_MYSQL:
			case Wrapper::DATABASE_MSSQL:
		}
		return null;
	}
	
	/**
	 * Returns the DROP TYPE definition for needed Postgres and related to implement the enumeration, or null if unnecessary.
	 * 
	 * This method is used automatically by Wrapper drop_table method and should not be called explicitly.
	 *
	 * @internal
	 * @param int $database one of the DATABASE_* constants defined in Wrapper to specify the database engine
	 * @throws Exception
	 * @return string|NULL
	 */
	public function get_drop_enum($database) {
		switch ($database) {
			case Wrapper::DATABASE_POSTGRESQL:
				if ($this->enum_id === null) throw new Exception("Postgres ENUM types need to have enum_id names defined");

				return "DROP TYPE " . Wrapper::_delimit($this->enum_id, $database);
			case Wrapper::DATABASE_MYSQL:
			case Wrapper::DATABASE_MSSQL:
		}
		return null;
	}
	
	/**
	 * @internal
	 */
	public function get_db_type($database) {
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
				$os = array();
				foreach ($this->options as $option) $os[] = Wrapper::_quote($option, $database);
				return "ENUM(" . implode(",", $os) . ")";
			case Wrapper::DATABASE_POSTGRESQL:
				return Wrapper::_delimit($this->enum_id, $database);
			case Wrapper::DATABASE_MSSQL:
		}
		throw new Exception("This type has not been implemented for this database yet", $database);
	}

	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		if (!is_string($value)) throw new TypeMismatchException("ENUM", $value);
		if (strlen($value) > 255) throw new TypeMismatchException("ENUM", $value, "Greater than 255 characters. Not supported");
		if (!in_array($value, $this->options)) throw new TypeMismatchException("ENUM", $value, "No such option in the ENUM array");

		return true;
	}

	/**
	 * Returns the available strings permitted by this enumeration.
	 * 
	 * @return string[]:
	 */
	public function get_options() {
		return $this->options;
	}

	/**
	 * @internal
	 */
	public function process_in(Param $param, $database) {
		switch ($database) {
			default:
				$param->pdotype = \PDO::PARAM_STR;
				$param->xsqlitype = "s";
		}
	}
}
