<?php
/**
 * Database types: Small integer.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/types/core/numeric.php";

/**
 * Database Type for SmallInt data (SMALLINT etc.)
 *
 * Small integer range is defined as -32768 to 32737 for signed and 0 to 65535 for unsigned.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_SmallInt extends Type_Numeric {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param boolean $unsigned either SIGNED or UNSIGNED to determine acceptance or rejection of negative (signed) values
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param int|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($unsigned = self::SIGNED, $not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct($unsigned ? 0 : -32768, $unsigned ? 65535 : 32767, $not_null, $default);
	}

	/**
	 * @internal
	 */
	public function get_db_type($database) {
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
			case Wrapper::DATABASE_POSTGRESQL:
			case Wrapper::DATABASE_MSSQL:
				return "SMALLINT" . $this->get_signing($database);
		}
		throw new Exception("This type has not been implemented for this database yet", $database);
	}
	
	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		if (!is_int($value)) throw new TypeMismatchException("SMALLINT", $value);
		$this->assert_range($value);
	 
		return true;
	}
	
	/**
	 * @internal
	 */
	public function process_in(Param $param, $database) {
		switch ($database) {
			default:
				$param->pdotype = \PDO::PARAM_INT;
				$param->xsqlitype = "i";
		}
	}
	
	/**
	 * @internal
	 */
	public function process_out(&$value) {
		if (!is_int($value) && !ctype_digit($value) && !preg_match("`^-?[0-9]+$`SD", $value)) throw new TypeMismatchException("INT", $value);
		settype($value, "int");
	}
}
