<?php
/**
 * Database types: Base36BigId.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "data/base.php";
require_once ABSTRACTION_ROOT_PATH . "database/types/core/string.php";

use \Abstraction\Data as Data;

/**
 * Database Type for hexidecimal-only character strings.
 * 
 * As per string, there is a hard limit of 255 characters.
 * 
 * @api
 * @author Pete Morris
 * @version 1.3.0
 */
class Type_HexString extends Type_String {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param int $max_length the maximum length of string value to allow
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($max_length, $not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct($max_length, $not_null, $default);
	}

	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		if (!preg_match(Data\Data::REGEX_PATTERN_HEXSTRING, $value)) throw new TypeMismatchException("HexString", $value);
		$value = strtolower($value);
		
		return true;
	}
}
