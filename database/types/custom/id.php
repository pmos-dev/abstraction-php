<?php
/**
 * Database types: ID.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/types/core/numeric.php";

/**
 * Database Type for integer Id key fields
 * 
 * Note, Id types are lower range bound at 1 rather than 0.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_Id extends Type_Numeric {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param int|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct(1, self::MAX_SIGNED_32BIT_INTEGER, $not_null, $default);
	}

	/**
	 * @internal
	 */
	public function get_db_type($database) {
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
			case Wrapper::DATABASE_POSTGRESQL:
			case Wrapper::DATABASE_MSSQL:
				return "INT" . $this->get_signing($database);
		}
		throw new Exception("This type has not been implemented for this database yet", $database);
	}
	
	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		if (!is_int($value)) throw new TypeMismatchException("ID", $value);
		$this->assert_range($value);
	 
		return true;
	}

	/**
	 * @internal
	 */
	public function process_in(Param $param, $database) {
		switch ($database) {
			default:
				$param->pdotype = \PDO::PARAM_INT;
				$param->xsqlitype = "i";
		}
	}
	
	/**
	 * @internal
	 */
	public function process_out(&$value) {
		if (!is_int($value) && !ctype_digit($value) && !preg_match("`^[0-9]+$`SD", $value)) throw new TypeMismatchException("INT", $value);
		settype($value, "int");
	}
}
