<?php
/**
 * Database types: IdName.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/types/core/string.php";

use \Abstraction\Data as Data;

/**
 * Database Type for limited character set data used as names etc.
 * 
 * Note, IdName is an Abstraction Data naming convention rather than an indication of the field being intended for indexing.
 * A hard upper limit of 64 characters is permitted by this data type.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_IdName extends Type_String {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct(64, $not_null, $default);
	}

	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		$value = trim($value);
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $value)) throw new TypeMismatchException("IdName", $value);
		
		return true;
	}
}
