<?php
/**
 * Database types: HexColor.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/types/custom/hexstring.php";

use \Abstraction\Data as Data;

/**
 * Database Type for hexidecimal encoded colors (i.e. 6 hex characters, 2 each to define R, G and B).
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_HexColor extends Type_HexString {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct(6, $not_null, $default);
	}

	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		if (!preg_match(Data\Data::REGEX_PATTERN_HEXCOLOR, $value)) throw new TypeMismatchException("HexColor", $value);
		$value = strtolower($value);
		
		return true;
	}
}
