<?php
/**
 * Database types: Base41BigId.
 * 
 * @copyright 2016 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/types/core/string.php";

use \Abstraction\Data as Data;

/**
 * Database Type for very large unique identifier data, encoded as base 41 (a-z, A-Z, 0-9).
 * 
 * Note, base 41 identifiers are always exactly 6 characters long, which is less than the base 36 and base 62 address spaces.
 * This means that there are a potential greater chance of collisions, although the address space (32-bit equivalent) should be fine for most applications.
 * 
 * @api
 * @author Pete Morris
 * @version 1.0.0
 */
class Type_Base41BigId extends Type_String {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct(6, $not_null, $default);
	}

	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		if (!preg_match(Data\Data::REGEX_PATTERN_BASE41_ID, $value)) throw new TypeMismatchException("Base41BigId", $value);
		
		return true;
	}
	
	/**
	 * Returns a randomly generated unique identifier encoded in base 41 format.
	 * @return string a 6 character value
	 */
	public static function generate_random_base41_id() {
		return Data\Base41::generate_random_base41_id();
	}
}
