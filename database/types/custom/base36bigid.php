<?php
/**
 * Database types: Base36BigId.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "data/base.php";
require_once ABSTRACTION_ROOT_PATH . "database/types/core/string.php";

use \Abstraction\Data as Data;

/**
 * Database Type for very large unique identifier data, encoded as base 36 (A-Z, 0-9).
 * 
 * Note, base 36 identifiers are always exactly 10 characters long.
 * NB, the capitalisation for Base36 seems to have changed around 2016 from lowercase to uppercase, so have to included a new 'true' for Base64BigId in order to ignore case
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_Base36BigId extends Type_String {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct(10, $not_null, $default);
	}

	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		if (!preg_match(Data\Data::REGEX_PATTERN_BASE36_ID, $value)) throw new TypeMismatchException("Base36BigId", $value);
		
		return true;
	}

	/**
	 * Returns a randomly generated unique identifier encoded in base 36 format.
	 * @return string a 10 character value
	 */
	public static function generate_random_base36_id() {
		return Data\Base36::generate_random_base36_id();
	}
}
