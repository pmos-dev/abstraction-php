<?php
/**
 * Database types: IdChar, Digit, Letter and LetterDigit.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/types/core/string.php";

use \Abstraction\Data as Data;

/**
 * Database Type for single character data.
 * 
 * Note, IdChar is an Abstraction Data naming convention rather than an indication of the field being intended for indexing.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_IdChar extends Type_String {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct(1, $not_null, $default);
	}

	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		if (!preg_match(Data\Data::REGEX_PATTERN_ID_CHAR, $value)) throw new TypeMismatchException("IdChar", $value);
		
		return true;
	}
}

/**
 * Database Type for single digits (as characters rather than numbers).
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_Digit extends Type_IdChar {
	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;
	
		if (!preg_match(Data\Data::REGEX_PATTERN_DIGIT, $value)) throw new TypeMismatchException("Digit", $value);
	
		return true;
	}
}

/**
 * Database Type for single letters (A-Z uppercase).
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_Letter extends Type_IdChar {
	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		if (!preg_match(Data\Data::REGEX_PATTERN_LETTER, $value)) throw new TypeMismatchException("Letter", $value);

		return true;
	}
}

/**
 * Database Type for single letters and digits (A-Z uppercase, 0-9).
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_LetterDigit extends Type_IdChar {
	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		if (!preg_match(Data\Data::REGEX_PATTERN_LETTERDIGIT, $value)) throw new TypeMismatchException("LetterDigit", $value);

		return true;
	}
}
