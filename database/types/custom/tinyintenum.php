<?php
/**
 * Database types: TinyIntEnum.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/types/custom/tinyint.php";

use \Abstraction\Data as Data;

/**
 * Database Type for enumerated TinyInt data.
 * 
 * Note, this is performed using the TinyInt type and PHP-side range checking rather than the Enum type.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_TinyIntEnum extends Type_TinyInt {
	private $options;
	
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param int[] $options available values for the enumeration
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param int|NULL $default default value, if any
	 * @param string|NULL $enum_id unique identifier for Postgres in order to automatically CREATE TYPE
	 * @throws Exception
	 */
	public function __construct(array $options, $not_null = self::ALLOW_NULL, $default = null, $enum_id = null) {
		parent::__construct(self::UNSIGNED, $not_null, $default);
		
		if (sizeof($options) === 0) throw new Exception("No options supplied for TINYINTENUM type");

		foreach ($options as &$option) if (!Data\Data::validate_int($option, 0, 255)) throw new Exception("Supplied option is not a TinyInt value");
		unset($option);
		
		$this->options = $options;
		
		$this->enum_id = $enum_id;
	}

	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		if (!in_array($value, $this->options)) throw new TypeMismatchException("TINYINTENUM", $value, "No such option in the TINYINTENUM array");
		
		return true;
	}
}
