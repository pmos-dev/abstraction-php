<?php
/**
 * Database types: Base62BigId.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/types/core/string.php";

use \Abstraction\Data as Data;

/**
 * Database Type for very large unique identifier data, encoded as base 62 (0-9, A-Z, a-z).
 * 
 * Note, base 62 identifiers are always exactly 8 characters long, which strictly speaking is less than the base 36 address space (36^10 vs 62^8), but to all intents and purposes is fine.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_Base62BigId extends Type_String {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct(8, $not_null, $default);
	}

	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		if (!preg_match(Data\Data::REGEX_PATTERN_BASE62_ID, $value)) throw new TypeMismatchException("Base62BigId", $value);
		
		return true;
	}
	
	/**
	 * Returns a randomly generated unique identifier encoded in base 62 format.
	 * @return string a 8 character value
	 */
	public static function generate_random_base62_id() {
		return Data\Base62::generate_random_base62_id();
	}
}
