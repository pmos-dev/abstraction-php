<?php
/**
 * Database types: Encrypted.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/crypt.php";
require_once ABSTRACTION_ROOT_PATH . "database/types/core/binary.php";

use \Abstraction\Data as Data;

/**
 * Database Type for storing arbitrary values in an encrypted form in the database.
 * 
 * Symmetric key encryption is used, using a shared passphrase.
 * Note, encryption and decryption is performed by the Abstraction type and is not native to the database engine.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_Encrypted extends Type_Binary {
	private $passphrase;
	
	/**
	 * Constructs a new instance of the type
	 *
	 * @param string $passphrase the shared passphrase
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any. This is not really applicable and should not be used.
	 * @throws Exception
	 */
	public function __construct($passphrase, $not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct($not_null, $default);
		
		$this->passphrase = $passphrase;
	}

	/**
	 * Encrypts the given value using symmetric key encryption and the stored passphrase.
	 * 
	 * This function is called automatically by process_in and should not be called directly.
	 * 
	 * @param mixed $value
	 * @return boolean
	 */
	public function encrypt(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		$salt = substr(md5(mt_rand() . microtime() . mt_rand()), 0, self::SALT_LENGTH);

		$value = "CRYPT:" . md5(hash("sha512", $salt . $this->passphrase, false)) . ":" . md5(hash("sha512", $salt . $value, false)) . ":" . $salt . ":" . Data\Crypt::symmetric_key_encrypt($value, $salt . $this->passphrase);
		
		return true;
	}

	const MD5_LENGTH = 32;
	const SALT_LENGTH = 8;
	const INDEX_PASSPHRASE_CHECKSUM = 6;	// 5 + 1
	const INDEX_VALUE_CHECKSUM = 39;	// 5 + 1 + 32 + 1
	const INDEX_SALT = 72;	// 5 + 1 + 32 + 1 + 32 + 1
	const INDEX_ENCRYPTED = 81;	// 5 + 1 + 32 + 1 + 32 + 1 + 8 + 1;
	
	/**
	 * Decrypts the given value using symmetric key encryption and the stored passphrase.
	 * 
	 * This function is called automatically by process_out and should not be called directly.
	 * 
	 * @param mixed $value
	 * @throws TypeMismatchException
	 * @return boolean
	 */
	public function decrypt(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		if (substr($value, 0, 5) !== "CRYPT") throw new TypeMismatchException("ENCRYPTED", $value, "Encrypted 'CRYPT' preface is not present; this value does not appear to be encrypted!");

		$salt = substr($value, self::INDEX_SALT, self::SALT_LENGTH);
		
		if (md5(hash("sha512", $salt . $this->passphrase, false)) !== substr($value, self::INDEX_PASSPHRASE_CHECKSUM, self::MD5_LENGTH)) throw new TypeMismatchException("ENCRYPTED", $value, "Passphrase checksum does not match. This data has been encrypted with a different passphrase.");

		$checksum = substr($value, self::INDEX_VALUE_CHECKSUM, self::MD5_LENGTH);
		$encrypted = substr($value, self::INDEX_ENCRYPTED);

		$value = Data\Crypt::symmetric_key_decrypt($encrypted, $salt . $this->passphrase);
		
		if (md5(hash("sha512", $salt . $value, false)) !== $checksum) throw new TypeMismatchException("ENCRYPTED", $value, "Data value checksum does not match. Unable to decrypt this data.");
		
		return true;
	}
	
	/**
	 * @internal
	 */
	public function process_in(Param $param, $database) {
		parent::process_in($param, $database);
			
		if ($param->value === null) return;
		$this->encrypt($param->value);
	}
	
	/**
	 * @internal
	 */
	public function process_out(&$value) {
		$this->decrypt($value);
	}
}
