<?php
/**
 * Database types: JSON.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/types/core/text.php";

/**
 * Database Type for storing arbitrary values within the database as structured JSON data.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_JSON extends Type_Text {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct($not_null, $default);
	}
	
	/**
	 * Encodes the given data into a JSON structure.
	 * 
	 * Null values are simply returned so as to allow for NOT_NULL checking.
	 * This function is called automatically by process_in and should not be called directly.
	 * 
	 * @param mixed $value the value to encode
	 * @return boolean always true as failure would throw an exception
	 */
	public function encode(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		$value = json_encode($value);
		
		return true;
	}
	
	/**
	 * Decodes the given JSON structure back into raw data.
	 * 
	 * Null values are simply returned so as to allow for NOT_NULL checking.
	 * This function is called automatically by process_out and should not be called directly.
	 * 
	 * @param mixed $value the value to decode
	 * @return boolean always true as failure would throw an exception
	 */
	public function decode(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		$value = json_decode($value, true);
		
		return true;
	}
	
	/**
	 * @internal
	 */
	public function process_in(Param $param, $database) {
		parent::process_in($param, $database);
			
		if ($param->value === null) return;
		$this->encode($param->value);
	}
	
	/**
	 * @internal
	 */
	public function process_out(&$value) {
		$this->decode($value);
	}
}
