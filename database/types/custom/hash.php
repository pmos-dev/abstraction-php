<?php
/**
 * Database types: Hash, MD5, SHA224, SHA256, SHA512.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/types/core/string.php";

/**
 * Database Type for storing hash values.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Type_Hash extends Type_String {
	private $name;
	
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param string $name hash algorithm to use
	 * @param int $length number of characters required to store the hash
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($name, $length, $not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct($length, $not_null, $default);
		
		$this->name = $name;
	}
	
	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;
		
		if (!is_string($value)) throw new TypeMismatchException("HASH", $value);
		if (!preg_match("`^[0-9a-f]{{$this->max_length}}$`SD", $value)) throw new TypeMismatchException("HASH", $value);
		
		return true;
	}

	/**
	 * Returns the hash of the given value using the hash function of the type subclass.
	 * @param mixed $value the value to hash
	 * @return string
	 */
	public function hash($value) {
		return hash($this->name, $value, false);
	}
}

/**
 * Database Type for storing MD5 hashes.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_MD5 extends Type_Hash {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct("md5", 32, $not_null, $default);
	}
}

/**
 * Database Type for storing 224-bit SHA hashes.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_SHA224 extends Type_Hash {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct("sha224", 56, $not_null, $default);
	}
}

/**
 * Database Type for storing 256-bit SHA hashes.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_SHA256 extends Type_Hash {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct("sha256", 64, $not_null, $default);
	}
}

/**
 * Database Type for storing 512-bit SHA hashes.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_SHA512 extends Type_Hash {
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct("sha512", 128, $not_null, $default);
	}
}
