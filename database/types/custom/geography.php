<?php
/**
 * Database types: Geography.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/types/core/string.php";

use \Abstraction\Data as Data;

/**
 * Database Type for long/lat co-ordinates.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_Geography extends Type_String {
	const REGEX_PATTERN_GEOGRAPHY = "`^(-?[0-9]{1,3}\.[0-9]{1,6}) (-?[0-9]{1,3}\.[0-9]{1,6})$`";
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct(23, $not_null, $default);
	}
	
	/**
	 * @internal
	 */
	public function get_db_type($database) {
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
			case Wrapper::DATABASE_MSSQL:
				return "VARCHAR(23)";
			case Wrapper::DATABASE_POSTGRESQL:
				return "geography";
		}
		throw new Exception("This type has not been implemented for this database yet", $database);
	}
	
	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		if (!preg_match(self::REGEX_PATTERN_GEOGRAPHY, $value)) throw new TypeMismatchException("Geography", $value);
		
		return true;
	}
	
	private static function encode_geography($value) {
		if (!Data\Data::validate_regex(self::REGEX_PATTERN_GEOGRAPHY, $value)) throw new Exception("Unable to encode geography");
		
		list($long, $lat) = explode(" ", $value);
		
		$long = sprintf('%F', $long);
		$lat = sprintf('%F', $lat);
		
		return "{$long} {$lat}";
	}
	
	private static function parse_geography($encoded) {
		if (!Data\Data::validate_regex(self::REGEX_PATTERN_GEOGRAPHY, $encoded)) return null;
		
		list($long, $lat) = explode(" ", $encoded);
		
		$long = rtrim($long, "0");
		if (preg_match("`\.$`", $long)) $long .= "0";
		$lat = rtrim($lat, "0");
		if (preg_match("`\.$`", $lat)) $lat .= "0";

		return "{$long} {$lat}";
	}
	
	/**
	 * @internal
	 */
	public function process_in(Param $param, $database) {
		$param->pdotype = \PDO::PARAM_STR;
		$param->xsqlitype = "s";
		
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
			case Wrapper::DATABASE_MSSQL:
				$param->value = self::encode_geography($param->value);
				break;
			case Wrapper::DATABASE_POSTGRESQL:
				$param->value = "todo";
				break;
			default:
				throw new Exception("This type has not been implemented for this database yet", $database);
		}
	}

	/**
	 * @internal
	 */
	public function process_out(&$value) {
		if (Data\Data::validate_regex(self::REGEX_PATTERN_GEOGRAPHY, $value)) {
			$value = self::parse_geography($value);
		}
	}
}
