<?php
/**
 * Database types: URL.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/types/core/text.php";

use \Abstraction\Data as Data;

/**
 * Database Type for URLs.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_URL extends Type_Text {
	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		if (strlen($value) > 1024) throw new TypeMismatchException("URL", $value);
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_URL, $value)) throw new TypeMismatchException("URL", $value);
		
		return true;
	}
}
