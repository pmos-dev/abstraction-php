<?php
/**
 * Database types: Geometry.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/types/core/string.php";

/**
 * Database Type for x/y co-ordinates.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Type_Geometry extends Type_String {
	const REGEX_PATTERN_GEOMETRY = "`^(-?[0-9]+(?:\.[0-9]+)?) (-?[0-9]+(?:\.[0-9]+)?)$`";
	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param string|NULL $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		parent::__construct(33, $not_null, $default);
	}
	
	/**
	 * @internal
	 */
	public function get_db_type($database) {
		switch ($database) {
			case Wrapper::DATABASE_MYSQL:
			case Wrapper::DATABASE_MSSQL:
				return "VARCHAR(33)";
			case Wrapper::DATABASE_POSTGRESQL:
				return "geometry";
		}
		throw new Exception("This type has not been implemented for this database yet", $database);
	}
	
	/**
	 * @internal
	 */
	public function assert(&$value) {
		parent::assert($value);
		if ($value === null) return true;

		if (!preg_match(self::REGEX_PATTERN_GEOMETRY, $value)) throw new TypeMismatchException("Geometry", $value);
		
		return true;
	}
}
