<?php
/**
 * Database abstraction.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Database;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "config.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

const MAX_SIGNED_32BIT_INTEGER = 2147483647;

use \Abstraction as Abstraction;

/**
 * @internal
 */
class Exception extends Abstraction\Exception {}

/**
 * @internal
 */
class SQLException extends Exception {
	public function __construct($sql) {
		parent::__construct(Abstraction\EXCEPTIONS_SHOW_SQL_CODE ? $sql : "");
	}
}

/**
 * @internal
 */
class TypeMismatchException extends Abstraction\TypeMismatchException {}

/**
 * Root class of all strong Type_ classes used by the Abstraction framework
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Type {
	public $not_null;
	public $default;
	protected $primary_key = false;
	
	const ALLOW_NULL = false;
	const NOT_NULL = true;

	/**
	 * Constructs a new instance of the type
	 * 
	 * @param bool $not_null either ALLOW_NULL or NOT_NULL to determine acceptance or rejection of true null values
	 * @param mixed $default default value, if any
	 * @throws Exception
	 */
	public function __construct($not_null = self::ALLOW_NULL, $default = null) {
		if (!is_bool($not_null)) throw new Exception("NOT NULL parameter is not boolean", $not_null);
		
		$this->not_null = $not_null;
		$this->default = $default;
	}

	/**
	 * Returns the database engine's name for this type.
	 * 
	 * This method should be overridden by subclasses.
	 * 
	 * @param int $database one of the DATABASE_* constants defined in Wrapper to specify the database engine
	 * @return string
	 */
	abstract public function get_db_type($database);

	/**
	 * Flag this type as also being a primary key.
	 * 
	 * @return void
	 */
	public function set_primary_key() {
		$this->primary_key = true;
	}

	/**
	 * Returns a representation of the type suitable for use in CREATE TABLE statements for the given database engine
	 * 
	 * @param int $database one of the DATABASE_* constants defined in Wrapper to specify the database engine
	 * @return string
	 */
	public function render($database) {
		$modifiers = "";
		if ($this->not_null) $modifiers = " NOT NULL";
	
		$default = "";
		if ($this->default !== null) $default = " DEFAULT " . Wrapper::_quote($this->default, $database);
		else {
			if (!$this->not_null) $default = " DEFAULT NULL";
		}
		if ($this->primary_key) $default .= " PRIMARY KEY";
	
		return $this->get_db_type($database) . "{$modifiers}{$default}";
	}

	/**
	 * Validates the given value against the type definition.
	 * 
	 * This method should be overridden by subclasses in order to perform meaningful validation for their type.
	 * 
	 * @param mixed $value the value to validate
	 * @throws TypeMismatchException thrown if the validation fails
	 * @return boolean
	 */
	public function assert(&$value) {
		if ($value === null && $this->not_null) throw new TypeMismatchException("", "NULL", "NOTNULL modifier is set");
		return true;
	}

	/**
	 * Applies any transformation required for incoming data (into database) to make it applicable for the type.
	 * 
	 * This method should be overridden by subclasses if necessary to perform any meaningful transformation.
	 * Also sets the various PDO and xSQLi types for the Param.
	 * 
	 * @param Param $param the host Param object containing the data
	 * @param int $database one of the DATABASE_* constants defined in Wrapper to specify the database engine
	 * @return void
	 */
	abstract public function process_in(Param $param, $database);

	/**
	 * Applies any transformation required for outgoing data (from database) to make it applicable for the type.
	 *  
	 * This method should be overridden by subclasses if necessary to perform any meaningful transformation.
	 * 
	 * @param mixed $value the raw data returned from the database
	 * @return void
	 */
	public function process_out(&$value) {}
}

// import all the various types
foreach (explode(",", "binary,bool,date,datetime,enum,float,inetaddr,int,numeric,serialid,string,text,time") as $type) require_once ABSTRACTION_ROOT_PATH . "database/types/core/{$type}.php";
foreach (explode(",", "base36bigid,base62bigid,base41bigid,email,encrypted,hash,hexcolor,hexstring,id,idchar,idname,json,smallint,tinyint,tinyintenum,url,geography,geometry") as $type) require_once ABSTRACTION_ROOT_PATH . "database/types/custom/{$type}.php";

/**
 * Combined storage for a value and its associated data type.
 * 
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class Param {
	public $type;
	public $pdotype;
	public $xsqlitype;
	public $value;

	public function __construct($value, Type $type) {
		$this->type = $type;
		$this->value = $value;
	}

	public function process($method, $database) {
		$this->type->assert($this->value);
		
		if ($this->value === null) {
			if ($method === Wrapper::METHOD_PDO) {
				$this->pdotype = \PDO::PARAM_NULL;
				return;
			}
		}

		$this->type->process_in($this, $database);
	}
}

/**
 * Combined storage for an array of values and their associated data type, for use in IN clauses
 * 
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class InArrayParam extends Param {
	public function process($method, $database) {
		throw new Exception("Attempting to process an InArrayParam. This should not be possible");
	}
}

/**
 * Database functionality wrapper.
 * 
 * Supports MySQL, MSSQL, Postgres and ODBC to various degrees. PDO is strongly recommended over xSQLi, since MSSQLi is no longer being actively maintained.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Wrapper {
	const DATABASE_MYSQL = 1;
	const DATABASE_MSSQL = 2;
	const DATABASE_POSTGRESQL = 3;
	const DATABASE_ODBC = 4;

	const METHOD_PDO = 10;
	
	// @phpcs:ignore Generic.NamingConventions.UpperCaseConstantName
	const METHOD_xSQLi = 11;
	
	const LOCK_READ = "READ";
	const LOCK_WRITE = "WRITE";

	private $xsqli;
	private $pdo;
	protected $db_type;
	private $method;
	
	private $preprepared;

	private $is_transaction_running;
	
	/**
	 * Constructs a new wrapper around the given database interface.
	 * 
	 * @param mixed $db an instance of PDO, mysqli or the deprecated MSSQLi class
	 * @throws Exception
	 */
	public function __construct($db) {
		if (!is_object($db)) throw new Exception("No database object supplied");

		if ($db instanceof \PDO) {
			switch ($driver = $db->getAttribute(\PDO::ATTR_DRIVER_NAME)) {
				case "mysql":
					$this->db_type = self::DATABASE_MYSQL; 
					break;
				case "pgsql":
					$this->db_type = self::DATABASE_POSTGRESQL; 
					break;
				case "mssql":	// NB this is not a valid driver at the moment; Linux support doesn't appear to be available
					$this->db_type = self::DATABASE_MSSQL; 
					break;
				case "odbc":
					$this->db_type = self::DATABASE_ODBC; 
					break;
				default: 
					throw new Exception("Unsupported database type: {$driver}"); 
			}

			$this->method = self::METHOD_PDO;
			$this->pdo = $db;
			$this->sqli = null;
		} elseif ($db instanceof \mysqli) {
			$this->method = self::METHOD_xSQLi;
			$this->pdo = null;
			$this->xsqli = $db;
		/*} elseif ($db instanceof \mssqli) {
			$this->method = self::METHOD_xSQLi;
			$this->pdo = null;
			$this->xsqli = $db;*/
		}

		$this->preprepared = array();

		$this->transaction = 0;
	}

	/**
	 * Returns the engine type of the underlying database interface.
	 * 
	 * One of various values:
	 * - DATABASE_MYSQL
	 * - DATABASE_MSSQL
	 * - DATABASE_POSTGRESQL
	 * - DATABASE_ODBC
	 * 
	 * @return string
	 */
	public function get_db_type() {
		return $this->db_type;
	}

	/**
	 * Forces a transaction to begin on the underlying database interface.
	 * 
	 * @throws Exception
	 */
	public function transaction_begin() {
		if ($this->is_transaction_running) throw new Exception("A transaction is already running");

		if ($this->method === self::METHOD_PDO) {
			if (false === $this->pdo->beginTransaction()) throw new Exception("Unable to begin transaction");
		} else throw new Exception("Transactions are not yet supported for this database type");

		$this->is_transaction_running = true;
	}

	/**
	 * Attempts to start a transaction on the underlying database interface if one isn't already running.
	 * 
	 * @return boolean whether a new transaction was started
	 */
	public function transaction_claim() {
		if (!$this->is_transaction_running) {
			$this->transaction_begin();
			return true;
		}
		return false;
	}

	/**
	 * Forces an existing running transaction on the underlying database interface to commit.
	 * 
	 * @throws Exception
	 */
	public function transaction_commit() {
		if (!$this->is_transaction_running) throw new Exception("No transaction is running");

		if ($this->method === self::METHOD_PDO) {
			if (false === $this->pdo->commit()) throw new Exception("Unable to commit transaction");
		} else throw new Exception("Transactions are not yet supported for this database type");

		$this->is_transaction_running = false;
	}

	/**
	 * Rolls back any running transactions on the underlying database interface.
	 * 
	 * @throws Exception
	 * @return boolean true if the rollback succeded, or false if no transaction was running
	 */
	public function transaction_rollback() {
		if (!$this->is_transaction_running) return false;

		if ($this->method === self::METHOD_PDO) {
			if (false === $this->pdo->rollback()) throw new Exception("Unable to rollback transaction");
		} else throw new Exception("Transactions are not yet supported for this database type");

		$this->is_transaction_running = false;
		
		return true;
	}

	/**
	 * Locks the given tables.
	 * 
	 * Takes an associated array of table names and lock types, e.g. LOCK_READ or LOCK_WRITE.
	 * 
	 * @param string[] $tables
	 * @throws Exception table locking is not supported by the underlying interface or the lock type is invalid
	 * @return boolean
	 */
	public function lock_tables(array $tables) {
		if (sizeof($tables) == 0) return false;
		
		$locks = array();
		foreach ($tables as $table => $type) {
			if ($type !== self::LOCK_READ && $type !== self::LOCK_WRITE) throw new Exception("Lock type is invalid", $type);
			$locks[] = $this->delimit($table) . " {$type}";
		}

		$locks = implode(",", $locks);
		if ($this->method === self::METHOD_PDO) {
			$this->pdo->exec("LOCK TABLES {$locks}");
		} else throw new Exception("Table locking is not supported yet for this database type");
		
		return true;
	}

	/**
	 * Unlocks any previously locked tables.
	 * 
	 * @throws Exception table locking is not supported by the underlying interface
	 * @return true always returns true regardless of action performed
	 */
	public function unlock_tables() {
		if ($this->method === self::METHOD_PDO) {
			$this->pdo->exec("UNLOCK TABLES");
		} else throw new Exception("Table locking is not supported yet for this database type");
		
		return true;
	}

	/**
	 * Forces a database result value through its corresponding type validator.
	 * 
	 * @param mixed $value
	 * @param Type $type
	 * @return true always returns true, as mismatches are thrown via TypeMismatchException
	 */
	private function strongtype_result(&$value, Type $type) {
		if ($value !== null) $type->process_out($value);
		
		$type->assert($value);
		
		return true;
	}

	/**
	 * Predefines a piece of SQL for subsequent execution one or more times. Only available for PDO database interfaces.
	 * 
	 * NB, this is NOT the same as SQL prepared statements and stored procedures! Preprepare works purely at the Abstraction level, not the interface level.
	 * 
	 * @param string $name unique identifier for the predefined functionality
	 * @param string $sql the SQL definition
	 * @param Type[] $param_types any parameter types, using an associative array of param name => type format
	 * @param Type[] $result_types any result types, using an associative array of field name => type format
	 * @throws Exception
	 * @throws SQLException thrown if the underlying database interface rejects the SQL query
	 */
	public function preprepare($name, $sql, array $param_types = null, array $result_types = null) {
		if ($this->method !== self::METHOD_PDO) throw new Exception("Preprepared statements are only supported for PDO method");
		if (isset($this->preprepared[$name])) throw new Exception("Preprepared statement already exists for: {$name}");

		$sql = trim($sql);

		if (false === ($prepared = $this->pdo->prepare($sql))) throw new SQLException($sql);
		
		if ($result_types !== null) foreach ($result_types as $test) if (!($test instanceof Type)) throw new Exception("One or more result types is not an instance of Type");
		
		$this->preprepared[$name] = array(
			"statement" => &$prepared, 
			"param_types" => $param_types,
			"result_types" => $result_types,
			"is_select" => preg_match("`^SELECT `iD", $sql)
		);
	}

	/**
	 * Redefines a previously defined prepared with new SQL code, parameters or result types.
	 * 
	 * @param string $name the existing name of the prepared identifier
	 * @param string $sql the new SQL definition
	 * @param Type[] $param_types the new parameter types, using an associative array of param name => type format
	 * @param Type[] $result_types the new result types, using an associative array of field name => type format
	 * @throws Exception
	 * @throws SQLException thrown if the underlying database interface rejects the SQL query
	 */
	public function reprepare($name, $sql, array $param_types = null, array $result_types = null) {
		if ($this->method !== self::METHOD_PDO) throw new Exception("Preprepared statements are only supported for PDO method");
		if (!isset($this->preprepared[$name])) throw new Exception("Preprepared statement does not currently exists for: {$name}");

		$sql = trim($sql);

		if (false === ($prepared = $this->pdo->prepare($sql))) throw new SQLException($sql);
		
		if ($result_types !== null) foreach ($result_types as $test) if (!($test instanceof Type)) throw new Exception("One or more result types is not an instance of Type");
		
		$this->preprepared[$name] = array(
			"statement" => &$prepared, 
			"param_types" => $param_types,
			"result_types" => $result_types,
			"is_select" => preg_match("`^SELECT `iD", $sql)
		);
	}

	/**
	 * Executes a query previously defined using preprepare.
	 * 
	 * @param string $name the unique identifier of the query
	 * @param mixed[] $values any values to use as parameters, taking an associative array param name => value structure
	 * @throws Exception
	 * @throws SQLException thrown if the underlying database interface rejects the SQL query
	 * @return array|bool true for modifying queries, array of results for SELECTs
	 */
	public function execute_params($name, array $values = null) {
		if ($this->method !== self::METHOD_PDO) throw new Exception("Preprepared statements are only supported for PDO method");
		if (!isset($this->preprepared[$name])) throw new Exception("Preprepared statement does not exist");

		$prepared = &$this->preprepared[$name]["statement"];

		$params = array();
		if ($values !== null) foreach ($values as $var => &$value) {
			$params[$var] = new Param($value, $this->preprepared[$name]["param_types"][$var]);
			$params[$var]->process($this->method, $this->db_type);
			unset($value);
		}

		self::set_params_pdo($prepared, $params);

		if (false === $prepared->execute()) {
			ob_start(); 
			print_r($params); 
			$display = ob_get_clean();
			throw new SQLException("Error executing prepared query: {$name} : {$display}");
		}
		
		if (defined("SQL_DEBUG_FAKE_DELAY")) usleep(SQL_DEBUG_FAKE_DELAY);

		$results = self::handle_results_pdo($prepared, $this->preprepared[$name]["result_types"]);
		
		return $results;
	}

	/**
	 * Executes a query previously defined using preprepare, and only accepts a single matching row or none.
	 * 
	 * @param string $name the unique identifier of the query
	 * @param mixed[] $values any values to use as parameters, taking an associative array param name => value structure
	 * @throws Exception
	 * @return mixed[]|NULL
	 */
	public function execute_params_single($name, array $values = null) {
		$result = $this->execute_params($name, $values);
		if (!is_array($result)) throw new Exception("Result did not return rows");
		if (sizeof($result) == 0) return null;
		if (sizeof($result) != 1) throw new Exception("Result returned more than 1 row");
		
		return $result[0];
	}
	
	/**
	 * Executes a query previously defined using preprepare, and only accepts a single field column to generate a list (array).
	 * 
	 * @param string $name the unique identifier of the query
	 * @param mixed[] $values any values to use as parameters, taking an associative array param name => value structure
	 * @throws Exception
	 * @return array
	 */
	public function execute_params_list($name, array $values = null) {
		$results = $this->execute_params($name, $values);
		if (!is_array($results)) throw new Exception("Result did not return rows");

		$list = array();
		foreach ($results as $row) $list[] = array_shift($row);

		return $list;
	}

	/**
	 * Executes a query previously defined using preprepare, and only accepts a single value result.
	 * 
	 * @param string $name the unique identifier of the query
	 * @param mixed[] $values any values to use as parameters, taking an associative array param name => value structure
	 * @param bool $allow_null whether to allow no-matches to be returned as null instead of throwing an exception
	 * @throws Exception
	 * @return mixed|NULL
	 */
	public function execute_params_value($name, array $values = null, $allow_null = false) {
		$result = $this->execute_params_single($name, $values);
		if ($allow_null && ($result === null)) return null;
		if (sizeof($result) != 1) throw new Exception("Result is not a single value: " . sizeof($result));

		return array_shift($result);
	}

	/**
	 * @internal
	 * @param resource $prepared
	 * @param Param[] $params
	 * @return void
	 */
	private static function set_params_pdo(&$prepared, $params) {
		foreach ($params as $name => $object) $prepared->bindValue($name, $object->value, $object->pdotype);
	}

	/**
	 * @internal
	 * @param resource $prepared
	 * @param Param[] $params
	 * @param string[] $named_params
	 * @throws Exception
	 * @return void
	 */
	private static function set_params_xsqli(&$prepared, &$params, $named_params) {
		$types = "";
		$values = array();

		foreach ($named_params as $name) {
			if (!isset($params[$name])) throw new Exception("Named parameter {$name} is not present");
			$object = &$params[$name];

			$types .= $object->xsqlitype;
			$values[] = &$object->value;

			unset($object);
		}

		array_unshift($values, $types);

		if (false === call_user_func_array(array($prepared, "bind_param"), $values)) throw new Exception("Error binding parameters");
	}

	/**
	 * @internal
	 * @param resource $prepared
	 * @param Type[] $result_types
	 * @throws Exception
	 * @return boolean|mixed[]
	 */
	private function handle_results_pdo(&$prepared, array $result_types = null) {
		if ($result_types !== null) {
			if ($prepared->columnCount() != sizeof($result_types)) throw new Exception("Supplied result types is not the same length as actual results");
			foreach ($result_types as $name => $test) if (!($test instanceof Type)) throw new Exception("One or more result types is not an instance of Type");
		}
		
		if ($prepared->columnCount() == 0) return true;

		$field_names = array();
		for ($i = 0; $i < $prepared->columnCount(); $i++) {
			$meta = $prepared->getColumnMeta($i);
			$field_names[$i] = $meta["name"];
		}
		
		if ($result_types !== null) {
			foreach ($field_names as $incoming) if (!array_key_exists($incoming, $result_types)) throw new Exception("Returned row result field does not exist in the defined result structure", $incoming);
			foreach (array_keys($result_types) as $outgoing) if (!in_array($outgoing, $field_names)) throw new Exception("Defined result structure field is missing from the returned row results", $outgoing);
		}
		
		$results = array();
		while ($row = $prepared->fetch(\PDO::FETCH_ASSOC)) {
			if ($row === false) throw new Exception("An error occurred whilst fetching the results");
			$result = array();	// not 100% sure if the fetched row is 'bound', so to be safe we make a copy
			foreach ($row as $name => $value) {
				if ($this->db_type === self::DATABASE_POSTGRESQL && $result_types !== null && $result_types[$name] instanceof Type_Binary && is_resource($value)) {
					$value = stream_get_contents($value);
				}
				
				if ($result_types !== null) $this->strongtype_result($value, $result_types[$name]);
				$result[$name] = $value;
			}
			$results[] = $result;
		}

		return $results;
	}

	/**
	 * @internal
	 * @param resource $prepared
	 * @param Type[] $result_types
	 * @throws Exception
	 * @return boolean|mixed[]
	 */
	private function handle_results_xsqli(&$prepared, array $result_types = null) {
		if (null === ($metadata = $prepared->result_metadata())) return true;
		if (false === $metadata) return true;
		
		$fields = $metadata->fetch_fields();
		
		if ($result_types !== null) if (sizeof($fields) != sizeof($result_types)) throw new Exception("Supplied result types is not the same length as actual results");
		foreach (array_values($result_types) as $test) if (!($test instanceof Type)) throw new Exception("One or more result types is not an instance of Type");
		
		$field_names = array();
		$field_count = 0;
		foreach ($fields as $field) $field_names[$field_count++] = $field->name;
		
		if ($result_types !== null) {
			foreach ($field_names as $incoming) if (!array_key_exists($incoming, $result_types)) throw new Exception("Returned row result field does not exist in the defined result structure", $incoming);
			foreach (array_keys($result_types) as $outgoing) if (!in_array($outgoing, $field_names)) throw new Exception("Defined result structure field is missing from the returned row results", $outgoing);
		}
		
		$values = array(); 
		$bind = array();
		for ($i = $field_count, $j = 0; $i-- > 0; $j++) {
			$values[$j] = "";
			$bind[$j] = &$values[$j]; 
		}
		
		call_user_func_array(array($prepared, "bind_result"), $bind);
		
		$prepared->store_result();
		
		$results = array();
		while ($prepared->fetch()) {
			$result = array();
			for ($i = $field_count, $j = 0; $i-- > 0; $j++) {
				if ($result_types !== null) $this->strongtype_result($values[$i], $result_types[$field_names[$j]]);
				$result[$field_names[$j]] = $values[$j];
			}
			$results[] = $result;
		}

		return $results;
	}

	/**
	 * @internal
	 * @param string $sql
	 * @var array $array
	 * @return mixed[]
	 */
	private static function rewrite_named_params_sql(&$sql) {
		if (!preg_match_all("`(^|[^a-z0-9]):([a-z0-9]+)($|[^a-z0-9])`iD", $sql, $array)) return array();
		if (!isset($array[2])) return array();
		$params = $array[2];

		$sql = preg_replace("`(^|[^a-z0-9]):([a-z0-9]+)($|[^a-z0-9])`iD", "\\1?\\3", $sql);
		return $params;
	}

	/**
	 * Executes the specified SQL against the underlying database interface.
	 * 
	 * @param string $sql the SQL definition
	 * @param Param[] $params any parameter types, using an associative array of param name => param format
	 * @param Type[] $result_types any result types, using an associative array of field name => type format
	 * @throws Exception
	 * @return array|bool true for modifying queries, array of results for SELECTs
	 */
	public function query_params($sql, array $params = null, array $result_types = null) {
		$sql = trim($sql);

		// handle InArrayParams, which require SQL re-writing as well as parameter re-assigning
		if ($params !== null) {
			foreach ($params as $key => $object) {
				if (!is_object($object) || !($object instanceof Param)) throw new Exception("Supplied parameter is not an Param object");
				
				if ($object instanceof InArrayParam) {
					if (sizeof($object->value) === 0) {
						// empty array, so need to fudge a zero-length in array using null
						$params[$key] = new Param(null, new Type_String(1, Type::ALLOW_NULL));
						continue;
					}
					
					unset($params[$key]);
					
					$subkeys = array();
					$i = 0;
					foreach ($object->value as $v) {
						$subkey = ":${key}_${i}";
						$subkeys[] = $subkey;
						$params[$subkey] = new Param($v, $object->type);
						
						$i++;
					}
					$subkeys = implode(",", $subkeys);
					
					$sql = preg_replace("`(:${key})(\s|[,)]|$)`", "${subkeys}\\2", $sql);
				}
			}
		}
		
		if ($this->method === self::METHOD_PDO) if (false === ($prepared = $this->pdo->prepare($sql))) throw new Exception("Error in statement: {$sql}");
		if ($this->method === self::METHOD_xSQLi) {
			$named_params = self::rewrite_named_params_sql($sql);
			if (false === ($prepared = $this->xsqli->prepare($sql))) throw new Exception("Error in statement: {$sql}");
		}

		if ($params !== null) {
			foreach (array_values($params) as $object) {
				if (!is_object($object) || !($object instanceof Param)) throw new Exception("Supplied parameter is not an Param object");
				$object->process($this->method, $this->db_type);
			}

			if ($this->method === self::METHOD_PDO) self::set_params_pdo($prepared, $params);
			if ($this->method === self::METHOD_xSQLi) self::set_params_xsqli($prepared, $params, $named_params);
		}

		if (false === $prepared->execute()) {
			ob_start(); 
			print_r($params); 
			$display = ob_get_clean();
			throw new Exception("Error executing query: {$sql} : {$display}");
		}
		
		if (defined("SQL_DEBUG_FAKE_DELAY")) usleep(SQL_DEBUG_FAKE_DELAY);

		if ($this->method === self::METHOD_PDO) $results = self::handle_results_pdo($prepared, $result_types);
		if ($this->method === self::METHOD_xSQLi) $results = self::handle_results_xsqli($prepared, $result_types);

		return $results;
	}

	/**
	 * Executes the specified SQL against the underlying database interface, and only accepts a single matching row or none.
	 * 
	 * @param string $sql the SQL definition
	 * @param Param[] $params any parameter types, using an associative array of param name => param format
	 * @param Type[] $result_types any result types, using an associative array of field name => type format
	 * @throws Exception
	 * @return mixed[]|NULL array of results from the single row returned
	 */
	public function query_params_single($sql, array $params = null, array $result_types = null) {
		$result = $this->query_params($sql, $params, $result_types);
		if (!is_array($result)) throw new Exception("Result did not return rows");
		if (sizeof($result) == 0) return null;
		if (sizeof($result) != 1) throw new Exception("Result returned more than 1 row");
		
		return $result[0];
	}

	/**
	 * Executes the specified SQL against the underlying database interface, and only accepts a single field column to generate a list (array).
	 * 
	 * @param string $sql the SQL definition
	 * @param Param[] $params any parameter types, using an associative array of param name => param format
	 * @param string $result_field the fieldname of the result column
	 * @param Type $result_type the result type
	 * @throws Exception
	 * @return array
	 */
	public function query_params_list($sql, array $params = null, $result_field, Type $result_type = null) {
		if ($result_type !== null) $result_type = array($result_field => $result_type);
		
		$results = $this->query_params($sql, $params, $result_type);
		if (!is_array($results)) throw new Exception("Result did not return rows");

		$list = array();
		foreach ($results as $row) $list[] = $row[$result_field];

		return $list;
	}
	
	/**
	 * Executes the specified SQL against the underlying database interface, and only accepts a single value result.
	 * 
	 * @param string $sql the SQL definition
	 * @param Param[] $params any parameter types, using an associative array of param name => param format
	 * @param string $result_field the fieldname of the result column
	 * @param Type $result_type the result type
	 * @param bool $allow_null whether to allow no-matches to be returned as null instead of throwing an exception
	 * @throws Exception
	 * @return NULL|mixed
	 */
	public function query_params_value($sql, array $params = null, $result_field, Type $result_type = null, $allow_null = false) {
		if ($result_type !== null) $result_type = array($result_field => $result_type);
		
		$result = $this->query_params_single($sql, $params, $result_type);
		if ($allow_null && ($result === null)) return null;
		if (sizeof($result) != 1) throw new Exception("Result is not a single value: " . sizeof($result));

		return $result[$result_field];
	}

	/**
	 * Formats a parameter such as a field or table name with delimited syntax. Static method intended for model usage, not for general purpose use.
	 * 
	 * Syntax varies from database engine to database engine.
	 * 
	 * @internal
	 * @param string $param
	 * @param int $db_type one of the database engine type class constants, e.g. DATABASE_MYSQL
	 * @throws Exception
	 * @return string
	 */
	public static function _delimit($param, $db_type) {
		if ($db_type === self::DATABASE_POSTGRESQL) return "\"{$param}\"";
		if ($db_type === self::DATABASE_MYSQL) return "`{$param}`";
	
		throw new Exception("Delimitor support has not been defined for this database");
	}
	
	/**
	 * Formats a parameter such as a field or table name with delimited syntax as per the wrapped database engine.
	 * 
	 * @param string $param
	 * @return string
	 */
	public function delimit($param) {
		return self::_delimit($param, $this->db_type);
	}

	/**
	 * Builds a fully defined table.field syntax from two independent undelimited parts.
	 * 
	 * @param string $table table name
	 * @param string $field field name
	 * @return string
	 */
	public function tablefield($table, $field) {
		return $this->delimit($table) . "." . $this->delimit($field);
	}
	
	/**
	 * Formats a string with quoted syntax. Static method intended for model usage, not for general purpose use.
	 * 
	 * Syntax varies from database engine to database engine.
	 *
	 * @internal
	 * @param string $value
	 * @param int $db_type one of the database engine type class constants, e.g. DATABASE_MYSQL
	 * @throws Exception
	 * @return string
	 */
	public static function _quote($value, $db_type) {
		if ($db_type === self::DATABASE_POSTGRESQL) return "'" . str_replace("'", "''", $value) . "'";
		if ($db_type === self::DATABASE_MYSQL) return "\"" . addslashes($value) . "\"";
	
		throw new Exception("Quote support has not been defined for this database");
	}
	
	/**
	 * Formats a string with quoted syntax as per the wrapped database engine.
	 * 
	 * @param string $value
	 * @return string
	 */
	public function quote($value) {
		return self::_quote($value, $this->db_type);
	}

	/**
	 * @internal
	 * @param string $method either "INSERT" or "REPLACE"
	 * @param string $table the table to INSERT into
	 * @param Param[] $values an associative array of field values, taking the format name => Param
	 * @param bool $autoinc whether the query is expected to generate (and return) an serial/auto_increment field
	 * @throws Exception
	 * @return bool|int true on success, false on failure, or the value of the autoinc field if requested
	 */
	private function insert_or_replace_row($method, $table, array $values, $autoinc = false) {
		$params = array(); 
		$fields = array(); 
		$qs = array(); 
		$i = 1;
		foreach ($values as $field => $param) {
			if (preg_match("_[\[\];/\\?'\"\r\n\t\\0`]_", $field)) throw new Exception("Condition field contains an invalid character: {$field}");
			
			if ($this->db_type === self::DATABASE_POSTGRESQL) $fields[] = $this->delimit($field);	// postgres doesn't like Table.Field syntax for some reason?
			else $fields[] = $this->tablefield($table, $field);
			
			$qs[] = ":value{$i}";
			$params["value{$i}"] = $param;
			$i++;
		}
		
		$returning = "";
		if ($autoinc !== false && $this->db_type === self::DATABASE_POSTGRESQL) $returning = "RETURNING \"{$autoinc}\"";

		if (false === ($result = $this->query_params("
			{$method} INTO " . $this->delimit($table) . "
			(" . implode(",", $fields) . ")
			VALUES (" . implode(",", $qs) . ")
			{$returning}
		", $params))) return false;
		
		if (false === $autoinc) return true;

		if ($this->db_type === self::DATABASE_POSTGRESQL) return $result[0][$autoinc];
		return $this->pdo->lastInsertId();
	}

	/**
	 * Inserts a new row into the specified table using the given values.
	 * 
	 * @param string $table the table to INSERT into
	 * @param Param[] $values an associative array of field values, taking the format name => Param
	 * @param bool $autoinc whether the query is expected to generate (and return) an serial/auto_increment field
	 * @return bool|int true on success, false on failure, or the value of the autoinc field if requested
	 */
	public function insert_row($table, array $values, $autoinc = false) {
		return $this->insert_or_replace_row("INSERT", $table, $values, $autoinc);
	}

	/**
	 * Returns whether the underlying database engine has native support for REPLACE INTO statements
	 * 
	 * @return boolean
	 */
	public function does_database_support_replace_statement() {
		if ($this->db_type === self::DATABASE_MYSQL) return true;
		return false;
	}
	
	/**
	 * Inserts or replaces a new row into the specified table using the given values.
	 * 
	 * NB this is only supported by some database engines, such as MySQL.
	 * 
	 * @param string $table the table to REPLACE into
	 * @param Param[] $values an associative array of field values, taking the format name => Param
	 * @param bool $autoinc whether the query is expected to generate (and return) an serial/auto_increment field
	 * @return bool|int true on success, false on failure, or the value of the autoinc field if requested
	 */
	public function replace_row($table, array $values, $autoinc = false) {
		if (!$this->does_database_support_replace_statement()) throw new Exception("REPLACE INTO statements are not supported for this database");
		return $this->insert_or_replace_row("REPLACE", $table, $values, $autoinc);
	}

	/**
	 * @internal
	 * @param string $table
	 * @param Param[] $conditions
	 * @param array $params
	 * @throws Exception
	 * @return string
	 */
	private function build_where_clause($table, &$conditions, &$params) {
		$build = array(); 
		$i = 1;
		foreach ($conditions as $field => $param) {
			if (preg_match("_[\[\];/\\?'\"\r\n\t\\0`]_", $field)) throw new Exception("Condition field contains an invalid character: {$field}");
			$build[] = "(" . $this->tablefield($table, $field) . "=:condition{$i})";
			$params["condition{$i}"] = $param;
			$i++;
		}
		return implode(" AND ", $build);
	}

	/**
	 * Runs an update query against the database using the specified details.
	 * 
	 * Note that transactions are used, so failures should be rolled back automatically.
	 * 
	 * @param string $table the table to be UPDATEd
	 * @param Param[] $values an associative array of values to be changed, using the name => Param notation
	 * @param Param[] $conditions an associative array of conditions to be matched (all performed via equality checking) to identify rows
	 * @param bool $singular whether to pre-check that only 1 row will be effected by the query. NB this is not the same as a LIMIT 1, it is a true pre-check of matching row count
	 * @throws Exception
	 * @return boolean true on success, false on failure
	 */
	public function update_rows_by_conditions($table, array $values, array $conditions, $singular = false) {
		if (sizeof($values) == 0) throw new Exception("No update values");
		if (sizeof($conditions) == 0) throw new Exception("No conditions supplied");

		$_TRANSACTION_OWNER = $this->transaction_claim();

		if ($singular) if (1 !== ($count = $this->count_rows_by_conditions($table, $conditions))) {
			$this->transaction_rollback();
			throw new Exception("Singular check failure: {$count} rows match");
		}

		$updates = array(); 
		$params = array(); 
		$i = 1;
		foreach ($values as $field => $param) {
			if (preg_match("_[\[\];/\\?'\"\r\n\t\\0`]_", $field)) throw new Exception("Condition field contains an invalid character: {$field}");

			if ($this->db_type === self::DATABASE_POSTGRESQL) $updates[] = $this->delimit($field) . "=:value{$i}";	// postgres doesn't like Table.Field syntax for some reason?
			else $updates[] = $this->tablefield($table, $field) . "=:value{$i}";
			
			$params["value{$i}"] = $param;
			$i++;
		}
		
		$build = $this->build_where_clause($table, $conditions, $params);

		$result = $this->query_params("
			UPDATE " . $this->delimit($table) . "
			SET " . implode(",", $updates) . "
			WHERE {$build}
		", $params);

		if ($_TRANSACTION_OWNER) {
			if ($result) $this->transaction_commit();
			else $this->transaction_rollback();
		}
		
		return $result;
	}

	/**
	 * Deletes rows from a table that match the specified conditions.
	 *  
	 * Note that transactions are used, so failures should be rolled back automatically.
	 * 
	 * @param string $table the table to delete from
	 * @param Param[] $conditions an associative array of conditions to be matched (all performed via equality checking) to identify rows
	 * @param bool $singular whether to pre-check that only 1 row will be effected by the query. NB this is not the same as a LIMIT 1, it is a true pre-check of matching row count
	 * @throws Exception
	 * @return boolean true on success, false on failure
	 */
	public function delete_rows_by_conditions($table, array $conditions, $singular = false) {
		if (sizeof($conditions) == 0) throw new Exception("No conditions supplied");
		
		$_TRANSACTION_OWNER = $this->transaction_claim();

		if ($singular) if (1 !== ($count = $this->count_rows_by_conditions($table, $conditions))) {
			$this->transaction_rollback();
			throw new Exception("Singular check failure: {$count} rows match");
		}

		$params = array();
		$build = $this->build_where_clause($table, $conditions, $params);
		
		$result = $this->query_params("
			DELETE FROM " . $this->delimit($table) . "
			WHERE {$build}
		", $params);
		
		if ($_TRANSACTION_OWNER) {
			if ($result) $this->transaction_commit(); 
			else $this->transaction_rollback();
		}

		return $result;
	}

	/**
	 * Performs a simplistic SELECT query to return rows that match the specified conditions.
	 *  
	 * @param string $table the table to query
	 * @param string[] $fields the names of the fields to return from the table
	 * @param Param[] $conditions an associative array of conditions to be matched (all performed via equality checking) to identify rows
	 * @throws Exception
	 * @return array
	 */
	public function select_rows_by_conditions($table, array $fields, array $conditions) {
		if (sizeof($fields) === 0) throw new Exception("Select rows by conditions supplied zero-length field array");
		if (sizeof($conditions) == 0) throw new Exception("No conditions supplied");
		
		$query_fields = array();
		foreach ($fields as $name => $type) {
			if (preg_match("_[\[\];/\\?'\"\r\n\t\\0`]_", $name)) throw new Exception("Field contains an invalid character", $name);
			$query_fields[] = $this->tablefield($table, $name);
			
			if (!($type instanceof Type)) throw new Exception("Field type is not an instance of Type", $type);
		}
		$query_fields = implode(",", $query_fields);
		
		$params = array();
		$build = $this->build_where_clause($table, $conditions, $params);

		return $this->query_params("
			SELECT {$query_fields}
			FROM " . $this->delimit($table) . "
			WHERE {$build}
		", $params, $fields);
	}

	/**
	 * Returns the number of rows in a table matching the given conditions.
	 *  
	 * @param string $table the table to be searched
	 * @param Param[] $conditions an associative array of conditions to be matched (all performed via equality checking) to identify rows
	 * @var array $params
	 * @throws Exception
	 * @return int
	 */
	public function count_rows_by_conditions($table, array $conditions) {
		if (sizeof($conditions) == 0) throw new Exception("No conditions supplied");
		
		$build = $this->build_where_clause($table, $conditions, $params);

		return $this->query_params_value("
			SELECT COUNT(*) AS " . $this->delimit("count") . "
			FROM " . $this->delimit($table) . "
			WHERE {$build}
		", $params, "count", new Type_Int(Type_Int::UNSIGNED, Type::NOT_NULL));
	}

	/**
	 * Returns whether one or more rows in a table match the given conditions.
	 *  
	 * @param string $table the table to be searched
	 * @param Param[] $conditions an associative array of conditions to be matched (all performed via equality checking) to identify rows
	 * @throws Exception
	 * @return bool
	 */
	public function does_row_exist_by_conditions($table, array $conditions) {
		return $this->count_rows_by_conditions($table, $conditions) > 0;
	}

	//-------------------------------------------------------------------------

	private $_LOG_CREATE_DELETE_ALTER = false;
	
	/**
	 * Not for general use.
	 */
	public function set_log_create_delete_alter(array &$log) {
		$this->_LOG_CREATE_DELETE_ALTER = &$log;
	}
	
	private function internal_log_query_params($sql) {
		if (is_array($this->_LOG_CREATE_DELETE_ALTER)) array_push($this->_LOG_CREATE_DELETE_ALTER, $sql);
		
		$this->query_params($sql);
	}
	
	/**
	 * Creates a new database table using the supplied structure.
	 * 
	 * @param string $table the table name
	 * @param Type[] $structure an associative array of fields, using the name => Type format
	 * @returns void
	 */
	public function create_table($table, array $structure) {
		$sql = $this->generate_create_table_sql($table, $structure);
		$this->internal_log_query_params($sql);
	}

	/**
	 * Generates the SQL that needed to create a new database table using the supplied structure.
	 * 
	 * @param string $table the table name
	 * @param Type[] $structure an associative array of fields, using the name => Type format
	 * @returns string
	 */
	public function generate_create_table_sql($table, array $structure) {
		$fields = array();
		foreach ($structure as $name => $type) {
			$check = "";
			if ($type instanceof Type_Enum) if (null !== ($create_enum = $type->get_create_enum($this->db_type))) {
				$this->query_params($create_enum);
			}
			if ($type instanceof Type_Numeric) $check = $type->get_check($name, $this->db_type);
			
			$fields[] = $this->delimit($name) . " " . $type->render($this->db_type) . $check;
		}
		$fields = implode(",", $fields);
		
		$engine = "";
		if ($this->db_type === self::DATABASE_MYSQL) $engine = " ENGINE=InnoDB";
		
		return "CREATE TABLE " . $this->delimit($table) . " ({$fields}){$engine}";
	}

	/**
	 * Drops a table from the database.
	 * 
	 * Dropping of any Postgres CREATE TYPEs automatically created will be cascaded.
	 * 
	 * @param string $table the table name
	 * @param Type[] $structure an associative array of fields, using the name => Type format
	 * @return void
	 */
	public function drop_table($table, array $structure) {
		$this->internal_log_query_params("DROP TABLE " . $this->delimit($table));
				
		foreach (array_values($structure) as $type) {
			if ($type instanceof Type_Enum) if (null !== ($drop_enum = $type->get_drop_enum($this->db_type))) {
				$this->internal_log_query_params($drop_enum);
			}
		}
	}

	/**
	 * Returns the Abstraction syntax for a view name using its parent table and sub-name.
	 * 
	 * @internal
	 * @param string $table the parent table name
	 * @param string $name the sub-name
	 * @return string
	 */
	public function view($table, $name) {
		return "{$table}__{$name}";
	}

	/**
	 * Creates a new database view using the supplied SQL.
	 * 
	 * @param string $table the parent table name
	 * @param string $name the sub-name (to identify the view)
	 * @param string $sql the view definition
	 * @returns void
	 */
	public function create_view($table, $name, $sql) {
		$this->internal_log_query_params("CREATE VIEW " . $this->delimit($this->view($table, $name)) . " AS {$sql}");
	}

	/**
	 * Drops a view from the database.
	 * 
	 * @param string $table the parent table name
	 * @param string $name the sub-name (to identify the view)
	 * @returns void
	 */
	public function drop_view($table, $name) {
		$this->internal_log_query_params("DROP VIEW " . $this->delimit($this->view($table, $name)));
	}

	/**
	 * Executes an ALTER TABLE to add a new FOREIGN KEY constraint to a table.
	 * 
	 * @param string $table the source table to be modified
	 * @param string $dest_table the destination table containing the remote key match
	 * @param string $field the field in the source table to create the key on
	 * @param string $dest_id the field in the destination table to link to
	 * @param string $on_delete one of CASCADE, SET NULL or RESTRICT
	 * @param string $on_update one of CASCADE, SET NULL or RESTRICT
	 * @param string $constraintname option to override the default auto-generated constrain name, since MySQL has limited number of characters permitted
	 * @return void
	 */
	public function create_foreign_key($table, $dest_table, $field, $dest_id, $on_delete = "CASCADE", $on_update = "CASCADE", $constraintname = false) {
		$sql = $this->generate_create_foreign_key_sql($table, $dest_table, $field, $dest_id, $on_delete, $on_update, $constraintname);
		
		$this->internal_log_query_params($sql);
	}

	/**
	 * Generates the SQL needed to ALTER TABLE to add a new FOREIGN KEY constraint to a table.
	 * 
	 * @param string $table the source table to be modified
	 * @param string $dest_table the destination table containing the remote key match
	 * @param string $field the field in the source table to create the key on
	 * @param string $dest_id the field in the destination table to link to
	 * @param string $on_delete one of CASCADE, SET NULL or RESTRICT
	 * @param string $on_update one of CASCADE, SET NULL or RESTRICT
	 * @param string $constraintname option to override the default auto-generated constrain name, since MySQL has limited number of characters permitted
	 * @return string
	 */
	public function generate_create_foreign_key_sql($table, $dest_table, $field, $dest_id, $on_delete = "CASCADE", $on_update = "CASCADE", $constraintname = false) {
		if ($constraintname === false) $constraintname = "f_{$table}_{$dest_table}_{$field}_{$dest_id}";
		
		return "
			ALTER TABLE " . $this->delimit($table) . " 
			ADD CONSTRAINT " . $this->delimit($constraintname) . "
			FOREIGN KEY (" . $this->delimit($field) . ")
				REFERENCES " . $this->delimit($dest_table) . " (" . $this->delimit($dest_id) . ")
				ON DELETE {$on_delete}
				ON UPDATE {$on_update}
		";
	}

	/**
	 * Executes an ALTER TABLE to add a new UNIQUE KEY constraint to a table.
	 * 
	 * @param string $table the table name
	 * @param string[] $fields the field name(s) to build the unique constraint upon
	 * @param string $constraintname option to override the default auto-generated constrain name, since MySQL has limited number of characters permitted
	 * @return void
	 */
	public function create_unique_key($table, array $fields, $constraintname = false) {
		$sql = $this->generate_create_unique_key_sql($table, $fields, $constraintname);
		$this->internal_log_query_params($sql);
	}

	/**
	 * Generates the SQL needed to ALTER TABLE to add a new UNIQUE KEY constraint to a table.
	 * 
	 * @param string $table the table name
	 * @param string[] $fields the field name(s) to build the unique constraint upon
	 * @param string $constraintname option to override the default auto-generated constrain name, since MySQL has limited number of characters permitted
	 * @return string
	 */
	public function generate_create_unique_key_sql($table, array $fields, $constraintname = false) {
		$build = array(); 
		$name = array();
		foreach ($fields as $field) {
			$build[] = self::delimit($field);
			$name[] = $field;
		}
		$build = implode(",", $build);
		
		if ($constraintname === false) $constraintname = "constraint_unique_{$table}_" . implode("_", $name);
		
		$key = " KEY";
		if ($this->db_type === self::DATABASE_POSTGRESQL) $key = "";

		return "
			ALTER TABLE " . $this->delimit($table) . " 
			ADD CONSTRAINT " . $this->delimit($constraintname) . "
			UNIQUE{$key} ({$build})
		";
	}
	
	/**
	 * Executes an CREATE INDEX to add a new index to a table.
	 *
	 * @param string $table the table name
	 * @param string[] $fields the field name(s) to build the index upon
	 * @param string $indexname option to override the default auto-generated index name, since MySQL has limited number of characters permitted
	 * @return void
	 */
	public function create_index($table, array $fields, $indexname = false) {
		$sql = $this->generate_create_index_sql($table, $fields, $indexname);
		$this->internal_log_query_params($sql);
	}
	
	/**
	 * Generates the SQL needed to CREATE INDEX on a table.
	 *
	 * @param string $table the table name
	 * @param string[] $fields the field name(s) to build the unique index upon
	 * @param string $indexname option to override the default auto-generated index name, since MySQL has limited number of characters permitted
	 * @return string
	 */
	public function generate_create_index_sql($table, array $fields, $indexname = false) {
		$build = array(); 
		$name = array();
		foreach ($fields as $field) {
			$build[] = self::delimit($field);
			$name[] = $field;
		}
		$build = implode(",", $build);
		
		if ($indexname === false) $indexname = "index_{$table}_" . implode("_", $name);
		
		return "
			CREATE INDEX " . $this->delimit($indexname) . " 
			ON " . $this->delimit($table) . " ({$build})
		";
	}
}
