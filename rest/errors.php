<?php
/**
 * HTTP REST errors
 * 
 * @copyright 2018 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\REST;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction as Abstraction;

/**
 * @internal
 */
class HTTP_Exception extends Abstraction\Exception {}

class HTTP_Error extends HTTP_Exception {
	public $httpcode;
	
	/**
	 * Generic HTTP error.
	 * 
	 * @param int $httpcode the HTTP response code, 404, 500, etc.
	 * @param string $message the message associated with the error.
	 * @param mixed $value any value associated with the error.
	 * @param mixed|null $extra any additional information associated with the error.
	 */
	public function __construct($httpcode, $message, $value = "\t_NO_VALUE", $extra = null) {
		parent::__construct($message, $value, $extra);
		
		$this->httpcode = $httpcode;
	}
}

class Error_NotFound extends HTTP_Error {
	public function __construct($message) {
		parent::__construct(404, $message);
	}
}

class Error_BadRequest extends HTTP_Error {
	public function __construct($message) {
		parent::__construct(400, $message);
	}
}

class Error_Unauthorized extends HTTP_Error {
	public function __construct($message) {
		parent::__construct(401, $message);
	}
}

class Error_Conflict extends HTTP_Error {
	public function __construct($message) {
		parent::__construct(409, $message);
	}
}

class Error_Forbidden extends HTTP_Error {
	public function __construct($message) {
		parent::__construct(403, $message);
	}
}

class Error_InternalServerError extends HTTP_Error {
	public function __construct($message) {
		parent::__construct(500, $message);
	}
}

class Error_ServiceUnavailable extends HTTP_Error {
	public function __construct($message) {
		parent::__construct(503, $message);
	}
}

/**
 * This is a throwable Exception to allow GET requests to return 204 on occasion rather than returning null.
 * @author pete
 *
 */
class Success_NoContent extends HTTP_Error {
	public function __construct($message) {
		parent::__construct(204, $message);
	}
}
