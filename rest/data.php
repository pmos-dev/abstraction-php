<?php
/**
 * This is a helper script for REST data from PATCH and PUT methods.
 * PHP automatically builds $_GET and $_POST superglobals, but not for PUT and PATCH.
 * The data for these is contained within the stdin of the request, so parse it here, and build two new globals (as close to superglobals), $_PUT and $_PATCH
 *
 * @copyright 2018 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\REST;

$_PATCH = array();
$_PUT = array();

if (isset($_SERVER) && is_array($_SERVER) && array_key_exists("REQUEST_METHOD", $_SERVER)) {
	switch ($_SERVER["REQUEST_METHOD"]) {
		case "PATCH":
			try {
				$incoming = file_get_contents('php://input');
				if ("" === ($incoming = trim($incoming))) break;
				parse_str($incoming, $_PATCH);
			// @phpcs:ignore Generic.CodeAnalysis.EmptyStatement
			} catch(\Exception $e) {}
			break;
		case "PUT":
			try {
				$incoming = file_get_contents('php://input');
				if ("" === ($incoming = trim($incoming))) break;
				parse_str($incoming, $_PUT);
			// @phpcs:ignore Generic.CodeAnalysis.EmptyStatement
			} catch(\Exception $e) {}
			break;
	}
}
