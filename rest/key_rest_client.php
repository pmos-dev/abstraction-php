<?php
/**
 * REST client for Base62 ID key-based authorisation servers.
 * 
 * @copyright 2018 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\REST;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "core.php";
require_once ABSTRACTION_ROOT_PATH . "rest/rest_client.php";

use \Abstraction\Data as Data;

/**
 * @internal
 */
class KeyRESTClient_Exception extends RESTClient_Exception {}

class KeyRESTClient extends RESTClient {
	const TYPE_BASE36 = "base36";
	const TYPE_BASE62 = "base62";
	
	const METHOD_KEY = "key";
	
	private $type;
	
	public function __construct($root_api_url, $postdata_format, $type, $key) {
		parent::__construct($root_api_url, $postdata_format);

		if (!Data\Data::validate_option(array(self::TYPE_BASE36, self::TYPE_BASE62), $type)) throw new KeyRESTClient_Exception("Key type is not valid");
		$this->type = $type;

		switch ($this->type) {
			case self::TYPE_BASE36:
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE36_ID, $key)) throw new KeyRESTClient_Exception("Key is not Base36");
				break;
			case self::TYPE_BASE62:
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $key)) throw new KeyRESTClient_Exception("Key is not Base62");
				break;
		}
		
		$this->set_authorisation(self::METHOD_KEY, $key);
	}
	
	public function get_type() {
		return $this->type;
	}
}
