<?php
/**
 * Express style REST path parsing.
 * 
 * @copyright 2018 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\REST;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "core.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";

use \Abstraction as Abstraction;
use \Abstraction\Data as Data;

/**
 * @internal
 */
class RESTPath_Exception extends Abstraction\Exception {}

/**
 * REST path validation and parsing utilities.
 * 
 * Uses the Node.js Express style of path parameters.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Path {
	const REGEX_PATTERN_REST = "`^(/(([-a-z0-9]+)|(:[a-z0-9\(\)\|\[\]]+)))+$`";
	const REGEX_PATTERN_REST_PATH = "`^[-a-z0-9]+$`";
	const REGEX_PATTERN_REST_PARAM_VALUE = "`^:([a-z0-9]+)(?:\[(id|idname|int|base36|base62|ids|idnames|base36s|base62s|ymd|ymdhis)\])?$`";
	const REGEX_PATTERN_REST_PARAM_OPTION = "`^:([a-z0-9]+)[(]([a-zA-Z0-9]{1,32}(?:[|][a-zA-Z0-9]{1,32})*)[)]$`";
	
	private $params;
	private $types;
	private $regex;

	/**
	 * Constructs a new path parser.
	 * 
	 * Path examples:
	 * /events
	 * /events/:id
	 * /events/:id[id]
	 * /events/:id/people/:name[idname]/tickets/:number[int]
	 * 
	 * @param string $path the REST path to compile.
	 * @var array $array
	 * @throws RESTPath_Exception if an invalid path is supplied.
	 */
	public function __construct($path) {
		if (!Data\Data::validate_regex(self::REGEX_PATTERN_REST, $path)) throw new RESTPath_Exception("Invalid URL path for REST");

		$components = explode("/", $path);
		array_shift($components);

		$this->params = array();
		$rebuild = array();

		foreach ($components as $component) {
			if (Data\Data::validate_regex(self::REGEX_PATTERN_REST_PATH, $component)) {
				$rebuild[] = $component;
				continue;
			}

			if (Data\Data::validate_regex(self::REGEX_PATTERN_REST_PARAM_OPTION, $component)) {
				preg_match(self::REGEX_PATTERN_REST_PARAM_OPTION, $component, $array);

				$var = $array[1];
				$regex = "(?:{$array[2]})";
				
				$rebuild[] = "(${regex})";
				$this->params[] = $array[1];
				$this->types[] = "option";
				
				continue;
			}
				
			if (Data\Data::validate_regex(self::REGEX_PATTERN_REST_PARAM_VALUE, $component)) {
				preg_match(self::REGEX_PATTERN_REST_PARAM_VALUE, $component, $array);
				
				$var = $array[1];
				$regex = "[-a-z0-9]+";
				$type = "string";
				
				if (sizeof($array) > 2) switch ($array[2]) {
					case "id":
						$regex = "[0-9]{1,8}";
						$type = "id";
						break;
					case "idname":
						$regex = "[A-Za-z0-9()'#](?:[-A-Za-z0-9()'_#. ]{0,62}[-A-Za-z0-9()'_#.])?";
						$type = "idname";
						break;
					case "int":
						$regex = "-?[0-9]{1,10}";
						$type = "int";
						break;
					case "base36":
						$regex = "[0-9A-Za-z]{10}";
						$type = "base36";
						break;
					case "base62":
						$regex = "[0-9A-Za-z]{8}";
						$type = "base62";
						break;
					case "ymd":
						$regex = "(?:[0-9]{4})-(?:[0-9]{2})-(?:[0-9]{2})";
						$type = "ymd";
						break;
					case "ymdhis":
						$regex = "(?:[0-9]{4})-(?:[0-9]{2})-(?:[0-9]{2})(?: |%20)(?:[0-9]{2}):(?:[0-9]{2}):(?:[0-9]{2})";
						$type = "ymdhis";
						break;
						
					// we allow an optional trailing comma, as it's a way of forcing a single idnames array to be returned as an array rather than being intepreted as a singular item request.
					case "ids":
						$regex = "(?:[0-9]{1,8})(?:,[0-9]{1,8})*,?";
						$type = "ids";
						break;
					case "idnames":
						$regex = "(?:[A-Za-z0-9()'#](?:[-A-Za-z0-9()'_#. ]{0,62}[-A-Za-z0-9()'_#.])?)(?:,(?:[A-Za-z0-9()'#](?:[-A-Za-z0-9()'_#. ]{0,62}[-A-Za-z0-9()'_#.])?))*,?";
						$type = "idnames";
						break;
					case "base36s":
						$regex = "(?:[0-9A-Za-z]{10})(?:,[0-9A-Za-z]{10})*,?";
						$type = "base36s";
						break;
					case "base62s":
						$regex = "(?:[0-9A-Za-z]{8})(?:,[0-9A-Za-z]{8})*,?";
						$type = "base62s";
						break;
					default:
						throw new RESTPath_Exception("Unknown path parameter type");
				}

				$rebuild[] = "(${regex})";
				$this->params[] = $var;
				$this->types[] = $type;
			
				continue;
			}
			
			throw new RESTPath_Exception("Unable to parse path component");
		}
		
		$this->regex = "`^/" . implode("/", $rebuild) . "$`";
	}
	
	/**
	 * Parses the given path string into an associated array of values, based upon the parser.
	 * 
	 * @param string $path the path to parse.
	 * @throws RESTPath_Exception
	 * @return mixed[]|false the associative array of values extracted from the path, or false if it was not a valid path.
	 */
	public function parse($path) {
		if (preg_match("`^(.*)/$`", $path, $array)) $path = $array[1];
		if ($path === "") return false;
		
		if (!preg_match($this->regex, $path, $array)) return false;
		array_shift($array);

		if (sizeof($array) !== sizeof($this->params)) return false;
		$values = array();
		$i = 0;
		foreach ($this->params as $var) {
			$value = $array[$i];
			
			switch ($this->types[$i]) {
				case "string":
					break;
				case "option":
					break;
				case "id":
					if (!Data\Data::validate_id($value)) return false;
					break;
				case "idname":
					if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $value)) return false;
					break;
				case "int":
					if (!Data\Data::validate_int($value)) return false;
					break;
				case "base36":
					if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE36_ID, $value)) return false;
					break;
				case "base62":
					if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $value)) return false;
					break;
				case "ymd":
					if (!Data\Data::validate_string($value)) return false;
					if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATE_YMD, $value)) return false;
					break;
				case "ymdhis":
					if (!Data\Data::validate_string($value)) return false;
					$value = str_replace("%20", " ", $value);
					if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATETIME_YMDHIS, $value)) return false;
					break;
				case "ids":
					$ids = explode(",", $value);
					if (preg_match("`,$`", $value)) array_pop($ids);	// remove the trailing comma, if any. See explanation in the REGEX definition
					foreach ($ids as $id) if (!Data\Data::validate_id($id)) return false;
					$value = $ids;
					break;
				case "idnames":
					$idnames = explode(",", $value);
					if (preg_match("`,$`", $value)) array_pop($idnames);	// remove the trailing comma, if any. See explanation in the REGEX definition
					foreach ($idnames as $idname) if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $idname)) return false;
					$value = $idnames;
					break;
				case "base36s":
					$base36s = explode(",", $value);
					if (preg_match("`,$`", $value)) array_pop($base36s);	// remove the trailing comma, if any. See explanation in the REGEX definition
					foreach ($base36s as $base36) if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE36_ID, $base36)) return false;
					$value = $base36s;
					break;
				case "base62s":
					$base62s = explode(",", $value);
					if (preg_match("`,$`", $value)) array_pop($base62s);	// remove the trailing comma, if any. See explanation in the REGEX definition
					foreach ($base62s as $base62) if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $base62)) return false;
					$value = $base62s;
					break;
				default:
					throw new RESTPath_Exception("Unknown type");
			}
			
			$values[$var] = $value;
			$i++;
		}

		return $values;
	}
}
