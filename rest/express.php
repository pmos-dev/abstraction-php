<?php
/**
 * Express style API handler.
 * 
 * @copyright 2018 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\REST;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "core.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "rest/path.php";
require_once ABSTRACTION_ROOT_PATH . "rest/errors.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/rest/rest.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\REST as REST;

/**
 * Mimic port of Node.js Express server to provide similar functionality for PHP.
 * 
 * Like Express, paths use the format /example/things/:id/logs
 * However, this is extended to allow for type checking e.g. /example/things/:id[int]/logs/names/name:[idname]
 * These are all defined in path.php 
 * 
 * @copyright 2018 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
class Express {
	private $rest;
	private $operations;
	
	/**
	 * Constructs a new instance of this Express handler.
	 * 
	 * @param REST\REST $rest the REST output renderer to use.
	 */
	public function __construct(REST\REST $rest) {
		$this->rest = $rest;
		
		$this->operations = array();
	}
	
	/**
	 * Register a HEAD request handler.
	 * 
	 * @param string $path the query path to register, including any values.
	 * @param callable $code the callback to run, ($values, $queries) => void
	 * @return void
	 */
	public function head($path, $code) {
		$r = new Path($path);
		$this->operations[] = array(
				"method" => "HEAD",
				"rest" => $r,
				"code" => $code
		);
	}
	
	/**
	 * Register a GET request handler.
	 *
	 * @param string $path the query path to register, including any values.
	 * @param callable $code the callback to run, ($values, $queries) => void
	 * @return void
	 */
	public function get($path, $code) {
		$r = new Path($path);
		$this->operations[] = array(
				"method" => "GET",
				"rest" => $r,
				"code" => $code
		);
	}
	
	/**
	 * Register a POST request handler.
	 *
	 * @param string $path the query path to register, including any values.
	 * @param callable $code the callback to run, ($values, $data) => void
	 * @return void
	 */
	public function post($path, $code) {
		$r = new Path($path);
		$this->operations[] = array(
				"method" => "POST",
				"rest" => $r,
				"code" => $code
		);
	}
	
	/**
	 * Register a PUT request handler.
	 *
	 * @param string $path the query path to register, including any values.
	 * @param callable $code the callback to run, ($values, $data) => void
	 * @return void
	 */
	public function put($path, $code) {
		$r = new Path($path);
		$this->operations[] = array(
				"method" => "PUT",
				"rest" => $r,
				"code" => $code
		);
	}
	
	/**
	 * Register a PATCH request handler.
	 *
	 * @param string $path the query path to register, including any values.
	 * @param callable $code the callback to run, ($values, $data) => void
	 * @return void
	 */
	public function patch($path, $code) {
		$r = new Path($path);
		$this->operations[] = array(
				"method" => "PATCH",
				"rest" => $r,
				"code" => $code
		);
	}
	
	/**
	 * Register a DELETE request handler.
	 *
	 * @param string $path the query path to register, including any values.
	 * @param callable $code the callback to run, ($values, $queries) => void
	 * @return void
	 */
	public function delete($path, $code) {
		$r = new Path($path);
		$this->operations[] = array(
				"method" => "DELETE",
				"rest" => $r,
				"code" => $code
		);
	}
	
	/**
	 * Attempt to match and execute any of the registered handlers.
	 * 
	 * Handlers should terminate themselves, outputting JSON or otherwise, so none of them return.
	 * As such, the only possible return valid is false, for when no handler was matched.
	 * 
	 * @throws HTTP_Exception if the HTTP method cannot be identified.
	 * @return boolean false if no handlers are found and run.
	 */
	public function attempt() {
		global $_PATCH, $_PUT;
		
		// handle OPTIONS pre-flight CORS
		if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
			foreach ($this->operations as $operation) {
				if ($operation["method"] !== $_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]) continue;
				if (false === ($values = $operation["rest"]->parse($_REQUEST["path"]))) continue;
				
				// ideally we would run the next line to improve security, but browser caching of OPTIONS responses might be flakey, so let's just leave the enable_origin to be all methods.
				//header("Access-Control-Allow-Methods: {$operation["method"]}");
				
				$this->rest->noContent();
			}
		}
		
		foreach ($this->operations as $operation) {
			if ($operation["method"] !== $_SERVER["REQUEST_METHOD"]) continue;
			if (false === ($values = $operation["rest"]->parse($_REQUEST["path"]))) continue;

			$queries = array();
			foreach ($_GET as $key => $value) {
				if (Data\Data::validate_option("xsi,key,path", $key)) continue;
				$queries[$key] = $value;
			}
			
			if (Data\Data::validate_option("HEAD,DELETE", $operation["method"]) ){
				try {
					$operation["code"]($values, $queries);
					$this->rest->noContent();
				} catch(HTTP_Error $e) {
					$this->rest->responseCodeError($e->httpcode, $e->getMessage());
				}
				continue;
			}
			if (Data\Data::validate_option("GET", $operation["method"]) ){
				try {
					$outcome = $operation["code"]($values, $queries);
					$this->rest->ok($outcome);
				} catch(HTTP_Error $e) {
					$this->rest->responseCodeError($e->httpcode, $e->getMessage());
				}
				continue;
			}
			if (Data\Data::validate_option("POST", $operation["method"])) {
				$data = array();
				foreach ($_POST as $key => $value) {
					if (Data\Data::validate_option("xsi,key,path", $key)) continue;
					$data[$key] = $value;
				}
				
				try {
					$outcome = $operation["code"]($values, $data);
					$this->rest->created($outcome);
				} catch(HTTP_Error $e) {
					$this->rest->responseCodeError($e->httpcode, $e->getMessage());
				}
				continue;
			}
			if (Data\Data::validate_option("PATCH", $operation["method"])) {
				$data = array();
				foreach ($_PATCH as $key => $value) {
					if (Data\Data::validate_option("xsi,key,path", $key)) continue;
					$data[$key] = $value;
				}
				
				try {
					$outcome = $operation["code"]($values, $data);
					$this->rest->ok($outcome);
				} catch(HTTP_Error $e) {
					$this->rest->responseCodeError($e->httpcode, $e->getMessage());
				}
				continue;
			}
			if (Data\Data::validate_option("PUT", $operation["method"])) {
				$data = array();
				foreach ($_PUT as $key => $value) {
					if (Data\Data::validate_option("xsi,key,path", $key)) continue;
					$data[$key] = $value;
				}
				
				try {
					$outcome = $operation["code"]($values, $data);
					$this->rest->ok($outcome);
				} catch(HTTP_Error $e) {
					$this->rest->responseCodeError($e->httpcode, $e->getMessage());
				}
				continue;
			}
			
			throw new HTTP_Exception("Unknown operation method. This shouldn't be possible?");
		}
		
		return false;
	}
}
