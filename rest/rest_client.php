<?php
/**
 * Generic REST client.
 * 
 * @copyright 2018 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\REST;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "core.php";
require_once ABSTRACTION_ROOT_PATH . "connect/http/direct.php";
require_once ABSTRACTION_ROOT_PATH . "rest/errors.php";

use \Abstraction as Abstraction;
use \Abstraction\Data as Data;
use \Abstraction\Connect\HTTP as HTTP;

/**
 * @internal
 */
class RESTClient_Exception extends Abstraction\Exception {}

/**
 * This is a generic REST client class for interacting with a generic REST server, as may be created by the corresponding RESTAPI.
 * 
 * NB. individual HTTP cURL requests are built for each REST call, so state is not persistent.
 * 
 * @copyright 2018 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
class RESTClient {
	const POSTDATA_JSON = 'json';
	const POSTDATA_FORM = 'form';
	
	private $root_api_url;
	private $postdata_format;
	private $authorisation_method;
	private $authorisation_value;
	
	/**
	 * Constructs a new instance of this REST client.
	 * 
	 * @param string $root_api_url the root URL of the REST server
	 * @param string $postdata_format what format to send POST/PUT/PATCH data in. POSTDATA_JSON or POSTDATA_FORM (HTML/PHP's normal formencoding)
	 * @var array $array
	 * @throws RESTClient_Exception
	 */
	public function __construct($root_api_url, $postdata_format) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_URL, $root_api_url)) throw new RESTClient_Exception("Invalid root URL");
		
		if (preg_match("`^(.+)/$`", $root_api_url, $array)) $root_api_url = $array[1];
		
		$this->root_api_url = $root_api_url;
		
		if (!Data\Data::validate_option(array(
				self::POSTDATA_FORM,
				self::POSTDATA_JSON
		), $postdata_format)) throw new RESTClient_Exception("Invalid POSTDATA format");
		
		$this->postdata_format = $postdata_format;
		
		$this->authorisation_method = null;
		$this->authorisation_value = null;
	}

	/**
	 * Set an Authorization header for all future calls.
	 * 
	 * @param string $method the authorisation method
	 * @param string $value the authorisation value/passphrase/etc.
	 * @throws RESTClient_Exception
	 */
	public function set_authorisation($method, $value) {
		if (!Data\Data::validate_regex("`^[a-z0-9]{1,32}(?:-?[a-z0-9]{1,32})*$`i", $method)) throw new RESTClient_Exception("Invalid Authorization method");
		if (!Data\Data::validate_regex("`^[-a-z0-9+/]+$`i", $value)) throw new RESTClient_Exception("Invalid Authorization value");
		
		$this->authorisation_method = $method;
		$this->authorisation_value = $value;
	}

	/**
	 * Builds the actual HTTP\Direct object.
	 * 
	 * Subclasses should override if desired to add additional defaults.
	 * 
	 * @return \Abstraction\Connect\HTTP\Direct
	 */
	protected function build_remote() {
		$remote = new HTTP\Direct();
		
		if ($this->authorisation_method !== null && $this->authorisation_value !== null) {
			$remote->add_header("Authorization: {$this->authorisation_method} {$this->authorisation_value}");
		}
		
		return $remote;
	}
	
	private static function build_url($path, array $params) {
		$parts = explode("?", $path);
		
		// Raw URL encode everything except for forward slash and comma
		$path = rawurlencode($parts[0]);
		$path = str_replace("%2F", "/", $path);
		$path = str_replace("%2C", ",", $path);
		
		$querystring = null;
		if (sizeof($parts) > 1) $querystring = $parts[1];
		
		if ($params !== null && sizeof($params) > 0) {
			$encoded = http_build_query($params);
			if ($encoded !== "") {
				if ($querystring !== null) $querystring .= "&${encoded}";
				else $querystring = $encoded;
			}
		}
		
		if ($querystring !== null) $path .= "?${querystring}";

		return $path;
	}
	
	private function handle_response_code(HTTP\Direct $remote, $result) {
		$message = "";
		
		if ($result !== null) {
			if (Data\Data::validate_string($result)) $result = @json_decode($result);
			if (is_array($result) && array_key_exists("message", $result) && Data\Data::validate_string($result["message"])) $message = $result["message"];
		}
		
		switch ($remote->get_response_code()) {
			case 400:
				throw new Error_BadRequest($message);
			case 401:
				throw new Error_Unauthorized($message);
			case 403:
				throw new Error_Forbidden($message);
			case 404:
				throw new Error_NotFound($message);
			case 409:
				throw new Error_Conflict($message);
			case 500:
				throw new Error_InternalServerError($message);
			case 503:
				throw new Error_ServiceUnavailable($message);
			default:
				// assume ok
		}
	}
	
	/**
	 * Execute a standard REST GET call.
	 * 
	 * @param string $path the REST path.
	 * @param array $params any query parameters to supply
	 * @throws HTTP_Error
	 * @throws RESTClient_Exception
	 * @return mixed the decoded JSON result of the call.
	 */
	public function get($path, array $params) {
		$path = self::build_url($path, $params);
		
		$remote = $this->build_remote();
		$result = $remote->get_json("{$this->root_api_url}{$path}");
		
		try {
			$this->handle_response_code($remote, $result);
		} catch(HTTP_Error $e) {
			$remote->close();
			throw $e;
		}
		$remote->close();
		
		if ($result === null) throw new RESTClient_Exception("The returned API response was not valid JSON");
		
		return $result;
	}
	
	/**
	 * Execute a standard REST HEAD call.
	 *
	 * @param string $path the REST path.
	 * @param array $params any query parameters to supply
	 * @throws HTTP_Error
	 * @throws RESTClient_Exception
	 * @return true always true, otherwise an error should be thrown
	 */
	public function head($path, array $params) {
		$path = self::build_url($path, $params);
		
		$remote = $this->build_remote();
		$result = $remote->head("{$this->root_api_url}{$path}");
		
		try {
			$this->handle_response_code($remote, $result);
		} catch(HTTP_Error $e) {
			$remote->close();
			throw $e;
		}
		$remote->close();
		
		return true;
	}
	
	/**
	 * Execute a standard REST POST call.
	 *
	 * @param string $path the REST path.
	 * @param array $data any data to provide the call.
	 * @throws HTTP_Error
	 * @throws RESTClient_Exception
	 * @return mixed the decoded JSON result of the call.
	 */
	public function post($path, array $data) {
		$remote = $this->build_remote();
		
		$result = null;
		switch ($this->postdata_format) {
			case self::POSTDATA_FORM:
				if ($data === null) $data = array();
				$result = $remote->post("{$this->root_api_url}{$path}", $data);
				break;
			case self::POSTDATA_JSON:
				if ($data === null) $data = "";
				$result = $remote->postJson("{$this->root_api_url}{$path}", $data);
				break;
			default:
				$remote->close();
				throw new RESTClient_Exception("Unknown POST data format type");
		}
		
		try {
			$this->handle_response_code($remote, $result);
		} catch(HTTP_Error $e) {
			$remote->close();
			throw $e;
		}
		$remote->close();
		
		return json_decode($result, true);
	}
	
	/**
	 * Execute a standard REST PUT call.
	 *
	 * @param string $path the REST path.
	 * @param array $data any data to provide the call.
	 * @throws HTTP_Error
	 * @throws RESTClient_Exception
	 * @return mixed the decoded JSON result of the call.
	 */
	public function put($path, array $data) {
		$remote = $this->build_remote();
		
		$result = null;
		switch ($this->postdata_format) {
			case self::POSTDATA_FORM:
				if ($data === null) $data = array();
				$result = $remote->put("{$this->root_api_url}{$path}", $data);
				break;
			case self::POSTDATA_JSON:
				if ($data === null) $data = "";
				$result = $remote->putJson("{$this->root_api_url}{$path}", $data);
				break;
			default:
				$remote->close();
				throw new RESTClient_Exception("Unknown POST data format type");
		}
		
		try {
			$this->handle_response_code($remote, $result);
		} catch(HTTP_Error $e) {
			$remote->close();
			throw $e;
		}
		$remote->close();
		
		return json_decode($result, true);
	}
	
	/**
	 * Execute a standard REST PATCH call.
	 *
	 * @param string $path the REST path.
	 * @param array $data any data to provide the call.
	 * @throws HTTP_Error
	 * @throws RESTClient_Exception
	 * @return mixed the decoded JSON result of the call.
	 */
	public function patch($path, array $data) {
		$remote = $this->build_remote();
		
		$result = null;
		switch ($this->postdata_format) {
			case self::POSTDATA_FORM:
				if ($data === null) $data = array();
				$result = $remote->patch("{$this->root_api_url}{$path}", $data);
				break;
			case self::POSTDATA_JSON:
				if ($data === null) $data = "";
				$result = $remote->patchJson("{$this->root_api_url}{$path}", $data);
				break;
			default:
				$remote->close();
				throw new RESTClient_Exception("Unknown POST data format type");
		}
		
		try {
			$this->handle_response_code($remote, $result);
		} catch(HTTP_Error $e) {
			$remote->close();
			throw $e;
		}
		$remote->close();
		
		return json_decode($result, true);
	}
	
	/**
	 * Execute a standard REST DELETE call.
	 *
	 * @param string $path the REST path.
	 * @param array $params any query parameters to provide the call.
	 * @throws HTTP_Error
	 * @throws RESTClient_Exception
	 * @return true always true, otherwise an error should be thrown
	 */
	public function delete($path, array $params) {
		$path = self::build_url($path, $params);
		
		$remote = $this->build_remote();
		$result = $remote->delete_json("{$this->root_api_url}{$path}");

		try {
			$this->handle_response_code($remote, $result);
		} catch(HTTP_Error $e) {
			$remote->close();
			throw $e;
		}
		$remote->close();

		return true;
	}
}
