<?php
/**
 * Generic REST API.
 * 
 * @copyright 2018 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\REST;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "core.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/rest/rest.php";
require_once ABSTRACTION_ROOT_PATH . "rest/express.php";
require_once ABSTRACTION_ROOT_PATH . "rest/errors.php";

use \Abstraction\Data as Data;

/**
 * This is a generic REST API class for creating a generic REST server.
 * 
 * It is based around the Express approach to REST APIs. See express.php
 * 
 * @copyright 2018 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
class RESTAPI extends Express {
	/**
	 * Converts an empty array into a typecasted empty object.
	 * 
	 * JSON considers [] and {} to be different things, but PHP's arrays are both associative and sequential for empty arrays.
	 * Therefore json_encode defaults to empty arrays as being []. This means that associative array objects with no fields are encoded as [] rather than {}.
	 * This function replaces the empty array with a typecasted object to force encoding to {}.
	 * NB, this changes the variable pass by reference.
	 * 
	 * @param []|mixed $var the variable to check and re-encode if relevant.
	 * @return void
	 */
	public static function empty_assoc_array_to_object(&$var) {
		if (Data\Data::is_sequential_array($var) && sizeof($var) === 0) $var = new \stdClass();
	}
	
	/**
	 * Return a parsing of the Authorization HTTP header.
	 * 
	 * @var array $array
	 * @throws Error_Unauthorized
	 * @throws Error_BadRequest
	 * @return string[] an associated array of authorization method and authorization 'value' (e.g. password or key).
	 */
	public static function get_authorisation() {
		$http_header_authorisation = null;
		if (Data\Data::validate_string($_SERVER["HTTP_AUTHORIZATION"])) $http_header_authorisation = $_SERVER["HTTP_AUTHORIZATION"];
		
		$apache_http_headers = getallheaders();
		if ($apache_http_headers !== false) {
			if (Data\Data::validate_string($apache_http_headers["Authorization"])) $http_header_authorisation = $apache_http_headers["Authorization"];
		}
		
		if ($http_header_authorisation === null) throw new Error_Unauthorized("No Authorization header given");
		
		if (!preg_match("`^([a-z0-9]{1,32}(?:-?[a-z0-9]{1,32})*) ([-a-z0-9+/]+)$`i", $http_header_authorisation, $array)) throw new Error_BadRequest("Invalid Authorization header given");
		list(, $method, $value) = $array;
		
		return array("method" => $method, "value" => $value);
	}
	
	private $lockdown = false;
	
	/**
	 * Returns if the lockdown flag has been set.
	 * 
	 * @return boolean
	 */
	public function is_lockdown() {
		return $this->lockdown;
	}

	/**
	 * Sets or unsets the lockdown flag.
	 * 
	 * This is useful to rapidly disable the whole API for whatever reason.
	 * 
	 * @param boolean $state true to disable the API [lockdown flag], false to enabled the API.
	 */
	public function set_lockdown($state) {
		$this->lockdown = $state;
	}
	
	/**
	 * @internal
	 */
	private function monkey_patch_code($code) {
		return function (array $values, array $queries_or_data) use ($code) {
			if ($this->is_lockdown()) throw new Error_Forbidden("Access denied");
			
			return $code($values, $queries_or_data);
		};
	}
	
	public function head($path, $code) {
		parent::head($path, $this->monkey_patch_code($code));
	}
	
	public function get($path, $code) {
		parent::get($path, $this->monkey_patch_code($code));
	}
	
	public function post($path, $code) {
		parent::post($path, $this->monkey_patch_code($code));
	}
	
	public function put($path, $code) {
		parent::put($path, $this->monkey_patch_code($code));
	}
	
	public function patch($path, $code) {
		parent::patch($path, $this->monkey_patch_code($code));
	}
	
	public function delete($path, $code) {
		parent::delete($path, $this->monkey_patch_code($code));
	}
}
