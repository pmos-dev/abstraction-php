<?php
/**
 * Data management, parsing and validation.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Data;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction as Abstraction;

/**
 * @internal
 */
class Data_Exception extends Abstraction\Exception {}

/**
 * @internal
 */
class Data_TypeMismatchException extends Abstraction\TypeMismatchException {}

const MAX_SIGNED_32BIT_INTEGER = 2147483647;

/**
 * Data validation, conversion and parsing utilities. All methods are static.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Data {
	const REGEX_PATTERN_NUMERIC = "`^-?[0-9]{1,10}(\.[0-9]{1,64})?$`SD";
	const REGEX_PATTERN_INT = "`^-?[0-9]{1,10}$`SD";
	const REGEX_PATTERN_ID = "`^[0-9]{1,8}$`SD";
	const REGEX_PATTERN_DIGIT = "`^[0-9]$`SD";
	
	const REGEX_PATTERN_ID_CHAR = "`^[a-z0-9'#]$`iSD";
	const REGEX_PATTERN_ID_NAME = "`^[a-z0-9()'#]([-a-z0-9()'_#. ]{0,62}[-a-z0-9()'_#.])?$`iSD";
	const REGEX_PATTERN_LETTER = "`^[A-Z]$`SD";

	const REGEX_PATTERN_LETTERDIGIT = "`^[A-Z0-9]$`SD";
	
	const REGEX_PATTERN_TEXT_255 = "`^.{1,255}$`SD";

	const REGEX_PATTERN_MD5 = "`^[a-f0-9]{32}$`iSD";
	const REGEX_PATTERN_SHA224 = "`^[a-f0-9]{56}$`iSD";
	const REGEX_PATTERN_SHA256 = "`^[a-f0-9]{64}$`iSD";
	const REGEX_PATTERN_SHA512 = "`^[a-f0-9]{128}$`iSD";
	const REGEX_PATTERN_BASE36_ID = "`^[0-9A-Z]{10}$`iSD";	// this can be case insensitive because the MySQL = is itself case insensitive!
	const REGEX_PATTERN_BASE41_ID = "`^[0-9A-Za-e]{6}$`SD";
	const REGEX_PATTERN_BASE62_ID = "`^[0-9A-Za-z]{8}$`SD";
	
	const REGEX_PATTERN_HEXCOLOR = "`^[a-f0-9]{6}$`iSD";
	const REGEX_PATTERN_HEXSTRING = "`^[a-f0-9]{1,255}$`iSD";
	const REGEX_PATTERN_CSS_COLOR = "`^#([0-9a-f]{3}){1,2}$`iSD";

	const REGEX_PATTERN_DATE_DMY = "`^([0-9]{2})/([0-9]{2})/([0-9]{4})$`SD";
	const REGEX_PATTERN_DATETIME_DMYHI = "`^([0-9]{2})/([0-9]{2})/([0-9]{4}) ([0-9]{2}):([0-9]{2})$`SD";
	const REGEX_PATTERN_TIME_HI = "`^([0-9]{2}):([0-9]{2})$`SD";
	const REGEX_PATTERN_DATE_YMD = "`^([0-9]{4})-([0-9]{2})-([0-9]{2})$`SD";
	const REGEX_PATTERN_DATETIME_YMDHIS = "`^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$`SD";
	const REGEX_PATTERN_TIME_HIS = "`^([0-9]{2}):([0-9]{2}):([0-9]{2})$`SD";
	const REGEX_PATTERN_DATETIME_YMDHIS_COMPRESSED = "`^([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})$`SD";
	
	const REGEX_PATTERN_EMAIL = "`^[-a-z0-9._%+']{1,64}@([-a-z0-9]{1,64}\.)*[-a-z]{2,12}$`iSD";
	const REGEX_PATTERN_PHONE = "`^[+()0-9#*]?[- +()0-9#*]*[0-9#*]$`SD";

	const REGEX_PATTERN_URL = "`^http(s)?://([a-z0-9][-a-z0-9]{0,62})(\.[a-z0-9][-a-z0-9]{0,62}){0,32}(:[0-9]+)?(/[-a-z0-9/._?&=!~*'():#\[\]@$+,;%]{0,940})?$`iSD";
	const REGEX_PATTERN_IP = "`^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$`iSD";
	
	// @phpcs:ignore Generic.NamingConventions.UpperCaseConstantName
	const REGEX_PATTERN_IPv4 = "`^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$`iSD";
	// @phpcs:ignore Generic.NamingConventions.UpperCaseConstantName
	const REGEX_PATTERN_IPv6 = "`^((([0-9a-f]{1,4}:){7,7}[0-9a-f]{1,4})|(([0-9a-f]{1,4}:){1,7}:)|(([0-9a-f]{1,4}:){1,6}:[0-9a-f]{1,4})|(([0-9a-f]{1,4}:){1,5}(:[0-9a-f]{1,4}){1,2})|(([0-9a-f]{1,4}:){1,4}(:[0-9a-f]{1,4}){1,3})|(([0-9a-f]{1,4}:){1,3}(:[0-9a-f]{1,4}){1,4})|(([0-9a-f]{1,4}:){1,2}(:[0-9a-f]{1,4}){1,5})|([0-9a-f]{1,4}:((:[0-9a-f]{1,4}){1,6}))|(:((:[0-9a-f]{1,4}){1,7}|:)))$`i";

	private static function validate_null(&$variable, $include_empty_string = false) {
		if (!isset($variable)) $variable = null;
		if ($include_empty_string && $variable === "") $variable = null;
		
		return $variable === null;
	}
	
	public static function validate_primative(&$variable) {
		if (!isset($variable) || $variable === null) return false;
		if (is_array($variable)) return false;
		if (is_object($variable)) return false;
		if (is_resource($variable)) return false;
		
		return true;
	}
	
	/**
	 * Validates the given variable against a regular expression.
	 * 
	 * Checks for existence, type appropriateness, maximum length of 1024 chars, and overall regex pattern match.
	 * 
	 * @param string $pattern regex pattern to match against.
	 * @param string|int|float $variable variable to validate
	 * @throws Data_Exception
	 * @return boolean
	 */
	public static function validate_regex($pattern, &$variable) {
		if (!self::validate_primative($variable)) return false;
		
		if (strlen($variable) > 1024) throw new Data_Exception("Variable length is greater than 1024: this is not supported for inbuilt regex checking.");
		
		return preg_match($pattern, $variable) === 1;
	}

	public static function validate_regex_or_null($pattern, &$variable, $include_empty_string = false) {
		return self::validate_null($variable, $include_empty_string) || self::validate_regex($pattern, $variable);
	}
		
	/**
	 * Validates the given variable as a string.
	 * 
	 * Checks for existence, type appropriateness, and maximum length of 255 chars.
	 * <b>Note, strings that pass the initial type checks are whitespace-trimmed before length checking!</b>
	 * If the validation is marked as being 'optional', then non-existent variables will be populated with empty string values, otherwise the string must not be empty.
	 * 
	 * @param string $variable variable to validate
	 * @param boolean $optional whether to allow empty strings and set non-existent variables as empty.
	 * @return boolean
	 */
	public static function validate_string(&$variable, $optional = false) {
		if ($optional && !isset($variable)) { 
			$variable = ""; 
			return true; 
		} elseif (!isset($variable)) {
			return false;
		}
		
		if (is_array($variable)) return false;
		if (is_object($variable)) return false;
		if (is_resource($variable)) return false;
		
		$variable = trim($variable);
		if (!$optional && $variable === "") return false;
		if (strlen($variable) > 255) return false;
		
		return true;
	}
	
	public static function validate_string_or_null(&$variable, $optional = false, $include_empty_string = false) {
		return self::validate_null($variable, $include_empty_string) || self::validate_string($variable, $optional);
	}
	
	/**
	 * Validates the given variable against a list of possible string options.
	 * 
	 * Checks for string data type, and automatically selects between array or csv for the option list. Is case sensitive.
	 * 
	 * @param array|string $options either an array of string options or a csv list.
	 * @param string|int|float $variable variable to validate
	 * @param string $explode_character the character to explode the csv list on if applicable, default ","
	 * @throws Data_Exception
	 * @return boolean
	 */
	public static function validate_option($options, &$variable, $explode_character = ",") {
		if (!self::validate_string($variable)) return false;
		
		if (!is_array($options) && is_string($options)) $options = explode($explode_character, $options);
		if (!is_array($options)) throw new Data_Exception("Options is not an array and could not be successfully exploded as a csv list");

		return in_array($variable, $options);
	}
	
	public static function validate_option_or_null($options, &$variable, $explode_character = ",", $include_empty_string = false) {
		return self::validate_null($variable, $include_empty_string) || self::validate_option($options, $variable, $explode_character);
	}
	
	/**
	 * Validates the given variable as a number.
	 * 
	 * Checks for appropriate data content, and automatically converts string-based integers and floats to their strongly typed versions.
	 * 
	 * @param int|float|string $variable variable to validate
	 * @return boolean
	 */
	public static function validate_numeric(&$variable) {
		if (!isset($variable)) return false;

		if (is_int($variable)) {
			$variable = intval($variable);
			return true;
		}
		if (is_float($variable)) {
			$variable = floatval($variable);
			return true;
		}
		if (!is_string($variable)) return false;
		if (!self::validate_string($variable)) return false;
		
		if (!self::validate_regex(self::REGEX_PATTERN_NUMERIC, $variable)) return false;
		
		if (self::validate_regex(self::REGEX_PATTERN_INT, $variable)) return self::validate_int($variable);
				
		$variable = floatval($variable);
		return true;
	}
	
	public static function validate_numeric_or_null(&$variable, $include_empty_string = false) {
		return self::validate_null($variable, $include_empty_string) || self::validate_numeric($variable);
	}
	
	/**
	 * Validates the given variable as an integer.
	 * 
	 * Checks for appropriate data content, and automatically converts string-based integers to their strongly typed versions.
	 * Also ensures that the value is not greater than 32 bit signed integer storage capacity.
	 * 
	 * @param int|string $variable variable to validate
	 * @param int|NULL $lower lower inclusive permitted value range
	 * @param int|NULL $upper upper inclusive permitted value range
	 * @throws Data_TypeMismatchException
	 * @return boolean
	 */
	public static function validate_int(&$variable, $lower = null, $upper = null) {
		if (!isset($variable)) return false;

		if (is_int($variable)) {
			$variable = intval($variable);
			if ($variable > MAX_SIGNED_32BIT_INTEGER) throw new Data_TypeMismatchException("INT", $variable, "32 bit overflow");
			if ($lower !== null && $variable < $lower) return false;
			if ($upper !== null && $variable > $upper) return false;
			return true;
		}
		if (!is_string($variable)) return false;
		if (!self::validate_string($variable)) return false;
		
		if (self::validate_regex(self::REGEX_PATTERN_INT, $variable)) {
			$variable = intval($variable);
			if ($variable > MAX_SIGNED_32BIT_INTEGER) throw new Data_TypeMismatchException("INT", $variable, "32 bit overflow");
			if ($lower !== null && $variable < $lower) return false;
			if ($upper !== null && $variable > $upper) return false;
			return true;
		}
		return false;
	}
	
	public static function validate_int_or_null(&$variable, $lower = null, $upper = null, $include_empty_string = false) {
		return self::validate_null($variable, $include_empty_string) || self::validate_int($variable, $lower, $upper);
	}
	
	/**
	 * Validates the given variable as an 'ID'.
	 * 
	 * An ID is defined as an unsigned integer. Performs the pre-checks of validate_int on the variable first.
	 * 
	 * @param int|string $variable variable to validate
	 * @return boolean
	 */
	public static function validate_id(&$variable) {
		if (!self::validate_int($variable, 1)) return false;
		return true;
	}
	
	public static function validate_id_or_null(&$variable, $include_empty_string = false) {
		return self::validate_null($variable, $include_empty_string) || self::validate_id($variable);
	}
	
	/**
	 * Validates the given variable an 'ID object'.
	 * 
	 * An ID object is defined as an array with an ID field (which may be optionally specified) containing an ID value.
	 * 
	 * @param mixed[] $variable variable to validate, which must be an array is order to not return false
	 * @param string $field customised ID field to override the default of "id"
	 * @return boolean
	 */
	public static function validate_id_object(&$variable, $field = "id") {
		if (!isset($variable)) return false;
		if (!is_array($variable)) return false;

		return self::validate_id($variable[$field]);
	}
	
	public static function validate_id_object_or_null(&$variable) {
		return self::validate_null($variable) || self::validate_id_object($variable);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Calculates the most likely boolean state of a checkbox parameter.
	 * 
	 * Considers a variety of possible values supplied by potential forms to evaluate whether a checkbox is ticked or not.
	 * Note, unlike the validate_ methods, this does not alter the variable to boolean.
	 * 
	 * @param string|int|bool|NULL $variable variable to evaluate, usually from $_GET or $_POST 
	 * @return boolean
	 */
	public static function checkbox_boolean(&$variable) {
		if (!isset($variable)) return false;
		if (
				$variable === true || 
				$variable === 1 || $variable === "1" || 
				$variable === "on" || $variable === "On" || $variable === "ON" ||
				$variable === "yes" || $variable === "Yes" || $variable === "YES" || 
				$variable === "true" || $variable === "True" || $variable === "TRUE"
		) return $variable = true;
		return false;
	}

	/**
	 * Returns a list of valid checkbox selections for a given prefix.
	 * 
	 * @param array $list usually either $_GET or $_POST
	 * @param string $prefix the variable prefix
	 * @var array $array
	 * @return array an array of matched IDs
	 */
	public static function checkbox_array(array $list, $prefix) {
		$ids = array();
		foreach ($list as $var => $val) {
			if (!preg_match("`^{$prefix}_([0-9]+)$`i", $var, $array)) continue;

			list(, $id) = $array;
			if (!self::validate_id($id)) continue;
			
			if (self::checkbox_boolean($val)) $ids[] = $id;
		}
		return $ids;
	}

	/**
	 * Returns a trimmed piece of text or null if empty.
	 * 
	 * Used for optional database text fields, trims the given data of header and tail whitespace, and converts any resulting empty strings to explicit null.
	 * 
	 * @param string $variable the variable to evaluate
	 * @param bool|int $limit number of characters to limit the string to, or false for unlimited.
	 * @return NULL|string
	 */
	public static function trim_text_or_null(&$variable, $limit = false) {
		if (!isset($variable)) return null;
		
		$variable = trim($variable);
		if ("" === trim($variable)) return null;
		
		$copy = trim($variable);
		if ($limit === false) return $copy;
		
		if (strlen($copy) > $limit) $copy = trim(substr($copy, 0, $limit));
		
		return $copy;
	}
	
	/**
	 * Returns a trimmed piece of text or empty string if empty or null.
	 * 
	 * Used for output from optional database text fields which may be using null as a placeholder for empty string.
	 * This is particularly pertenent for MySQL 5.6+ which doesn't allow TEXT NOT NULL DEFAULT '' any more. 
	 * 
	 * @param string $variable the variable to evaluate
	 * @param string $limit number of characters to limit the string to, or false for unlimited.
	 * @return string
	 */
	public static function trim_text_or_empty_string(&$variable, $limit = false) {
		if (null === ($variable = self::trim_text_or_null($variable, $limit))) return "";
		return $variable;
	}
	
	/**
	 * Forcibly limits the length of a string to the given length.
	 * 
	 * NB, the appended suffix (if any) will lengthen beyond the specific length by however long the suffix is.
	 * 
	 * @param string $string the original string
	 * @param int $max_length the maximum length of the string.
	 * @param string|false|null $hellip the suffix to append to the end of the shortened string, or false/null for none.
	 * @param boolean $hard_limit true to simply truncate the string; false to try and find an appropriate piece of whitespace before the length limit to break on.
	 * @var array $array
	 * @return string the limited length string.
	 */
	public static function limitlength($string, $max_length, $hellip = "...", $hard_limit = true) {
		if (strlen($string) <= $max_length) return $string;

		$result = "";
		if ($hard_limit || ($max_length < 10)) {
			$result .= substr($string, 0, $max_length);
		} else {
			if (!preg_match("`^(.{" . ($max_length - 8) . ",{$max_length}})\W`", $string, $array)) $result .= substr($string, 0, $max_length);
			else {
				$result .= $array[1];
			}
		}
		
		if ($hellip !== false && $hellip !== null) $result .= $hellip;
		
		return $result;
	}
	
	/**
	 * Automatically de-indents a multiline piece of text to its lowest possible level.
	 * 
	 * This removes tab whitespace on all lines up to the point that the least indented line is flush left.
	 * 
	 * This is particularly useful for automatically generated SQL code.
	 * 
	 * @param string $text the text to consider.
	 * @var array $array
	 * @return string the de-indented text.
	 */
	public static function deindent($text) {
		$indent = null;
		foreach (explode("\n", $text) as $line) {
			if ("" === trim($line)) continue;
	
			$i = 0;
			while (preg_match("`^\t(.*)$`", $line, $array)) {
				$i++;
				list(, $line) = $array;
			}
	
			if ($indent === null || $indent > $i) $indent = $i;
		}
	
		if ($indent > 0) $text = preg_replace("`^\t{{$indent}}`m", "", $text);
		return $text;
	}

	/**
	 * Convenience method to check if one string starts with another.
	 * 
	 * @param string $text the string to check in
	 * @param string $test to string to look for
	 * @param boolean $allow_entire_string true to allow entire string matchs; false to only allow strictly 'starting'
	 * @return boolean
	 */
	public static function starts_with($text, $test, $allow_entire_string = true) {
		$text_length = strlen($text);
		$test_length = strlen($test);
		if ($text_length < $test_length) return false;
		if (!$allow_entire_string && $text_length === $test_length) return false;
		
		return substr($text, 0, $test_length) === $test;
	}
		
	/**
	 * Convenience method to check if one string ends with another.
	 *
	 * @param string $text the string to check in
	 * @param string $test to string to look for
	 * @param boolean $allow_entire_string true to allow entire string matchs; false to only allow strictly 'ending'
	 * @return boolean
	 */
	public static function ends_with($text, $test, $allow_entire_string = true) {
		$text_length = strlen($text);
		$test_length = strlen($test);
		if ($text_length < $test_length) return false;
		if (!$allow_entire_string && $text_length === $test_length) return false;
		
		$offset = $text_length - $test_length;
		
		return substr($text, $offset) === $test;
	}
	
	/**
	 * Adds a terminal slash to a path string if not already there.
	 * 
	 * @param string $path the path string
	 * @return string
	 */
	public static function enslash_path($path) {
		if (!self::ends_with($path, "/")) $path .= "/";
		return $path;
	}
	
	/**
	 * Removes a terminal slash from a path string if there.
	 *
	 * @param string $path the path string
	 * @return string
	 */
	public static function deslash_path($path) {
		if (self::ends_with($path, "/")) $path = substr($path, 0, strlen($path) - 1);
		return $path;
	}

	/**
	 * Removes markup tags [...] from a string
	 * 
	 * @param string $markup the string containing the markup
	 * @return string
	 */
	public static function demarkup($markup) {
		$markup = preg_replace("`\[[a-z]+( [a-z]+=((\"[^\"]+\")|[^ \]]+))*\]`i", "", $markup);	// opening tags
		$markup = preg_replace("`\[/[a-z]+]`i", "", $markup);	// closing tags
		
		return $markup;
	}

	//-------------------------------------------------------------------------
	
	/**
	 * Returns the ordinal unit for a number, e.g. st, nd, rd, th
	 * 
	 * @param int $value
	 * @throws Data_TypeMismatchException if the supplied $value is not an integer
	 * @return string
	 */
	public static function ordinal_unit($value) {
		if (!self::validate_int($value)) throw new Data_TypeMismatchException('Cannot compute ordinal unit for invalid integer', $value);
		
		$s = strval($value);
		if (strlen($s) === 1) {
			switch ($s) {
				case "1":	return "st";
				case "2":	return "nd";
				case "3":	return "rd";
				default:	return "th";
			}
		}
		
		$last = substr($s, strlen($s) - 2, 1);
		if ($last === "1") return "th";	// 10-19
		
		return self::ordinal_unit(intval($last));
	}
	
	/**
	 * Formats an integer with a 'pretty' ordinal unit, e.g. 1st, 2nd, 3rd, 4th ... 21st, 99th
	 * @param int $value
	 * @throws Data_TypeMismatchException if the supplied $value is not an integer
	 * @return string
	 */
	public static function pretty_ordinal($value) {
		if (!self::validate_int($value)) throw new Data_TypeMismatchException('Cannot display pretty with ordinal unit for invalid integer', $value);
		
		return "${value}" . self::ordinal_unit(abs($value));
	}
	
	//-------------------------------------------------------------------------
	
	const PRETTY_DATE_STYLE_PRETTY = "pretty";
	const PRETTY_DATE_STYLE_PRETTY_ABSOLUTE = "pretty_absolute";
	const PRETTY_DATE_STYLE_DMY = "dmy";
	const PRETTY_DATE_STYLE_SQL = "sql";
	const PRETTY_DATE_STYLE_CSV = "csv";
	
	/**
	 * Formats a date in a 'pretty' style.
	 * 
	 * Dates are returned in the format specified: e.g. 2013-05-24 10:15:30
	 * - PRETTY_DATE_STYLE_PRETTY: Fri 24 May 2013
	 * - PRETTY_DATE_STYLE_PRETTY_ABSOLUTE: Ad per pretty, but not with "Today", "Yesterday" etc.
	 * - PRETTY_DATE_STYLE_DMY: 24/05/13
	 * - PRETTY_DATE_STYLE_CSV: (as per DMY)
	 * - PRETTY_DATE_STYLE_SQL: 2013-05-24
	 * 
	 * @param string $date any string date that can be parsed by strtotime()
	 * @param string $style PRETTY_DATE_STYLE_PRETTY, PRETTY_DATE_STYLE_DMY or PRETTY_DATE_STYLE_SQL 
	 * @throws Data_Exception
	 * @return string
	 */
	public static function pretty_date($date, $style = self::PRETTY_DATE_STYLE_PRETTY) {
		$timestamp = strtotime($date);

		if ($style == self::PRETTY_DATE_STYLE_PRETTY || $style == self::PRETTY_DATE_STYLE_PRETTY_ABSOLUTE) {
			if ($style == self::PRETTY_DATE_STYLE_PRETTY) {
				if (date("Ymd") == date("Ymd", $timestamp)) return "Today";
				if (date("Ymd") == date("Ymd", ($timestamp + (60 * 60 * 24)))) return "Yesterday";
				if (date("Ymd") == date("Ymd", ($timestamp - (60 * 60 * 24)))) return "Tomorrow";
			}
			return date("D j M Y", $timestamp);
		}

		if ($style == self::PRETTY_DATE_STYLE_DMY) return date("d/m/y", $timestamp);
		if ($style == self::PRETTY_DATE_STYLE_CSV) return date("d/m/y", $timestamp);
		if ($style == self::PRETTY_DATE_STYLE_SQL) return date("Y-m-d", $timestamp);

		throw new Data_Exception("Unknown date style");
	}

	/**
	 * Formats a time in a 'pretty' style.
	 * 
	 * This is primarily used for building pretty_datetime, and on its own is not especially 'pretty'. There is only the one style.
	 * 
	 * @param string $date any string date that can be parsed by strtotime()
	 * @return string
	 */
	public static function pretty_time($date) {
		$timestamp = strtotime($date);
		return date("H:i", $timestamp);
	}

	/**
	 * Formats a datetime timestamp in a 'pretty' style.
	 * 
	 * Timestamps are returned in the format specified: e.g. 2013-05-24 10:15:30
	 * - PRETTY_DATE_STYLE_PRETTY: Fri 24 May 2013, 10:15
	 * - PRETTY_DATE_STYLE_PRETTY_ABSOLUTE: Ad per pretty, but not with "Today", "Yesterday" etc.
	 * - PRETTY_DATE_STYLE_DMY: 24/05/13, 10:15
	 * - PRETTY_DATE_STYLE_CSV: 24/05/13 10:15
	 * - PRETTY_DATE_STYLE_SQL: 2013-05-24 10:15
	 * 
	 * @param string $date any string date that can be parsed by strtotime()
	 * @param string $style PRETTY_DATE_STYLE_PRETTY, PRETTY_DATE_STYLE_DMY or PRETTY_DATE_STYLE_SQL 
	 * @throws Data_Exception
	 * @return string
	 */
	public static function pretty_datetime($date, $style = self::PRETTY_DATE_STYLE_PRETTY) {
		return self::pretty_date($date, $style) . ($style === self::PRETTY_DATE_STYLE_PRETTY || $style === self::PRETTY_DATE_STYLE_PRETTY_ABSOLUTE || $style === self::PRETTY_DATE_STYLE_DMY ? "," : "") . " " . self::pretty_time($date);
	}
	
	/**
	 * Formats a datetime timestamp range in a 'pretty' style.
	 * 
	 * Timestamps are returned in the format specified: e.g. 2013-05-24 10:15:30
	 * - PRETTY_DATE_STYLE_PRETTY: Fri 24 May 2013 10:15
	 * - PRETTY_DATE_STYLE_DMY: 24/05/13 10:15
	 * - PRETTY_DATE_STYLE_SQL: 2013-05-24 10:15
	 * 
	 * Datetimes on the same day only include the date for the start time, whereas different days are displayed in full.
	 * 
	 * @param string $a any string date that can be parsed by strtotime()
	 * @param string $b any string date that can be parsed by strtotime()
	 * @param string $style PRETTY_DATE_STYLE_PRETTY, PRETTY_DATE_STYLE_DMY or PRETTY_DATE_STYLE_SQL 
	 * @throws Data_Exception
	 * @return string
	 */
	public static function pretty_datetime_range($a, $b, $style = self::PRETTY_DATE_STYLE_PRETTY) {
		if (self::is_same_date($a, $b)) return self::pretty_datetime($a, $style) . " - " . self::pretty_time($b);
		
		return self::pretty_datetime($a, $style) . " - " . self::pretty_datetime($b, $style);
	}
	
	/**
	 * Formats a datetime timestamp range duration in a 'pretty' style.
	 * 
	 * NB, negative durations are returned as their abs value.
	 *
	 * @param string $a any string date that can be parsed by strtotime()
	 * @param string $b any string date that can be parsed by strtotime()
	 * @param string $style PRETTY_DATE_STYLE_PRETTY, PRETTY_DATE_STYLE_DMY or PRETTY_DATE_STYLE_SQL
	 * @throws Data_Exception
	 * @return string
	 */
	public static function pretty_datetime_duration($a, $b) {
		$delta = abs(strtotime($b) - strtotime($a));
		
		if ($delta === 0) return "";
		if ($delta === 1) return "1 sec";
		
		if ($delta < 60) return "${delta} secs";
		
		$delta /= 60;
		if (intval(round($delta)) === 1) return "1 min";
		if ($delta < 60) {
			return intval(round($delta)) . " mins";
		}
		
		$delta /= 60;
		if (intval(round($delta)) === 1) return "1 hour";
		if ($delta < 24) return intval(round($delta)) . " hours";
		
		$delta /= 24;
		if (intval(round($delta)) === 1) return "1 day";
		return intval(round($delta)) . " days";
	}
	
	/**
	 * Returns D/M/Y formatting for a given Y-M-D date, or false on failure.
	 * 
	 * @param string $ymd
	 * @var array $array
	 * @return boolean|string
	 */
	public static function date_ymd_to_dmy($ymd) {
		if (!preg_match(self::REGEX_PATTERN_DATE_YMD, $ymd, $array)) return false;
		list(/*_*/, $year, $month, $day) = $array;
		
		return "{$day}/{$month}/{$year}";
	}

	/**
	 * Returns D/M/Y H:I formatting for a given Y-M-D H:I:S date, or false on failure.
	 * 
	 * @param string $ymdhis
	 * @var array $array
	 * @return boolean|string
	 */
	public static function datetime_ymdhis_to_dmyhi($ymdhis) {
		if (!preg_match(self::REGEX_PATTERN_DATETIME_YMDHIS, $ymdhis, $array)) return false;
		list(/*_*/, $year, $month, $day, $hour, $min, /*_*/) = $array;
		
		return "{$day}/{$month}/{$year} {$hour}:{$min}";
	}

	/**
	 * Returns Y-M-D formatting for a given D/M/Y date, or false on failure.
	 * 
	 * @param string $dmy
	 * @var array $array
	 * @return boolean|string
	 */
	public static function date_dmy_to_ymd($dmy) {
		if (!preg_match(self::REGEX_PATTERN_DATE_DMY, $dmy, $array)) return false;
		list(/*_*/, $day, $month, $year) = $array;
		
		return "{$year}-{$month}-{$day}";
	}
	
	/**
	 * Returns Y-M-D H:I:00 formatting for a given D/M/Y H:I date, or false on failure.
	 * 
	 * @param string $dmyhi
	 * @var array $array
	 * @return boolean|string
	 */
	public static function datetime_dmyhi_to_ymdhis($dmyhi) {
		if (!preg_match(self::REGEX_PATTERN_DATETIME_DMYHI, $dmyhi, $array)) return false;
		list(/*_*/, $day, $month, $year, $hour, $min) = $array;
		
		return "{$year}-{$month}-{$day} {$hour}:{$min}:00";
	}

	/**
	 * Returns YmdHis compressed formatting for a given Y-m-d H:i:s date, or false on failure.
	 *
	 * @param string $ymdhis
	 * @return boolean|string
	 */
	public static function datetime_ymdhis_to_compressed($ymdhis) {
		if (!self::validate_regex(self::REGEX_PATTERN_DATETIME_YMDHIS, $ymdhis)) return false;
		
		return preg_replace("`[-: ]`", "", $ymdhis);
	}
	
	/**
	 * Returns Y-m-d H:i:s formatting for a given compressed date, or false on failure.
	 *
	 * @param string $ymdhis
	 * @var array $array
	 * @return boolean|string
	 */
	public static function datetime_compressed_to_ymdhis($compressed) {
		if (!preg_match(self::REGEX_PATTERN_DATETIME_YMDHIS_COMPRESSED, $compressed, $array)) return false;
		list(/*_*/, $year, $month, $day, $hour, $min, $sec) = $array;
		
		return "{$year}-{$month}-{$day} {$hour}:{$min}:{$sec}";
	}
	
	/**
	 * Splits a Y-M-D H:I:S datetime timestamp into its component Y-M-D date and H:I:S time parts, or returns false on failure.
	 *  
	 * @param string $ymdhis
	 * @var array $array
	 * @return boolean|string[]
	 */
	public static function split_datetime_ymdhis($ymdhis) {
		if (!preg_match(self::REGEX_PATTERN_DATETIME_YMDHIS, $ymdhis, $array)) return false;
		list(/*_*/, $year, $month, $day, $hour, $min, $sec) = $array;
		
		return array("{$year}-{$month}-{$day}", "{$hour}:{$min}:{$sec}");
	}
	
	/**
	 * Returns whether two datetimes occur on the same date (day) irrespective of the time
	 *  
	 * @param string $a a YMDHIS formatted date
	 * @param string $b a YMDHIS formatted date
	 * @return boolean
	 */
	public static function is_same_date($a, $b) {
		if (!preg_match(self::REGEX_PATTERN_DATETIME_YMDHIS, $a)) throw new Data_TypeMismatchException("Date YMDHIS", $a);
		if (!preg_match(self::REGEX_PATTERN_DATETIME_YMDHIS, $b)) throw new Data_TypeMismatchException("Date YMDHIS", $b);
		
		list($da, ) = self::split_datetime_ymdhis($a);
		list($db, ) = self::split_datetime_ymdhis($b);
		
		return $da === $db;
	}

	/**
	 * Increments the given date by the specified interval.
	 * 
	 * This allows for looping over days in a range without running into problems with daylight saving time problems encountered by simply appending 24*3600 to a Unix timestamp.
	 *
	 * @param string $date a YMD formatted date
	 * @param string $interval the interval to apply, e.g. +1 day
	 * @return string
	 */
	public static function increment_date($date, $interval) {
		if (!preg_match(self::REGEX_PATTERN_DATE_YMD, $date)) throw new Data_TypeMismatchException("Date YMD", $date);
	
		return date("Y-m-d", strtotime("{$date} 00:00:00 {$interval}"));
	}
	
	/**
	 * Automatically orders two datetimes so that the start and end are the right way round.
	 *  
	 * @param string $start a YMDHIS formatted date
	 * @param string $end a YMDHIS formatted date
	 * @return void
	 */
	public static function auto_order_datetime(&$start, &$end) {
		if (!preg_match(self::REGEX_PATTERN_DATETIME_YMDHIS, $start)) throw new Data_TypeMismatchException("Date YMDHIS", $start);
		if (!preg_match(self::REGEX_PATTERN_DATETIME_YMDHIS, $end)) throw new Data_TypeMismatchException("Date YMDHIS", $end);
		
		if (strtotime($end) < strtotime($start)) list($end, $start) = array($start, $end);
	}

	/**
	 * Automatically orders two datetimes so that the start and end are the right way round.
	 *
	 * @param string $start a YMD formatted date
	 * @param string $end a YMD formatted date
	 * @return void
	 */
	public static function auto_order_date(&$start, &$end) {
		if (!preg_match(self::REGEX_PATTERN_DATE_YMD, $start)) throw new Data_TypeMismatchException("Date YMD", $start);
		if (!preg_match(self::REGEX_PATTERN_DATE_YMD, $end)) throw new Data_TypeMismatchException("Date YMD", $end);
	
		if (strtotime("{$end} 00:00:00") < strtotime("{$start} 00:00:00")) list($end, $start) = array($start, $end);
	}
	
	/**
	 * Returns D/M/Y formatting for a given Y-M-D date, or false on failure.
	 *
	 * @param string $ymd
	 * @return boolean|string
	 */
	public static function datetime_ymdhis_to_utc_ymdhis($ymdhis) {
		if (!preg_match(self::REGEX_PATTERN_DATETIME_YMDHIS, $ymdhis)) return false;

		return gmdate("Y-m-d H:i:s", strtotime($ymdhis));
	}
	
	/**
	 * Calculates the offset of London timezone against UTC for the given date and time.
	 * This can be used to determine whether GMT or BST, i.e. 0=GMT, 1=BST
	 * 
	 * @param string $ymdhis
	 * @return number
	 */
	public static function get_london_utc_offset($ymdhis) {
		$time = strtotime($ymdhis);
		$stated_hour = intval(date("H", $time));
		
		$dt = new \DateTime();
		$dt = $dt->setTimeZone(new \DateTimeZone("UTC"));
		
		$dt->setDate(
				intval(date("Y", $time)),
				intval(date("m", $time)),
				intval(date("d", $time))
		);
		$dt->setTime(
				$stated_hour,
				intval(date("i", $time)),
				intval(date("s", $time))
		);
		
		$dt->setTimeZone(new \DateTimeZone("Europe/London"));
		
		$actual_hour = intval($dt->format("H"));
		$offset = $actual_hour - $stated_hour;
		
		return $offset;
	}
	//-------------------------------------------------------------------------

	/**
	 * Iterates an array of ID object arrays and extracts the ID values.
	 * 
	 * @param array $array
	 * @param string $field customised ID field to override the default of "id"
	 * @throws Data_TypeMismatchException
	 * @return int[]
	 */
	public static function extract_ids_from_id_object_array(array $array, $field = "id") {
		$ids = array();
		foreach ($array as $object) {
			if (!self::validate_id_object($object, $field)) throw new Data_TypeMismatchException("Array of ID objects", $object, "Array does not contain solely ID objects");
			$ids[] = $object[$field];
		}
		return $ids;
	}

	/**
	 * Iterates an array of object arrays with an REGEX_PATTERN_ID_NAME field and extracts the ID values.
	 *
	 * @param array $array
	 * @param string $field the field that contains the ID_NAME
	 * @throws Data_TypeMismatchException
	 * @return string[]
	 */
	public static function extract_idnames_from_idname_object_array(array $array, $field) {
		$idnames = array();
		foreach ($array as $object) {
			if (!isset($object) || !is_array($object)) throw new Data_TypeMismatchException("Array of ID objects", $object, "Array does not contain associative arrays");
			if (!self::validate_regex(self::REGEX_PATTERN_ID_NAME, $object[$field])) throw new Data_TypeMismatchException("Array of ID objects", $object, "Array does not contain solely objects with such an ID_NAME field");
			$idnames[] = $object[$field];
		}
		return $idnames;
	}
	
	/**
	 * Searches an array of ID object arrays to see if the given ID object is contained within it.
	 * 
	 * Note that checking is performed by comparing the ID field values, not the entire ID object.
	 * 
	 * @param mixed[] $object ID object needle to search for
	 * @param array $array haystack of ID objects
	 * @param string $field customised ID field to override the default of "id"
	 * @throws Data_TypeMismatchException
	 * @return boolean
	 */
	public static function is_id_object_in_array(array $object, array $array, $field = "id") {
		if (!self::validate_id_object($object, $field)) throw new Data_TypeMismatchException("ID object", $object);
		
		foreach ($array as $a) {
			if (!self::validate_id_object($a, $field)) throw new Data_TypeMismatchException("Array of ID objects", $a, "Array does not contain solely ID objects");
			if ($a[$field] === $object[$field]) return true;
		}
		return false;
	}

	/**
	 * Removes any matching ID object from an array of ID objects, and returns the new array.
	 * 
	 * Note that checking is performed by comparing the ID field values, not the entire ID object.
	 * 
	 * @param mixed[] $object ID object needle to search for
	 * @param array $array haystack of ID objects
	 * @param string $field customised ID field to override the default of "id"
	 * @throws Data_TypeMismatchException
	 * @return array
	 */
	public static function array_remove_id_object(array $object, array $array, $field = "id") {
		if (!self::validate_id_object($object, $field)) throw new Data_TypeMismatchException("ID object", $object);
				
		$replacement = array();
		foreach ($array as $a) {
			if (!self::validate_id_object($a, $field)) throw new Data_TypeMismatchException("Array of ID objects", $a, "Array does not contain solely ID objects");
			if ($a[$field] === $object[$field]) continue;
			
			$replacement[] = $a;
		}
		
		return $replacement;
	}

	/**
	 * Builds a lookup table of ID field values mapping to their corresponding ID object.
	 * 
	 * @param array $objects unmapped array of ID objects
	 * @param string $id_field customised ID field to override the default of "id"
	 * @throws Data_TypeMismatchException
	 * @return array an associative array of ID value => ID object array.
	 */
	public static function build_firstclass_lookup(array $objects, $id_field = "id") {
		$lookup = array();
		foreach ($objects as $object) {
			if (!self::validate_id_object($object, $id_field)) throw new Data_TypeMismatchException("Array of ID objects", $objects, "Array does not contain solely ID objects");
			$lookup[$object[$id_field]] = $object;
		}
		
		return $lookup;
	}
	
	/**
	 * Builds a lookup table of UID (base62 or otherwise) field values mapping to their corresponding ID object.
	 *
	 * @param array $objects unmapped array of ID objects
	 * @param string $uid_field customised UID field to override the default of "uid"
	 * @throws Data_TypeMismatchException
	 * @return array an associative array of UID value => ID object array.
	 */
	public static function build_uid_lookup(array $objects, $uid_field = "uid") {
		$lookup = array();
		foreach ($objects as $object) {
			if (!self::validate_id_object($object)) throw new Data_TypeMismatchException("Array of ID objects", $objects, "Array does not contain solely ID objects");
			$lookup[$object[$uid_field]] = $object;
		}
		
		return $lookup;
	}
	
	/**
	 * Prunes an array of ID field objects to be unique.
	 * 
	 * Note, the original order of the array is preserved.
	 * 
	 * @param array $objects unmapped array of ID objects
	 * @param string $id_field customised ID field to override the default of "id"
	 * @throws Data_TypeMismatchException
	 * @return array the unique objects.
	 */
	public static function object_array_unique(array $objects, $id_field = "id") {
		$unique = array();
		$ids = array();
		foreach ($objects as $object) {
			if (!self::validate_id_object($object, $id_field)) throw new Data_TypeMismatchException("Array of ID objects", $objects, "Array does not contain solely ID objects");
			
			if (in_array($object[$id_field], $ids)) continue;
			$unique[] = $object;
			$ids[] = $object[$id_field];
		}
		
		return $unique;
	}
	
	/**
	 * Merges two or more arrays of ID field objects.
	 *
	 * Note, the original order of the arrays is preserved.
	 *
	 * @param ...array $arrays as many unmapped arrays of ID objects as required (at least 2)
	 * @param string $id_field an optional customised ID field to override the default of "id"
	 * @throws Data_TypeMismatchException
	 * @return array the merged objects.
	 */
	public static function object_array_merge() {
		// PHP less than 5.6 doesn't support ... notation, so have to use the historical functions
		
		if (func_num_args() < 2) throw new Data_TypeMismatchException("Merging object arrays requires at least 2 arguments", "");

		$arrays = array();
		$id_field = "id";
		for ($i = 0; $i < func_num_args(); $i++) {
			$arg = func_get_arg($i);
			if (is_array($arg)) {
				$arrays[] = $arg;
				continue;
			} else {
				if ($i === (func_num_args() - 1) && is_string($arg)) {
					$id_field = $arg;
				} else {
					throw new Data_TypeMismatchException("Parameters of object_array_merge must be arrays, except for the last one which may be a string.", "");
				}
			}
		}
		
		$combined = array();
		foreach ($arrays as $array) {
			foreach ($array as $object) {
				if (!self::validate_id_object($object, $id_field)) throw new Data_TypeMismatchException("Array of ID objects", $object, "Array does not contain solely ID objects");
				$combined[] = $object;
			}
		}
		
		return self::object_array_unique($combined, $id_field);
	}
	
	/**
	 * Parses and validates an array read from a CSV source (or similar) into a consistent grid format.
	 * 
	 * @param array $data the original array to process
	 * @param boolean $has_header_row true if the data has a header row; false if not
	 * @param boolean $pad_mismatch_columns true if mis-sized rows show be padded to fit; false to throw an exception
	 * @param boolean $pad_as_empty_string if using $pad_mismatch_columns, true to use an empty string; false to use null
	 * @throws Data_TypeMismatchException if the number of columns per row is not consistent after considering padding.
	 * @return mixed[][] an array of arrays (possibly associated if a header row exists) containing the grid.
	 */
	public static function array_csv_all(array $data, $has_header_row = false, $pad_mismatch_columns = false, $pad_as_empty_string = false) {
		$columns = null;
	
		if ($has_header_row) {
			$headers = array_shift($data);
			if (0 === ($columns = sizeof($headers))) return array();
		}
	
		if (sizeof($data) === 0) return array();
	
		if ($pad_mismatch_columns) {
			if (!$has_header_row) foreach ($data as $row) $columns = max($columns, sizeof($row));
				
			$padded = array();
			foreach ($data as $row) {
				while (sizeof($row) < $columns) $row[] = $pad_as_empty_string ? "" : null;
				$padded[] = $row;
			}
				
			$data = $padded;
		}
	
		if ($columns === null) $columns = sizeof($data[0]);
	
		$result = array();
		foreach ($data as $row) {
			if (sizeof($row) !== $columns) throw new Data_TypeMismatchException("CSV does not have an equal number of columns for each row", "");
				
			if ($has_header_row) {
				$assoc = array();
				foreach ($headers as $header) $assoc[$header] = array_shift($row);
	
				$row = $assoc;
			}
				
			$result[] = $row;
		}
	
		return $result;
	}
	
	/**
	 * Reads in a CSV file from an open file stream, and parses it through array_csv_all to return a consistent grid of data.
	 * 
	 * @param resource $fp the pointer to the file stream
	 * @param boolean $has_header_row true if the data has a header row; false if not
	 * @param boolean $pad_mismatch_columns true if mis-sized rows show be padded to fit; false to throw an exception
	 * @param boolean $pad_as_empty_string if using $pad_mismatch_columns, true to use an empty string; false to use null
	 * @param int $max_length the maximum length of line to allow, 4096 by default
	 * @throws Data_TypeMismatchException if the number of columns per row is not consistent after considering padding.
	 * @return mixed[][] an array of arrays (possibly associated if a header row exists) containing the grid.
	 */
	public static function fgetcsv_all($fp, $has_header_row = false, $pad_mismatch_columns = false, $pad_as_empty_string = false, $ignore_empty_lines = true, $max_length = 4096) {
		if (feof($fp)) return array();
	
		$data = array();
		while (!feof($fp)) {
			$row = fgetcsv($fp, $max_length);
			
			if (!$row) $row = array('');
			if (sizeof($row) === 1 && ($row[0] === '' || $row[0] === null) && $ignore_empty_lines) continue;
				
			$data[] = $row;
		}
		
		return self::array_csv_all($data, $has_header_row, $pad_mismatch_columns, $pad_as_empty_string);
	}

	/**
	 * Takes a file pointer resource, reads it into a temporary file, replacing CR with NL, and then closes the original file and returns a pointer to the new temporary file.
	 * 
	 * This is useful for parsing CSV files which have been generated on Mac computers.
	 * 
	 * @param resource $in the original file pointer
	 * @return resource the newly opened temporary file pointer
	 */
	public static function dirty_patch_file_cr_to_linebreaks($in) {
		$out = tmpfile();
		while (!feof($in)) {
			$s = fread($in, 4096);
			$s = str_replace("\r\n", "\n", $s);
			$s = str_replace("\r", "\n", $s);
			fwrite($out, $s);
		}
		fclose($in);
		
		fflush($out);
		fseek($out, 0);
		
		return $out;
	}
	
	public static function array_uremove(array $array, $callback) {
		$pruned = array();
		foreach ($array as $a) if ($callback($a)) $pruned[] = $a;
		
		return $pruned;
	}
	
	/**
	 * Removes any null values from a key-value associative array. Modifies the array in place.
	 * 
	 * @param array $array the associative array to process.
	 */
	public static function assoc_remove_null_values(array &$array) {
		$toremove = array();
		foreach ($array as $key => $value) if ($value === null) $toremove[] = $key;
		
		foreach ($toremove as $key) unset($array[$key]);
	}
	
	/**
	 * Returns if an array is associative, i.e. has string keys
	 * 
	 * @param array $array the potentially associative array to check.
	 * @return boolean true if the array is associative, false if it is sequential or isn't an array in the first place.
	 */
	public static function is_assoc_array(&$array) {
		if (!isset($array) || !is_array($array)) return false;
		
		if (sizeof(array_keys($array)) === 0) return true;
		
		// any string means associative, even if numeric also exist
		foreach (array_keys($array) as $key) {
			if (is_string($key)) return true;
		}
		
		return false;
	}
	
	/**
	 * Returns if an array is sequential, i.e. has only numbers
	 * 
	 * @param array $array the potentially associative array to check.
	 * @param boolean true to also check the index sequence is 0, 1, 2, 3... etc. with no gaps or out of order.
	 * @return boolean true if the array is sequential, false if it is associative, isn't an array in the first place, or isn't indexed as a sequence.
	 */
	public static function is_sequential_array(&$array, $check_sequence = false) {
		if (!isset($array) || !is_array($array)) return false;
		
		if (sizeof(array_keys($array)) === 0) return true;
		
		// no strings are allowed in sequential arrays
		$tally = 0;
		foreach (array_keys($array) as $key) {
			if (is_string($key)) return false;
			if ($check_sequence && $key !== $tally) return false;
			$tally++;
		}
		
		return true;
	}
	
	/**
	 * Attempts to merge two associative arrays, using deep intelligent merging.
	 * 
	 * Primatives replace existing. Sequential arrays are appended. Associative arrays are recursively merged.
	 *
	 * @param array|mixed $existing the existing value to consider.
	 * @param array|mixed $replacement the replacement value to consider.
	 * @throws Data_Exception if a mapping can't be achieved meaningfully.
	 * @return mixed the return merged/replaced value.
	 */
	public static function assoc_deep_merge_replace_primatives($existing, $replacement) {
		if (self::is_assoc_array($existing) && self::is_assoc_array($replacement)) {
			foreach ($replacement as $key => $value) {
				if (array_key_exists($key, $existing)) $existing[$key] = self::assoc_deep_merge_replace_primatives($existing[$key], $value);
				else $existing[$key] = $value;
			}
			return $existing;
		}

		if (self::is_sequential_array($existing) && self::is_sequential_array($replacement)) {
			foreach ($replacement as $value) {
				if (in_array($value, $existing)) {
					if (false === ($i = array_search($value, $existing))) throw new Data_Exception("Error merging arrays. Unable to derive index.");
					$existing[$i] = self::assoc_deep_merge_replace_primatives($existing[$i], $value);
				} else {
					$existing[] = $value;
				}
			}
			return $existing;
		}
		
		if (self::is_assoc_array($existing) && self::is_sequential_array($replacement)) {
			// no real way to do this, so just merge in
			foreach ($replacement as $value) {
				if (in_array($value, $existing)) {
					if (false === ($i = array_search($value, $existing))) throw new Data_Exception("Error merging arrays. Unable to derive index.");
					$existing[$i] = self::assoc_deep_merge_replace_primatives($existing[$i], $value);
				} else {
					$existing[] = $value;
				}
			}
			return $existing;
		}
		
		if (self::is_sequential_array($existing) && self::is_assoc_array($replacement)) {
			// no real way to do this, so just merge in
			foreach ($replacement as $key => $value) {
				if (@array_key_exists($key, $existing)) $existing[$key] = self::assoc_deep_merge_replace_primatives($existing[$key], $value);
				else $existing[$key] = $value;
			}
			return $existing;
		}

		if (is_array($existing) && self::validate_primative($replacement)) {
			// same for both associative and sequential
			$existing[] = $replacement;
			return $existing;
		}
		if (is_array($replacement) && self::validate_primative($existing)) {
			// same for both associative and sequential
			$replacement[] = $existing;
			return $replacement;
		}
		
		// definitely not an array now
		
		if (!self::validate_primative($existing) && !self::validate_null($existing)) throw new Data_Exception("Unable to derive existing value type for merge");
		if (!self::validate_primative($replacement) && !self::validate_null($replacement)) throw new Data_Exception("Unable to derive replacement value type for merge");

		// definitely a primative or null now
		
		if ($existing === null && $replacement === null) return null;
		if ($replacement === null) return $existing;
		return $replacement;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns a hexidecimal representation of binary data.
	 * 
	 * @param string $data data to be encoded
	 * @return string
	 */
	public static function hex_encode($data) {
		return bin2hex($data);
	}

	/**
	 * Decodes hexidecimal representations back into binary data.
	 * 
	 * @param string $hex hex string to be decoded
	 * @return string
	 */
	public static function hex_decode($hex) {
		return pack("H*", $hex);
	}

	/**
	 * Returns a hexidecimal representation of binary data, chunking it into 32 character with whitespace.
	 * 
	 * @param string $data data to be encoded
	 * @return string
	 */
	public static function hexchunk_encode($data) {
		return trim(chunk_split(self::hex_encode($data), 32, " "));
	}
	
	/**
	 * Decodes chunked hexidecimal representations back into binary data.
	 * 
	 * Note, any size chunking will be handled, not just 32 character chunking from hexchunk_encode, including varied size chunking.
	 * 
	 * @param string $hex hex string to be decoded
	 * @return string
	 */
	public static function hexchunk_decode($hex) {
		return self::hex_decode(str_replace(" ", "", $hex));
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Replaces common 'Word' characters with simple equivalents. These tend to include things like wide dashes and fancy quotation marks.
	 * 
	 * @param string $string the source string to be sanitised.
	 * @return mixed the sanitised string.
	 */
	public static function sanitise_word_characters($string) {
		return str_replace(array(chr(145), chr(146), chr(147), chr(148), chr(151), chr(226)), array("'", "'", '"', '"', '-', '-'), $string);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Computes the Damerau Levenshtein similarity between two strings
	 * 
	 * @param string $__this
	 * @param string $that
	 * @param boolean|int $limit
	 * @return [] an associated array of values
	 */
	public static function damerau_levenshtein($__this, $that, $limit = false) {
		$thisLength = strlen($__this);
		$thatLength = strlen($that);
		$matrix = array();
		
		// If the limit is not defined it will be calculate from this and that args.
		if (!self::validate_int($limit)) $limit = (($thatLength > $thisLength ? $thatLength : $thisLength));
		$limit++;
		
		for ($i = 0; $i < $limit; $i++) {
			$matrix[$i] = array($i);
			while (sizeof($matrix[$i]) < $limit) $matrix[$i][] = 0;
		}
		for ($i = 0; $i < $limit; $i++) {
			$matrix[0][$i] = $i;
		}
		
		if (abs($thisLength - $thatLength) > ($limit || 100)){
			return self::dl_prepare($limit || 100, $thisLength, $thatLength);
		}
		if ($thisLength === 0){
			return self::dl_prepare($thatLength, $thisLength, $thatLength);
		}
		if ($thatLength === 0){
			return self::dl_prepare($thisLength, $thisLength, $thatLength);
		}
		
		// Calculate matrix.
		for ($i = 1; $i <= $thisLength; ++$i) {
			$this_i = $__this[$i - 1];
			
			// Step 4
			for ($j = 1; $j <= $thatLength; ++$j) {
				// Check the jagged ld total so far
				if ($i === $j && $matrix[$i][$j] > 4) return self::dl_prepare($thisLength, $thisLength, $thatLength);
				
				$that_j = $that[$j - 1];
				$cost = ($this_i === $that_j) ? 0 : 1; // Step 5
				// Calculate the minimum (much faster than Math.min(...)).
				$min    = $matrix[$i - 1][$j    ] + 1; // Deletion.
				if (($t = $matrix[$i    ][$j - 1] + 1   ) < $min) $min = $t;   // Insertion.
				if (($t = $matrix[$i - 1][$j - 1] + $cost) < $min) $min = $t;   // Substitution.
				
				// Update matrix.
				$matrix[$i][$j] = ($i > 1 && $j > 1 && $this_i === $that[$j - 2] && $__this[$i - 2] === $that_j && ($t = $matrix[$i - 2][$j - 2] + $cost) < $min) ? $t : $min; // Transposition.
			}
		}
		
		return self::dl_prepare($matrix[$thisLength][$thatLength], $thisLength, $thatLength);
	}
	
	private static function dl_prepare($steps, $thisLength, $thatLength) {
		$length = max($thisLength, $thatLength);
		$relative = $length === 0 ? 0 : ($steps / $length);
		$similarity = 1 - $relative;
		
		return array(
			"steps" => $steps,
			"relative" => $relative,
			"similarity" => $similarity
		);
	}
	
	/**
	 * Uses Damerau Levenshtein to look for similar terms between two arrays
	 * 
	 * @param string[] $as the first array
	 * @param string[] $bs the second array
	 * @param float $threshold the similarity threshold to use
	 * @param boolean allow_substr_start whether to also allow comparison of the first 4 characters
	 * @return string[] similar terms
	 */
	public static function filter_similar_terms(array $as, array $bs, $threshold, $allow_substr_start = false) {
		$pruned_as = array();
		foreach ($as as $term) {
			if ("" === ($term = preg_replace("`[^a-z0-9]`", "", strtolower(trim($term))))) continue;
			if (!in_array($term, $pruned_as)) $pruned_as[] = $term;
		}
		
		$pruned_bs = array();
		foreach ($bs as $term) {
			if ("" === ($term = preg_replace("`[^a-z0-9]`", "", strtolower(trim($term))))) continue;
			if (!in_array($term, $pruned_bs)) $pruned_bs[] = $term;
		}

		if (sizeof($pruned_as) === 0 || sizeof($pruned_bs) === 0) return array();
		
		$similar = array();
		foreach ($pruned_as as $terma) {
			foreach ($pruned_bs as $termb) {
				$delta = self::damerau_levenshtein($terma, $termb);
				if ($delta["similarity"] >= $threshold) {
					$similar[] = $terma;
					continue;
				}
				
				if ($allow_substr_start && strlen($terma) > 3 && strlen($termb) > 3) {
					$delta = self::damerau_levenshtein(substr($terma, 0, 4), substr($termb, 0, 4));
					if ($delta["similarity"] >= $threshold) {
						$similar[] = $terma;
						continue;
					}
				}
			}
		}
		
		return $similar;
	}
	
	/**
	 * Splits a string into component terms to compare against another and return similar matching terms
	 * 
	 * @param string $terms the terms to look for
	 * @param string $haystack the haystack to search within
	 * @param float $threshold the similarity threshold to use
	 * @param boolean allow_substr_start whether to also allow comparison of the first 4 characters
	 * @return string[] similar terms
	 */
	public static function string_any_term_matches($terms, $haystack, $threshold, $allow_substr_start = false) {
		if (!self::validate_string($terms)) return array();
		if (!self::validate_string($haystack)) return array();
		
		$similar = self::filter_similar_terms(
				preg_split("`[ ,;+]`", $terms),
				preg_split("`[ ,;+]`", $haystack),
				$threshold, 
				$allow_substr_start
		);
		
		return $similar;
	}
	
	/**
	 * Removes common filler words such as prepositions from a list of words
	 * 
	 * @param string[] $words the source list of words
	 * @return string[] the pruned list of words
	 */
	public static function remove_filler_words(array $words) {
		$pruned = array();
		
		foreach ($words as $word) {
			if (strlen($word) < 2) continue;
			if (in_array(strtolower($word), explode(",", "the,be,to,of,and,a,in,that,have,i,it,for,on,with,as,do,at,this,by,from,they,we,or,an,all,would,there,their,what,so,if,when,can"))) continue;
			
			$pruned[] = $word;
		}
		
		return $pruned;
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Parses a script URL in REST notation into an array of commands/parameters.
	 * 
	 * The $_SERVER["REQUEST_URI"] variable can be used to build a simple auto-REST system. In this case, the following .htaccess file should be put into the folder containing the API.
	 * 
	 * RewriteEngine On
	 * RewriteBase /path/to/api/
	 * RewriteRule ^rest/.*$ rest.php [L]
	 * 
	 * ...and the prefix should be set to "/path/to/api/rest"
	 * 
	 * @param string prefix the REST prefix path. Trailing slashes should be included but are not mandatory.
	 * @param string|NULL $url the url to parse. If none is supplied, then the $_SERVER["REQUEST_URI"] variable is used. See notes.
	 * @return string[]|NULL the parsed REST parameters, or null if the url does not match the prefix.
	 * @throws Data_Exception
	 */
	public static function parse_rest_url($prefix, $url = null) {
		if (!self::validate_string($prefix)) throw new Data_Exception("Invalid prefix supplied", $prefix);
		
		if ($url === null) $url = $_SERVER["REQUEST_URI"];
		if (!self::validate_string($url)) throw new Data_Exception("Invalid URL supplied", $url);
		
		if (0 !== strpos($url, $prefix)) return null;
		
		$url = explode("/", substr($url, strlen($prefix)));

		// remove starting and trailing slash empty params
		while (sizeof($url) > 0 && "" === $url[0]) array_shift($url);
		while (sizeof($url) > 0 && "" === $url[sizeof($url) - 1]) array_pop($url);
		
		return $url;
	}

	//-------------------------------------------------------------------------

	/**
	 * Obfuscates an IPv4 address into another syntactially valid one, through a one-way function.
	 * 
	 * This is useful for some GDPR things, where you don't want to store the actual IP, but need a valid IP anyway, and still want to check for uniqueness rather than just random.
	 * 
	 * @param string $ip the existing IP address
	 * @throws Data_Exception if the given IP is not IPv4 format
	 * @return string the newly obfuscated IP address
	 */
	public static function obfuscate_ipv4($ip) {
		if (!self::validate_regex(self::REGEX_PATTERN_IPv4, $ip)) throw new Data_Exception("Given IP address is not an IPv4 address", $ip);
		
		$normalised = implode(array_map(
				function($chunk) { 
					return intval($chunk) + ""; 
				}, 
				explode(".", $ip)
		), ".");
		$chunks = explode(",", chunk_split(md5($normalised), 2, ","));
		$digits = array(0, 0, 0, 0);
		for ($i = 0, $j = 0; $i < 16; $i++, $j += ($i % 4) === 0 ? 1 : 0) {
			$digits[$j] += base_convert($chunks[$i], 16, 10);
		}
		foreach ($digits as &$digit) {
			$digit = ($digit % 253) + 1;
		}
		unset($digit);
		
		return implode(".", $digits);
	}
}
