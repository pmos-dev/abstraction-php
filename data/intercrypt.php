<?php
/**
 * Inter-language encryption and decryption support.
 * 
 * @copyright 2016 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Data;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction as Abstraction;

/**
 * @internal
 */
class InterCrypt_Exception extends Abstraction\Exception {}

/**
 * @internal
 */
class OpenSSL_Exception extends InterCrypt_Exception {}

/**
 * @internal
 */
class MetadataDeconstruction_Exception extends InterCrypt_Exception {}

/**
 * @internal
 */
class HashMatch_Exception extends InterCrypt_Exception {}

/**
 * Inter-language encryption and decryption methods. All methods are static.
 * 
 * @api
 * @author Pete Morris
 * @version 1.0.0
 */
class InterCrypt {
	/**
	 * Converts a string of hex values into its corresponding characters
	 * 
	 * This is essentially a wrapper for the hex2bin PHP function in order to match the API named methods required by InterCrypt.
	 * 
	 * @param string $hex the hex encoded string
	 * @return string the decoded result
	 */
	public static function dehex($hex) {
		return hex2bin($hex);
	}
	
	/**
	 * Represents a string as hexadecimal values for each character.
	 * 
	 * This is essentially a wrapper for the bin2hex PHP function in order to match the API named methods required by InterCrypt.
	 * 
	 * @param string $bin the original string
	 * @return string the encoded result
	 */
	public static function enhex($bin) {
		return bin2hex($bin);
	}
	
	/**
	 * Pads a string to the nearest (greater than) block size length with the specified character.
	 * 
	 * @param string $s the original string
	 * @param int $blocksize the blocksize to pad to (NB the string doesn't have to be shorter than the blocksize)
	 * @param string $c the character to pad with, which is usually 0x0
	 * @return string the padded string
	 */
	public static function pad($s, $blocksize, $c) {
		while ((strlen($s) % $blocksize) != 0) $s .= $c;
		return $s;
	}

	/**
	 * Removes any trailing padding characters from the given string, similar to a trim
	 * 
	 * @param string $s the padding string
	 * @param string $c the character to remove, which is usually 0x0
	 * @return string the trimmed string
	 */
	public static function unpad($s, $c) {
		return rtrim($s, $c);
	}

	/**
	 * Generates a random IV using an approved random number generator.
	 * 
	 * @param int $length the number of bytes required
	 * @return string the IV, encoded as a hex string
	 */
	public static function generateRandomIV($length) {
		return self::enhex(openssl_random_pseudo_bytes($length));
	}
	
	/**
	 * Generates a 128-bit key from the given password.
	 * 
	 * This is essentially a wrapper for the md5 PHP function in order to match the API named methods required by InterCrypt.
	 * 
	 * @param string $password the password to generate the key from
	 * @return string the key, encoded as a hex string
	 */
	public static function generateKey($password) {
		return md5($password);
	}

	/**
	 * Encrypts a string using secret key symmetric key encryption.
	 * 
	 * @param string $plaintext the string to encrypt, which doesn't need to be hex encoded
	 * @param string $key the key to encrypt with as a hex encoded string
	 * @param string $iv the IV to encrypt with as a hex encoded string
	 * @return string the ciphertext, encoded as a hex string
	 */
	public static final function symmetricEncrypt($plaintext, $key, $iv) {
		// @phpcs:ignore Generic.PHP.DeprecatedFunctions
		return self::enhex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, self::dehex($key), self::pad($plaintext, 16, chr(0)), MCRYPT_MODE_CBC, self::dehex($iv)));
	}
	
	/**
	 * Decrypts a string using secret key symmetric key encryption.
	 * 
	 * @param string $ciphertext the string to decrypt, encoded as a hex string
	 * @param string $key the key to decrypt with as a hex encoded string
	 * @param string $iv the IV to decrypt with as a hex encoded string
	 * @return string the plaintext, not encoded as a hex string
	 */
	public static final function symmetricDecrypt($ciphertext, $key, $iv) {
		// @phpcs:ignore Generic.PHP.DeprecatedFunctions
		return self::unpad(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, self::dehex($key), self::dehex($ciphertext), MCRYPT_MODE_CBC, self::dehex($iv)), chr(0));
	}

	/**
	 * Encrypts a string using public key asymmetric encryption.
	 * 
	 * For the PHP implementation of InterCrypt, the public key has to be specified in PEM format and is given as a string.
	 * Other language implementations may differ in what format they accept, but the key itself is the same as is the result.
	 * 
	 * @param string $plaintext the string to encrypt, which doesn't need to be hex encoded
	 * @param string $publickey the public key to encrypt with
	 * @return string the ciphertext, encoded as a hex string
	 * @throws OpenSSL_Exception if the OpenSSL encryption fails
	 */
	public static function publickeyEncrypt($plaintext, $publickey) {
		$key = openssl_pkey_get_public($publickey);
	
		$ciphertext = null;
		if (!openssl_public_encrypt($plaintext, $ciphertext, $key)) throw new OpenSSL_Exception("Unable to encrypt");
	
		return self::enhex($ciphertext);
	}
	
	/**
	 * Decrypts a string using private key asymmetric decryption.
	 * 
	 * For the PHP implementation of InterCrypt, the private key has to be specified in PEM format and is given as a string.
	 * Other language implementations may differ in what format they accept, but the key itself is the same as is the result.
	 * 
	 * @param string $ciphertext the string to decrypt, encoded as a hex string
	 * @param string $privatekey the private key to decrypt with
	 * @return string the plaintext, not encoded as a hex string
	 * @throws OpenSSL_Exception if the OpenSSL decryption fails
	 */
	public static function privatekeyDecrypt($ciphertext, $privatekey) {
		$key = openssl_pkey_get_private($privatekey);

		$plaintext = null;
		if (!openssl_private_decrypt(self::dehex($ciphertext), $plaintext, $key)) throw new OpenSSL_Exception("Unable to decrypt");
		
		return $plaintext;
	}

	/**
	 * Encrypts plaintext using the common practice of first using symmetric encryption on the long plaintext with a random key,
	 * and then using asymmetric encryption on that key and related metadata.
	 * 
	 * The resulting string can by automatically decrypted by any other InterCrypt implementation of the API decrypt method.
	 * 
	 * For the PHP implementation of InterCrypt, the public key has to be specified in PEM format and is given as a string.
	 * Other language implementations may differ in what format they accept, but the key itself is the same as is the result.
	 * 
	 * @param string $plaintext the plaintext to encrypt
	 * @param string $publickey the public key to encrypt with
	 * @return string the encrypted ciphertext and key metadata combined
	 * @throws OpenSSL_Exception if the OpenSSL encryption fails
	 */
	public static function encrypt($plaintext, $publicKey) {
		$iv = InterCrypt::generateRandomIV(16);
		$key = InterCrypt::generateRandomIV(16);
		$hash = md5($plaintext);

		$ciphertext = InterCrypt::symmetricEncrypt($plaintext, $key, $iv);

		$metadata = $iv . ":" . $key . ":" . $hash;
		$ciphermeta = InterCrypt::publickeyEncrypt($metadata, $publicKey);

		return $ciphertext . ":" . $ciphermeta;
	}
	
	/**
	 * Decrypts a string encrypted by the InterCrypt API encrypt method.
	 * 
	 * The ciphertext can have been generated from any other InterCrypt implementation of the API encrypt method.
	 * 
	 * For the PHP implementation of InterCrypt, the private key has to be specified in PEM format and is given as a string.
	 * Other language implementations may differ in what format they accept, but the key itself is the same as is the result.
	 * 
	 * @param string $combined the combined ciphertext and metadata, as generated by the encrypt method
	 * @param string $privatekey the private key to decrypt with
	 * @var array $array
	 * @return string the decrypted plaintext
	 * @throws MetadataDeconstruction_Exception if the deconstruction of the combined into metadata and/or its components fails.
	 * @throws HashMatch_Exception if the resulting decrypted plaintext does not match the metadata's expected hash
	 * @throws OpenSSL_Exception if the OpenSSL decryption fails
	 */
	public static function decrypt($combined, $privateKey) {
		if (!preg_match("`^([0-9a-z]+):([0-9a-z]+)$`", $combined, $array)) throw new MetadataDeconstruction_Exception("Cannot separate into ciphertext and metadata");
		list(, $ciphertext, $ciphermeta) = $array;
		
		$metadata = InterCrypt::privatekeyDecrypt($ciphermeta, $privateKey);
		
		if (!preg_match("`^([0-9a-z]+):([0-9a-z]+):([0-9a-z]+)$`", $metadata, $array)) throw new MetadataDeconstruction_Exception("Cannot deconstruct metadata");
		list(, $iv, $key, $hash) = $array;
		
		$plaintext = InterCrypt::symmetricDecrypt($ciphertext, $key, $iv);
		
		if ($hash !== md5($plaintext)) throw new HashMatch_Exception("Hashes do not match");
		
		return $plaintext;
	}
}
