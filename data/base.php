<?php
/**
 * Base conversion.
 *
 * @copyright 2016 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Data;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "core.php";

use \Abstraction as Abstraction;

/**
 * @internal
 */
class Base_Exception extends Abstraction\Exception {}

/**
 * @internal
 */
class Base_TypeMismatchException extends Abstraction\TypeMismatchException {}

/**
 * Generic base conversion for all bases (i.e. base 'n') up to and including base62.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Base {
	/**
	 * Chunks an integer up into an array of divided bases.
	 * 
	 * @param dec the long integer to chunk
	 * @param base the base size for each chunk
	 * @return int[] an array of integers for each chunk
	 */
	public static function dec_to_baseN_chunks($dec, $base) {
		if ($dec === 0) return array(0);
		$size = intval(floor(log($dec, $base)) + 1);

		$chunks = array();
		for ($i = $size, $j = 0; $i-- > 0; $j++) {
			$dividor = (int)pow($base, $i);
			$div = intval(floor($dec / $dividor));
			$chunks[] = $div;
				
			$dec -= $div * $dividor;
		}

		return $chunks;
	}
	
	/**
	 * Convert a single digit from base 10 (decimal) into base 'n'.
	 * 
	 * Note that the decimal digit must only be up to the 'n' - 1, i.e. so it can be represented as a single baseN digit.
	 * 
	 * @param int $digit the decimal digit to represent
	 * @param int $base the base to use
	 * @throws Base_TypeMismatchException if the base is out of range or the digit cannot be represented in this base.
	 * @return string the resulting baseN digit
	 */
	public static function digit_to_baseN($digit, $base) {
		if (!Data::validate_int($base, 1, 62)) throw new Base_TypeMismatchException("Base is less than 1 or greater than 62. This cannot be represented using 0-9A-Za-z notation", $base);
		if (!Data::validate_int($digit, 0, $base - 1)) throw new Base_TypeMismatchException("Digit out of range of base", $digit + " vs base " + $base);

		if ($digit < 10) return "{$digit}";

		$digit -= 10;
		if ($digit < 26) return chr(65 + $digit);
		
		$digit -= 26;
		return chr(97 + $digit);
	}
	
	/**
	 * Converts a single digit from base 'n' into base 10 (decimal).
	 * 
	 * @param string $digit the baseN digit to decode
	 * @param int $base the base that the digit is currently encoded in
	 * @throws Base_TypeMismatchException if the base is out of range or the digit is not valid for this base.
	 * @return int the decimal value of this digit
	 */
	public static function baseN_to_digit($digit, $base) {
		if (!Data::validate_int($base, 1, 62)) throw new Base_TypeMismatchException("Base is less than 1 or greater than 62. This cannot be represented using 0-9A-Za-z notation");

		$b = ord($digit);
		if ($b < 48) throw new Base_TypeMismatchException("Character is outside scope of given base", $digit + " vs base " + $base);
		if ($b < 58) {
			$d = $b - 48;
			if ($d >= $base) throw new Base_TypeMismatchException("Character is outside scope of given base", $digit + " vs base " + $base);
			return $d;
		}
		if ($b < 91) {
			$d = ($b - 65) + 10;
			if ($d >= $base) throw new Base_TypeMismatchException("Character is outside scope of given base", $digit + " vs base " + $base);
			return $d;
		}
		if ($b < 123) {
			$d = ($b - 97) + 10 + 26;
			if ($d >= $base) throw new Base_TypeMismatchException("Character is outside scope of given base", $digit + " vs base " + $base);
			return $d;
		}

		throw new Base_TypeMismatchException("Character is outside scope of given base", $digit + " vs base " + $base);
	}
	
	/**
	 * Converts a decimal integer into the specified base.
	 * 
	 * @param int $dec the decimal to convert.
	 * @param int $base the base to use.
	 * @return string the integer encoded as baseN.
	 */
	public static function dec_to_baseN($dec, $base) {
		$chunks = self::dec_to_baseN_chunks($dec, $base);
	
		$result = "";
		foreach ($chunks as $chunk) $result .= self::digit_to_baseN($chunk, $base);
	
		return $result;
	}
	
	/**
	 * Converts a baseN value into a decimal integer.
	 * 
	 * @param string $baseN the baseN value to convert.
	 * @param int $base the base to use.
	 * @throws Data_TypeMismatchException if the given baseN value is invalid.
	 * @return int the decimal integer of this baseN value.
	 */
	public static function baseN_to_dec($baseN, $base) {
		// this is largely unnecessary as the baseN_to_digit will check each character, but doesn't hurt to have an extra generic up-to-base62 character check here.
		if (!Data::validate_regex("`^[0-9A-Za-z]+$`", $baseN)) throw new Data_TypeMismatchException("Invalid digit for baseNDigit", $base);

		$dec = 0;
		while ($baseN !== "" && $baseN !== false) {	// false is returned on substr(..., 1) for a blank string
			$digit = self::baseN_to_digit(substr($baseN, 0, 1), $base);
			$baseN = substr($baseN, 1);
				
			$dec *= $base;
			$dec += $digit;
		}
	
		return $dec;
	}
	
	/**
	 * Generates a random ID of the given base up to the specified length.
	 * 
	 * Note that this is a string length generation, not an numeric limit.
	 * Base 10 IDs can be generated using this, however the value will be a string not an int, and may exceed the max int size.
	 * 
	 * @param int $length the length of the ID to generate
	 * @param int $base the base to use for the ID.
	 * @return string the random ID value.
	 */
	public static function generate_random_baseN_id($length, $base) {
		$sb = "";
		for ($i = $length; $i-- > 0;) {
			$digit = rand(0, $base - 1);
			$sb .= self::digit_to_baseN($digit, $base);
		}

		return $sb;
	}
	
	// The arbitary functions offer a wider range of bases, but are coded a bit differently 
	// from above, and are intended for more raw public use from the abstract class rather 
	// than the various Base36/62 etc. subclasses.
	// They are still losely compatible with the above.
	
	private static function dec_to_arbitary_baseN_digit($value, $n) {
		if (!is_int($value)) throw new Base_TypeMismatchException("Value is not an integer for base conversion");
		if ($n < 2 || $n > 256) throw new Base_TypeMismatchException("Base is out of range 2-256");
		if ($value >= $n) throw new Base_TypeMismatchException("Value is not containable within base");
		
		if ($n <= 10) return "{$value}";
		if ($n <= 36) {
			if ($value < 10) return self::dec_to_arbitary_baseN_digit($value, 10);
			$value -= 10;
			return chr(65 + $value);
		}
		
		if ($n <= 62) {
			if ($value < 36) return self::dec_to_arbitary_baseN_digit($value, 36);
			$value -= 36;
			return chr(97 + $value);
		}
		if ($n <= 64) {
			if ($value < 62) return self::dec_to_arbitary_baseN_digit($value, 62);
			if ($value === 62) return "+";
			return "/";
		}
		
		return chr($value);
	}
	
	private static function arbitary_baseN_digit_to_dec($digit, $n) {
		if ($n < 2 || $n > 256) throw new Base_TypeMismatchException("Base is out of range 2-256");
		
		if ($n <= 10) {
			$v = intval($digit);
			if ($v < 0 || $v > $n) throw new Base_Exception("Calculated value is not valid for base");
			return $v;
		}
		if ($n <= 36) {
			$o = ord($digit);
			if ($o >= 48 && $o <= 57) return self::arbitary_baseN_digit_to_dec($digit, 10);
			return ($o - 65) + 10;
		}
		if ($n <= 62) {
			$o = ord($digit);
			if ($o >= 48 && $o <= 57) return self::arbitary_baseN_digit_to_dec($digit, 10);
			if ($o >= 65 && $o <= 90) return self::arbitary_baseN_digit_to_dec($digit, 16);
			return ($o - 97) + 10 + 26;
		}
		if ($n <= 64) {
			if ($digit === "+") return 62;
			if ($digit === "/") return 63;
			return arbitary_baseN_digit_to_dec($digit, 62);
		}
		
		return ord($digit);
	}
	
	public static function dec_to_arbitary_baseN($value, $n) {
		if (!is_int($value)) throw new Base_TypeMismatchException("Value is not an integer for base conversion");
		if ($n < 2 || $n > 256) throw new Base_TypeMismatchException("Base is out of range 2-256");
		
		$magnitude = $n;
		while ($magnitude < $value) $magnitude *= $n;
		$magnitude = intval($magnitude);
		
		$converted = "";
		if ($magnitude < $value) throw new Base_Exception("Original magnitude is too small");
		
		while ($magnitude > 0) {
			if ($magnitude < 1) throw new Base_Exception("Magnitude dropped to below 1");
			if ($magnitude === 1) break;
			
			$v = intval(floor($value / $magnitude));
			
			$d = self::dec_to_arbitary_baseN_digit($v, $n);
			$converted .= $d;
			
			$value -= $v * $magnitude;
			
			$magnitude = intval($magnitude / $n);
		}
		
		$converted .= self::dec_to_arbitary_baseN_digit($value, $n);
		
		$converted = ltrim($converted, $n <= 64 ? "0" : chr(0));
		return $converted === "" ? ($n <= 64 ? "0" : chr(0)) : $converted;
	}
	
	public static function arbitary_baseN_to_dec($value, $n) {
		if (!is_string($value)) throw new Base_TypeMismatchException("Value is not an string for base conversion");
		if ($n < 2 || $n > 256) throw new Base_TypeMismatchException("Base is out of range 2-256");
		
		$magnitude = intval(pow($n, strlen($value)));
		
		$converted = 0;
		while ($value !== "") {
			$digit = $value[0];
			
			$converted *= $n;
			$converted += self::arbitary_baseN_digit_to_dec($digit, $n);
			$converted = intval($converted);
			
			$value = substr($value, 1);
			
			$magnitude = intval($magnitude / $n);
		}
		
		return $converted;
	}
}

/**
 * Specific implementation of Base for base 36 values.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Base36 extends Base {
	const BASE = 36;

	/**
	 * Chunks an integer up into an array of divided base 36 values.
	 * 
	 * @param dec the long integer to chunk
	 * @return int[] an array of integers for each chunk
	 */
	public static function dec_to_chunks($dec) {
		return self::dec_to_baseN_chunks($dec, self::BASE);
	}
	
	/**
	 * Convert a single digit from base 10 (decimal) into base 36.
	 * 
	 * Note that the decimal digit must only be up to the 'n' - 1, i.e. so it can be represented as a single baseN digit.
	 * 
	 * @param int $digit the decimal digit to represent
	 * @throws Base_TypeMismatchException if the base is out of range or the digit cannot be represented in this base.
	 * @return string the resulting base36 digit
	 */
	public static function digit_to_base($digit) {
		return self::digit_to_baseN($digit, self::BASE);
	}
	
	/**
	 * Converts a single digit from base 36 into base 10 (decimal).
	 * 
	 * @param string $digit the base36 digit to decode
	 * @throws Base_TypeMismatchException if the base is out of range or the digit is not valid for this base.
	 * @return int the decimal value of this digit
	 */
	public static function base_to_digit($digit) {
		return self::baseN_to_digit(strtoupper($digit), self::BASE);
	}
	
	/**
	 * Converts a decimal integer into base36.
	 * 
	 * @param int $dec the decimal to convert.
	 * @return string the integer encoded as base36.
	 */
	public static function dec_to_base($dec) {
		return self::dec_to_baseN($dec, self::BASE);
	}
	
	/**
	 * Converts a base36 value into a decimal integer.
	 * 
	 * @param string $base the base36 value to convert.
	 * @throws Data_TypeMismatchException if the given base36 value is invalid.
	 * @return int the decimal integer of this base36 value.
	 */
	public static function base_to_dec($base) {
		return self::baseN_to_dec(strtoupper($base), self::BASE);
	}
	
	/**
	 * Generates a 10 digit length random base36 ID.
	 * 
	 * @return string the random ID value.
	 */
	public static function generate_random_base36_id() {
		return self::generate_random_baseN_id(10, self::BASE);
	}
}

/**
 * Specific implementation of Base for base 41 values.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Base41 extends Base {
	const BASE = 41;
	
	/**
	 * Chunks an integer up into an array of divided base 41 values.
	 * 
	 * @param dec the long integer to chunk
	 * @return int[] an array of integers for each chunk
	 */
	public static function dec_to_chunks($dec) {
		return self::dec_to_baseN_chunks($dec, self::BASE);
	}
	
	/**
	 * Convert a single digit from base 10 (decimal) into base 41.
	 * 
	 * Note that the decimal digit must only be up to the 'n' - 1, i.e. so it can be represented as a single baseN digit.
	 * 
	 * @param int $digit the decimal digit to represent
	 * @throws Base_TypeMismatchException if the base is out of range or the digit cannot be represented in this base.
	 * @return string the resulting base41 digit
	 */
	public static function digit_to_base($digit) {
		return self::digit_to_baseN($digit, self::BASE);
	}
	
	/**
	 * Converts a single digit from base 41 into base 10 (decimal).
	 * 
	 * @param string $digit the base41 digit to decode
	 * @throws Base_TypeMismatchException if the base is out of range or the digit is not valid for this base.
	 * @return int the decimal value of this digit
	 */
	public static function base_to_digit($digit) {
		return self::baseN_to_digit($digit, self::BASE);
	}
	
	/**
	 * Converts a decimal integer into base41.
	 * 
	 * @param int $dec the decimal to convert.
	 * @return string the integer encoded as base41.
	 */
	public static function dec_to_base($dec) {
		return self::dec_to_baseN($dec, self::BASE);
	}
	
	/**
	 * Converts a base41 value into a decimal integer.
	 * 
	 * @param string $base the base41 value to convert.
	 * @throws Data_TypeMismatchException if the given base41 value is invalid.
	 * @return int the decimal integer of this base41 value.
	 */
	public static function base_to_dec($base) {
		return self::baseN_to_dec($base, self::BASE);
	}
	
	/**
	 * Generates a 6 digit length random base41 ID.
	 * 
	 * NB, base41 IDs specifically use a shorter length than base36 and base62 IDs.
	 * This is by design for applications which only need a shorter ID address space.
	 * The scope can be increased by using the parent static function to generate a longer ID.
	 * 
	 * @return string the random ID value.
	 */
	public static function generate_random_base41_id() {
		return self::generate_random_baseN_id(6, self::BASE);
	}

	const REGEX_PATTERN_HEX_QUAD = "`^([0-9a-f]{4})+$`i";
	const REGEX_PATTERN_BASE41_TRIPLE = "`^([0-9A-Za-e]{3})+$`";
	
	/**
	 * Converts a hex-quad value (i.e. multiples of 4-digit hex, e.g. 0000-FFFF x n) into base41 notation, which is 3-digit triples.
	 * 
	 * @param string $hex the hex value, which must be of length divisible by 4.
	 * @throws Data_TypeMismatchException if the given hex value is invalid or not of length divisible by 4.
	 * @return string the base41 encoded value.
	 */
	public static function hex_to_base41($hex) {
		if (!Data::validate_regex(self::REGEX_PATTERN_HEX_QUAD, $hex)) throw new Data_TypeMismatchException("hex_to_base41 is only applicable to multiples of quad hex encoded values (0000-FFFF). Length must therefore be divisible by 4", $hex);
		
		$rebuild = "";
		for ($i = strlen($hex) >> 2, $j = 0; $i-- > 0; $j += 4) {
			$dec = hexdec(substr($hex, $j, 4));
			$rebuild .= str_pad(self::dec_to_base($dec), 3, "0", STR_PAD_LEFT);
		}
		
		return $rebuild;
	}
	
	/**
	 * Converts a base41 triple (i.e. multiples of 3-digit base41, e.g. 000-ceH x n) into hex notation, which is 4-digit quads.
	 * 
	 * @param string $base41 the base41 value, which must be of length divisible by 3.
	 * @throws Data_TypeMismatchException if the given base41 value is invalid or not of length divisible by 3.
	 * @return string the hex encoded value.
	 */
	public static function base41_to_hex($base41) {
		if (!Data::validate_regex(self::REGEX_PATTERN_BASE41_TRIPLE, $base41)) throw new Data_TypeMismatchException("base41_to_hex is only applicable to multiples of triple base41 encoded values (000-eee). Length must therefore be divisible by 3", $base41);
		
		$rebuild = "";
		for ($i = strlen($base41) / 3, $j = 0; $i-- > 0; $j += 3) {
			$dec = self::base_to_dec(substr($base41, $j, 3));
			if ($dec > 65535) throw new Data_TypeMismatchException("Resulting base41 triple is greater than decimal 65535. This isn't applicable to hex quad conversion.", $dec);
			$rebuild .= str_pad(dechex($dec), 4, "0", STR_PAD_LEFT);
		}
		
		return $rebuild;
	}
}

/**
 * Specific implementation of Base for base 62 values.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Base62 extends Base {
	const BASE = 62;
	
	/**
	 * Chunks an integer up into an array of divided base 62 values.
	 * 
	 * @param dec the long integer to chunk
	 * @return int[] an array of integers for each chunk
	 */
	public static function dec_to_chunks($dec) {
		return self::dec_to_baseN_chunks($dec, self::BASE);
	}
	
	/**
	 * Convert a single digit from base 10 (decimal) into base 62.
	 * 
	 * Note that the decimal digit must only be up to the 'n' - 1, i.e. so it can be represented as a single baseN digit.
	 * 
	 * @param int $digit the decimal digit to represent
	 * @throws Base_TypeMismatchException if the base is out of range or the digit cannot be represented in this base.
	 * @return string the resulting base62 digit
	 */
	public static function digit_to_base($digit) {
		return self::digit_to_baseN($digit, self::BASE);
	}
	
	/**
	 * Converts a single digit from base 62 into base 10 (decimal).
	 * 
	 * @param string $digit the base62 digit to decode
	 * @throws Base_TypeMismatchException if the base is out of range or the digit is not valid for this base.
	 * @return int the decimal value of this digit
	 */
	public static function base_to_digit($digit) {
		return self::baseN_to_digit($digit, self::BASE);
	}
	
	/**
	 * Converts a decimal integer into base62.
	 * 
	 * @param int $dec the decimal to convert.
	 * @return string the integer encoded as base62.
	 */
	public static function dec_to_base($dec) {
		return self::dec_to_baseN($dec, self::BASE);
	}
	
	/**
	 * Converts a base62 value into a decimal integer.
	 * 
	 * @param string $base the base62 value to convert.
	 * @throws Data_TypeMismatchException if the given base62 value is invalid.
	 * @return int the decimal integer of this base62 value.
	 */
	public static function base_to_dec($baseN) {
		return self::baseN_to_dec($baseN, self::BASE);
	}
	
	/**
	 * Generates an 8 digit length random base62 ID.
	 * 
	 * @return string the random ID value.
	 */
	public static function generate_random_base62_id() {
		return self::generate_random_baseN_id(8, self::BASE);
	}
}
