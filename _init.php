<?php
/**
 * Bootstrap for the entire Abstraction framework.
 * 
 * Requires the ABSTRACTION_ROOT_PATH global constant to have been previously defined.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");

require_once ABSTRACTION_ROOT_PATH . "config.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";

require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "data/base.php";
require_once ABSTRACTION_ROOT_PATH . "data/crypt.php";

require_once ABSTRACTION_ROOT_PATH . "database/database.php";

require_once ABSTRACTION_ROOT_PATH . "renderer/http/http.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/inline/inline.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/json/json.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/rest/rest.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/html.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/htmljquery/html.php";

require_once ABSTRACTION_ROOT_PATH . "rest/data.php";
require_once ABSTRACTION_ROOT_PATH . "rest/path.php";
require_once ABSTRACTION_ROOT_PATH . "rest/express.php";

require_once ABSTRACTION_ROOT_PATH . "framework/framework.php";
