<?php
/**
 * Core configuration flags and constants
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction;

const EXCEPTIONS_ENVIRONMENT_HTML = false;
const EXCEPTIONS_SHOW_VALUES = true;
const EXCEPTIONS_SHOW_SQL_CODE = true;

define("SQL_DEBUG_FAKE_DELAY", 0 * 1000);

// Have to disable for PHP 8.1 upwards as the number of deprecation errors is huge
// error_reporting(E_ALL | E_NOTICE | E_STRICT | E_DEPRECATED);
