<?php
/**
 * Multi-parent many-to-many relationship model support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/models.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

use \Abstraction\Database as Database;

/**
 * @internal
 */
class MultiParentLinkTable_Exception extends Exception {}

/**
 * @internal
 */
class MultiParentLinkTable_NotIdObjectException extends NotIdObjectException {
	public function __construct($value) {
		parent::__construct($value, "Bad MultiParent link table parameter value (not an ID object)");
	}
}

/**
 * Defines a root model for multi-parent many-to-many link models.
 *
 * Many-to-many relationship link tables sit between three or more FirstClass (or its subclasses) tables and join upon their primary keys using foreign keys. Technically 2 tables can be used, but it is safer to use M2MLinkTable for that.
 * A key difference between many-to-many models, as opposed to a FirstClass with multiple foreign keys, is that there is no serial primary key, and instead the join forms a unique key itself. This means that you can only link the same models rows together once, which is fully intentional.
 * Because the rows of an MultiParentLinkTable are not themselves first class rows, they are automatically deleted if any joined models' corresponding rows are deleted, and so are more of an implementation layer than a specific model. However, they can store data in fields alongside the join.
 *
 * @api
 * @author Pete Morris
 * @version 2.0.0
 */
abstract class MultiParentLinkTable extends Models {
	protected $parents;

	/**
	 * Constructs a new instance of this link table.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param FirstClass[] $parents an associated array of linkage field names and parent FirstClass model to link to
	 * @param Database\Type[] $structure an optional associated array to define any additional model structure, taking the format field name => field type object
	 * @throws MultiParentLinkTable_Exception
	 */
	public function __construct(Database\Wrapper $database, $table, array $parents, array $structure = array()) {
		$this->parents = array();
		
		$foreign_keys = array();
		foreach ($parents as $firstclass_field => $firstclass) {
			if (array_key_exists($firstclass_field, $this->parents)) throw new MultiParentSecondClass_Exception("Each parent for a MultiParentSecondClass model can only be used once.");
				
			if (array_key_exists($firstclass_field, $structure)) throw new MultiParentSecondClass_Exception("The firstclass fields are implicit for MultiParentSecondClass models and should not be explicitly stated in the structure");
				
			$this->parents[$firstclass_field] = $firstclass;
				
			$structure[$firstclass_field] = new Database\Type_Id(Database\Type::NOT_NULL);
				
			$foreign_keys[$firstclass_field] = new ForeignKey($firstclass, "CASCADE", "CASCADE", $table . "__fk_{$firstclass_field}");
		}
		
		$unique_keys = array(
			new UniqueKey(array_keys($parents), $table . "__uk")
		);

		parent::__construct($database, $table, $structure, $foreign_keys, $unique_keys);
	}

	//-------------------------------------------------------------------------

	/**
	 * Returns the specified parent FirstClass model associated with this MultiParenetSecondClass model
	 *
	 * @return FirstClass
	 */
	public function get_parent($firstclass_field) {
		if (!array_key_exists($firstclass_field, $this->parents)) throw new MultiParentLinkTable_Exception("Specified firstclass parent by that field name does not exist");
	
		return $this->parents[$firstclass_field];
	}
	
	/**
	 * Returns the field names linked to the multiple parent FirstClass models.
	 *
	 * @return string[]
	 */
	public function get_link_fields() {
		return array_keys($this->parents);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns all rows in the specified model joined to the given other parents.
	 * 
	 * @param string $table the name of the model containing the rows to return
	 * @param array $parents an associated array of other parent FirstClass rows
	 */
	public function list_by_firstclass_parents($selection, array $others) {
		if (!array_key_exists($selection, $this->parents)) throw new MultiParentLinkTable_Exception("The specified return model does not exist within the model parent list");
		if (array_key_exists($selection, $others)) throw new MultiParentLinkTable_Exception("The model to return is also specified as a lookup parent");
		if (sizeof(array_keys($others)) !== (sizeof(array_keys($this->parents)) - 1)) throw new MultiParentLinkTable_Exception("The other parent rows must be exactly the total number minus 1");
		
		$clauses = array();
		$params = array();
		foreach ($others as $firstclass_field => $parent) {
			if (!array_key_exists($firstclass_field, $this->parents)) throw new MultiParentLinkTable_Exception("The specified parent model does not exist within the model parent list");
			self::assert_id_object($parent, $this->parents[$firstclass_field]->get_id_field());
			
			$clauses[] = $this->modelfield($this, $firstclass_field) . "=:{$firstclass_field}";
			$params[$firstclass_field] = new Database\Param($parent[$this->parents[$firstclass_field]->get_id_field()], $this->structure[$firstclass_field]);
		}

		$fields = array();
		foreach (array_keys($this->parents[$selection]->get_structure()) as $field) $fields[] = $this->modelfield($this->parents[$selection], $field);
		$fields = implode(",", $fields);

		$order_clause = "";
		if ($this->parents[$selection] instanceof Orderable) {
			$order_clause = "ORDER BY " . $this->parents[$selection]->get_ordered_field() . " ASC";
		}
		
		return $this->database->query_params("
			SELECT {$fields}
			FROM " . $this->model($this) . "
			INNER JOIN " . $this->model($this->parents[$selection]) . " 
				ON " . $this->modelfield($this, $selection) . "=" . $this->modelfield($this->parents[$selection], $this->parents[$selection]->get_id_field()) . "
			WHERE " . implode(" AND ", $clauses) . "
			{$order_clause}
		", $params, $this->parents[$selection]->get_structure());
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns whether a link exists or not between the given parent FirstClass rows.
	 * 
	 * @param array $parents an associated array of parent model rows.
	 * @return bool true if the row exists; false if not
	 */
	public function is_linked(array $parents) {
		$conditions = array();
		foreach ($this->parents as $firstclass_field => $table) {
			if (!array_key_exists($firstclass_field, $parents)) throw new MultiParentLinkTable_Exception("The required parent model does not exist within the specified parent list");
			self::assert_id_object($parents[$firstclass_field], $table->get_id_field());
				
			$conditions[$firstclass_field] = new Database\Param($parents[$firstclass_field][$table->get_id_field()], $this->structure[$firstclass_field]);
		}
		
		return $this->database->does_row_exist_by_conditions($this->table, $conditions);
	}

	/**
	 * Returns the row linking the given parent model rows.
	 * 
	 * This is primarily for returning supplimentary data defined within the structure.
	 *
	 * @param array $parents an associated array of parent model rows.
	 * @throws MultiParentLinkTable_Exception
	 * @return mixed[] the linking row
	 */
	public function get_link_row(array $parents) {
		$conditions = array();
		foreach ($this->parents as $firstclass_field => $table) {
			if (!array_key_exists($firstclass_field, $parents)) throw new MultiParentLinkTable_Exception("The required parent model does not exist within the specified parent list");
			self::assert_id_object($parents[$firstclass_field], $table->get_id_field());
				
			$conditions[$firstclass_field] = new Database\Param($parents[$firstclass_field][$table->get_id_field()], $this->structure[$firstclass_field]);
		}
		
		$rows = $this->database->select_rows_by_conditions($this->table, $this->get_structure(), $conditions);
		if (sizeof($rows) === 0) throw new MultiParentLinkTable_Exception("No such link row exists");
		if (sizeof($rows) > 1) throw new MultiParentLinkTable_Exception("More than one link row appears to exist. This should not be possible.");
		
		return $rows[0];
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Attempts to link the specified model rows, using any additional specified values.
	 * 
	 * Note that any parent combination can only be linked once, so additional linking will throw an exception.
	 * The link fields themselves are set automatically and should not be specified in the values.
	 * 
	 * @param array $parents an associated array of parent model rows.
	 * @param mixed[] $values any additional values the model requires
	 * @throws Exception
	 * @throws MultiParentLinkTable_Exception
	 * @return true always returns true; failure throws an exception
	 */
	public function link(array $parents, array $values = array()) {
		foreach ($this->parents as $firstclass_field => $table) {
			if (!array_key_exists($firstclass_field, $parents)) throw new MultiParentLinkTable_Exception("The required parent model does not exist within the specified parent list");
			self::assert_id_object($parents[$firstclass_field], $table->get_id_field());

			if (array_key_exists($firstclass_field, $values)) throw new Exception("The parent firstclass fields are implicit for MultiParentLinkTable models and should not be explicitly provided in the values");
			$values[$firstclass_field] = $parents[$firstclass_field][$table->get_id_field()];
		}
		
		if (false === $this->insert($values)) {
			$this->database->transaction_rollback();
			throw new MultiParentLinkTable_Exception("Unable to create entry in MultiParent link table");
		}
		
		return true;
	}

	/**
	 * Attempts to unlink the specified parent rows.
	 * 
	 * By default, attempting to unlink a non-existent link will throw an exception. This can be suppressed by setting the $_IGNORE_MISSING_LINK flag.
	 * 
	 * @param array $parents an associated array of parent model rows.
	 * @param boolean $_IGNORE_MISSING_LINK true if non-existing links should be silently ignored.
	 * @throws MultiParentLinkTable_Exception
	 * @return true always returns true; failure throws an exception
	 */
	public function unlink(array $parents, $_IGNORE_MISSING_LINK = false) {
		$conditions = array();
		foreach ($this->parents as $firstclass_field => $table) {
			if (!array_key_exists($firstclass_field, $parents)) throw new MultiParentLinkTable_Exception("The required parent model does not exist within the specified parent list");
			self::assert_id_object($parents[$firstclass_field], $table->get_id_field());
				
			$conditions[$firstclass_field] = new Database\Param($parents[$firstclass_field][$table->get_id_field()], $this->structure[$firstclass_field]);
		}
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		if (1 !== ($count = $this->database->count_rows_by_conditions($this->table, $conditions))) {
			if ($count === 0 && $_IGNORE_MISSING_LINK) {
				if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
				return true;
			}
			
			$this->database->transaction_rollback();
			throw new MultiParentLinkTable_Exception("Unable to unlink because no such link exists or more than one exists");
		}
		
		if (false === $this->database->delete_rows_by_conditions($this->table, $conditions)) {
			$this->database->transaction_rollback();
			throw new MultiParentLinkTable_Exception("Unable to remove entry in MultiParent link table");
		}
	
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}

	/**
	 * Wipes any rows joined to the specified parent models.
	 * 
	 * @param mixed[] $parents the models to match against.
	 * @throws MultiParentLinkTable_Exception
	 * @return true always returns true; failure throws an exception
	 */
	public function delete_all_for_some_parents(array $parents) {
		if (sizeof(array_keys($parents)) === 0) throw new MultiParentLinkTable_Exception("No parent clauses specified to delete against");
		if (sizeof(array_keys($parents)) > (sizeof(array_keys($this->parents)) - 1)) throw new MultiParentLinkTable_Exception("The number of parent clauses must be at most one less than the total number");
		
		$conditions = array();
		foreach ($parents as $firstclass_field => $parent) {
			if (!array_key_exists($firstclass_field, $this->parents)) throw new MultiParentLinkTable_Exception("The specified parent model does not exist within the model parent list");
			self::assert_id_object($parent, $this->parents[$firstclass_field]->get_id_field());
		
			$conditions[$firstclass_field] = new Database\Param($parent[$this->parents[$firstclass_field]->get_id_field()], $this->structure[$firstclass_field]);
		}
		
		if (!$this->database->delete_rows_by_conditions($this->table, $conditions, false)) throw new MultiParentLinkTable_Exception("Unable to delete all MultiParent link objects from table", $this->table);

		return true;
	}

	/**
	 * Updates a row in the model's table with new data without affecting the existing link.
	 * 
	 * Any field apart from the foreign key link fields can be populated with new data.
	 * 
	 * @param mixed[] $row an existing model row, which must contain both the link table foreign keys.
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function update_link_row($row) {
		$conditions = array();
		foreach ($this->parents as $firstclass_field => $table) {
			self::assert_id_object($row, $firstclass_field);
			$conditions[$firstclass_field] = new Database\Param($row[$firstclass_field], $this->structure[$firstclass_field]);
		}
		
		$params = array();
		foreach ($row as $var => &$value) {
			if (array_key_exists($var, $this->parents)) continue;
				
			if (!isset($this->structure[$var])) throw new MultiParentLinkTable_Exception("Parameter variable is missing in the model structure", $var);
			$params[$var] = new Database\Param($value, $this->structure[$var]);
		}
		unset($value);

		if (!$this->database->update_rows_by_conditions($this->table, $params, $conditions, true)) throw new FirstClass_Exception("Unable to update row in the MultiParent link table", $this->table);

		return true;
	}

}
