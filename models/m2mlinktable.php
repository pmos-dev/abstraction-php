<?php
/**
 * Many-to-many relationship model support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/models.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

use \Abstraction\Database as Database;

/**
 * @internal
 */
class M2MLinkTable_Exception extends Exception {}

/**
 * @internal
 */
class M2MLinkTable_NotIdObjectException extends NotIdObjectException {
	public function __construct($value) {
		parent::__construct($value, "Bad M2M link table parameter value (not an ID object)");
	}
}

/**
 * Defines a root model for many-to-many link tables.
 *
 * Many-to-many relationship link tables sit between two FirstClass (or its subclasses) tables (A and B) and join upon their primary keys using foreign keys.
 * A key difference between many-to-many models, as opposed to a FirstClass with two foreign keys, is that there is no serial primary key, and instead the join forms a unique key itself. This means that you can only link the same two models rows together once, which is fully intentional.
 * Because the rows of an M2MLinkTable are not themselves first class rows, they are automatically deleted if either joined models' corresponding rows are deleted, and so are more of an implementation layer than a specific model. However, they can store data in fields alongside the join.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class M2MLinkTable extends Models {
	protected $table_a, $table_b;
	protected $link_a, $link_b;

	/**
	 * Constructs a new instance of this link table.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param FirstClass $table_a the first (A) FirstClass model to form the join upon
	 * @param FirstClass $table_b the second (B) FirstClass model to form the join upon
	 * @param string $link_a the name of the first (A) linkage field to create the foreign key upon
	 * @param string $link_b the name of the second (B) linkage field to create the foreign key upon
	 * @param Database\Type[] $structure an optional associated array to define any additional model structure, taking the format field name => field type object
	 * @throws M2MLinkTable_Exception
	 */
	public function __construct(Database\Wrapper $database, $table, FirstClass $table_a, FirstClass $table_b, $link_a, $link_b, array $structure = array()) {
		$this->table_a = &$table_a;
		$this->table_b = &$table_b;
		$this->link_a = $link_a;
		$this->link_b = $link_b;
		
		if (array_key_exists($link_a, $structure)) throw new M2MLinkTable_Exception("Link fields are implicit for M2M link table models and should not be explicitly stated in the structure", $link_a);
		$structure[$link_a] = new Database\Type_Id(Database\Type::NOT_NULL);
		
		if (array_key_exists($link_b, $structure)) throw new M2MLinkTable_Exception("Link fields are implicit for M2M link table models and should not be explicitly stated in the structure", $link_b);
		$structure[$link_b] = new Database\Type_Id(Database\Type::NOT_NULL);
		
		$foreign_keys = array(
			$link_a => new ForeignKey($table_a, "CASCADE", "CASCADE", $table . "__fk_a"),
			$link_b => new ForeignKey($table_b, "CASCADE", "CASCADE", $table . "__fk_b")
		);
		
		$unique_keys = array(
			new UniqueKey(array($link_a, $link_b), $table . "__uk")
		);

		parent::__construct($database, $table, $structure, $foreign_keys, $unique_keys);
	}

	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function define_views() {
		parent::define_views();

		$fields_a = array();
		foreach (array_keys($this->table_a->get_structure()) as $field) $fields_a[] = $this->derivedfield("temp_table_a", $field);
		$fields_a = implode(",", $fields_a);
		
		$fields_b = array();
		foreach (array_keys($this->table_b->get_structure()) as $field) $fields_b[] = $this->derivedfield("temp_table_b", $field);
		$fields_b = implode(",", $fields_b);
		
		$this->define_view("M2M_JOIN_A2B", "
			SELECT {$fields_a},
				" . $this->derivedfield("temp_table_b", $this->table_b->get_id_field()) . " AS " . $this->tempfield("remote_id") . "
			FROM " . $this->model($this->table_a) . " AS " . $this->derived("temp_table_a") . "
			INNER JOIN " . $this->model($this) . "
				ON " . $this->modelfield($this, $this->link_a) . "=" . $this->derivedfield("temp_table_a", $this->table_a->get_id_field()) . "
			INNER JOIN " . $this->model($this->table_b) . " AS " . $this->derived("temp_table_b") . "
				ON " . $this->modelfield($this, $this->link_b) . "=" . $this->derivedfield("temp_table_b", $this->table_b->get_id_field()) . "
		");

		$this->define_view("M2M_JOIN_B2A", "
			SELECT {$fields_b},
				" . $this->derivedfield("temp_table_a", $this->table_a->get_id_field()) . " AS " . $this->tempfield("remote_id") . "
			FROM " . $this->model($this->table_b) . " AS " . $this->derived("temp_table_b") . "
			INNER JOIN " . $this->model($this) . "
				ON " . $this->modelfield($this, $this->link_b) . "=" . $this->derivedfield("temp_table_b", $this->table_b->get_id_field()) . "
			INNER JOIN " . $this->model($this->table_a) . " AS " . $this->derived("temp_table_a") . "
				ON " . $this->modelfield($this, $this->link_a) . "=" . $this->derivedfield("temp_table_a", $this->table_a->get_id_field()) . "
		");

		$this->define_view("M2M_JOIN_DIRECT", "
			SELECT {$this->fields},
				" . $this->derivedfield("temp_table_a", $this->table_a->get_id_field()) . " AS " . $this->tempfield("a_id") . ",
				" . $this->derivedfield("temp_table_b", $this->table_b->get_id_field()) . " AS " . $this->tempfield("b_id") . "
			FROM " . $this->model($this) . "
			INNER JOIN " . $this->model($this->table_a) . " AS " . $this->derived("temp_table_a") . "
				ON " . $this->modelfield($this, $this->link_a) . "=" . $this->derivedfield("temp_table_a", $this->table_a->get_id_field()) . "
			INNER JOIN " . $this->model($this->table_b) . " AS " . $this->derived("temp_table_b") . "
				ON " . $this->modelfield($this, $this->link_b) . "=" . $this->derivedfield("temp_table_b", $this->table_b->get_id_field()) . "
		");
	}

	/**
	 * @internal
	 */
	protected function preprepare() {
		$fields_a = array();
		foreach (array_keys($this->table_a->get_structure()) as $field) $fields_a[] = $this->modelviewfield($this, "M2M_JOIN_A2B", $field);
		$fields_a = implode(",", $fields_a);
		
		$fields_b = array();
		foreach (array_keys($this->table_b->get_structure()) as $field) $fields_b[] = $this->modelviewfield($this, "M2M_JOIN_B2A", $field);
		$fields_b = implode(",", $fields_b);
		
		$order_a_clause = $order_b_clause = "";
		if ($this->table_a instanceof Ordered || $this->table_a instanceof OrientatedOrdered || $this->table_a instanceof Hierarchy) {
			$order_a_clause = "ORDER BY " . $this->table_a->get_ordered_field() . " ASC";
		}
		if ($this->table_b instanceof Ordered || $this->table_b instanceof OrientatedOrdered || $this->table_b instanceof Hierarchy) {
			$order_b_clause = "ORDER BY " . $this->table_b->get_ordered_field() . " ASC";
		}
		
		$this->database->preprepare("MODELS_M2MLINKTABLE__{$this->table}__LIST_AS_BY_B", "
			SELECT {$fields_a}
			FROM " . $this->modelview($this, "M2M_JOIN_A2B") . "
			WHERE " . $this->modelviewfield($this, "M2M_JOIN_A2B", "remote_id") . "=:id
			{$order_a_clause}
		", array(
			"id" => new Database\Type_ID(Database\Type::NOT_NULL)
		), $this->table_a->get_structure());

		$this->database->preprepare("MODELS_M2MLINKTABLE__{$this->table}__LIST_BS_BY_A", "
			SELECT {$fields_b}
			FROM " . $this->modelview($this, "M2M_JOIN_B2A") . "
			WHERE " . $this->modelviewfield($this, "M2M_JOIN_B2A", "remote_id") . "=:id
			{$order_b_clause}
		", array(
			"id" => new Database\Type_ID(Database\Type::NOT_NULL)
		), $this->table_b->get_structure());
	}
	
	//-------------------------------------------------------------------------

	public function get_table_a() {
		return $this->table_a;
	}
	
	public function get_table_b() {
		return $this->table_b;
	}
	
	public function get_link_a() {
		return $this->link_a;
	}
	
	public function get_link_b() {
		return $this->link_b;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns all rows in the A model joined to the given B model row.
	 * 
	 * @param array $b the row in the B model
	 */
	public function list_as_by_b(array $b) {
		self::assert_id_object($b, $this->table_b->get_id_field());
		
		return $this->database->execute_params("MODELS_M2MLINKTABLE__{$this->table}__LIST_AS_BY_B", array(
			"id" => $b[$this->table_b->get_id_field()]
		));
	}
	
	/**
	 * Returns all rows in the B model joined to the given A model row.
	 * 
	 * @param array $a the row in the A model
	 */
	public function list_bs_by_a(array $a) {
		self::assert_id_object($a, $this->table_a->get_id_field());
		
		return $this->database->execute_params("MODELS_M2MLINKTABLE__{$this->table}__LIST_BS_BY_A", array(
			"id" => $a[$this->table_a->get_id_field()]
		));
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns whether a link exists or not between the given A and B model rows.
	 * 
	 * @param mixed[] $a the row in the A model
	 * @param mixed[] $b the row in the B model
	 * @return bool true if the row exists; false if not
	 */
	public function is_linked(array $a, array $b) {
		self::assert_id_object($a, $this->table_a->get_id_field());
		self::assert_id_object($b, $this->table_b->get_id_field());
		
		return $this->database->does_row_exist_by_conditions($this->table, array(
			$this->link_a => new Database\Param($a[$this->table_a->get_id_field()], new Database\Type_Id(Database\Type::NOT_NULL)),
			$this->link_b => new Database\Param($b[$this->table_b->get_id_field()], new Database\Type_Id(Database\Type::NOT_NULL))
		));
	}

	/**
	 * Returns the row linking the given A and B model rows.
	 * 
	 * This is primarily for returning supplimentary data defined within the structure.
	 *
	 * @param mixed[] $a the row in the A model
	 * @param mixed[] $b the row in the B model
	 * @throws M2MLinkTable_Exception
	 * @return mixed[] the linking row
	 */
	public function get_link_row(array $a, array $b) {
		self::assert_id_object($a, $this->table_a->get_id_field());
		self::assert_id_object($b, $this->table_b->get_id_field());
	
		$rows = $this->database->select_rows_by_conditions($this->table, $this->get_structure(), array(
			$this->link_a => new Database\Param($a[$this->table_a->get_id_field()], new Database\Type_Id(Database\Type::NOT_NULL)),
			$this->link_b => new Database\Param($b[$this->table_b->get_id_field()], new Database\Type_Id(Database\Type::NOT_NULL))
		));
		if (sizeof($rows) === 0) throw new M2MLinkTable_Exception("No such link row exists");
		if (sizeof($rows) > 1) throw new M2MLinkTable_Exception("More than one link row appears to exist. This should not be possible.");
		
		return $rows[0];
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Attempts to link the two given A and B model rows, using any additional specified values.
	 * 
	 * Note that any two specific row pairs can only be linked once, so additional linking will throw an exception.
	 * The link fields themselves are set automatically and should not be specified in the values.
	 * 
	 * @param mixed[] $a the row in the A model
	 * @param mixed[] $b the row in the B model
	 * @param mixed[] $values any additional values the model requires
	 * @throws Exception
	 * @throws M2MLinkTable_Exception
	 * @return true always returns true; failure throws an exception
	 */
	public function link(array $a, array $b, array $values = array()) {
		self::assert_id_object($a, $this->table_a->get_id_field());
		self::assert_id_object($b, $this->table_b->get_id_field());
		
		if (array_key_exists($this->link_a, $values)) throw new Exception("The link_a field is implicit for M2MLinkTable models and should not be explicitly provided in the values");
		$values[$this->link_a] = $a[$this->table_a->get_id_field()];
		
		if (array_key_exists($this->link_b, $values)) throw new Exception("The link_b field is implicit for M2MLinkTable models and should not be explicitly provided in the values");
		$values[$this->link_b] = $b[$this->table_b->get_id_field()];
		
		if (false === $this->insert($values)) {
			$this->database->transaction_rollback();
			throw new M2MLinkTable_Exception("Unable to create entry in M2M link table");
		}
		
		return true;
	}

	/**
	 * Attempts to unlink the two given A and B model rows.
	 * 
	 * By default, attempting to unlink a non-existent link will throw an exception. This can be suppressed by setting the $_IGNORE_MISSING_LINK flag.
	 * 
	 * @param mixed[] $a the row in the A model
	 * @param mixed[] $b the row in the B model
	 * @param boolean $_IGNORE_MISSING_LINK true if non-existing links should be silently ignored.
	 * @throws M2MLinkTable_Exception
	 * @return true always returns true; failure throws an exception
	 */
	public function unlink(array $a, array $b, $_IGNORE_MISSING_LINK = false) {
		self::assert_id_object($a, $this->table_a->get_id_field());
		self::assert_id_object($b, $this->table_b->get_id_field());
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		if (1 !== ($count = $this->database->count_rows_by_conditions($this->table, array(
			$this->link_a => new Database\Param($a[$this->table_a->get_id_field()], new Database\Type_Id(Database\Type::NOT_NULL)),
			$this->link_b => new Database\Param($b[$this->table_b->get_id_field()], new Database\Type_Id(Database\Type::NOT_NULL))
		)))) {
			if ($count === 0 && $_IGNORE_MISSING_LINK) {
				if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
				return true;
			}
			
			$this->database->transaction_rollback();
			throw new M2MLinkTable_Exception("Unable to unlink because no such link exists or more than one exists", $a[$this->table_a->get_id_field()] . " - " . $b[$this->table_b->get_id_field()]);
		}
		
		if (false === $this->database->delete_rows_by_conditions($this->table, array(
			$this->link_a => new Database\Param($a[$this->table_a->get_id_field()], new Database\Type_Id(Database\Type::NOT_NULL)),
			$this->link_b => new Database\Param($b[$this->table_b->get_id_field()], new Database\Type_Id(Database\Type::NOT_NULL))
		))) {
			$this->database->transaction_rollback();
			throw new M2MLinkTable_Exception("Unable to remove entry in M2M link table");
		}
	
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}

	/**
	 * Wipes any rows in the B model joined to the given A model row.
	 * 
	 * @param mixed[] $a the row in the A model
	 * @throws M2MLinkTable_Exception
	 * @return true always returns true; failure throws an exception
	 */
	public function delete_all_for_a(array $a) {
		self::assert_id_object($a, $this->table_a->get_id_field());
		
		if (!$this->database->delete_rows_by_conditions($this->table, array(
			$this->link_a => new Database\Param($a[$this->table_a->get_id_field()], new Database\Type_Id(Database\Type::NOT_NULL))
		), false)) throw new M2MLinkTable_Exception("Unable to delete all M2M link objects from table", $this->table);

		return true;
	}

	/**
	 * Wipes any rows in the A model joined to the given B model row.
	 * 
	 * @param mixed[] $b the row in the B model
	 * @throws M2MLinkTable_Exception
	 * @return true always returns true; failure throws an exception
	 */
	public function delete_all_for_b(array $b) {
		self::assert_id_object($b, $this->table_b->get_id_field());
		
		if (!$this->database->delete_rows_by_conditions($this->table, array(
			$this->link_b => new Database\Param($b[$this->table_b->get_id_field()], new Database\Type_Id(Database\Type::NOT_NULL))
		), false)) throw new M2MLinkTable_Exception("Unable to delete all M2M link objects from table", $this->table);

		return true;
	}

	/**
	 * Updates a row in the model's table with new data without affecting the existing link.
	 * 
	 * Any field apart from the foreign key link fields can be populated with new data.
	 * 
	 * @param mixed[] $row an existing model row, which must contain both the link table foreign keys.
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function update_link_row($row) {
		self::assert_id_object($row, $this->link_a);
		self::assert_id_object($row, $this->link_b);
		
		$params = array();
		foreach ($row as $var => &$value) {
			if ($var == $this->link_a) continue;
			if ($var == $this->link_b) continue;
				
			if (!isset($this->structure[$var])) throw new M2MLinkTable_Exception("Parameter variable is missing in the model structure", $var);
			$params[$var] = new Database\Param($value, $this->structure[$var]);
		}
		unset($value);

		if (!$this->database->update_rows_by_conditions($this->table, $params, array(
			$this->link_a => new Database\Param($row[$this->link_a], $this->structure[$this->link_a]),
			$this->link_b => new Database\Param($row[$this->link_b], $this->structure[$this->link_b])
		), true)) throw new FirstClass_Exception("Unable to update row in the M2M link table", $this->table);

		return true;
	}

}
