<?php
/**
 * Ordered first-class model support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/firstclass.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/orderable.php";

use \Abstraction\Database as Database;

/**
 * @internal
 */
class Ordered_Exception extends FirstClass_Exception {}

/**
 * Defines a root model for ordered first-class structured models.
 * 
 * Ordered first-class models inherit their definition from FirstClass models, but also have a single SmallInt ordering field.
 * Because the ordering is performed using SmallInt, they are limited to a maximum of 65535 rows, and intended for small to medium sized lists rather than large data sets.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Ordered extends FirstClass implements Orderable {
	protected $ordered_field;

	/**
	 * Constructs a new instance of this ordered first-class model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Database\Type[] $structure an associated array to define the model structure, taking the format field name => field type object
	 * @param string $ordered_field the name of the ordering field, if not "ordered" 
	 * @param string $id_field the name of the identifier field, if not "id"
	 * @param ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @param IndexKey[] $index_keys any non-unique index keys to apply to the model
	 * @throws Ordered_Exception
	 */
	public function __construct(
			Database\Wrapper $database,
			$table,
			array $structure,
			$ordered_field = "ordered",
			$id_field = "id",
			array $foreign_keys = array(),
			array $unique_keys = array(),
			array $index_keys = array()
	) {
		if (array_key_exists($ordered_field, $structure)) throw new Ordered_Exception("The ordered field is implicit for ordered models and should not be explicitly stated in the structure");
		$this->ordered_field = $ordered_field;
		$structure[$ordered_field] = new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL);
				
		parent::__construct(
				$database,
				$table,
				$structure,
				$id_field,
				$foreign_keys,
				$unique_keys,
				$index_keys
		);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns the field which is used for ordering in the model.
	 * 
	 * @return string the name of the ordered field
	 */
	public function get_ordered_field() {
		return $this->ordered_field;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		$this->database->preprepare("MODELS_ORDERED__{$this->table}__LIST_ORDERED", "
			SELECT {$this->fields}
			FROM " . $this->model($this) . "
			ORDER BY " . $this->modelfield($this, $this->ordered_field) . " ASC
		", null, $this->structure);

		$this->database->preprepare("MODELS_ORDERED__{$this->table}__MAX_ORDERED", "
			SELECT MAX(" . $this->modelfield($this, $this->ordered_field) . ") AS " . $this->tempfield("max") . "
			FROM " . $this->model($this) . "
		", null, array(
			"max" => new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED)
		));

		$this->database->preprepare("MODELS_ORDERED__{$this->table}__CURRENT_ORDERED", "
			SELECT " . $this->modelfield($this, $this->ordered_field) . " AS " . $this->tempfield("ordered") . "
			FROM " . $this->model($this) . "
			WHERE " . $this->modelfield($this, $this->id_field) . "=:id
		", array(
			"id" => new Database\Type_SerialId()
		), array(
			"ordered" => new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL)
		));

		$this->database->preprepare("MODELS_ORDERED__{$this->table}__BY_ORDERED", "
			SELECT " . $this->modelfield($this, $this->id_field) . " AS " . $this->tempfield("id") . "
			FROM " . $this->model($this) . "
			WHERE " . $this->modelfield($this, $this->ordered_field) . "=:ordered
		", array(
			"ordered" => new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL)
		), array(
			"id" => new Database\Type_SerialId()
		));
	}

	//-------------------------------------------------------------------------

	/**
	 * Lists all data within the database for this model, ordered by the ordering field.
	 * 
	 * Roughly equivalent to SELECT * FROM <table> ORDER BY <ordered> ASC
	 * 
	 * @return array a non-associated array of results, themselves each associated arrays of field names to row values
	 */
	function list_ordered() {
		return $this->database->execute_params("MODELS_ORDERED__{$this->table}__LIST_ORDERED");
	}

	//---------------------------------------------------------------------

	const MOVE_UP = "up";
	const MOVE_DOWN = "down";
	const MOVE_TOP = "top";
	const MOVE_BOTTOM = "bottom";
	
	/**
	 * Moves a model row in the specified direction within the order.
	 * 
	 * Transaction support is enabled, so failed movement will roll back.
	 * 
	 * @param mixed[] $object an existing model row
	 * @param string $direction one of the direction values: MOVE_UP, MOVE_DOWN, MOVE_TOP or MOVE_BOTTOM
	 * @throws Database\Exception
	 * @throws Ordered_Exception
	 * @return mixed[] the moved model row, including its new ordering value
	 */
	function move(array $object, $direction) {
		self::assert_id_object($object, $this->id_field);
		
		if (!in_array($direction, array(self::MOVE_UP, self::MOVE_DOWN, self::MOVE_TOP, self::MOVE_BOTTOM), true)) throw new Hierarchy_Exception("Supplied direction is not valid: {$direction}");
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		$current = $this->database->execute_params_value("MODELS_ORDERED__{$this->table}__CURRENT_ORDERED", array("id" => $object[$this->id_field]));
		$current_max = $this->database->execute_params_value("MODELS_ORDERED__{$this->table}__MAX_ORDERED");

		switch($direction) {
			case self::MOVE_UP:
				if ($current === 0) {
					$this->database->transaction_rollback();
					throw new Ordered_Exception("Cannot move above the first ordered row");
				}

				$prev = $this->database->execute_params_value("MODELS_ORDERED__{$this->table}__BY_ORDERED", array(
					"ordered" => $current - 1
				));

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($prev, new Database\Type_SerialId())
				), true);

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current - 1, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId())
				), true);

				break;
			case self::MOVE_DOWN:
				if ($current === $current_max) {
					$this->database->transaction_rollback();
					throw new Ordered_Exception("Cannot move below the last ordered row");
				}

				$prev = $this->database->execute_params_value("MODELS_ORDERED__{$this->table}__BY_ORDERED", array(
					"ordered" => $current + 1
				));
				
				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($prev, new Database\Type_SerialId())
				), true);

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current + 1, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId())
				), true);

				break;
			case self::MOVE_TOP:
				if ($current === 0) break;	// if already topmost

				$this->database->query_params("
					UPDATE " . $this->model($this) . "
					SET " . $this->modelfield($this, $this->ordered_field) . "=" . $this->modelfield($this, $this->ordered_field) . "+1
					WHERE " . $this->modelfield($this, $this->ordered_field) . "<:threshold
				", array(
					"threshold" => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				));

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param(0, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId())
				), true);

				break;
			case self::MOVE_BOTTOM:
				if ($current === $current_max) break;	// if already last

				$this->database->query_params("
					UPDATE " . $this->model($this) . "
					SET " . $this->modelfield($this, $this->ordered_field) . "=" . $this->modelfield($this, $this->ordered_field) . "-1
					WHERE " . $this->modelfield($this, $this->ordered_field) . ">:threshold
				", array(
					"threshold" => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				));
				
				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current_max, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId()),
				), true);

				break;
		}
		
		$object = $this->_internal_firstclass_get_by_id($object[$this->id_field]);
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return $object;
	}
	
	//---------------------------------------------------------------------

	/**
	 * Inserts the given values into the database for this model.
	 * 
	 * Note, the identifier and ordering values are generated automatically and should not be specified explicitly.
	 * Rows are always inserted at the end of the ordering, incrementing the last value.
	 * Transaction support is enabled, so failed row insertion will roll back.
	 * 
	 * @param mixed[] $values an associated array values taking the format field name => value 
	 * @throws Exception
	 * @return mixed[] the newly created model row, including its new identifier value
	 */
	function insert(array $values) {
		if (array_key_exists($this->ordered_field, $values)) throw new Ordered_Exception("Do not explicitly specify an ordered value when inserting data into an Ordered model");
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$current_max = $this->database->execute_params_value("MODELS_ORDERED__{$this->table}__MAX_ORDERED", null, true);
		if ($current_max === null) $current_max = 0; 
		else $current_max++;

		$values[$this->ordered_field] = $current_max;

		$object = parent::insert($values);
	
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return $object;		
	}

	/**
	 * Deletes a row from the model's table using its identifier value.
	 * 
	 * Row ordering values are reindexed automatically.
	 * Transaction support is enabled, so failed row deletion or reindexing will roll back.
	 * 
	 * @param mixed[] $object an existing model row
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	function delete(array $object) {
		self::assert_id_object($object, $this->id_field);

		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$this->move($object, "bottom");
		parent::delete($object);
	
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}
}
