<?php
/**
 * Ordered second-class model support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/secondclass.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/orderable.php";

use \Abstraction\Database as Database;

/**
 * @internal
 */
class OrientatedOrdered_Exception extends SecondClass_Exception {}

/**
 * Defines a root model for ordered second-class structured models.
 * 
 * Ordered second-class models inherit their definition from SecondClass models rather than Ordered models, as multiple inheritance is not possible.
 * They have a single SmallInt ordering field as Ordered models do, with the same constraints, however this ordering is 'orientated' around the parent FirstClass model row, hence there are multiple orderings within the same table.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class OrientatedOrdered extends SecondClass implements Orderable {
	protected $ordered_field;
	
	/**
	 * Constructs a new instance of this 'orientated' ordered second-class model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Database\Type[] $structure an associated array to define the model structure, taking the format field name => field type object
	 * @param FirstClass $firstclass the parent FirstClass model to link to
	 * @param string $firstclass_field the name of the linkage field to create the foreign key upon
	 * @param string $ordered_field the name of the ordering field, if not "ordered" 
	 * @param string $id_field the name of the identifier field, if not "id"
	 * @param ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @param IndexKey[] $index_keys any non-unique index keys to apply to the model
	 * @throws OrientatedOrdered_Exception
	 */
	public function __construct(
			Database\Wrapper $database,
			$table,
			array $structure,
			FirstClass $firstclass,
			$firstclass_field,
			$ordered_field = "ordered",
			$id_field = "id",
			array $foreign_keys = array(),
			array $unique_keys = array(),
			array $index_keys = array()
	) {
		if (array_key_exists($ordered_field, $structure)) throw new OrientatedOrdered_Exception("The ordered field is implicit for orientatedordered models and should not be explicitly stated in the structure");
		$this->ordered_field = $ordered_field;
		$structure[$ordered_field] = new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL);
		
		parent::__construct(
				$database,
				$table,
				$structure,
				$firstclass,
				$firstclass_field,
				$id_field,
				$foreign_keys,
				$unique_keys,
				$index_keys
		);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns the field which is used for ordering in the model.
	 * 
	 * @return string the name of the ordered field
	 */
	public function get_ordered_field() {
		return $this->ordered_field;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		$fields = array();
		foreach (array_keys($this->structure) as $field) $fields[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field);
		$fields = implode(",", $fields);
		
		$this->database->preprepare("MODELS_ORIENTATEDORDERED__{$this->table}__LIST_ORDERED", "
			SELECT {$fields}
			FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=:firstclass
			ORDER BY " . $this->modelviewfield($this, "SECONDCLASS_JOIN", $this->ordered_field) . " ASC
		", array(
			"firstclass" => new Database\Type_Id(Database\Type_Id::NOT_NULL)
		), $this->structure);

		$this->database->preprepare("MODELS_ORIENTATEDORDERED__{$this->table}__MAX_ORDERED", "
			SELECT MAX(" . $this->modelviewfield($this, "SECONDCLASS_JOIN", $this->ordered_field) . ") AS " . $this->tempfield("max") . "
			FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=:firstclass
		", array(
			"firstclass" => new Database\Type_Id(Database\Type_Id::NOT_NULL)
		), array(
			"max" => new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED)
		));

		$this->database->preprepare("MODELS_ORIENTATEDORDERED__{$this->table}__CURRENT_ORDERED", "
			SELECT " . $this->modelfield($this, $this->ordered_field) . " AS " . $this->tempfield("ordered") . "
			FROM " . $this->model($this) . "
			WHERE " . $this->modelfield($this, $this->id_field) . "=:id
		", array(
			"id" => new Database\Type_SerialId()
		), array(
			"ordered" => new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL)
		));

		$this->database->preprepare("MODELS_ORIENTATEDORDERED__{$this->table}__BY_ORDERED", "
			SELECT " . $this->modelviewfield($this, "SECONDCLASS_JOIN", $this->id_field) . " AS " . $this->tempfield("id") . "
			FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=:firstclass
			AND " . $this->modelviewfield($this, "SECONDCLASS_JOIN", $this->ordered_field) . "=:ordered
		", array(
			"firstclass" => new Database\Type_Id(Database\Type_Id::NOT_NULL),
			"ordered" => new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL)
		), array(
			"id" => new Database\Type_SerialId()
		));
	}

	//-------------------------------------------------------------------------

	/**
	 * Lists all rows within this model's table that are linked to the specified FirstClass model row, ordered by the ordering field.
	 * 
	 * @param mixed[] $firstclass an existing row in the parent FirstClass model.
	 * @return array a non-associated array of results, themselves each associated arrays of field names to row values
	 */
	function list_ordered_by_firstclass(array $firstclass) {
		self::assert_id_object($firstclass, $this->firstclass->get_id_field());

		return $this->database->execute_params("MODELS_ORIENTATEDORDERED__{$this->table}__LIST_ORDERED", array("firstclass" => $firstclass[$this->firstclass->get_id_field()]));
	}

	//---------------------------------------------------------------------

	const MOVE_UP = "up";
	const MOVE_DOWN = "down";
	const MOVE_TOP = "top";
	const MOVE_BOTTOM = "bottom";
	
	/**
	 * Moves a model row in the specified direction within the order, restricted to the ordering of the specified parent FirstClass model row.
	 * 
	 * Note, the FirstClass parent model row is derived automatically from the existing model row rather than explicitly specified.
	 * Transaction support is enabled, so failed movement will roll back.
	 * 
	 * @param mixed[] $object an existing model row
	 * @param string $direction one of the direction values: MOVE_UP, MOVE_DOWN, MOVE_TOP or MOVE_BOTTOM
	 * @throws Database\Exception
	 * @throws Ordered_Exception
	 * @return mixed[] the moved model row, including its new ordering value
	 */
	function move(array $object, $direction) {
		self::assert_id_object($object, $this->id_field);
		self::assert_id($object[$this->firstclass_field]);
		
		if (!in_array($direction, array(self::MOVE_UP, self::MOVE_DOWN, self::MOVE_TOP, self::MOVE_BOTTOM), true)) throw new OrientatedOrdered_Exception("Supplied direction is not valid: {$direction}");
				
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		$current = $this->database->execute_params_value("MODELS_ORIENTATEDORDERED__{$this->table}__CURRENT_ORDERED", array("id" => $object[$this->id_field]));
		$current_max = $this->database->execute_params_value("MODELS_ORIENTATEDORDERED__{$this->table}__MAX_ORDERED", array("firstclass" => $object[$this->firstclass_field]));

		switch($direction) {
			case self::MOVE_UP:
				if ($current === 0) {
					$this->database->transaction_rollback();
					throw new OrientatedOrdered_Exception("Cannot move above the first ordered row");
				}

				$prev = $this->database->execute_params_value("MODELS_ORIENTATEDORDERED__{$this->table}__BY_ORDERED", array(
					"firstclass" => $object[$this->firstclass_field],
					"ordered" => $current - 1
				));

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($prev, new Database\Type_SerialId())
				), true);

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current - 1, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId())
				), true);

				break;
			case self::MOVE_DOWN:
				if ($current === $current_max) {
					$this->database->transaction_rollback();
					throw new OrientatedOrdered_Exception("Cannot move below the last ordered row");
				}

				$prev = $this->database->execute_params_value("MODELS_ORIENTATEDORDERED__{$this->table}__BY_ORDERED", array(
					"firstclass" => $object[$this->firstclass_field],
					"ordered" => $current + 1
				));
				
				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($prev, new Database\Type_SerialId())
				), true);

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current + 1, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId())
				), true);

				break;
			case self::MOVE_TOP:
				if ($current === 0) break;	// if already topmost

				$this->database->query_params("
					UPDATE " . $this->model($this) . "
					SET " . $this->modelfield($this, $this->ordered_field) . "=" . $this->modelfield($this, $this->ordered_field) . "+1
					WHERE " . $this->modelfield($this, $this->firstclass_field) . "=:firstclass
					AND " . $this->modelfield($this, $this->ordered_field) . "<:threshold
				", array(
					"firstclass" => new Database\Param($object[$this->firstclass_field], new Database\Type_Id(Database\Type_Id::NOT_NULL)),
					"threshold" => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				));

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param(0, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId())
				), true);

				break;
			case self::MOVE_BOTTOM:
				if ($current === $current_max) break;	// if already last

				$this->database->query_params("
					UPDATE " . $this->model($this) . "
					SET " . $this->modelfield($this, $this->ordered_field) . "=" . $this->modelfield($this, $this->ordered_field) . "-1
					WHERE " . $this->modelfield($this, $this->firstclass_field) . "=:firstclass
					AND " . $this->modelfield($this, $this->ordered_field) . ">:threshold
				", array(
					"firstclass" => new Database\Param($object[$this->firstclass_field], new Database\Type_Id(Database\Type_Id::NOT_NULL)),
					"threshold" => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				));
				
				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current_max, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId()),
				), true);

				break;
		}
		
		$object = $this->_internal_firstclass_get_by_id($object[$this->id_field]);
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return $object;
	}
	
	/**
	 * Jumps a model row to a different parent firstclass, automatically cleaning up its previous space in the order.
	 * 
	 * This is essentially the same as a delete/re-insert, but the ID number won't change so any existing remote foreign keys will persist.
	 * Note, any unique key constraints etc. can't be checked, so will fail. Subclasses need to police this.
	 * Transaction support is enabled, so failed jumps will roll back.
	 *
	 * @param mixed[] $object an existing model row
	 * @param mixed[] $destination the new firstclass model row to jump to
	 * @throws Database\Exception
	 * @throws Ordered_Exception
	 * @return mixed[] the jumped model row, including its new ordering value and updated firstclass field.
	 */
	function jump(array $object, array $destination) {
		self::assert_id_object($object, $this->id_field);
		self::assert_id($object[$this->firstclass_field]);
		
		self::assert_id_object($destination, $this->firstclass->get_id_field());
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$this->move($object, "bottom");
		
		$current_max = $this->database->execute_params_value("MODELS_ORIENTATEDORDERED__{$this->table}__MAX_ORDERED", array("firstclass" => $destination[$this->firstclass->get_id_field()]), true);
		if ($current_max === null) $current_max = 0; 
		else $current_max++;

		$object[$this->firstclass_field] = $destination[$this->firstclass->get_id_field()];
		$object[$this->ordered_field] = $current_max;
		$this->update($object);	// can't use update_for_firstclass as we are changing the firstclass!
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return $object;
	}
	
	//---------------------------------------------------------------------

	/**
	 * Inserts the given values into the database for this model, associating it with the given parent FirstClass model row
	 * 
	 * Note, the identifier and ordered values are generated automatically and should not be specified explicitly.
	 * Rows are always inserted at the end of the ordering, incrementing the last value.
	 * Transaction support is enabled, so failed row insertion will roll back.
	 * 
	 * @param mixed[] $firstclass an existing row in the parent FirstClass model.
	 * @param mixed[] $values an associated array values taking the format field name => value
	 * @throws OrientatedOrdered_Exception
	 * @return mixed[] the newly created model row, including its new identifier value
	 */
	function insert_for_firstclass(array $firstclass, array $values) {
		self::assert_id_object($firstclass, $this->firstclass->get_id_field());
		
		if (array_key_exists($this->ordered_field, $values)) throw new OrientatedOrdered_Exception("Ordering fields should not be specified explicitly for orientated-ordered models");

		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$current_max = $this->database->execute_params_value("MODELS_ORIENTATEDORDERED__{$this->table}__MAX_ORDERED", array("firstclass" => $firstclass[$this->firstclass->get_id_field()]), true);
		if ($current_max === null) $current_max = 0; 
		else $current_max++;
		
		$values[$this->ordered_field] = $current_max;
		
		$object = parent::insert_for_firstclass($firstclass, $values);
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return $object;
	}

	/**
	 * Deletes a row from the model's table using its identifier value, validated against the FirstClass parent model view.
	 * 
	 * Row ordering values are reindexed automatically.
	 * Transaction support is enabled, so failed row deletion or reindexing will roll back.
	 * 
	 * @param mixed[] $firstclass an existing row in the parent FirstClass model.
	 * @param mixed[] $object an existing model row
	 * @throws OrientatedOrdered_Exception
	 * @throws SecondClass_Exception
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function delete_for_firstclass(array $firstclass, array $object) {
		self::assert_id_object($object, $this->id_field);

		if (!array_key_exists($this->firstclass_field, $object)) throw new OrientatedOrdered_Exception("Unable to find orientator ID within the object");
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$this->move($object, "bottom");
		parent::delete_for_firstclass($firstclass, $object);
	
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}
}
