<?php
/**
 * Root model support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "config.php";
require_once ABSTRACTION_ROOT_PATH . "core.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

use \Abstraction as Abstraction;
use \Abstraction\Data as Data;
use \Abstraction\Database as Database;

/**
 * @internal
 */
class Exception extends Abstraction\Exception {}

/**
 * @internal
 */
class TypeMismatchException extends Abstraction\TypeMismatchException {}

/**
 * @internal
 */
class BadParameterException extends TypeMismatchException {
	public function __construct($value, $message = "Bad model parameter value") {
		parent::__construct("", $value, $message);
	}
}

/**
 * @internal
 */
class NotIdObjectException extends BadParameterException {
	public function __construct($value, $message = null) {
		parent::__construct($value, $message === null ? "Bad model parameter value (not an ID object)" : $message);
	}
}

/**
 * Definition class for foreign keys within a model.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class ForeignKey {
	public $dest_table;
	public $on_delete, $on_update;
	public $constraint_name = false;

	/**
	 * Constructs a new instance to represent a foreign key in a model.
	 * 
	 * @param FirstClass $dest_table the destination (remote) table the foreign key points to
	 * @param string $on_delete action to perform on remote row deletion (e.g. CASCADE, SET NULL, RESTRICT etc.)
	 * @param string $on_update action to perform on remote row update (e.g. CASCADE, SET NULL, RESTRICT etc.)
	 * @param bool|string $constraint_name an overriding identifier for the constraint, since MySQL has limited character lengths for identifiers
	 */
	public function __construct(FirstClass $dest_table, $on_delete = "CASCADE", $on_update = "CASCADE", $constraint_name = false) {
		$this->dest_table = $dest_table;
		$this->on_delete = $on_delete;
		$this->on_update = $on_update;
		
		$this->constraint_name = $constraint_name;
	}
}

/**
 * Definition class for unique keys within a model.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class IndexKey {
	public $fields;
	public $constraint_name = false;

	/**
	 * Constructs a new instance to represent a index key in a model.
	 * 
	 * @param string[] $fields the field names that form the index key
	 * @param bool|string $constraint_name an overriding identifier for the constraint, since MySQL has limited character lengths for identifiers
	 */
	public function __construct(array $fields, $constraint_name = false) {
		$this->fields = $fields;
		$this->constraint_name = $constraint_name;
	}
}

/**
 * Definition class for unique keys within a model.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class UniqueKey extends IndexKey {
	/**
	 * Constructs a new instance to represent a unique key in a model.
	 *
	 * @param string[] $fields the field names that form the unique key
	 * @param bool|string $constraint_name an overriding identifier for the constraint, since MySQL has limited character lengths for identifiers
	 */
	public function __construct(array $fields, $constraint_name = false) {
		parent::__construct($fields, $constraint_name);
	}
}

/**
 * Various helper methods that models can make use of.
 * 
 * The root Model class extended this class, so all subsequent models can make use of them.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class ModelHelperMethods {
	protected $database;

	/**
	 * Constructs a new instance.
	 * 
	 * @param Database\Wrapper $database the database interface to use
	 */
	public function __construct(Database\Wrapper $database) {
		$this->database = $database;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Validates a variable as numeric (int or float), strongly typing it on success and throwing an exception on failure.
	 *
	 * @param mixed $value the variable to validate
	 * @param mixed|NULL $upper the upper inclusive value allowed in the range
	 * @param mixed|NULL $lower the lower inclusive value allowed in the range
	 * @throws BadParameterException
	 * @return void
	 */
	protected static function assert_numeric(&$value, $lower = null, $upper = null) {
		if (!Data\Data::validate_numeric($value, $lower, $upper)) throw new BadParameterException($value);
	}
	
	protected static function assert_numeric_or_null(&$value, $lower = null, $upper = null) {
		if (!Data\Data::validate_numeric_or_null($value, $lower, $upper)) throw new BadParameterException($value);
	}
	
	/**
	 * Validates a variable as an integer, strongly typing it on success and throwing an exception on failure.
	 * 
	 * @param mixed $value the variable to validate
	 * @param int|NULL $upper the upper inclusive value allowed in the range
	 * @param int|NULL $lower the lower inclusive value allowed in the range
	 * @throws BadParameterException
	 * @return void
	 */
	protected static function assert_int(&$value, $lower = null, $upper = null) {
		if (!Data\Data::validate_int($value, $lower, $upper)) throw new BadParameterException($value);
	}
	
	protected static function assert_int_or_null(&$value, $lower = null, $upper = null) {
		if (!Data\Data::validate_int_or_null($value, $lower, $upper)) throw new BadParameterException($value);
	}

	/**
	 * Validates a variable as an integer suitable for Id usage (>0), strongly typing it on success and throwing an exception on failure.
	 * 
	 * @param mixed $id the variable to validate
	 * @throws BadParameterException
	 * @return void
	 */
	protected static function assert_id(&$id) {
		if (!Data\Data::validate_id($id)) throw new BadParameterException($id);
	}

	protected static function assert_id_or_null(&$id) {
		if (!Data\Data::validate_id_or_null($id)) throw new BadParameterException($id);
	}
	
	/**
	 * Validates a variable as an Id object array, strongly typing its Id field on success and throwing an exception on failure.
	 * 
	 * @param array $object the variable to validate
	 * @param string $id the name of the id field, if not "id"
	 * @throws BadParameterException
	 * @return void
	 */
	protected static function assert_id_object(&$object, $field = "id") {
		if (!Data\Data::validate_id_object($object, $field)) throw new NotIdObjectException($object);
	}

	protected static function assert_id_object_or_null(&$object, $field = "id") {
		if (!Data\Data::validate_id_object_or_null($object, $field)) throw new NotIdObjectException($object);
	}
	
	/**
	 * Validates a variable as an array, throwing an exception on failure.
	 * 
	 * @param array $array the variable to validate
	 * @throws BadParameterException
	 * @return void
	 */
	protected static function assert_array(&$array) {
		if (!is_array($array)) throw new BadParameterException($array);
	}

	/**
	 * Validates a variable as a non-blank string, ensuring it is neither empty nor >255 characters.
	 * 
	 * Note, strong typing is not set for non-strings so as to allow other methods to make use of it for non-string data.
	 * 
	 * @param mixed $variable the variable to validate
	 * @throws BadParameterException
	 * @return void
	 */
	protected static function assert_string(&$variable) {
		if (!Data\Data::validate_string($variable)) throw new BadParameterException($variable);
	}

	protected static function assert_string_or_null(&$variable) {
		if (!Data\Data::validate_string_or_null($variable)) throw new BadParameterException($variable);
	}
	
	/**
	 * Validates a variable against a regular expression pattern match.
	 * 
	 * @param string $pattern the regular expression definition
	 * @param mixed $variable the variable to validate
	 * @throws BadParameterException
	 * @return void
	 */
	protected static function assert_regex($pattern, &$variable) {
		self::assert_string($variable);
		if (!Data\Data::validate_regex($pattern, $variable)) throw new BadParameterException($variable);
	}
	
	protected static function assert_regex_or_null($pattern, &$variable) {
		self::assert_string_or_null($variable);
		if (!Data\Data::validate_regex_or_null($pattern, $variable)) throw new BadParameterException($variable);
	}
	
	/**
	 * Validates a variable against an array of possible options.
	 * 
	 * @param string[] $options the available options
	 * @param mixed $variable the variable to validate
	 * @throws BadParameterException
	 * @return void
	 */
	protected static function assert_option($options, &$variable, $explode_character = ",") {
		self::assert_string($variable);
		if (!Data\Data::validate_option($options, $variable, $explode_character)) throw new BadParameterException($variable);
	}

	protected static function assert_option_or_null($options, &$variable, $explode_character = ",") {
		self::assert_string_or_null($variable);
		if (!Data\Data::validate_option_or_null($options, $variable, $explode_character)) throw new BadParameterException($variable);
	}
	
	/**
	 * Validates a variable as a valid ID_NAME.
	 * 
	 * @param mixed $name the variable to validate
	 * @throws BadParameterException
	 * @return void
	 */
	protected static function assert_id_name(&$name) {
		self::assert_string($name);
		self::assert_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name);
	}

	protected static function assert_id_name_or_null(&$name) {
		self::assert_string_or_null($name);
		self::assert_regex_or_null(Data\Data::REGEX_PATTERN_ID_NAME, $name);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	public function get_database() {
		return $this->database;
	}

	/**
	 * Returns delimited syntax for the given table.
	 * 
	 * @param string $table the table name
	 * @return string
	 */
	protected function table($table) {
		return $this->database->delimit($table);
	}
	
	/**
	 * Returns delimited syntax for the given view.
	 * 
	 * @param string $table the table name
	 * @param string $name the view unique identifier
	 * @return string
	 */
	protected function view($table, $name) {
		return $this->database->delimit($this->database->view($table, $name));
	}
	
	/**
	 * Returns a model's table in delimited syntax.
	 * 
	 * @param Models $model the model
	 * @return string
	 */
	public function model(Models $model) {
		return $this->table($model->get_table());
	}

	/**
	 * Returns a model's view in delimited syntax.
	 * 
	 * @param Models $model the model
	 * @param string $name the view unique identifier
	 * @return string
	 */
	protected function modelview(Models $model, $name) {
		return $this->view($model->get_table(), $name);
	}

	/**
	 * Helper method to return a derived table (i.e. temporary semantic naming) in delimited syntax.
	 * @param string $name the name of the derived table
	 * @return string
	 */
	protected function derived($name) {
		return $this->table($name);
	}
	
	/**
	 * Returns delimited syntax for a field of the given table.
	 * 
	 * @param string $table the table name
	 * @param string $field the field name
	 * @return string
	 */
	protected function tablefield($table, $field) {
		return $this->database->tablefield($table, $field);
	}
	
	/**
	 * Returns delimited syntax for a field of the given view.
	 * 
	 * @param string $table the table name
	 * @param string $name the view unique identifier
	 * @param string $field the field name
	 * @return string
	 */
	protected function viewfield($table, $name, $field) {
		return $this->database->tablefield($this->database->view($table, $name), $field);
	}
	
	/**
	 * Returns a model's field in delimited syntax.
	 * 
	 * @param Models $model the model
	 * @param string $field the field name
	 * @return string
	 */
	public function modelfield(Models $model, $field) {
		return $this->tablefield($model->get_table(), $field);
	}
	
	/**
	 * Returns a model's view field in delimited syntax.
	 * 
	 * @param Models $model the model
	 * @param string $name the view unique identifier
	 * @param string $field the field name
	 * @return string
	 */
	protected function modelviewfield(Models $model, $name, $field) {
		return $this->viewfield($model->get_table(), $name, $field);
	}

	/**
	 * Helper method to return a derived table's field in delimited syntax.
	 * 
	 * @param string $name the name of the derived table
	 * @param string $field the field name
	 * @return string
	 */
	protected function derivedfield($name, $field) {
		return $this->tablefield($name, $field);
	}
	
	/**
	 * Helper method to return a temporary field in delimited syntax.
	 * 
	 * @param string $name the name of the temporary field.
	 * @return string
	 */
	protected function tempfield($name) {
		return $this->database->delimit($name);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns an associative array of the model's structure as delimited fields, in the format field name => delimited model field
	 * 
	 * @param Models $model
	 * @return string[]
	 */
	protected function modelfield_array(Models $model) {
		$fields = array();
		foreach (array_keys($model->get_structure()) as $field) $fields[$field] = $this->modelfield($model, $field);
	
		return $fields;
	}

	/**
	 * Returns a CSV formatted list of the model's structure in delimited fields.
	 * 
	 * @param Models $model
	 * @return string
	 */
	protected function modelfield_csv(Models $model) {
		return implode(",", array_values($this->modelfield_array($model))); 
	}

	/**
	 * Returns an associative array of the model's structure as delimited fields, in the format field name => delimited model field
	 * 
	 * @param Models $model
	 * @param string $name the view unique identifier
	 * @return string[]
	 */
	protected function modelviewfield_array(Models $model, $name) {
		$fields = array();
		foreach (array_keys($model->get_structure()) as $field) $fields[$field] = $this->modelviewfield($model, $name, $field);
	
		return $fields;
	}

	/**
	 * Returns a CSV formatted list of the model's structure in delimited fields.
	 * 
	 * @param Models $model
	 * @param string $name the view unique identifier
	 * @return string
	 */
	protected function modelviewfield_csv(Models $model, $name) {
		return implode(",", array_values($this->modelviewfield_array($model, $name))); 
	}
}

/**
 * Root model for all further defined models.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Models extends ModelHelperMethods {
	protected $table;
	protected $structure;
	protected $fields;
	protected $foreign_keys;
	protected $unique_keys;
	
	private $views;
	private $view_creation_order;	// this is needed so that databases such as postgres can create and drop views in the right order, i.e. reverse drop

	/**
	 * Constructs a new instance of this model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Database\Type[] $structure an associated array to define the model structure, taking the format field name => field type object
	 * @param ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @param IndexKey[] $index_keys any non-unique index keys to apply to the model
	 */
	public function __construct(
			Database\Wrapper $database,
			$table,
			array $structure,
			array $foreign_keys = array(),
			array $unique_keys = array(),
			array $index_keys = array()
	) {
		parent::__construct($database);

		$this->table = $table;
		$this->structure = $structure;
		$this->foreign_keys = $foreign_keys;
		$this->unique_keys = $unique_keys;
		$this->index_keys = $index_keys;
		
		$this->fields = $this->modelfield_csv($this);

		$this->views = array();
		$this->view_creation_order = array();
		
		$this->define_views();
		$this->preprepare();
	}

	/**
	 * Returns an associated array corresponding to the model structure.
	 * 
	 * @return Database\Type[]
	 */
	public function get_structure() {
		return $this->structure;
	}

	/**
	 * Returns the table name within the database.
	 * 
	 * @return string
	 */
	public function get_table() {
		return $this->table;
	}

	/**
	 * Returns an associated array corresponding to the model's defined foreign keys (if any)
	 * 
	 * @return ForeignKey[]
	 */
	public function get_foreign_keys() {
		return $this->foreign_keys;
	}
	
	/**
	 * Returns an associated array corresponding to the model's defined unique keys (if any)
	 *
	 * @return UniqueKey[]
	 */
	public function get_unique_keys() {
		return $this->unique_keys;
	}
	
	/**
	 * Returns an associated array corresponding to the model's defined non-unique index keys (if any)
	 *
	 * @return IndexKey[]
	 */
	public function get_index_keys() {
		return $this->index_keys;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Defines any preprepared statements within the model.
	 * 
	 * This method should be overridden by model subclasses to define their own preprepared statements, but a call to parent::preprepared() should still always be in place to propagate upwards.
	 * 
	 * @return void
	 */
	protected function preprepare() {
		$this->database->preprepare("MODELS__{$this->table}__LIST_ALL", "
			SELECT {$this->fields}
			FROM " . $this->table($this->table) . "
		", null, $this->structure);
	}
	
	/**
	 * Defines a new view for this model.
	 * 
	 * @param string $name the unique identifier name to use for this view definition
	 * @param string $sql the SQL code for this view
	 * @throws Exception
	 * @return void
	 */
	protected function define_view($name, $sql) {
		if (array_key_exists($name, $this->views)) throw new Exception("A view definition within the model with that name already exists", $name);
		$this->views[$name] = $sql;
		$this->view_creation_order[] = $name;
	}

	/**
	 * Defines any views within the model.
	 * 
	 * This method should be overridden by model subclasses to define their own views, but a call to parent::define_views() should still always be in place to propagate upwards.
	 * 
	 * @return void
	 */
	protected function define_views() {}

	/**
	 * Lists the defined views for this model.
	 * 
	 * This is useful to manual hacking to retrospectively add new fields to existing models in the database etc.
	 * 
	 * @return string[] an associative array of names and SQL.
	 */
	public function list_views() {
		$views = array();
		foreach ($this->view_creation_order as $name) $views[$name] = $this->views[$name];
		
		return $views;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Creates a new table in the database for this model.
	 * 
	 * Note that foreign keys and associated structural elements (e.g. CREATE TYPE) are automatically created alongside.
	 * 
	 * @throws Exception
	 * @return void
	 */
	public function create_table() {
		$this->database->create_table($this->table, $this->structure);
		
		foreach ($this->foreign_keys as $link_field => $foreign) {
			if (!($foreign instanceof ForeignKey)) throw new Exception("Foreign key destinations must be represented as ForeignKey objects");
			$this->database->create_foreign_key($this->table, $foreign->dest_table->get_table(), $link_field, $foreign->dest_table->get_id_field(), $foreign->on_delete, $foreign->on_update, $foreign->constraint_name);
		}

		foreach ($this->unique_keys as $unique) {
			if (!($unique instanceof UniqueKey)) throw new Exception("Unique field destinations must be represented as UniqueField objects");
			$this->database->create_unique_key($this->table, $unique->fields, $unique->constraint_name);
		}
		
		foreach ($this->index_keys as $index) {
			if (!($index instanceof IndexKey)) throw new Exception("Index field destinations must be represented as IndexField objects");
			$this->database->create_index($this->table, $index->fields, $index->constraint_name);
		}
		
		foreach ($this->view_creation_order as $name) {
			$this->database->create_view(
					$this->table,
					$name,
					$this->views[$name]
			);
		}
	}

	/**
	 * Returns the SQL needed to create this table retrospectively in an existing database.
	 * 
	 * Note that foreign keys and associated structural elements (e.g. CREATE TYPE) are automatically created alongside.
	 * 
	 * @throws Exception
	 * @return array
	 */
	public function show_create_table_sql() {
		$foreign_keys = array();
		foreach ($this->foreign_keys as $link_field => $foreign) {
			if (!($foreign instanceof ForeignKey)) throw new Exception("Foreign key destinations must be represented as ForeignKey objects");
			$foreign_keys[] = $this->database->generate_create_foreign_key_sql($this->table, $foreign->dest_table->get_table(), $link_field, $foreign->dest_table->get_id_field(), $foreign->on_delete, $foreign->on_update, $foreign->constraint_name);
		}

		$unique_keys = array();
		foreach ($this->unique_keys as $unique) {
			if (!($unique instanceof UniqueKey)) throw new Exception("Unique field destinations must be represented as UniqueField objects");
			$unique_keys[] = $this->database->generate_create_unique_key_sql($this->table, $unique->fields, $unique->constraint_name);
		}

		$views = array();
		foreach ($this->view_creation_order as $name) {
			$views[] = "CREATE VIEW " . $this->view($this->table, $name) . " AS " . $this->views[$name];
		}
		
		return array(
			$this->database->generate_create_table_sql($this->table, $this->structure),
			$foreign_keys,
			$unique_keys,
			$views
		);
	}

	/**
	 * Drops the table in the database associated with this model.
	 * 
	 * Note that foreign keys and associated structural elements (e.g. CREATE TYPE) are automatically dropped alongside.
	 * 
	 * @return void
	 */
	public function drop_table() {
		// do this backwards to that databases like postgres that have referential integrety between views delete properly from the original creation order
		foreach (array_reverse($this->view_creation_order) as $name) {
			$this->database->drop_view($this->table, $name);
		}
		
		$this->database->drop_table($this->table, $this->structure);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Lists all data within the database for this model.
	 * 
	 * Roughly equivalent to SELECT * FROM <table>
	 * 
	 * @return array a non-associated array of results, themselves each associated arrays of field names to row values
	 */
	public function list_all() {
		return $this->database->execute_params("MODELS__{$this->table}__LIST_ALL");
	}

	/**
	 * Returns the number of rows in the database for this model.
	 * 
	 * @return int
	 */
	public function count_all() {
		return $this->database->query_params_value("
			SELECT COUNT(*) AS " . $this->tempfield("count") . "
			FROM " . $this->model($this) . "
		", null, "count", new Database\Type_Int(Database\Type_Int::UNSIGNED, Database\Type::NOT_NULL));
	}

	//---------------------------------------------------------------------

	/**
	 * Inserts the given values into the database for this model.
	 * 
	 * @param mixed[] $values an associated array values taking the format field name => value 
	 * @throws Exception
	 * @return bool
	 */
	public function insert(array $values) {
		$params = array();
		foreach ($values as $var => &$value) {
			if (!isset($this->structure[$var])) throw new Exception("Parameter variable is missing in the model structure", $var);
			$params[$var] = new Database\Param($value, $this->structure[$var]);
		}
		unset($value);

		$result = $this->database->insert_row($this->table, $params);
		
		return $result;
	}
}
