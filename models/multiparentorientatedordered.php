<?php
/**
 * Ordered second-class model support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/multiparentsecondclass.php";
require_once ABSTRACTION_ROOT_PATH . "models/orientatedordered.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/orderable.php";

use \Abstraction\Database as Database;

/**
 * @internal
 */
class MultiParentOrientatedOrdered_Exception extends MultiParentSecondClass_Exception {}

/**
 * Defines a root model for ordered multi-parent second-class structured models.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class MultiParentOrientatedOrdered extends MultiParentSecondClass implements Orderable {
	protected $ordered_field;
	
	/**
	 * Constructs a new instance of this 'orientated' ordered multi-parent second-class model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Database\Type[] $structure an associated array to define the model structure, taking the format field name => field type object
	 * @param FirstClass[] $parents an associated array of linkage field names and parent FirstClass model to link to
	 * @param string $ordered_field the name of the ordering field, if not "ordered" 
	 * @param string $id_field the name of the identifier field, if not "id"
	 * @param ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @param IndexKey[] $index_keys any non-unique index keys to apply to the model
	 * @throws MultiParentOrientatedOrdered_Exception
	 */
	public function __construct(
			Database\Wrapper $database,
			$table,
			array $structure,
			array $parents,
			$ordered_field = "ordered",
			$id_field = "id",
			array $foreign_keys = array(),
			array $unique_keys = array(),
			array $index_keys = array()
	) {
		if (array_key_exists($ordered_field, $structure)) throw new MultiParentOrientatedOrdered_Exception("The ordered field is implicit for orientatedordered models and should not be explicitly stated in the structure");
		$this->ordered_field = $ordered_field;
		$structure[$ordered_field] = new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL);
				
		parent::__construct(
				$database,
				$table,
				$structure,
				$parents,
				$id_field,
				$foreign_keys,
				$unique_keys,
				$index_keys
		);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns the field which is used for ordering in the model.
	 * 
	 * @return string the name of the ordered field
	 */
	public function get_ordered_field() {
		return $this->ordered_field;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		$fields = array();
		foreach (array_keys($this->structure) as $field) $fields[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $field);
		$fields = implode(",", $fields);

		$clauses = array();
		$params = array("id" => $this->structure[$this->id_field]);
		foreach ($this->firstclass as $firstclass_field => $firstclass) {
			$clauses[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $firstclass_field) . "=:firstclass_{$firstclass_field}";
			$params["firstclass_{$firstclass_field}"] = $this->structure[$firstclass_field];
		}
		
		$this->database->preprepare("MODELS_MULTIPARENTORIENTATEDORDERED__{$this->table}__LIST_ORDERED", "
			SELECT {$fields}
			FROM " . $this->modelview($this, "MULTIPARENTSECONDCLASS_JOIN") . "
			WHERE " . implode(" AND ", $clauses) . "
			ORDER BY " . $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $this->ordered_field) . " ASC
		", $params, $this->structure);

		$this->database->preprepare("MODELS_MULTIPARENTORIENTATEDORDERED__{$this->table}__MAX_ORDERED", "
			SELECT MAX(" . $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $this->ordered_field) . ") AS " . $this->tempfield("max") . "
			FROM " . $this->modelview($this, "MULTIPARENTSECONDCLASS_JOIN") . "
			WHERE " . implode(" AND ", $clauses) . "
		", $params, array(
			"max" => new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED)
		));

		$this->database->preprepare("MODELS_MULTIPARENTORIENTATEDORDERED__{$this->table}__CURRENT_ORDERED", "
			SELECT " . $this->modelfield($this, $this->ordered_field) . " AS " . $this->tempfield("ordered") . "
			FROM " . $this->model($this) . "
			WHERE " . $this->modelfield($this, $this->id_field) . "=:id
		", array(
			"id" => new Database\Type_SerialId()
		), array(
			"ordered" => new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL)
		));

		$params["ordered"] = new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL); 
		$this->database->preprepare("MODELS_MULTIPARENTORIENTATEDORDERED__{$this->table}__BY_ORDERED", "
			SELECT " . $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $this->id_field) . " AS " . $this->tempfield("id") . "
			FROM " . $this->modelview($this, "MULTIPARENTSECONDCLASS_JOIN") . "
			WHERE " . implode(" AND ", $clauses) . "
			AND " . $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $this->ordered_field) . "=:ordered
		", $params, array(
			"id" => new Database\Type_SerialId()
		));
	}

	//-------------------------------------------------------------------------

	/**
	 * Lists all rows within this model's table that are linked to the specified FirstClass model row, ordered by the ordering field.
	 * 
	 * @param mixed[] $firstclass_objects an associated array of existing rows in the parents.
	 * @return array a non-associated array of results, themselves each associated arrays of field names to row values
	 */
	function list_ordered_by_firstclass_parents(array $firstclass_objects) {
		$params = array();
		foreach ($firstclass_objects as $firstclass_field => $firstclass_object) {
			self::assert_id_object($firstclass_object, $this->firstclass[$firstclass_field]->get_id_field());
			$params["firstclass_{$firstclass_field}"] = $firstclass_object[$this->firstclass[$firstclass_field]->get_id_field()];
		}
		
		return $this->database->execute_params("MODELS_MULTIPARENTORIENTATEDORDERED__{$this->table}__LIST_ORDERED", $params);
	}

	//---------------------------------------------------------------------

	/**
	 * Moves a model row in the specified direction within the order, restricted to the ordering of the specified parent FirstClass model row.
	 * 
	 * Note, the FirstClass parent model row is derived automatically from the existing model row rather than explicitly specified.
	 * Transaction support is enabled, so failed movement will roll back.
	 * 
	 * @param mixed[] $object an existing model row
	 * @param string $direction one of the direction values: MOVE_UP, MOVE_DOWN, MOVE_TOP or MOVE_BOTTOM
	 * @throws Database\Exception
	 * @throws Ordered_Exception
	 * @return mixed[] the moved model row, including its new ordering value
	 */
	function move(array $object, $direction) {
		self::assert_id_object($object, $this->id_field);
		
		if (!in_array($direction, array(OrientatedOrdered::MOVE_UP, OrientatedOrdered::MOVE_DOWN, OrientatedOrdered::MOVE_TOP, OrientatedOrdered::MOVE_BOTTOM), true)) throw new MultiParentOrientatedOrdered_Exception("Supplied direction is not valid: {$direction}");
				
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		$current = $this->database->execute_params_value("MODELS_MULTIPARENTORIENTATEDORDERED__{$this->table}__CURRENT_ORDERED", array("id" => $object[$this->id_field]));
	
		$params = array();
		foreach (array_keys($this->firstclass) as $field) {
			self::assert_id($object[$field]);
			$params["firstclass_{$field}"] = $object[$field];
		}
		$current_max = $this->database->execute_params_value("MODELS_MULTIPARENTORIENTATEDORDERED__{$this->table}__MAX_ORDERED", $params);

		switch($direction) {
			case OrientatedOrdered::MOVE_UP:
				if ($current === 0) {
					$this->database->transaction_rollback();
					throw new MultiParentOrientatedOrdered_Exception("Cannot move above the first ordered row");
				}

				$params = array();
				foreach (array_keys($this->firstclass) as $field) {
					self::assert_id($object[$field]);
					$params["firstclass_{$field}"] = $object[$field];
				}
				
				$params["ordered"] = $current - 1;
				$prev = $this->database->execute_params_value("MODELS_MULTIPARENTORIENTATEDORDERED__{$this->table}__BY_ORDERED", $params);

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($prev, new Database\Type_SerialId())
				), true);

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current - 1, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId())
				), true);

				break;
			case OrientatedOrdered::MOVE_DOWN:
				if ($current === $current_max) {
					$this->database->transaction_rollback();
					throw new MultiParentOrientatedOrdered_Exception("Cannot move below the last ordered row");
				}

				$params = array();
				foreach (array_keys($this->firstclass) as $field) {
					self::assert_id($object[$field]);
					$params["firstclass_{$field}"] = $object[$field];
				}
				
				$params["ordered"] = $current + 1;
				$prev = $this->database->execute_params_value("MODELS_MULTIPARENTORIENTATEDORDERED__{$this->table}__BY_ORDERED", $params);
				
				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($prev, new Database\Type_SerialId())
				), true);

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current + 1, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId())
				), true);

				break;
			case OrientatedOrdered::MOVE_TOP:
				if ($current === 0) break;	// if already topmost

				$clauses = array();
				$params = array();
				foreach ($this->firstclass as $firstclass_field => $firstclass) {
					$clauses[] = $this->modelfield($this, $firstclass_field) . "=:firstclass_{$firstclass_field}";
					$params["firstclass_{$firstclass_field}"] = new Database\Param($object[$firstclass_field], new Database\Type_Id(Database\Type_Id::NOT_NULL));
				}
				$params["threshold"] = new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL));
				
				$this->database->query_params("
					UPDATE " . $this->model($this) . "
					SET " . $this->modelfield($this, $this->ordered_field) . "=" . $this->modelfield($this, $this->ordered_field) . "+1
					WHERE " . implode(" AND ", $clauses) . "
					AND " . $this->modelfield($this, $this->ordered_field) . "<:threshold
				", $params);

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param(0, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId())
				), true);

				break;
			case OrientatedOrdered::MOVE_BOTTOM:
				if ($current === $current_max) break;	// if already last
				
				$clauses = array();
				$params = array();
				foreach ($this->firstclass as $firstclass_field => $firstclass) {
					$clauses[] = $this->modelfield($this, $firstclass_field) . "=:firstclass_{$firstclass_field}";
					$params["firstclass_{$firstclass_field}"] = new Database\Param($object[$firstclass_field], new Database\Type_Id(Database\Type_Id::NOT_NULL));
				}
				$params["threshold"] = new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL));
				
				$this->database->query_params("
					UPDATE " . $this->model($this) . "
					SET " . $this->modelfield($this, $this->ordered_field) . "=" . $this->modelfield($this, $this->ordered_field) . "-1
					WHERE " . implode(" AND ", $clauses) . "
					AND " . $this->modelfield($this, $this->ordered_field) . ">:threshold
				", $params);
				
				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current_max, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId()),
				), true);

				break;
		}
		
		$object = $this->_internal_firstclass_get_by_id($object[$this->id_field]);
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return $object;
	}
	
	/**
	 * Jumps a model row to a different parent firstclass set, automatically cleaning up its previous space in the order.
	 * 
	 * This is essentially the same as a delete/re-insert, but the ID number won't change so any existing remote foreign keys will persist.
	 * Note, any unique key constraints etc. can't be checked, so will fail. Subclasses need to police this.
	 * Transaction support is enabled, so failed jumps will roll back.
	 *
	 * @param mixed[] $object an existing model row
	 * @param mixed[] $destination_objects the new firstclass parent models row to jump to
	 * @throws Database\Exception
	 * @throws Ordered_Exception
	 * @return mixed[] the jumped model row, including its new ordering value and updated firstclass fields.
	 */
	function jump(array $object, array $destination_objects) {
		self::assert_id_object($object, $this->id_field);

		$params = array();
		foreach ($destination_objects as $firstclass_field => $firstclass_object) {
			self::assert_id_object($firstclass_object, $this->firstclass[$firstclass_field]->get_id_field());
			$params["firstclass_{$firstclass_field}"] = $firstclass_object[$this->firstclass[$firstclass_field]->get_id_field()];
		}
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$this->move($object, "bottom");
		
		$current_max = $this->database->execute_params_value("MODELS_MULTIPARENTORIENTATEDORDERED__{$this->table}__MAX_ORDERED", $params, true);
		if ($current_max === null) $current_max = 0; 
		else $current_max++;

		foreach ($destination_objects as $firstclass_field => $firstclass_object) {
			$object[$firstclass_field] = $firstclass_object[$this->firstclass[$firstclass_field]->get_id_field()];
		}
		$object[$this->ordered_field] = $current_max;
		$this->update($object);	// can't use update_for_firstclass as we are changing the firstclass!
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return $object;
	}
	
	//---------------------------------------------------------------------

	/**
	 * Inserts the given values into the database for this model, associating it with the given parent FirstClass model row
	 * 
	 * Note, the identifier and ordered values are generated automatically and should not be specified explicitly.
	 * Rows are always inserted at the end of the ordering, incrementing the last value.
	 * Transaction support is enabled, so failed row insertion will roll back.
	 * 
	 * @param mixed[] $firstclass_objects an associative array of existing rows in the parent FirstClass models.
	 * @param mixed[] $values an associated array values taking the format field name => value
	 * @throws MultiParentOrientatedOrdered_Exception
	 * @return mixed[] the newly created model row, including its new identifier value
	 */
	function insert_for_firstclass_parents(array $firstclass_objects, array $values) {
		$params = array();
		foreach ($firstclass_objects as $firstclass_field => $firstclass_object) {
			self::assert_id_object($firstclass_object, $this->firstclass[$firstclass_field]->get_id_field());
			$params["firstclass_{$firstclass_field}"] = $firstclass_object[$this->firstclass[$firstclass_field]->get_id_field()];
		}
		
		if (array_key_exists($this->ordered_field, $values)) throw new MultiParentOrientatedOrdered_Exception("Ordering fields should not be specified explicitly for orientated-ordered models");

		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$current_max = $this->database->execute_params_value("MODELS_MULTIPARENTORIENTATEDORDERED__{$this->table}__MAX_ORDERED", $params, true);
		if ($current_max === null) $current_max = 0; 
		else $current_max++;
		
		$values[$this->ordered_field] = $current_max;
		
		$object = parent::insert_for_firstclass_parents($firstclass_objects, $values);
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return $object;
	}

	/**
	 * Deletes a row from the model's table using its identifier value, validated against the FirstClass parent models views.
	 * 
	 * Row ordering values are reindexed automatically.
	 * Transaction support is enabled, so failed row deletion or reindexing will roll back.
	 * 
	 * @param mixed[] $firstclass_objects an associated array of existing rows in the parent FirstClass models.
	 * @param mixed[] $object an existing model row
	 * @throws MultiParentOrientatedOrdered_Exception
	 * @throws SecondClass_Exception
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function delete_for_firstclass_parents(array $firstclass_objects, array $object) {
		self::assert_id_object($object, $this->id_field);

		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$this->move($object, "bottom");
		parent::delete_for_firstclass_parents($firstclass_objects, $object);
	
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}
}
