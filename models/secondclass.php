<?php
/**
 * Second-class model support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/firstclass.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

use \Abstraction\Database as Database;

/**
 * @internal
 */
class SecondClass_Exception extends FirstClass_Exception {}

/**
 * Defines a root model for second-class structured models.
 * 
 * Second-class models inherit their definition from FirstClass models, but also have a parent FirstClass (or indeed SecondClass, Ordered, OrientatedOrdered etc.) model which they are linked to via a foreign key. This allows them to be chained together off a single parent FirstClass.
 * Chained second-class models are useful, as when the parent node is deleted, all the second-class model rows cascade delete too. This ensures referential integrity and avoids orphaned data in the table.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class SecondClass extends FirstClass {
	protected $id_field;
	protected $firstclass;
	protected $firstclass_field;

	/**
	 * Constructs a new instance of this second-class model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Database\Type[] $structure an associated array to define the model structure, taking the format field name => field type object
	 * @param FirstClass $firstclass the parent FirstClass model to link to
	 * @param string $firstclass_field the name of the linkage field to create the foreign key upon
	 * @param string $id_field the name of the identifier field, if not "id"
	 * @param ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @param IndexKey[] $index_keys any non-unique index keys to apply to the model
	 * @throws SecondClass_Exception
	 */
	public function __construct(
			Database\Wrapper $database,
			$table,
			array $structure,
			FirstClass $firstclass,
			$firstclass_field,
			$id_field = "id",
			array $foreign_keys = array(),
			array $unique_keys = array(),
			array $index_keys = array()
	) {
		if (array_key_exists($firstclass_field, $structure)) throw new SecondClass_Exception("The firstclass field is implicit for second-class models and should not be explicitly stated in the structure");
		
		$structure[$firstclass_field] = new Database\Type_Id(Database\Type::NOT_NULL);

		if (array_key_exists($firstclass_field, $foreign_keys)) throw new SecondClass_Exception("Do not specify the foreign key relationship directly for second-class models");
		$foreign_keys[$firstclass_field] = new ForeignKey($firstclass, "CASCADE", "CASCADE", $table . "__fk_2nd");
		
		$this->firstclass = $firstclass;
		$this->firstclass_field = $firstclass_field;
		
		parent::__construct(
				$database,
				$table,
				$structure,
				$id_field,
				$foreign_keys,
				$unique_keys,
				$index_keys
		);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * The SECONDCLASS_JOIN view defined here is used extensively as a shorthand way of guaranteeing referential integrity during further SELECT queries.
	 * 
	 * @internal
	 */
	protected function define_views() {
		parent::define_views();
		
		$this->define_view("SECONDCLASS_JOIN", "
			SELECT {$this->fields},
				" . $this->modelfield($this->firstclass, $this->firstclass->get_id_field()) . " AS " . $this->tempfield("firstclass_id") . "
			FROM " . $this->model($this) . "
			INNER JOIN " . $this->model($this->firstclass) . "
				ON " . $this->modelfield($this, $this->firstclass_field) . "=" . $this->modelfield($this->firstclass, $this->firstclass->get_id_field()) . "
		");
	}

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		$fields = array();
		foreach (array_keys($this->structure) as $field) $fields[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field);
		$fields = implode(",", $fields);
		
		$this->database->preprepare("MODELS_SECONDCLASS__{$this->table}__GET_BY_FIRSTCLASS_AND_ID", "
			SELECT {$fields}
			FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "SECONDCLASS_JOIN", $this->id_field) . "=:id
			AND " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=:firstclass
		", array(
			"id" => $this->structure[$this->id_field],
			"firstclass" => $this->structure[$this->firstclass_field]
		), $this->structure);

		$this->database->preprepare("MODELS_SECONDCLASS__{$this->table}__LIST_BY_FIRSTCLASS", "
			SELECT {$fields}
			FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=:firstclass
		", array(
			"firstclass" => $this->structure[$this->firstclass_field]
		), $this->structure);

		$this->database->preprepare("MODELS_SECONDCLASS__{$this->table}__LIST_ALL_IDS_BY_FIRSTCLASS", "
			SELECT " . $this->modelviewfield($this, "SECONDCLASS_JOIN", $this->id_field) . "
			FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=:firstclass
		", array(
			"firstclass" => $this->structure[$this->firstclass_field]
		), array($this->id_field => $this->structure[$this->id_field]));
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns the parent FirstClass model associated with this SecondClass model
	 * 
	 * @return FirstClass
	 */
	public function get_firstclass() {
		return $this->firstclass;
	}
	
	/**
	 * Returns the field name linked to the parent FirstClass model.
	 * 
	 * @return string
	 */
	public function get_firstclass_field() {
		return $this->firstclass_field;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Lists all rows within this model's table that are linked to the specified FirstClass model row.
	 * 
	 * @param mixed[] $firstclass_object an existing row in the parent FirstClass model.
	 * @return array a non-associated array of results, themselves each associated arrays of field names to row values
	 */
	public function list_by_firstclass(array $firstclass_object) {
		self::assert_id_object($firstclass_object, $this->firstclass->get_id_field());
				
		return $this->database->execute_params("MODELS_SECONDCLASS__{$this->table}__LIST_BY_FIRSTCLASS", array(
			"firstclass" => $firstclass_object[$this->firstclass->get_id_field()]
		));
	}

	/**
	 * Overrides the get_by_id method in FirstClass to always throw an exception.
	 * 
	 * SecondClass models should use the get_by_firstclass_and_id method instead.
	 * This is largely because overridden methods cannot have additional parameters not specified in the parent, so get_by_id($firstclass, $id) is not valid.
	 *
	 * @internal
	 * @param int $id not applicable
	 * @throws SecondClass_Exception
	 * @return void
	 */
	public function get_by_id($id) {
		throw new SecondClass_Exception("Do not call get_by_id directly, use get_by_firstclass_and_id instead");
	}

	/**
	 * Needed for allowing subsequent models to override the get_by_firstclass_and_id method yet still call it themselves.
	 * 
	 * @internal
	 * @param mixed[] $firstclass_object an existing row in the parent FirstClass model.
	 * @param int $id the model row identifier
	 */
	protected function _internal_secondclass_get_by_firstclass_and_id(array $firstclass_object, $id) {
		self::assert_id_object($firstclass_object, $this->firstclass->get_id_field());

		return $this->database->execute_params_single("MODELS_SECONDCLASS__{$this->table}__GET_BY_FIRSTCLASS_AND_ID", array(
			"id" => $id,
			"firstclass" => $firstclass_object[$this->firstclass->get_id_field()]
		));
	}

	/**
	 * Returns the model row corresponding to the given identifier, validated against the FirstClass parent model view.
	 * 
	 * This method prevents rows not explicitly linked to the specified parent model row from being fetched.
	 *  
	 * @param mixed[] $firstclass_object an existing row in the parent FirstClass model.
	 * @param int $id the model row identifier
	 * @return mixed[]|null the corresponding row or null if none match
	 */
	public function get_by_firstclass_and_id(array $firstclass_object, $id) {
		self::assert_id($id);
		
		return $this->_internal_secondclass_get_by_firstclass_and_id($firstclass_object, $id);
	}

	/**
	 * Overrides the bulkget_by_ids method in FirstClass to always throw an exception.
	 * 
	 * SecondClass models should use the bulkget_by_firstclass_and_ids method instead.
	 * This is largely because overridden methods cannot have additional parameters not specified in the parent, so bulkget_by_ids($firstclass, $ids) is not valid.
	 *
	 * @internal
	 * @param int[] $ids not applicable
	 * @param string[] $orderby not applicable
	 * @throws SecondClass_Exception
	 * @return void
	 */
	public function bulkget_by_ids(array $ids, array $orderby = null) {
		throw new SecondClass_Exception("Do not call bulkget_by_ids directly, use bulkget_by_firstclass_and_ids instead");
	}

	/**
	 * Returns an array of model rows for the given identifiers, validated against the FirstClass parent model view.
	 * 
	 * This method prevents rows not explicitly linked to the specified parent model row from being fetched.
	 *  
	 * @param mixed[] $firstclass_object an existing row in the parent FirstClass model.
	 * @param int[] $ids the mode row identifier values as an array
	 * @param string[] $orderby an optional associative array of field names => direction
	 * @return array
	 */
	public function bulkget_by_firstclass_and_ids(array $firstclass_object, array $ids, array $orderby = null) {
		if (sizeof($ids) == 0) return array();
		if (sizeof($ids) > 255) {
			$slice = array_slice($ids, 0, 255);
			$remainder = array_slice($ids, 255);
			
			return array_merge($this->bulkget_by_firstclass_and_ids($firstclass_object, $slice), $this->bulkget_by_firstclass_and_ids($firstclass_object, $remainder));
		}

		self::assert_id_object($firstclass_object, $this->firstclass->get_id_field());
		foreach ($ids as $id) self::assert_id($id);
		$ids = implode(",", $ids);
		
		$orderbyclauses = array();
		if (is_array($orderby)) foreach ($orderby as $field => $direction) $orderbyclauses[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field) . " {$direction}";
		$orderbyclauses = sizeof($orderbyclauses) === 0 ? "" : ("ORDER BY " . implode(",", $orderbyclauses));
		
		$fields = array();
		foreach (array_keys($this->structure) as $field) $fields[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field);
		$fields = implode(",", $fields);
		
		return $this->database->query_params("
			SELECT {$fields}
			FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "SECONDCLASS_JOIN", $this->id_field) . " IN ({$ids})
			AND " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=:firstclass
			{$orderbyclauses}
		", array(
			"firstclass" => new Database\Param($firstclass_object[$this->firstclass->get_id_field()], $this->structure[$this->firstclass_field])
		), $this->structure);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Overrides the corresponding method in FirstClass to always throw an exception.
	 * 
	 * SecondClass models should use the search_ids_for_..._fields_by_firstclass method instead.
	 *
	 * @internal
	 * @param string $value not applicable
	 * @param string[] $fields not applicable
	 * @throws SecondClass_Exception
	 * @return void
	 */
	public function search_ids_for_encrypted_fields($value, array $fields) {
		throw new SecondClass_Exception("Do not call search_ids_for_encrypted_fields directly, use search_ids_for_encrypted_fields_by_firstclass instead");
	}

	/**
	 * Overrides the corresponding method in FirstClass to always throw an exception.
	 * 
	 * SecondClass models should use the search_ids_for_..._fields_by_firstclass method instead.
	 *
	 * @internal
	 * @param string $value not applicable
	 * @param string[] $fields not applicable
	 * @throws SecondClass_Exception
	 * @return void
	 */
	public function search_ids_for_nonencrypted_fields($value, array $fields) {
		throw new SecondClass_Exception("Do not call search_ids_for_encrypted_fields directly, use search_ids_for_encrypted_fields_by_firstclass instead");
	}
	
	/**
	 * Overrides the corresponding method in FirstClass to always throw an exception.
	 * 
	 * SecondClass models should use the search_ids_for_..._fields_by_firstclass method instead.
	 *
	 * @internal
	 * @param string $value not applicable
	 * @param string[] $fields not applicable
	 * @param string[] $orderby not applicable
	 * @throws SecondClass_Exception
	 * @return void
	 */
	public function search_fields($value, array $fields, array $orderby = null) {
		throw new SecondClass_Exception("Do not call search directly, use search_by_firstclass instead");
	}
	
	/**
	 * Trawls the table for matching values in the specified non-encrypted fields, validated against the FirstClass parent model view.
	 * 
	 * Searching is performed using relatively efficient LIKE conditions.
	 * Note, this method is primarily used as part of search_fields_by_firstclass and is rarely called by itself.
	 *  
	 * @param mixed[] $firstclass an existing row in the parent FirstClass model.
	 * @param string $value the value to search for
	 * @param string[] $fields an array of model fields to search
	 * @throws SecondClass_Exception
	 * @return int[] the model identifier values for any matching rows
	 */
	public function search_ids_for_nonencrypted_fields_by_firstclass(array $firstclass, $value, array $fields) {
		if (sizeof($fields) == 0) return array();
	
		$conditions = array();
		foreach ($fields as $field) {
			if (!isset($this->structure[$field])) throw new SecondClass_Exception("Unknown field to perform encrypted search on", $field);
			if ($this->structure[$field] instanceof Database\Type_Encrypted) throw new SecondClass_Exception("Attempting to search on an encrypted field", $field);
			if (!($this->structure[$field] instanceof Database\Type_String || $this->structure[$field] instanceof Database\Type_Text)) throw new SecondClass_Exception("Attempting to search on a non-string/text field", $field);
			
			$conditions[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field) . " LIKE :valuea";
			$conditions[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field) . " LIKE :valueb";
			$conditions[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field) . " LIKE :valuec";
			$conditions[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field) . " LIKE :valued";
		}
	
		return $this->database->query_params_list("
			SELECT " . $this->id_field . "
			FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
			WHERE (" . implode(" OR ", $conditions) . ")
			AND " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=:firstclass
		", array(
			"valuea" => new Database\Param($value, new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valueb" => new Database\Param("{$value}%", new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valuec" => new Database\Param("%{$value}", new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valued" => new Database\Param("%{$value}%", new Database\Type_String(255, Database\Type::NOT_NULL)),
			"firstclass" => new Database\Param($firstclass[$this->firstclass->get_id_field()], $this->structure[$this->firstclass_field])
		), $this->id_field, $this->structure[$this->id_field]);
	}
	
	/**
	 * Trawls the table for matching values in the specified encrypted fields, validated against the FirstClass parent model view.
	 * 
	 * Searching is performed in a relatively inefficient manner by querying all rows in the table, decrypting the fields, and then comparing programmatically.
	 * Note, this method is primarily used as part of search_fields_by_firstclass and is rarely called by itself.
	 *  
	 * @param mixed[] $firstclass an existing row in the parent FirstClass model.
	 * @param string $value the value to search for
	 * @param string[] $fields an array of model fields to search
	 * @throws SecondClass_Exception
	 * @return int[] the model identifier values for any matching rows
	 */
	public function search_ids_for_encrypted_fields_by_firstclass(array $firstclass, $value, array $fields) {
		self::assert_id_object($firstclass, $this->firstclass->get_id_field());
		
		if (sizeof($fields) == 0) return array();
		
		$results = array();
		$fieldnames = array();
		foreach ($fields as $field) {
			if (!isset($this->structure[$field])) throw new SecondClass_Exception("Unknown field to perform encrypted search on", $field);
			if (!($this->structure[$field] instanceof Database\Type_Encrypted)) throw new SecondClass_Exception("Attempting to search on a non-encrypted field", $field);
			
			$results[$field] = $this->structure[$field];
			$fieldnames[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field);
		}
		$fieldnames = implode(",", $fieldnames);
		
		$results[$this->id_field] = $this->structure[$this->id_field];
		
		$all_ids = $this->database->execute_params_list("MODELS_SECONDCLASS__{$this->table}__LIST_ALL_IDS_BY_FIRSTCLASS", array(
			"firstclass" => $firstclass[$this->firstclass->get_id_field()]
		));
		if (sizeof($all_ids) == 0) return array();

		$chunks = array_chunk($all_ids, 500);
		$matches = array();
		foreach ($chunks as $ids) {
			if (sizeof($ids) == 0) continue;
			
			foreach ($this->database->query_params("
				SELECT {$fieldnames},
					" . $this->modelviewfield($this, "SECONDCLASS_JOIN", $this->id_field) . "
				FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
				WHERE " . $this->modelviewfield($this, "SECONDCLASS_JOIN", $this->id_field) . " IN (" . implode(",", $ids) . ")
				AND " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=:firstclass
			", array(
				"firstclass" => new Database\Param($firstclass[$this->firstclass->get_id_field()], $this->structure[$this->firstclass_field])
			), $results) as $row) {
				foreach ($fields as $field) {
					if ($row[$field] === null) continue;
					if (strpos(strtolower($row[$field]), strtolower($value)) === false) continue;
					$matches[$row[$this->id_field]] = $row[$this->id_field];
				}
			}
		}
		
		return array_values($matches);
	}
	
	/**
	 * Trawls the model's table for matching values within the specified fields, encrypted or non-encrypted, validated against the FirstClass parent model view.
	 *
	 * Note, encrypted searches are only performed if one or more of the fields are actually of the type Encrypted.
	 * 
	 * @param mixed[] $firstclass an existing row in the parent FirstClass model.
	 * @param string $value the value to search for
	 * @param string[] $fields an array of model fields to search
	 * @param string[] $orderby an optional associative array of field names => direction, not null for none
	 * @throws SecondClass_Exception
	 * @return array a non-associated array of results, themselves each associated arrays of field names to row values
	 */
	public function search_fields_by_firstclass(array $firstclass, $value, array $fields, array $orderby = null) {
		$encrypted = array(); 
		$regular = array();
		foreach ($fields as $field) {
			if (!isset($this->structure[$field])) throw new SecondClass_Exception("Unknown field to perform encrypted search on", $field);
			if ($this->structure[$field] instanceof Database\Type_Encrypted) $encrypted[] = $field;
			else $regular[] = $field;
		}
		
		return $this->bulkget_by_firstclass_and_ids($firstclass, array_unique(array_merge(
			$this->search_ids_for_encrypted_fields_by_firstclass($firstclass, $value, $encrypted),
			$this->search_ids_for_nonencrypted_fields_by_firstclass($firstclass, $value, $regular)
		)), $orderby);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Overrides the corresponding method in FirstClass to always throw an exception.
	 * 
	 * SecondClass models should use the insert_for_firstclass method instead.
	 *
	 * @internal
	 * @param string[] $values not applicable
	 * @throws SecondClass_Exception
	 * @return void
	 */
	public function insert(array $values) {
		throw new SecondClass_Exception("Do not call insert directly, use insert_for_firstclass instead");
	}

	/**
	 * Inserts the given values into the database for this model, associating it with the given parent FirstClass model row
	 * 
	 * Note, the identifier value is generated automatically and should not be specified explicitly.
	 * Transaction support is enabled, so failed row insertion will roll back.
	 * 
	 * @param mixed[] $firstclass_object an existing row in the parent FirstClass model.
	 * @param mixed[] $values an associated array values taking the format field name => value
	 * @throws SecondClass_Exception
	 * @return mixed[] the newly created model row, including its new identifier value
	 */
	public function insert_for_firstclass(array $firstclass_object, array $values) {
		self::assert_id_object($firstclass_object, $this->firstclass->get_id_field());
		
		if (array_key_exists($this->firstclass_field, $values)) throw new SecondClass_Exception("Firstclass fields should not be specified explicitly for second-class models");

		$values[$this->firstclass_field] = $firstclass_object[$this->firstclass->get_id_field()];
		
		return parent::insert($values);
	}

	/**
	 * Overrides the corresponding method in FirstClass to always throw an exception.
	 * 
	 * SecondClass models should use the delete_for_firstclass method instead.
	 *
	 * @internal
	 * @param mixed[] $object not applicable
	 * @throws SecondClass_Exception
	 * @return void
	 */
	public function delete(array $object) {
		throw new SecondClass_Exception("Do not call delete directly, use delete_for_firstclass instead");
	}
	
	/**
	 * Deletes a row from the model's table using its identifier value, validated against the FirstClass parent model view.
	 * 
	 * @param array $firstclass_object
	 * @param mixed[] $object an existing model row
	 * @throws SecondClass_Exception
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function delete_for_firstclass(array $firstclass_object, array $object) {
		self::assert_id_object($firstclass_object, $this->firstclass->get_id_field());
		self::assert_id_object($object, $this->id_field);
		
		if ($firstclass_object[$this->firstclass->get_id_field()] !== $object[$this->firstclass_field]) throw new SecondClass_Exception("Firstclass orientator mismatch in the secondclass model data");

		if (!$this->database->delete_rows_by_conditions($this->table, array(
			$this->id_field => new Database\Param($object[$this->id_field], $this->structure[$this->id_field]),
			$this->firstclass_field => new Database\Param($firstclass_object[$this->firstclass->get_id_field()], $this->structure[$this->firstclass_field])
		), true)) throw new FirstClass_Exception("Unable to delete second class object from table", $this->table);

		return true;
	}

	/**
	 * Wipes all rows from the model's table that are dependent on the specified FirstClass parent model row.
	 * 
	 * @param array $firstclass_object
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function delete_all_for_firstclass(array $firstclass_object) {
		self::assert_id_object($firstclass_object, $this->firstclass->get_id_field());
		
		if (!$this->database->delete_rows_by_conditions($this->table, array(
			$this->firstclass_field => new Database\Param($firstclass_object[$this->firstclass->get_id_field()], $this->structure[$this->firstclass_field])
		), false)) throw new FirstClass_Exception("Unable to delete all second class objects from table", $this->table);

		return true;
	}

	/**
	 * Updates a row in the model's table with new data using its identifier value, validated against the FirstClass parent model.
	 * 
	 * Any field apart from the identifier field and parent link field can be populated with new data.
	 * 
	 * @param array $firstclass_object
	 * @param mixed[] $object an existing model row
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function update_for_firstclass(array $firstclass_object, array $object) {
		self::assert_id_object($firstclass_object, $this->firstclass->get_id_field());
		self::assert_id_object($object);

		if ($firstclass_object[$this->firstclass->get_id_field()] !== $object[$this->firstclass_field]) throw new SecondClass_Exception("Firstclass orientator mismatch in the secondclass model data");
		
		return $this->update($object);
	}
}
