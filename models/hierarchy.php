<?php
/**
 * Hierarchy model support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/secondclass.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/orderable.php";

use \Abstraction\Database as Database;

/**
 * @internal
 */
class Hierarchy_Exception extends SecondClass_Exception {}

/**
 * Defines a root model for hierarchy structured models.
 *
 * Hierarchies are second-class models which form a tree structure, that is, child nodes may be dependent on other hierarchy nodes rather than just the parent FirstClass.
 * Hierarchy nodes can be ordered against their parent node, and moved not only vertically but also horizontally across the node structure, allowing entire branches to be reorganised.
 * Because of their complex nature, they should not be used for vast data storage, and limited to only a few thousand rows as an absolute max within each FirstClass model parent.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Hierarchy extends SecondClass implements Orderable {
	protected $ordered_field;
	protected $parent_field;

	/**
	 * Constructs a new instance of this 'orientated' ordered second-class model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Database\Type[] $structure an associated array to define the model structure, taking the format field name => field type object
	 * @param FirstClass $firstclass the parent FirstClass model to link to
	 * @param string $firstclass_field the name of the linkage field to create the foreign key upon
	 * @param string $ordered_field the name of the ordering field, if not "ordered" 
	 * @param string $parent_field the name of the parent node field, if not "parent"
	 * @param string $id_field the name of the identifier field, if not "id"
	 * @param ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @param IndexKey[] $index_keys any non-unique index keys to apply to the model
	 * @throws Hierarchy_Exception
	 * @throws SecondClass_Exception
	 */
	public function __construct(
			Database\Wrapper $database,
			$table,
			array $structure,
			FirstClass $firstclass,
			$firstclass_field,
			$ordered_field = "ordered",
			$parent_field = "parent",
			$id_field = "id",
			array $foreign_keys = array(),
			array $unique_keys = array(),
			array $index_keys = array()
	) {
		if (array_key_exists($ordered_field, $structure)) throw new Hierarchy_Exception("The ordered field is implicit for hierarchy models and should not be explicitly stated in the structure");
		$this->ordered_field = $ordered_field;
		$structure[$ordered_field] = new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL);
		
		if (array_key_exists($parent_field, $structure)) throw new Hierarchy_Exception("The parent field is implicit for hierarchy models and should not be explicitly stated in the structure");
		$this->parent_field = $parent_field;
		$structure[$parent_field] = new Database\Type_Id(Database\Type::ALLOW_NULL);
		
		if (array_key_exists($parent_field, $foreign_keys)) throw new SecondClass_Exception("Do not specify the foreign key relationship directly for hierarchy models");
		$foreign_keys[$parent_field] = new ForeignKey($this, "CASCADE", "CASCADE", $table . "__fk_parent");
		
		parent::__construct(
				$database,
				$table,
				$structure,
				$firstclass,
				$firstclass_field,
				$id_field,
				$foreign_keys,
				$unique_keys,
				$index_keys
		);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns the field which is used for ordering in the model.
	 * 
	 * @return string the name of the ordered field
	 */
	public function get_ordered_field() {
		return $this->ordered_field;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function define_views() {
		parent::define_views();
		
		$fields = array();
		foreach (array_keys($this->structure) as $field) $fields[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field);
		$fields = implode(",", $fields);
		
		$this->define_view("HIERARCHY_JOIN", "
			SELECT {$fields},
				" . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . ",
				" . $this->derivedfield("parent", $this->firstclass->get_id_field()) . " AS " . $this->tempfield("parent_id") . "
			FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
			LEFT JOIN " . $this->model($this) . " AS " . $this->derived("parent") . "
				ON " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "parent") . "=" . $this->derivedfield("parent", $this->id_field) . "
		");
	}
	
	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		$fields = array();
		foreach (array_keys($this->structure) as $field) $fields[] = $this->modelviewfield($this, "HIERARCHY_JOIN", $field);
		$fields = implode(",", $fields);
		
		$this->database->preprepare("MODELS_HIERARCHY__{$this->table}__GET_ROOT", "
			SELECT {$fields}
			FROM " . $this->modelview($this, "HIERARCHY_JOIN") . "
			WHERE " . $this->modelviewfield($this, "HIERARCHY_JOIN", "firstclass_id") . "=:firstclass
			AND " . $this->modelviewfield($this, "HIERARCHY_JOIN", "parent_id") . " IS NULL
			LIMIT 1
		", array(
			"firstclass" => new Database\Type_Id(Database\Type_Id::NOT_NULL)
		), $this->structure);
		
		$this->database->preprepare("MODELS_HIERARCHY__{$this->table}__LIST_CHILDREN_ORDERED", "
			SELECT {$fields}
			FROM " . $this->modelview($this, "HIERARCHY_JOIN") . "
			WHERE " . $this->modelviewfield($this, "HIERARCHY_JOIN", "firstclass_id") . "=:firstclass
			AND " . $this->modelviewfield($this, "HIERARCHY_JOIN", "parent_id") . "=:parent
			ORDER BY " . $this->modelviewfield($this, "HIERARCHY_JOIN", $this->ordered_field) . " ASC
		", array(
			"firstclass" => new Database\Type_Id(Database\Type_Id::NOT_NULL),
			"parent" => new Database\Type_Id(Database\Type_Id::NOT_NULL)
		), $this->structure);

		$this->database->preprepare("MODELS_HIERARCHY__{$this->table}__MAX_ORDERED", "
			SELECT MAX(" . $this->modelviewfield($this, "HIERARCHY_JOIN", $this->ordered_field) . ") AS " . $this->tempfield("max") . "
			FROM " . $this->modelview($this, "HIERARCHY_JOIN") . "
			WHERE " . $this->modelviewfield($this, "HIERARCHY_JOIN", "firstclass_id") . "=:firstclass
			AND " . $this->modelviewfield($this, "HIERARCHY_JOIN", "parent_id") . "=:parent
		", array(
			"firstclass" => new Database\Type_Id(Database\Type_Id::NOT_NULL),
			"parent" => new Database\Type_Id(Database\Type_Id::NOT_NULL)
		), array(
			"max" => new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED)
		));

		$this->database->preprepare("MODELS_HIERARCHY__{$this->table}__CURRENT_ORDERED", "
			SELECT " . $this->modelfield($this, $this->ordered_field) . " AS " . $this->tempfield("ordered") . "
			FROM " . $this->model($this) . "
			WHERE " . $this->modelfield($this, $this->id_field) . "=:id
		", array(
			"id" => new Database\Type_SerialId()
		), array(
			"ordered" => new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL)
		));

		$this->database->preprepare("MODELS_HIERARCHY__{$this->table}__BY_ORDERED", "
			SELECT " . $this->modelviewfield($this, "HIERARCHY_JOIN", $this->id_field) . " AS " . $this->tempfield("id") . "
			FROM " . $this->modelview($this, "HIERARCHY_JOIN") . "
			WHERE " . $this->modelviewfield($this, "HIERARCHY_JOIN", "firstclass_id") . "=:firstclass
			AND " . $this->modelviewfield($this, "HIERARCHY_JOIN", "parent_id") . "=:parent
			AND " . $this->modelviewfield($this, "HIERARCHY_JOIN", $this->ordered_field) . "=:ordered
		", array(
			"firstclass" => new Database\Type_Id(Database\Type_Id::NOT_NULL),
			"parent" => new Database\Type_Id(Database\Type_Id::NOT_NULL),
			"ordered" => new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL)
		), array(
			"id" => new Database\Type_SerialId()
		));
	}

	//-------------------------------------------------------------------------

	/**
	 * Returns the root node for a given FirstClass parent row.
	 * 
	 * @param mixed[] $firstclass an existing row in the parent FirstClass model.
	 * @return mixed[]|null the root node, or null if none exists
	 */
	function get_root_by_firstclass(array $firstclass) {
		self::assert_id_object($firstclass, $this->firstclass->get_id_field());

		return $this->database->execute_params_single("MODELS_HIERARCHY__{$this->table}__GET_ROOT", array("firstclass" => $firstclass[$this->firstclass->get_id_field()]));
	}

	/**
	 * Returns all immediate children for the specified node, restricted to the given FirstClass parent row.
	 * 
	 * Only direct children are returned, not all descendants.
	 * Note, the semantic meaning of this method name is "list ordered children, for the given FirstClass", not "list children, ordered by the given FirstClass".
	 * 
	 * @param mixed[] $firstclass an existing row in the parent FirstClass model.
	 * @param mixed[] $parent the parent model row
	 * @return array a non-associated array of results, themselves each associated arrays of field names to row values
	 */
	function list_children_ordered_by_firstclass(array $firstclass, array $parent) {
		self::assert_id_object($firstclass, $this->firstclass->get_id_field());
		self::assert_id_object($parent, $this->id_field);
		
		return $this->database->execute_params("MODELS_HIERARCHY__{$this->table}__LIST_CHILDREN_ORDERED", array(
			"firstclass" => $firstclass[$this->firstclass->get_id_field()],
			"parent" => $parent[$this->id_field]
		));
	}

	//---------------------------------------------------------------------

	const MOVE_UP = "up";
	const MOVE_DOWN = "down";
	const MOVE_TOP = "top";
	const MOVE_BOTTOM = "bottom";
	const MOVE_LEFT = "left";
	const MOVE_RIGHT = "right";
	
	/**
	 * Moves a model row in the specified direction within the hierarchy, restricted to the ordering of the specified parent FirstClass model row.
	 * 
	 * Note, the FirstClass parent model row is derived automatically from the existing model row rather than explicitly specified.
	 * Transaction support is enabled, so failed movement will roll back.
	 * 
	 * @param mixed[] $object an existing model row
	 * @param string $direction one of the direction values: MOVE_UP, MOVE_DOWN, MOVE_TOP, MOVE_BOTTOM, MOVE_LEFT or MOVE_RIGHT
	 * @throws Hierarchy_Exception
	 * @return mixed[] the moved model row, including its new ordering and parent values
	 */
	function move(array $object, $direction) {
		self::assert_id_object($object, $this->id_field);
		self::assert_id($object[$this->firstclass_field]);
		
		if ($object["parent"] === null) throw new Hierarchy_Exception("Cannot move root node");
		
		if (!in_array($direction, array(self::MOVE_UP, self::MOVE_DOWN, self::MOVE_TOP, self::MOVE_BOTTOM, self::MOVE_LEFT, self::MOVE_RIGHT), true)) throw new Hierarchy_Exception("Supplied direction is not valid: {$direction}");
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		self::assert_id($object[$this->parent_field]);
		
		$firstclass_object = array("id" => $object[$this->firstclass_field]);	// this is non-ideal, but it's hard to chain secondclasses otherwise, since get_by_id isn't allowed. 
		
		$parent = $this->get_by_firstclass_and_id($firstclass_object, $object[$this->parent_field]);

		$current = $this->database->execute_params_value("MODELS_HIERARCHY__{$this->table}__CURRENT_ORDERED", array("id" => $object[$this->id_field]));
		$current_max = $this->database->execute_params_value("MODELS_HIERARCHY__{$this->table}__MAX_ORDERED", array(
			"firstclass" => $object[$this->firstclass_field],
			"parent" => $parent[$this->id_field]
		));

		switch($direction) {
			case self::MOVE_UP:
				if ($current === 0) {
					$this->database->transaction_rollback();
					throw new Hierarchy_Exception("Cannot move above the first ordered row");
				}

				$prev = $this->database->execute_params_value("MODELS_HIERARCHY__{$this->table}__BY_ORDERED", array(
					"firstclass" => $object[$this->firstclass_field],
					"parent" => $parent[$this->id_field],
					"ordered" => $current - 1
				));

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($prev, new Database\Type_SerialId())
				), true);

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current - 1, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId())
				), true);

				break;
			case self::MOVE_DOWN:
				if ($current === $current_max) {
					$this->database->transaction_rollback();
					throw new Hierarchy_Exception("Cannot move below the last ordered row");
				}

				$prev = $this->database->execute_params_value("MODELS_HIERARCHY__{$this->table}__BY_ORDERED", array(
					"firstclass" => $object[$this->firstclass_field],
					"parent" => $parent[$this->id_field],
					"ordered" => $current + 1
				));
				
				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($prev, new Database\Type_SerialId())
				), true);

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current + 1, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId())
				), true);

				break;
			case self::MOVE_TOP:
				if ($current === 0) break;	// if already topmost

				$this->database->query_params("
					UPDATE " . $this->model($this) . "
					SET " . $this->modelfield($this, $this->ordered_field) . "=" . $this->modelfield($this, $this->ordered_field) . "+1
					WHERE " . $this->modelfield($this, $this->firstclass_field) . "=:firstclass
					AND " . $this->modelfield($this, $this->parent_field) . "=:parent
					AND " . $this->modelfield($this, $this->ordered_field) . "<:threshold
				", array(
					"firstclass" => new Database\Param($object[$this->firstclass_field], new Database\Type_Id(Database\Type_Id::NOT_NULL)),
					"parent" => new Database\Param($parent[$this->id_field], new Database\Type_Id(Database\Type_Id::NOT_NULL)),
					"threshold" => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				));

				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param(0, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId())
				), true);

				break;
			case self::MOVE_BOTTOM:
				if ($current === $current_max) break;	// if already last

				$this->database->query_params("
					UPDATE " . $this->model($this) . "
					SET " . $this->modelfield($this, $this->ordered_field) . "=" . $this->modelfield($this, $this->ordered_field) . "-1
					WHERE " . $this->modelfield($this, $this->firstclass_field) . "=:firstclass
					AND " . $this->modelfield($this, $this->parent_field) . "=:parent
					AND " . $this->modelfield($this, $this->ordered_field) . ">:threshold
				", array(
					"firstclass" => new Database\Param($object[$this->firstclass_field], new Database\Type_Id(Database\Type_Id::NOT_NULL)),
					"parent" => new Database\Param($parent[$this->id_field], new Database\Type_Id(Database\Type_Id::NOT_NULL)),
					"threshold" => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				));
				
				$this->database->update_rows_by_conditions($this->table, array(
					$this->ordered_field => new Database\Param($current_max, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId()),
				), true);

				break;
			case self::MOVE_LEFT:
				if ($parent["parent"] === null) { 
					$this->database->transaction_rollback();
					throw new Hierarchy_Exception("Cannot move leftwards on top of root node");
				}
				
				$grandparent = $this->get_by_firstclass_and_id($firstclass_object, $parent["parent"]);
				
				$parent_max = $this->database->execute_params_value("MODELS_HIERARCHY__{$this->table}__MAX_ORDERED", array(
					"firstclass" => $object[$this->firstclass_field],
					"parent" => $grandparent[$this->id_field]
				));
				
				$this->database->update_rows_by_conditions($this->table, array(
					$this->parent_field => new Database\Param($grandparent[$this->id_field], new Database\Type_Id(Database\Type_SmallInt::NOT_NULL)),
					$this->ordered_field => new Database\Param($parent_max + 1, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId()),
				), true);
				
				$this->database->query_params("
					UPDATE " . $this->model($this) . "
					SET " . $this->modelfield($this, $this->ordered_field) . "=" . $this->modelfield($this, $this->ordered_field) . "-1
					WHERE " . $this->modelfield($this, $this->firstclass_field) . "=:firstclass
					AND " . $this->modelfield($this, $this->parent_field) . "=:parent
					AND " . $this->modelfield($this, $this->ordered_field) . ">:threshold
				", array(
					"firstclass" => new Database\Param($object[$this->firstclass_field], new Database\Type_Id(Database\Type_Id::NOT_NULL)),
					"parent" => new Database\Param($parent[$this->id_field], new Database\Type_Id(Database\Type_Id::NOT_NULL)),
					"threshold" => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				));
				
				break;
			case self::MOVE_RIGHT:
				if ($object["ordered"] === 0) {
					$this->database->transaction_rollback();
					throw new Hierarchy_Exception("Cannot move rightwards because there is no higher sibbling");
				}
				
				$prev = $this->database->execute_params_value("MODELS_HIERARCHY__{$this->table}__BY_ORDERED", array(
					"firstclass" => $object[$this->firstclass_field],
					"parent" => $parent[$this->id_field],
					"ordered" => $current - 1
				));
				
				$prev_max = $this->database->execute_params_value("MODELS_HIERARCHY__{$this->table}__MAX_ORDERED", array(
					"firstclass" => $object[$this->firstclass_field],
					"parent" => $prev
				), true);
				if ($prev_max === null) $prev_max = -1;
				
				$this->database->update_rows_by_conditions($this->table, array(
					$this->parent_field => new Database\Param($prev, new Database\Type_Id(Database\Type_SmallInt::NOT_NULL)),
					$this->ordered_field => new Database\Param($prev_max + 1, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				), array(
					$this->id_field => new Database\Param($object[$this->id_field], new Database\Type_SerialId()),
				), true);
				
				$this->database->query_params("
					UPDATE " . $this->model($this) . "
					SET " . $this->modelfield($this, $this->ordered_field) . "=" . $this->modelfield($this, $this->ordered_field) . "-1
					WHERE " . $this->modelfield($this, $this->firstclass_field) . "=:firstclass
					AND " . $this->modelfield($this, $this->parent_field) . "=:parent
					AND " . $this->modelfield($this, $this->ordered_field) . ">:threshold
				", array(
					"firstclass" => new Database\Param($object[$this->firstclass_field], new Database\Type_Id(Database\Type_Id::NOT_NULL)),
					"parent" => new Database\Param($parent[$this->id_field], new Database\Type_Id(Database\Type_Id::NOT_NULL)),
					"threshold" => new Database\Param($current, new Database\Type_SmallInt(Database\Type_SmallInt::UNSIGNED, Database\Type_SmallInt::NOT_NULL))
				));
				
				break;
		}
		
		$object = $this->_internal_firstclass_get_by_id($object[$this->id_field]);
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return $object;
	}
	
	//---------------------------------------------------------------------

	/**
	 * Inserts a new root node for the given FirstClass model row.
	 * 
	 * Note, the identifier, ordered and parent values are generated automatically and should not be specified explicitly.
	 * You can only have one root per FirstClass model row.
	 * Transaction support is enabled, so failed row insertion will roll back.
	 * 
	 * @param mixed[] $firstclass an existing row in the parent FirstClass model
	 * @param mixed[] $values an associated array values taking the format field name => value
	 * @throws Hierarchy_Exception
	 * @return mixed[] the newly created model row, including its new identifier value
	 */
	function insert_root_for_firstclass(array $firstclass, array $values) {
		self::assert_id_object($firstclass, $this->firstclass->get_id_field());
		
		if (array_key_exists($this->ordered_field, $values)) throw new Hierarchy_Exception("Ordering fields should not be specified explicitly for hierarchy models");
		if (array_key_exists($this->parent_field, $values)) throw new Hierarchy_Exception("Parent fields should not be specified explicitly for hierarchy models");
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		if (null !== $this->get_root_by_firstclass($firstclass)) throw new Hierarchy_Exception("A root node already exists for this hierarchy's firstclass");
		
		$values[$this->ordered_field] = 0;
		$values[$this->parent_field] = null;
		
		$object = parent::insert_for_firstclass($firstclass, $values);
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return $object;
	}
	
	/**
	 * Inserts a new child node against the specified parent node within the context of the given FirstClass model row.
	 * 
	 * Note, the identifier, ordered and parent values are generated automatically and should not be specified explicitly.
	 * Rows are always inserted at the end of the sibbling ordering, incrementing the last value for the branch.
	 * Transaction support is enabled, so failed row insertion will roll back.
	 * 
	 * @param mixed[] $firstclass an existing row in the parent FirstClass model
	 * @param mixed[] $parent an existing row in model
	 * @param mixed[] $values an associated array values taking the format field name => value
	 * @throws Hierarchy_Exception
	 * @return mixed[]
	 */
	function insert_for_firstclass_and_parent(array $firstclass, array $parent, array $values) {
		self::assert_id_object($firstclass, $this->firstclass->get_id_field());
		self::assert_id_object($parent, $this->id_field);
		
		if (array_key_exists($this->ordered_field, $values)) throw new Hierarchy_Exception("Ordering fields should not be specified explicitly for hierarchy models");
		if (array_key_exists($this->parent_field, $values)) throw new Hierarchy_Exception("Parent fields should not be specified explicitly for hierarchy models");
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$current_max = $this->database->execute_params_value("MODELS_HIERARCHY__{$this->table}__MAX_ORDERED", array(
			"firstclass" => $firstclass[$this->firstclass->get_id_field()],
			"parent" => $parent[$this->id_field]
		), true);
		if ($current_max === null) $current_max = 0; 
		else $current_max++;
		
		$values[$this->ordered_field] = $current_max;
		$values[$this->parent_field] = $parent[$this->id_field];
		
		$object = parent::insert_for_firstclass($firstclass, $values);
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return $object;
	}
	
	/**
	 * Overrides the corresponding method in SecondClass to always throw an exception.
	 * 
	 * Hierarchy models should use the insert_root_for_firstclass or insert_for_firstclass_and_parent methods instead.
	 *
	 * @internal
	 * @param mixed[] $firstclass not applicable
	 * @param string[] $values not applicable
	 * @throws Hierarchy_Exception
	 * @return void
	 */
	function insert_for_firstclass(array $firstclass, array $values) {
		throw new Hierarchy_Exception("Do not call insert_for_firstclass explicitly on hierarchy objects; use insert_root_for_firstclass or insert_for_firstclass_and_parent instead");
	}

	/**
	 * Deletes a root node row from the model's table using its identifier value, validated against the FirstClass parent model view.
	 * 
	 * Note, cascading deletion of sub nodes is theorically sound, however in practice MySQL sometimes struggles for very nested depedency trees >10 descendants deep.
	 * Transaction support is enabled, so failed row insertion will roll back.
	 * 
	 * @param mixed[] $firstclass an existing row in the parent FirstClass model
	 * @param mixed[] $object an existing model row
	 * @throws Hierarchy_Exception
	 * @return true always returns true; failure will generate an exception
	 */
	public function delete_root_for_firstclass(array $firstclass, array $object) {
		self::assert_id_object($object, $this->id_field);
	
		if (!array_key_exists($this->firstclass_field, $object)) throw new Hierarchy_Exception("Unable to find orientator ID within the object");
	
		if (!array_key_exists("parent", $object) || $object["parent"] !== null) throw new Hierarchy_Exception("This is not a root node object");
	
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
	
		parent::delete_for_firstclass($firstclass, $object);
	
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
	
		return true;
	}

	/**
	 * Deletes a child node row from the model's table using its identifier value and the specified parent, validated against the FirstClass parent model view.
	 * 
	 * Note, cascading deletion of sub nodes is theorically sound, however in practice MySQL sometimes struggles for very nested depedency trees >10 descendants deep.
	 * Transaction support is enabled, so failed row insertion will roll back.
	 * 
	 * @param mixed[] $firstclass an existing row in the parent FirstClass model
	 * @param mixed[] $parent the parent model row
	 * @param mixed[] $object an existing model row
	 * @throws Hierarchy_Exception
	 * @return true always returns true; failure will generate an exception
	 */
	public function delete_for_firstclass_and_parent(array $firstclass, array $parent, array $object) {
		self::assert_id_object($object, $this->id_field);
		self::assert_id_object($parent, $this->id_field);
		
		if (!array_key_exists($this->firstclass_field, $object)) throw new Hierarchy_Exception("Unable to find orientator ID within the object");
		
		if (!array_key_exists("parent", $object) || $object["parent"] === null) throw new Hierarchy_Exception("This is a root node object and cannot be deleted; use delete_root_for_firstclass instead");
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$this->move($object, "bottom");
		parent::delete_for_firstclass($firstclass, $object);
	
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}

	/**
	 * Overrides the corresponding method in SecondClass to always throw an exception.
	 * 
	 * Hierarchy models should use the delete_root_for_firstclass or delete_for_firstclass_and_parent methods instead.
	 *
	 * @internal
	 * @param mixed[] $firstclass not applicable
	 * @param string[] $values not applicable
	 * @throws Hierarchy_Exception
	 * @return void
	 */
	function delete_for_firstclass(array $firstclass, array $values) {
		throw new Hierarchy_Exception("Do not call delete_for_firstclass explicitly on hierarchy objects; use delete_root_for_firstclass or delete_for_firstclass_and_parent instead");
	}

	//---------------------------------------------------------------------

	/**
	 * Annoyingly, because MySQL treats same-table foreign keys updates as ON UPDATE RESTRICT, defragmentation of Hierarchy models is not possible.
	 * 
	 * @throws Hierarchy_Exception
	 * @return void
	 */
	function defragment() {
		throw new Hierarchy_Exception("Defragmentation of Hierarchy models is not possible.");
	}
}
