<?php
/**
 * Many-to-many relationship model support within the same table.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/m2mlinktable.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

use \Abstraction\Database as Database;

/**
 * @internal
 */
class Relation_Exception extends M2MLinkTable_Exception {}

/**
 * Defines a root model for relation tables.
 *
 * Relation tables are a special M2MLinkTable, joining the same table together with A<->B relations automatically.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Relation extends M2MLinkTable {
	protected $model;

	/**
	 * Constructs a new instance of this link table.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param FirstClass $model the FirstClass model to form the join upon
	 * @param Database\Type[] $structure an optional associated array to define any additional model structure, taking the format field name => field type object
	 * @throws M2MLinkTable_Exception
	 */
	public function __construct(Database\Wrapper $database, $table, FirstClass $model, array $structure = array()) {
		parent::__construct($database, $table, $model, $model, "src", "dest", $structure);

		$this->model = $model;
	}

	//-------------------------------------------------------------------------

	public function get_table_a() {
		throw new Relation_Exception("Table_A isn't application for relations. Use get_model");
	}
	
	public function get_table_b() {
		throw new Relation_Exception("Table_B isn't application for relations. Use get_model");
	}
	
	public function get_link_a() {
		throw new Relation_Exception("Link_A isn't application for relations.");
	}
	
	public function get_link_b() {
		throw new Relation_Exception("Link_B isn't application for relations.");
	}
	
	public function get_model() {
		return $this->model;
	}
	
	//-------------------------------------------------------------------------

	public function list_as_by_b(array $b) {
		throw new Relation_Exception("list_as_by_b isn't application for relations. Use list_related");
	}
	
	/**
	 * Returns all rows in the model that are related.
	 *
	 * @param array $src the row in the model
	 */
	public function list_related(array $src) {
		self::assert_id_object($src, $this->model->get_id_field());
		
		return parent::list_bs_by_a($src);
	}
	
	/**
	 * Returns all rows in the B model joined to the given A model row.
	 * 
	 * @param array $a the row in the A model
	 */
	public function list_bs_by_a(array $a) {
		throw new Relation_Exception("list_bs_by_a isn't application for relations. Use list_related");
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Attempts to link the two given model rows, using any additional specified values.
	 * 
	 * Note that any two specific row pairs can only be linked once, so additional linking will throw an exception.
	 * The link fields themselves are set automatically and should not be specified in the values.
	 * 
	 * @param mixed[] $src the source row
	 * @param mixed[] $dest the destination row
	 * @param mixed[] $values any additional values the model requires
	 * @throws Relation_Exception
	 * @throws M2MLinkTable_Exception
	 * @return true always returns true; failure throws an exception
	 */
	public function link(array $src, array $dest, array $values = array()) {
		self::assert_id_object($src, $this->model->get_id_field());
		self::assert_id_object($dest, $this->model->get_id_field());
		
		if ($src[$this->model->get_id_field()] === $dest[$this->model->get_id_field()]) throw new Relation_Exception("Unable to relate to the same model row");

		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		parent::link($src, $dest, $values);
		parent::link($dest, $src, $values);

		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}

	/**
	 * Attempts to unlink the two given model rows.
	 * 
	 * By default, attempting to unlink a non-existent link will throw an exception. This can be suppressed by setting the $_IGNORE_MISSING_LINK flag.
	 * 
	 * @param mixed[] $src the source row in the model
	 * @param mixed[] $dest the destination row in the model
	 * @param boolean $_IGNORE_MISSING_LINK true if non-existing links should be silently ignored.
	 * @throws Relation_Exception
	 * @throws M2MLinkTable_Exception
	 * @return true always returns true; failure throws an exception
	 */
	public function unlink(array $src, array $dest, $_IGNORE_MISSING_LINK = false) {
		self::assert_id_object($src, $this->model->get_id_field());
		self::assert_id_object($dest, $this->model->get_id_field());
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		parent::unlink($src, $dest, $_IGNORE_MISSING_LINK);
		parent::unlink($dest, $src, $_IGNORE_MISSING_LINK);
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}

	public function delete_all_for_a(array $a) {
		throw new Relation_Exception("delete_all_for_a isn't application for relations. Use delete_all_relations");
	}

	public function delete_all_for_b(array $b) {
		throw new Relation_Exception("delete_all_for_a isn't application for relations. Use delete_all_relations");
	}
	
	/**
	 * Wipes any rows in the model joined either way to the given row.
	 *
	 * @param mixed[] $row the row in the model
	 * @throws M2MLinkTable_Exception
	 * @return true always returns true; failure throws an exception
	 */
	public function delete_all_relations($row) {
		self::assert_id_object($row, $this->model->get_id_field());
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		parent::delete_all_for_a($row);
		parent::delete_all_for_b($row);

		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}

	/**
	 * Updates a row in the model's table with new data without affecting the existing link.
	 * 
	 * Any field apart from the foreign key link fields can be populated with new data.
	 * 
	 * @param mixed[] $row an existing model row, which must contain both the link table foreign keys.
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function update_link_row($row) {
		throw new Relation_Exception("update_link_row isn't application for relations. Use update_relation instead");
	}
	
	/**
	 * Updates the join between two a row in the model's table with new data without affecting the existing link.
	 *
	 * Any field apart from the foreign key link fields can be populated with new data.
	 *
	 * @param mixed[] $row an existing model row, which must contain both the link table foreign keys.
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function update_relation($row) {
		self::assert_id_object($row, "src");
		self::assert_id_object($row, "dest");

		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		parent::update_link_row($row);
		
		// fudge here
		$src = $row["src"];
		$dest = $row["dest"];
		$row["dest"] = $src;
		$row["src"] = $dest;

		parent::update_link_row($row);
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();

		return true;
	}

}
