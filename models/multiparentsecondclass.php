<?php
/**
 * Multi-parent second-class model support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/firstclass.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

use \Abstraction\Database as Database;

/**
 * @internal
 */
class MultiParentSecondClass_Exception extends FirstClass_Exception {}

/**
 * Defines a root model for multi-parent second-class structured models.
 * 
 * Multi-parent second-class models are similar to FirstClass models, but have two or more parents (technically one is possible, but it is pointless). They are most useful for creating multiple entry links between other tables, such as data tables that require more than one entry per pair.
 * Note that MultiParentSecondClass models inherit from the FirstClass model definition, but not the SecondClass definition.
 * 
 * @api
 * @author Pete Morris
 * @version 2.0.0
 */
abstract class MultiParentSecondClass extends FirstClass {
	protected $id_field;
	protected $firstclass;

	/**
	 * Constructs a new instance of this second-class model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Database\Type[] $structure an associated array to define the model structure, taking the format field name => field type object
	 * @param FirstClass[] $parents an associated array of linkage field names and parent FirstClass model to link to
	 * @param string $id_field the name of the identifier field, if not "id"
	 * @param ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @param IndexKey[] $index_keys any non-unique index keys to apply to the model
	 * @throws MultiParentSecondClass_Exception
	 */
	public function __construct(
			Database\Wrapper $database,
			$table,
			array $structure,
			array $parents,
			$id_field = "id",
			array $foreign_keys = array(),
			array $unique_keys = array(),
			array $index_keys = array()
	) {
		$this->firstclass = array();
		$this->firstclass_field = array();
		
		foreach ($parents as $firstclass_field => $firstclass) {
			if (array_key_exists($firstclass_field, $this->firstclass)) throw new MultiParentSecondClass_Exception("Each parent for a MultiParentSecondClass model can only be used once.");
			
			if (array_key_exists($firstclass_field, $structure)) throw new MultiParentSecondClass_Exception("The firstclass fields are implicit for MultiParentSecondClass models and should not be explicitly stated in the structure");
			if (array_key_exists($firstclass_field, $foreign_keys)) throw new MultiParentSecondClass_Exception("Do not specify the foreign key relationship directly for MultiParentSecondClass models");
			
			$this->firstclass[$firstclass_field] = $firstclass;
			
			$structure[$firstclass_field] = new Database\Type_Id(Database\Type::NOT_NULL);
			
			$foreign_keys[$firstclass_field] = new ForeignKey($firstclass, "CASCADE", "CASCADE", $table . "__fk_2nd_{$firstclass_field}");
		}
		
		if (sizeof($this->firstclass) === 0) throw new MultiParentSecondClass_Exception("No parents were defined for the MultiParentSecondClass model");
		if (sizeof($this->firstclass) === 1) throw new MultiParentSecondClass_Exception("Only one parent specified for MultiParentSecondClass model. Whilst this is technically possible, it is safer to use SecondClass as your base model instead.");
		
		parent::__construct(
				$database,
				$table,
				$structure,
				$id_field,
				$foreign_keys,
				$unique_keys,
				$index_keys
		);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * The MULTIPARENTSECONDCLASS_JOIN view defined here is used extensively as a shorthand way of guaranteeing referential integrity during further SELECT queries.
	 * 
	 * @internal
	 */
	protected function define_views() {
		parent::define_views();
		
		// This view is really a bit pointless, as it just returns the same data as the original column, but with "firstclass_" appended on the front. However, it does give a small level of guarantee that the field is the correct one, so we might as well retain it.		
		$derivedfields = array();
		$joins = array();
		foreach ($this->firstclass as $firstclass_field => $firstclass) {
			$derivedfields[] = $this->modelfield($firstclass, $firstclass->get_id_field()) . " AS " . $this->tempfield("firstclass_{$firstclass_field}");
			$joins[] = "INNER JOIN " . $this->model($firstclass) . " ON " . $this->modelfield($this, $firstclass_field) . "=" . $this->modelfield($firstclass, $firstclass->get_id_field());
		}
		
		$this->define_view("MULTIPARENTSECONDCLASS_JOIN", "
			SELECT {$this->fields}, " . implode(",", $derivedfields) . "
			FROM " . $this->model($this) . "
			" . implode(" ", $joins) . "
		");
	}

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		$fields = array();
		foreach (array_keys($this->structure) as $field) $fields[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $field);
		$fields = implode(",", $fields);
		
		$clauses = array();
		$params = array("id" => $this->structure[$this->id_field]);
		foreach ($this->firstclass as $firstclass_field => $firstclass) {
			$clauses[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", "firstclass_{$firstclass_field}") . "=:firstclass_{$firstclass_field}";
			$params["firstclass_{$firstclass_field}"] = $this->structure[$firstclass_field];
		}

		$this->database->preprepare("MODELS_MULTIPARENTSECONDCLASS__{$this->table}__GET_BY_FIRSTCLASS_PARENTS_AND_ID", "
			SELECT {$fields}
			FROM " . $this->modelview($this, "MULTIPARENTSECONDCLASS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $this->id_field) . "=:id
			AND " . implode(" AND ", $clauses) . "
		", $params, $this->structure);

		unset($params["id"]);
		
		$this->database->preprepare("MODELS_MULTIPARENTSECONDCLASS__{$this->table}__LIST_BY_FIRSTCLASS_PARENTS", "
			SELECT {$fields}
			FROM " . $this->modelview($this, "MULTIPARENTSECONDCLASS_JOIN") . "
			WHERE " . implode(" AND ", $clauses) . "
		", $params, $this->structure);

		$this->database->preprepare("MODELS_MULTIPARENTSECONDCLASS__{$this->table}__LIST_ALL_IDS_BY_FIRSTCLASS_PARENTS", "
			SELECT " . $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $this->id_field) . "
			FROM " . $this->modelview($this, "MULTIPARENTSECONDCLASS_JOIN") . "
			WHERE " . implode(" AND ", $clauses) . "
		", $params, array($this->id_field => $this->structure[$this->id_field]));
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns the specified parent FirstClass model associated with this MultiParenetSecondClass model
	 * 
	 * @return FirstClass
	 */
	public function get_firstclass($firstclass_field) {
		if (!array_key_exists($firstclass_field, $this->firstclass)) throw new MultiParentSecondClass_Exception("Specified firstclass parent by that field name does not exist");

		return $this->firstclass[$firstclass_field];
	}
	
	/**
	 * Returns the field names linked to the multiple parent FirstClass models.
	 * 
	 * @return string[]
	 */
	public function get_firstclass_fields() {
		return array_keys($this->firstclass);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Lists all rows within this model's table that are linked to the specified FirstClass model rows.
	 * 
	 * @param mixed[] $firstclass_object an associated array of existing rows in the parent FirstClass models.
	 * @return array a non-associated array of results, themselves each associated arrays of field names to row values
	 */
	public function list_by_firstclass_parents(array $firstclass_objects) {
		$params = array();
		foreach ($firstclass_objects as $firstclass_field => $firstclass_object) {
			self::assert_id_object($firstclass_object, $this->firstclass[$firstclass_field]->get_id_field());
			$params["firstclass_{$firstclass_field}"] = $firstclass_object[$this->firstclass[$firstclass_field]->get_id_field()];
		}
		
		return $this->database->execute_params("MODELS_MULTIPARENTSECONDCLASS__{$this->table}__LIST_BY_FIRSTCLASS_PARENTS", $params);
	}

	/**
	 * Overrides the get_by_id method in FirstClass to always throw an exception.
	 * 
	 * MultiParentSecondClass models should use the get_by_firstclass_parents_and_id method instead.
	 * This is largely because overridden methods cannot have additional parameters not specified in the parent, so get_by_id($firstclass, $id) is not valid.
	 *
	 * @internal
	 * @param int $id not applicable
	 * @throws MultiParentSecondClass_Exception
	 * @return void
	 */
	public function get_by_id($id) {
		throw new MultiParentSecondClass_Exception("Do not call get_by_id directly, use get_by_firstclass_parents_and_id instead");
	}

	/**
	 * Needed for allowing subsequent models to override the get_by_firstclass_and_id method yet still call it themselves.
	 * 
	 * @internal
	 * @param mixed[] $firstclass_objects an associated array of existing rows in the parent FirstClass models.
	 * @param int $id the model row identifier
	 */
	protected function _internal_secondclass_get_by_firstclass_parents_and_id(array $firstclass_objects, $id) {
		$params = array("id" => $id);
		foreach ($firstclass_objects as $firstclass_field => $firstclass_object) {
			self::assert_id_object($firstclass_object, $this->firstclass[$firstclass_field]->get_id_field());
			$params["firstclass_{$firstclass_field}"] = $firstclass_object[$this->firstclass[$firstclass_field]->get_id_field()];
		}

		return $this->database->execute_params_single("MODELS_MULTIPARENTSECONDCLASS__{$this->table}__GET_BY_FIRSTCLASS_PARENTS_AND_ID", $params);
	}

	/**
	 * Returns the model row corresponding to the given identifier, validated against the FirstClass parent model view.
	 * 
	 * This method prevents rows not explicitly linked to the specified parent model row from being fetched.
	 *  
	 * @param mixed[] $firstclass_objects an associated array of existing rows in the parent FirstClass models.
	 * @param int $id the model row identifier
	 * @return mixed[]|null the corresponding row or null if none match
	 */
	public function get_by_firstclass_and_id(array $firstclass_objects, $id) {
		self::assert_id($id);
		
		return $this->_internal_secondclass_get_by_firstclass_parents_and_id($firstclass_objects, $id);
	}

	/**
	 * Overrides the bulkget_by_ids method in FirstClass to always throw an exception.
	 * 
	 * MultiParentSecondClass models should use the bulkget_by_firstclass_and_ids method instead.
	 * This is largely because overridden methods cannot have additional parameters not specified in the parent, so bulkget_by_ids($firstclass, $ids) is not valid.
	 *
	 * @internal
	 * @param int[] $ids not applicable
	 * @param string[] $orderby not applicable
	 * @throws MultiParentSecondClass_Exception
	 * @return void
	 */
	public function bulkget_by_ids(array $ids, array $orderby = null) {
		throw new MultiParentSecondClass_Exception("Do not call bulkget_by_ids directly, use bulkget_by_firstclass_parents_and_ids instead");
	}

	/**
	 * Returns an array of model rows for the given identifiers, validated against the FirstClass parent model view.
	 * 
	 * This method prevents rows not explicitly linked to the specified parent model row from being fetched.
	 *  
	 * @param mixed[] $firstclass_objects an associated array of existing rows in the parent FirstClass models.
	 * @param int[] $ids the mode row identifier values as an array
	 * @param string[] $orderby an optional associative array of field names => direction
	 * @return array
	 */
	public function bulkget_by_firstclass_parents_and_ids(array $firstclass_objects, array $ids, array $orderby = null) {
		if (sizeof($ids) == 0) return array();
		if (sizeof($ids) > 255) {
			$slice = array_slice($ids, 0, 255);
			$remainder = array_slice($ids, 255);
			
			return array_merge($this->bulkget_by_firstclass_parents_and_ids($firstclass_objects, $slice), $this->bulkget_by_firstclass_parents_and_ids($firstclass_objects, $remainder));
		}

		$clauses = array();
		$params = array();
		foreach ($firstclass_objects as $firstclass_field => $firstclass_object) {
			self::assert_id_object($firstclass_object, $this->firstclass[$firstclass_field]->get_id_field());
			$params["firstclass_{$firstclass_field}"] = new Database\Param($firstclass_object[$this->firstclass[$firstclass_field]->get_id_field()], $this->structure[$firstclass_field]);
			$clauses[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", "firstclass_{$firstclass_field}") . "=:firstclass_{$firstclass_field}";
		}

		foreach ($ids as $id) self::assert_id($id);
		$ids = implode(",", $ids);
		
		$orderbyclauses = array();
		if (is_array($orderby)) foreach ($orderby as $field => $direction) $orderbyclauses[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $field) . " {$direction}";
		$orderbyclauses = sizeof($orderbyclauses) === 0 ? "" : ("ORDER BY " . implode(",", $orderbyclauses));
		
		$fields = array();
		foreach (array_keys($this->structure) as $field) $fields[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $field);
		$fields = implode(",", $fields);
		
		return $this->database->query_params("
			SELECT {$fields}
			FROM " . $this->modelview($this, "MULTIPARENTSECONDCLASS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $this->id_field) . " IN ({$ids})
			AND " . implode(" AND ", $clauses) . "
			{$orderbyclauses}
		", $params, $this->structure);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Overrides the corresponding method in FirstClass to always throw an exception.
	 * 
	 * MultiParentSecondClass models should use the search_ids_for_..._fields_by_firstclass method instead.
	 *
	 * @internal
	 * @param string $value not applicable
	 * @param string[] $fields not applicable
	 * @throws MultiParentSecondClass_Exception
	 * @return void
	 */
	public function search_ids_for_encrypted_fields($value, array $fields) {
		throw new MultiParentSecondClass_Exception("Do not call search_ids_for_encrypted_fields directly, use search_ids_for_encrypted_fields_by_firstclass_parents instead");
	}

	/**
	 * Overrides the corresponding method in FirstClass to always throw an exception.
	 * 
	 * MultiParentSecondClass models should use the search_ids_for_..._fields_by_firstclass method instead.
	 *
	 * @internal
	 * @param string $value not applicable
	 * @param string[] $fields not applicable
	 * @throws MultiParentSecondClass_Exception
	 * @return void
	 */
	public function search_ids_for_nonencrypted_fields($value, array $fields) {
		throw new MultiParentSecondClass_Exception("Do not call search_ids_for_encrypted_fields directly, use search_ids_for_encrypted_fields_by_firstclass_parents instead");
	}
	
	/**
	 * Overrides the corresponding method in FirstClass to always throw an exception.
	 * 
	 * MultiParentSecondClass models should use the search_ids_for_..._fields_by_firstclass method instead.
	 *
	 * @internal
	 * @param string $value not applicable
	 * @param string[] $fields not applicable
	 * @param string[] $orderby not applicable
	 * @throws MultiParentSecondClass_Exception
	 * @return void
	 */
	public function search_fields($value, array $fields, array $orderby = null) {
		throw new MultiParentSecondClass_Exception("Do not call search directly, use search_by_firstclass_parents instead");
	}
	
	/**
	 * Trawls the table for matching values in the specified non-encrypted fields, validated against the FirstClass parents model view.
	 * 
	 * Searching is performed using relatively efficient LIKE conditions.
	 * Note, this method is primarily used as part of search_fields_by_firstclass_parents and is rarely called by itself.
	 *  
	 * @param mixed[] $firstclass an associated array of existing rows in the parent FirstClass models.
	 * @param string $value the value to search for
	 * @param string[] $fields an array of model fields to search
	 * @throws MultiParentSecondClass_Exception
	 * @return int[] the model identifier values for any matching rows
	 */
	public function search_ids_for_nonencrypted_fields_by_firstclass(array $firstclass, $value, array $fields) {
		if (sizeof($fields) == 0) return array();
	
		$conditions = array();
		foreach ($fields as $field) {
			if (!isset($this->structure[$field])) throw new MultiParentSecondClass_Exception("Unknown field to perform encrypted search on", $field);
			if ($this->structure[$field] instanceof Database\Type_Encrypted) throw new MultiParentSecondClass_Exception("Attempting to search on an encrypted field", $field);
			if (!($this->structure[$field] instanceof Database\Type_String || $this->structure[$field] instanceof Database\Type_Text)) throw new MultiParentSecondClass_Exception("Attempting to search on a non-string/text field", $field);
			
			$conditions[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $field) . " LIKE :valuea";
			$conditions[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $field) . " LIKE :valueb";
			$conditions[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $field) . " LIKE :valuec";
			$conditions[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $field) . " LIKE :valued";
		}
	
		$clauses = array();
		$params = array(
			"valuea" => new Database\Param($value, new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valueb" => new Database\Param("{$value}%", new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valuec" => new Database\Param("%{$value}", new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valued" => new Database\Param("%{$value}%", new Database\Type_String(255, Database\Type::NOT_NULL))
		);
		foreach ($firstclass as $firstclass_field => $firstclass_object) {
			self::assert_id_object($firstclass_object, $this->firstclass[$firstclass_field]->get_id_field());
			$params["firstclass_{$firstclass_field}"] = new Database\Param($firstclass_object[$this->firstclass[$firstclass_field]->get_id_field()], $this->structure[$firstclass_field]);
			$clauses[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", "firstclass_{$firstclass_field}") . "=:firstclass_{$firstclass_field}";
		}
		
		return $this->database->query_params_list("
			SELECT " . $this->id_field . "
			FROM " . $this->modelview($this, "MULTIPARENTSECONDCLASS_JOIN") . "
			WHERE (" . implode(" OR ", $conditions) . ")
			AND " . implode(" AND ", $clauses) . "
		", $params, $this->id_field, $this->structure[$this->id_field]);
	}
	
	/**
	 * Trawls the table for matching values in the specified encrypted fields, validated against the FirstClass parent models view.
	 * 
	 * Searching is performed in a relatively inefficient manner by querying all rows in the table, decrypting the fields, and then comparing programmatically.
	 * Note, this method is primarily used as part of search_fields_by_firstclass and is rarely called by itself.
	 *  
	 * @param mixed[] $firstclass an associated array of existing rows in the parent FirstClass models.
	 * @param string $value the value to search for
	 * @param string[] $fields an array of model fields to search
	 * @throws MultiParentSecondClass_Exception
	 * @return int[] the model identifier values for any matching rows
	 */
	public function search_ids_for_encrypted_fields_by_firstclass(array $firstclass, $value, array $fields) {
		self::assert_id_object($firstclass, $this->firstclass->get_id_field());
		
		if (sizeof($fields) == 0) return array();
		
		$results = array();
		$fieldnames = array();
		foreach ($fields as $field) {
			if (!isset($this->structure[$field])) throw new MultiParentSecondClass_Exception("Unknown field to perform encrypted search on", $field);
			if (!($this->structure[$field] instanceof Database\Type_Encrypted)) throw new MultiParentSecondClass_Exception("Attempting to search on a non-encrypted field", $field);
			
			$results[$field] = $this->structure[$field];
			$fieldnames[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $field);
		}
		$fieldnames = implode(",", $fieldnames);
		
		$results[$this->id_field] = $this->structure[$this->id_field];
		
		$params = array();
		foreach ($firstclass as $firstclass_field => $firstclass_object) {
			self::assert_id_object($firstclass_object, $this->firstclass[$firstclass_field]->get_id_field());
			$params["firstclass_{$firstclass_field}"] = $firstclass_object[$this->firstclass[$firstclass_field]->get_id_field()];
		}
		
		$all_ids = $this->database->execute_params_list("MODELS_MULTIPARENTSECONDCLASS__{$this->table}__LIST_ALL_IDS_BY_FIRSTCLASS_PARENTS", $params);
		if (sizeof($all_ids) == 0) return array();

		$clauses = array();
		$params = array();
		foreach ($firstclass as $firstclass_field => $firstclass_object) {
			$params["firstclass_{$firstclass_field}"] = new Database\Param($firstclass_object[$this->firstclass[$firstclass_field]->get_id_field()], $this->structure[$firstclass_field]);
			$clauses[] = $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", "firstclass_{$firstclass_field}") . "=:firstclass_{$firstclass_field}";
		}
		
		$chunks = array_chunk($all_ids, 500);
		$matches = array();
		foreach ($chunks as $ids) {
			if (sizeof($ids) == 0) continue;
			
			foreach ($this->database->query_params("
				SELECT {$fieldnames},
					" . $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $this->id_field) . "
				FROM " . $this->modelview($this, "MULTIPARENTSECONDCLASS_JOIN") . "
				WHERE " . $this->modelviewfield($this, "MULTIPARENTSECONDCLASS_JOIN", $this->id_field) . " IN (" . implode(",", $ids) . ")
				AND " . implode(" AND ", $clauses) . "
			", $params, $results) as $row) {
				foreach ($fields as $field) {
					if ($row[$field] === null) continue;
					if (strpos(strtolower($row[$field]), strtolower($value)) === false) continue;
					$matches[$row[$this->id_field]] = $row[$this->id_field];
				}
			}
		}
		
		return array_values($matches);
	}
	
	/**
	 * Trawls the model's table for matching values within the specified fields, encrypted or non-encrypted, validated against the FirstClass parents model view.
	 *
	 * Note, encrypted searches are only performed if one or more of the fields are actually of the type Encrypted.
	 * 
	 * @param mixed[] $firstclass an associated array of existing rows in the parent FirstClass models.
	 * @param string $value the value to search for
	 * @param string[] $fields an array of model fields to search
	 * @param string[] $orderby an optional associative array of field names => direction, not null for none
	 * @throws MultiParentSecondClass_Exception
	 * @return array a non-associated array of results, themselves each associated arrays of field names to row values
	 */
	public function search_fields_by_firstclass_parents(array $firstclass, $value, array $fields, array $orderby = null) {
		$encrypted = array(); 
		$regular = array();
		foreach ($fields as $field) {
			if (!isset($this->structure[$field])) throw new MultiParentSecondClass_Exception("Unknown field to perform encrypted search on", $field);
			if ($this->structure[$field] instanceof Database\Type_Encrypted) $encrypted[] = $field;
			else $regular[] = $field;
		}
		
		return $this->bulkget_by_firstclass_and_ids($firstclass, array_unique(array_merge(
			$this->search_ids_for_encrypted_fields_by_firstclass($firstclass, $value, $encrypted),
			$this->search_ids_for_nonencrypted_fields_by_firstclass($firstclass, $value, $regular)
		)), $orderby);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Overrides the corresponding method in FirstClass to always throw an exception.
	 * 
	 * MultiParentSecondClass models should use the insert_for_firstclass_parents method instead.
	 *
	 * @internal
	 * @param string[] $values not applicable
	 * @throws MultiParentSecondClass_Exception
	 * @return void
	 */
	public function insert(array $values) {
		throw new MultiParentSecondClass_Exception("Do not call insert directly, use insert_for_firstclass_parents instead");
	}

	/**
	 * Inserts the given values into the database for this model, associating it with the given parent FirstClass model rows
	 * 
	 * Note, the identifier value is generated automatically and should not be specified explicitly.
	 * Transaction support is enabled, so failed row insertion will roll back.
	 * 
	 * @param mixed[] $firstclass_objects an associated array of existing rows in the parent FirstClass models.
	 * @param mixed[] $values an associated array values taking the format field name => value
	 * @throws MultiParentSecondClass_Exception
	 * @return mixed[] the newly created model row, including its new identifier value
	 */
	public function insert_for_firstclass_parents(array $firstclass_objects, array $values) {
		foreach ($this->firstclass as $firstclass_field => $firstclass) {
			self::assert_id_object($firstclass_objects[$firstclass_field], $firstclass->get_id_field());
			if (array_key_exists($firstclass_field, $values)) throw new MultiParentSecondClass_Exception("Firstclass fields should not be specified explicitly for multi-parent second-class models");
			$values[$firstclass_field] = $firstclass_objects[$firstclass_field][$firstclass->get_id_field()];
		}
		
		return parent::insert($values);
	}

	/**
	 * Overrides the corresponding method in FirstClass to always throw an exception.
	 * 
	 * MultiParentSecondClass models should use the delete_for_firstclass_parents method instead.
	 *
	 * @internal
	 * @param mixed[] $object not applicable
	 * @throws MultiParentSecondClass_Exception
	 * @return void
	 */
	public function delete(array $object) {
		throw new MultiParentSecondClass_Exception("Do not call delete directly, use delete_for_firstclass_parents instead");
	}
	
	/**
	 * Deletes a row from the model's table using its identifier value, validated against the FirstClass parent model view.
	 * 
	 * @param mixed[] $firstclass_objects an associated array of existing rows in the parent FirstClass models.
	 * @param mixed[] $object an existing model row
	 * @throws MultiParentSecondClass_Exception
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function delete_for_firstclass_parents(array $firstclass_objects, array $object) {
		self::assert_id_object($object, $this->id_field);
		
		$conditions = array($this->id_field => new Database\Param($object[$this->id_field], $this->structure[$this->id_field]));
		foreach ($this->firstclass as $firstclass_field => $firstclass) {
			self::assert_id_object($firstclass_objects[$firstclass_field], $firstclass->get_id_field());
			if ($firstclass_objects[$firstclass_field][$firstclass->get_id_field()] !== $object[$firstclass_field]) throw new MultiParentSecondClass_Exception("Firstclass parent mismatch in the multi-parent secondclass model data");
			
			$conditions[$firstclass_field] = new Database\Param($firstclass_objects[$firstclass_field][$firstclass->get_id_field()], $this->structure[$firstclass_field]);
		}

		if (!$this->database->delete_rows_by_conditions($this->table, $conditions, true)) throw new FirstClass_Exception("Unable to delete multi-parent second class object from table", $this->table);

		return true;
	}

	/**
	 * Wipes all rows from the model's table that are dependent on the specified FirstClass parent model rows.
	 * 
	 * @param mixed[] $firstclass_objects an associated array of existing rows in the parent FirstClass models.
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function delete_all_for_firstclass_parents(array $firstclass_objects) {
		$conditions = array();
		foreach ($this->firstclass as $firstclass_field => $firstclass) {
			self::assert_id_object($firstclass_objects[$firstclass_field], $firstclass->get_id_field());
				
			$conditions[$firstclass_field] = new Database\Param($firstclass_objects[$firstclass_field][$firstclass->get_id_field()], $this->structure[$firstclass_field]);
		}
		
		if (!$this->database->delete_rows_by_conditions($this->table, $conditions, false)) throw new FirstClass_Exception("Unable to delete all multi-parent second class object from table", $this->table);

		return true;
	}

	/**
	 * Wipes all rows from the model's table that are dependent on the specified FirstClass parent model rows.
	 * 
	 * This method does not require all of the model rows to be specified, so is an aggressive deletion which is potentially dangerous!
	 * 
	 * @param mixed[] $firstclass_objects an associated array of existing rows in the parent FirstClass models.
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function delete_all_for_some_firstclass_parents(array $firstclass_objects) {
		if (sizeof($firstclass_objects) === 0) throw new MultiParentSecondClass_Exception("No parent rows specified to build the delete condition on");
		
		$conditions = array();
		foreach ($firstclass_objects as $firstclass_field => $firstclass_object) {
			if (!array_key_exists($firstclass_field, $this->firstclass)) throw new MultiParentSecondClass_Exception("Specified parent FirstClass does not exist in the model");
			
			self::assert_id_object($firstclass_object, $this->firstclass[$firstclass_field]->get_id_field());
			if ($firstclass_object[$this->firstclass[$firstclass_field]->get_id_field()] !== $object[$firstclass_field]) throw new MultiParentSecondClass_Exception("Firstclass parent mismatch in the multi-parent secondclass model data");
				
			$conditions[$firstclass_field] = new Database\Param($firstclass_object[$this->firstclass[$firstclass_field]->get_id_field()], $this->structure[$firstclass_field]);
		}
		
		if (!$this->database->delete_rows_by_conditions($this->table, $conditions, false)) throw new FirstClass_Exception("Unable to delete all multi-parent second class object from table", $this->table);

		return true;
	}

	/**
	 * Updates a row in the model's table with new data using its identifier value, validated against the FirstClass parent models.
	 * 
	 * Any field apart from the identifier field and parent link fields can be populated with new data.
	 * 
	 * @param mixed[] $firstclass_objects an associated array of existing rows in the parent FirstClass models.
	 * @param mixed[] $object an existing model row
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function update_for_firstclass_parents(array $firstclass_objects, array $object) {
		self::assert_id_object($object);
		
		foreach ($this->firstclass as $firstclass_field => $firstclass) {
			self::assert_id_object($firstclass_objects[$firstclass_field], $firstclass->get_id_field());
			if ($firstclass_objects[$firstclass_field][$firstclass->get_id_field()] !== $object[$firstclass_field]) throw new MultiParentSecondClass_Exception("Firstclass parent mismatch in the multi-parent secondclass model data");
		}
		
		return $this->update($object);
	}
}
