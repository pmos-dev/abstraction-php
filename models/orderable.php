<?php
/**
 * Orderable models interface.
 * 
 * @copyright 2014 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");

interface Orderable {
	/**
	 * Returns the field which is used for ordering in the model.
	 * 
	 * @return string the name of the ordered field
	 */
	public function get_ordered_field();
}
