<?php
/**
 * First-class model support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Abstraction\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "models/models.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;

/**
 * @internal
 */
class FirstClass_Exception extends Exception {}

/**
 * Defines a root model for first-class structured models.
 * 
 * First class models are defined as having a single SerialId field as a primary key. This is defined internally within the FirstClass model, and cannot be defined explicitly by subclasses.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class FirstClass extends Models {
	protected $id_field;

	/**
	 * Constructs a new instance of this FirstClass model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param string $table the table name within the database
	 * @param Database\Type[] $structure an associated array to define the model structure, taking the format field name => field type object
	 * @param string $id_field the name of the identifier field, if not "id"
	 * @param ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @param IndexKey[] $index_keys any non-unique index keys to apply to the model
	 * @throws FirstClass_Exception
	 */
	public function __construct(
			Database\Wrapper $database,
			$table,
			array $structure,
			$id_field = "id",
			array $foreign_keys = array(),
			array $unique_keys = array(),
			array $index_keys = array()
	) {
		if (array_key_exists($id_field, $structure)) throw new FirstClass_Exception("The id field is implicit for FirstClass models and should not be explicitly stated in the structure");
		
		$this->id_field = $id_field;
	
		$structure[$id_field] = new Database\Type_SerialId();
	
		parent::__construct(
				$database,
				$table,
				$structure,
				$foreign_keys,
				$unique_keys,
				$index_keys
		);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns the identifier field for the model, usually "id".
	 * 
	 * @return string
	 */
	public function get_id_field() {
		return $this->id_field;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		$this->database->preprepare("MODELS_FIRSTCLASS__{$this->table}__GET_BY_ID", "
			SELECT {$this->fields}
			FROM " . $this->model($this) . "
			WHERE " . $this->modelfield($this, $this->id_field) . "=:id
		", array(
			"id" => $this->structure[$this->id_field]
		), $this->structure);
	
		$this->database->preprepare("MODELS_FIRSTCLASS__{$this->table}__LIST_ALL_IDS", "
			SELECT " . $this->modelfield($this, $this->id_field) . "
			FROM " . $this->model($this) . "
		", null, array($this->id_field => $this->structure[$this->id_field]));

		switch ($this->database->get_db_type()) {
			case Database\Wrapper::DATABASE_MYSQL:
				$this->database->preprepare("MODELS_FIRSTCLASS__{$this->table}__DEFRAGMENT", "
					UPDATE " . $this->model($this) . "
					SET " . $this->modelfield($this, $this->id_field) . "=:newid
					WHERE " . $this->modelfield($this, $this->id_field) . "=:oldid
					LIMIT 1
				", array(
					"newid" => new Database\Type_Id(Database\Type::NOT_NULL),
					"oldid" => new Database\Type_Id(Database\Type::NOT_NULL)
				), null);
				break;
			case Database\Wrapper::DATABASE_POSTGRESQL:
				$this->database->preprepare("MODELS_FIRSTCLASS__{$this->table}__DEFRAGMENT", "
					UPDATE " . $this->model($this) . "
					SET " . $this->database->delimit($this->id_field) . "=:newid
					WHERE " . $this->modelfield($this, $this->id_field) . "=:oldid
				", array(
					"newid" => new Database\Type_Id(Database\Type::NOT_NULL),
					"oldid" => new Database\Type_Id(Database\Type::NOT_NULL)
				), null);
				break;
			default:
				throw new Exception("Database type is not supported");
		}
	}

	//-------------------------------------------------------------------------

	/**
	 * Returns the current maximum value of the identifier field for the model, or null if none exists.
	 * 
	 * @return int|NULL;
	 */
	public function get_max_id() {
		return $this->database->query_params_value("
			SELECT MAX(" . $this->modelfield($this, $this->id_field) . ") AS " . $this->tempfield("max") . "
			FROM " . $this->model($this) . "
		", null, "max", new Database\Type_Id(Database\Type::ALLOW_NULL), true);
	}

	/**
	 * Needed for allowing subsequent models (i.e. Secondclass) to override the get_by_id method yet still call it themselves.
	 * 
	 * @internal
	 * @param int $id the model row identifier
	 * @return mixed[]|NULL the corresponding row or null if none match
	 */
	protected function _internal_firstclass_get_by_id($id) {
		return $this->database->execute_params_single("MODELS_FIRSTCLASS__{$this->table}__GET_BY_ID", array(
			"id" => $id
		));
	}

	/**
	 * Returns the model row corresponding to the given identifier
	 *  
	 * @param int $id the model row identifier
	 * @return mixed[]|NULL the corresponding row or null if none match
	 */
	public function get_by_id($id) {
		self::assert_id($id);
		
		return $this->_internal_firstclass_get_by_id($id);
	}

	/**
	 * Reloads the given model row from its identifier
	 * 
	 * @param mixed[] $object an existing model row
	 * @return mixed[]|NULL the corresponding row or null if none match
	 */
	public function refresh(array $object) {
		self::assert_id_object($object, $this->id_field);
		
		return $this->_internal_firstclass_get_by_id($object["id"]);
	}

	/**
	 * Returns an array of model rows for the given identifiers.
	 * 
	 * @param int[] $ids the mode row identifier values as an array
	 * @param string[] $orderby an optional associative array of field names => direction
	 * @return array
	 */
	public function bulkget_by_ids(array $ids, array $orderby = null) {
		if (sizeof($ids) == 0) return array();
		if (sizeof($ids) > 255) {
			$slice = array_slice($ids, 0, 255);
			$remainder = array_slice($ids, 255);
			
			return array_merge($this->bulkget_by_ids($slice), $this->bulkget_by_ids($remainder));
		}
		
		foreach ($ids as $id) self::assert_id($id);
		$ids = implode(",", $ids);
		
		$orderbyclauses = array();
		if (is_array($orderby)) foreach ($orderby as $field => $direction) $orderbyclauses[] = $this->modelfield($this, $field) . " {$direction}";
		$orderbyclauses = sizeof($orderbyclauses) === 0 ? "" : ("ORDER BY " . implode(",", $orderbyclauses));
		
		return $this->database->query_params("
			SELECT {$this->fields}
			FROM " . $this->model($this) . "
			WHERE " . $this->modelfield($this, $this->id_field) . " IN ({$ids})
			{$orderbyclauses}
		", null, $this->structure);
	}

	//-------------------------------------------------------------------------

	/**
	 * Trawls the table for matching values in the specified non-encrypted fields.
	 * 
	 * Searching is performed using relatively efficient LIKE conditions.
	 * Note, this method is primarily used as part of search_fields and is rarely called by itself.
	 *  
	 * @param string $value the value to search for
	 * @param string[] $fields an array of model fields to search
	 * @throws FirstClass_Exception
	 * @return int[] the model identifier values for any matching rows
	 */
	public function search_ids_for_nonencrypted_fields($value, array $fields) {
		if (sizeof($fields) == 0) return array();
	
		$conditions = array();
		foreach ($fields as $field) {
			if (!isset($this->structure[$field])) throw new FirstClass_Exception("Unknown field to perform encrypted search on", $field);
			if ($this->structure[$field] instanceof Database\Type_Encrypted) throw new FirstClass_Exception("Attempting to search on an encrypted field", $field);
			if (!($this->structure[$field] instanceof Database\Type_String || $this->structure[$field] instanceof Database\Type_Text)) throw new FirstClass_Exception("Attempting to search on a non-string/text field", $field);
			
			$conditions[] = $this->modelfield($this, $field) . " LIKE :valuea";
			$conditions[] = $this->modelfield($this, $field) . " LIKE :valueb";
			$conditions[] = $this->modelfield($this, $field) . " LIKE :valuec";
			$conditions[] = $this->modelfield($this, $field) . " LIKE :valued";
		}
	
		return $this->database->query_params_list("
			SELECT " . $this->id_field . "
			FROM " . $this->model($this) . "
			WHERE " . implode(" OR ", $conditions) . "
		", array(
			"valuea" => new Database\Param($value, new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valueb" => new Database\Param("{$value}%", new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valuec" => new Database\Param("%{$value}", new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valued" => new Database\Param("%{$value}%", new Database\Type_String(255, Database\Type::NOT_NULL))
		), $this->id_field, $this->structure[$this->id_field]);
	}

	/**
	 * Trawls the table for matching values in the specified encrypted fields.
	 * 
	 * Searching is performed in a relatively inefficient manner by querying all rows in the table, decrypting the fields, and then comparing programmatically.
	 * Note, this method is primarily used as part of search_fields and is rarely called by itself.
	 *  
	 * @param string $value the value to search for
	 * @param string[] $fields an array of model fields to search
	 * @throws FirstClass_Exception
	 * @return int[] the model identifier values for any matching rows
	 */
	public function search_ids_for_encrypted_fields($value, array $fields) {
		if (sizeof($fields) == 0) return array();
	
		$results = array();
		$fieldnames = array();
		foreach ($fields as $field) {
			if (!isset($this->structure[$field])) throw new FirstClass_Exception("Unknown field to perform encrypted search on", $field);
			if (!($this->structure[$field] instanceof Database\Type_Encrypted)) throw new FirstClass_Exception("Attempting to search on a non-encrypted field", $field);
				
			$results[$field] = $this->structure[$field];
			$fieldnames[] = $this->modelfield($this, $field);
		}
		$fieldnames = implode(",", $fieldnames);
	
		$results[$this->id_field] = $this->structure[$this->id_field];
	
		$all_ids = $this->database->execute_params_list("MODELS_FIRSTCLASS__{$this->table}__LIST_ALL_IDS");
		if (sizeof($all_ids) == 0) return array();
	
		$chunks = array_chunk($all_ids, 500);
		$matches = array();
		foreach ($chunks as $ids) {
			if (sizeof($ids) == 0) continue;
				
			foreach ($this->database->query_params("
				SELECT {$fieldnames},
					" . $this->modelfield($this, $this->id_field) . "
				FROM " . $this->model($this) . "
				WHERE " . $this->modelfield($this, $this->id_field) . " IN (" . implode(",", $ids) . ")
			", null, $results) as $row) {
				foreach ($fields as $field) {
					if ($row[$field] === null) continue;
					if (strpos(strtolower($row[$field]), strtolower($value)) === false) continue;
					$matches[$row[$this->id_field]] = $row[$this->id_field];
				}
			}
		}
	
		return array_values($matches);
	}

	/**
	 * Trawls the model's table for matching values within the specified fields, encrypted or non-encrypted.
	 *
	 * Note, encrypted searches are only performed if one or more of the fields are actually of the type Encrypted.
	 * 
	 * @param string $value the value to search for
	 * @param string[] $fields an array of model fields to search
	 * @param string[] $orderby an optional associative array of field names => direction, not null for none
	 * @throws FirstClass_Exception
	 * @return array a non-associated array of results, themselves each associated arrays of field names to row values
	 */
	public function search_fields($value, array $fields, array $orderby = null) {
		$encrypted = array(); 
		$regular = array();
		foreach ($fields as $field) {
			if (!isset($this->structure[$field])) throw new FirstClass_Exception("Unknown field to perform encrypted search on", $field);
			if ($this->structure[$field] instanceof Database\Type_Encrypted) $encrypted[] = $field;
			else $regular[] = $field;
		}
		
		return $this->bulkget_by_ids(array_unique(array_merge(
			$this->search_ids_for_encrypted_fields($value, $encrypted),
			$this->search_ids_for_nonencrypted_fields($value, $regular)
		)), $orderby);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Inserts the given values into the database for this model.
	 * 
	 * Note, the identifier value is generated automatically and should not be specified explicitly.
	 * Transaction support is enabled, so failed row insertion will roll back.
	 * 
	 * @param mixed[] $values an associated array values taking the format field name => value 
	 * @throws Exception
	 * @return mixed[] the newly created model row, including its new identifier value
	 */
	public function insert(array $values) {
		if (array_key_exists($this->id_field, $values)) throw new FirstClass_Exception("ID fields should not be specified explicitly for FirstClass models");

		$params = array();
		foreach ($values as $var => &$value) {
			if (!array_key_exists($var, $this->structure)) throw new FirstClass_Exception("Parameter variable is missing in the model structure", $var);
			$params[$var] = new Database\Param($value, $this->structure[$var]);
		}
		unset($value);

		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		$id = $this->database->insert_row($this->table, $params, $this->id_field);	// most id field strings will evaluate as 'true' anyway, especially since === is used
		if ($id === null || $id === false) {
			$this->database->transaction_rollback();
			throw new FirstClass_Exception("There was an error inserting the data into table", $this->table);
		}
		if (!Data\Data::validate_id($id)) {
			$this->database->transaction_rollback();
			throw new TypeMismatchException("ID", $id, "FirstClass model insertion returned a identity field which was not an ID");
		}
		
		$object = $this->_internal_firstclass_get_by_id($id);
		
		if (!Data\Data::validate_id_object($object)) {
			$this->database->transaction_rollback();
			throw new NotIdObjectException("ID object", $object, "FirstClass model insertion did not create a valid ID object");
		}
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return $object;		
	}

	/**
	 * Deletes a row from the model's table using its identifier value.
	 * 
	 * @param mixed[] $object an existing model row
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	public function delete(array $object) {
		self::assert_id_object($object, $this->id_field);

		if (!$this->database->delete_rows_by_conditions($this->table, array(
			$this->id_field => new Database\Param($object[$this->id_field], $this->structure[$this->id_field])
		), true)) throw new FirstClass_Exception("Unable to delete first class object from table", $this->table);

		return true;
	}

	/**
	 * Updates a row in the model's table with new data using its identifier value.
	 * 
	 * Any field apart from the identifier field can be populated with new data.
	 * 
	 * @param mixed[] $object an existing model row
	 * @throws FirstClass_Exception
	 * @return boolean always true; failure throws an exception
	 */
	function update(array $object) {
		self::assert_id_object($object);

		$params = array();
		foreach ($object as $var => &$value) {
			if ($var == $this->id_field) continue;

			if (!isset($this->structure[$var])) throw new FirstClass_Exception("Parameter variable is missing in the model structure", $var);
			$params[$var] = new Database\Param($value, $this->structure[$var]);
		}
		unset($value);

		if (!$this->database->update_rows_by_conditions($this->table, $params, array(
			$this->id_field => new Database\Param($object[$this->id_field], $this->structure[$this->id_field])
		), true)) throw new FirstClass_Exception("Unable to update first class object in table", $this->table);

		return true;
	}

	//-------------------------------------------------------------------------

	/**
	 * Defragments the underlying model's table identifiers.
	 * 
	 * This is useful after a lot of rows have been deleted and re-inserted, as serial and auto_increment values do not recycle gaps.
	 * After defragmentation, the serial value is reset to the new maximum.
	 * Foreign keys of dependent models need to be set to 'cascade' in order to avoid corruption when defragmenting.
	 * Transaction support is enabled, so any failure will roll back the entire defragmentation attempt.
	 * 
	 * @throws Exception
	 * @var array $array
	 * @return true always returns true; failure throws an exception
	 */
	function defragment() {
		$this->database->lock_tables(array($this->table => Database\Wrapper::LOCK_WRITE));

		// largely unnecessary due to the locking, but turns off auto_commit which speeds things up a lot
		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$all_ids = array();
		foreach ($this->database->execute_params_list("MODELS_FIRSTCLASS__{$this->table}__LIST_ALL_IDS") as $id) $all_ids[$id] = $id;
		
		if (sizeof($all_ids) == 0) return true;
		ksort($all_ids);
		
		$last_lowest = 0;
		foreach (array_keys($all_ids) as $id) {
			for ($i = $last_lowest + 1; $i < $id; $i++) {
				if (!isset($all_ids[$i])) break;
			}
			if ($i === $id) continue;	// no more gaps available

			$this->database->execute_params("MODELS_FIRSTCLASS__{$this->table}__DEFRAGMENT", array(
				"newid" => $last_lowest = $i,
				"oldid" => $id
			));

			unset($all_ids[$id]);
			$all_ids[$i] = $i;
		}
		
		$max_id = $this->get_max_id();
		if ($max_id === null || $max_id === 0) $max_id = 1;
		else $max_id++;

		self::assert_id($max_id);
		
		switch ($this->database->get_db_type()) {
			case Database\Wrapper::DATABASE_MYSQL:
				$this->database->query_params("
					ALTER TABLE " . $this->model($this) . " AUTO_INCREMENT=:max
				", array(
					"max" => new Database\Param($max_id, new Database\Type_Id(Database\Type::ALLOW_NULL))
				));
				break;
			case Database\Wrapper::DATABASE_POSTGRESQL:
				$sequence = $this->database->query_params_value("
					SELECT pg_get_serial_sequence(" . $this->database->quote($this->table) . ", " . $this->database->quote($this->id_field) . ") AS " . $this->tempfield("sequence") . "
				", null, "sequence", new Database\Type_String(255, Database\Type::NOT_NULL));

				if (!preg_match("`^public\.(.+)$`iD", $sequence, $array)) throw new Exception("Unable to ascertain sequence name for model");
				
				// for some reason parameters are not allowed for RESTART WITH :max, so have to use the number inline
				$this->database->query_params("
					ALTER SEQUENCE " . $this->database->delimit($array[1]) . " RESTART WITH {$max_id}
				");
				break;
			default:
				throw new Exception("Database type is not supported");
		}
				
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();

		$this->database->unlock_tables();
		
		return true;
	}
}
